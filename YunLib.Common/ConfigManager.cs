﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.Common
{
    public static class ConfigManager
    {
        /// <summary>
        /// 当前控制台波特率
        /// </summary>
        public static int CmdPort
        {
            get
            {
                string cmdPort = ConfigurationManager.AppSettings["CmdPort"];

                if (string.IsNullOrWhiteSpace(cmdPort))
                    return 9600;
                else
                    return Convert.ToInt32(cmdPort);
            }
        }

        public static int AccountCount
        {
            get
            {
                string str = ConfigurationManager.AppSettings["AccountCount"];
                return Convert.ToInt32(str);
            }
        }

        /// <summary>
        /// 当前IM刷卡波特率
        /// </summary>
        public static int IMPort
        {
            get
            {
                string imPort = ConfigurationManager.AppSettings["IMPort"];

                if (string.IsNullOrWhiteSpace(imPort))
                    return 115200;
                else
                    return Convert.ToInt32(imPort);
            }
        }

        /// <summary>
        /// 当前控制台串口号
        /// </summary>
        public static string Control
        {
            get
            {
                string control = ConfigurationManager.AppSettings["Control"];

                if (string.IsNullOrWhiteSpace(control))
                    return "COM3";
                else
                    return control;
            }
        }

        public static string ReadCardType
        {
            get
            {
                string readCardType = ConfigurationManager.AppSettings["ReadCardType"];

                if (string.IsNullOrWhiteSpace(readCardType))
                    return "1";
                else
                    return readCardType;
            }
        }

        public static string IsNeedInitData
        {
            get
            {
                string isNeedInitData = ConfigurationManager.AppSettings["IsNeedInitData"];

                if (string.IsNullOrWhiteSpace(isNeedInitData))
                    throw new Exception("请设置isNeedInitData地址.");
                else
                    return isNeedInitData;
            }
        }

        /// <summary>
        /// 当前控制台串口号
        /// </summary>
        public static string Scan
        {
            get
            {
                string scan = ConfigurationManager.AppSettings["Scan"];

                if (string.IsNullOrWhiteSpace(scan))
                    return "COM6";
                else
                    return scan;
            }
        }

        /// <summary>
        /// 高频刷卡串口
        /// </summary>
        public static string Pos
        {
            get
            {
                string pos = ConfigurationManager.AppSettings["Pos"];

                if (string.IsNullOrWhiteSpace(pos))
                    return "COM4";
                else
                    return pos;
            }
        }

        /// <summary>
        /// 超高频刷卡串口
        /// </summary>
        public static string UHF
        {
            get
            {
                string pos = ConfigurationManager.AppSettings["UHF"];

                if (string.IsNullOrWhiteSpace(pos))
                    return "COM11";
                else
                    return pos;
            }
        }

        /// <summary>
        /// 开灯时间
        /// </summary>
        public static string StartTime
        {
            get
            {
                string start = ConfigurationManager.AppSettings["StartTime"];

                if (string.IsNullOrWhiteSpace(start))
                    return "0";
                else
                    return start;
            }
        }

        /// <summary>
        /// 关灯时间
        /// </summary>
        public static string EndTime
        {
            get
            {
                string end = ConfigurationManager.AppSettings["EndTime"];

                if (string.IsNullOrWhiteSpace(end))
                    return "0";
                else
                    return end;
            }
        }

        /// <summary>
        /// 当前图书管理员卡号
        /// </summary>
        public static string BooksManager
        {
            get
            {
                string booksManager = ConfigurationManager.AppSettings["BooksManager"];

                if (string.IsNullOrWhiteSpace(booksManager))
                    throw new Exception("请设置图书管理员卡号.");
                else
                    return booksManager;
            }
        }

        /// <summary>
        /// ZooKeeper服务器地址
        /// </summary>
        public static string ZooKeeperIP
        {
            get
            {
                string zooKeeperIP = ConfigurationManager.AppSettings["ZooKeeperIP"];

                if (string.IsNullOrWhiteSpace(zooKeeperIP))
                    throw new Exception("请设置Zookeeper地址.");
                else
                    return zooKeeperIP;
            }
        }

        /// <summary>
        /// ZooKeeper节点路径
        /// </summary>
        public static string ZookeeperPath
        {
            get
            {
                string zookeeperPath = ConfigurationManager.AppSettings["ZookeeperPath"];

                if (string.IsNullOrWhiteSpace(zookeeperPath))
                    throw new Exception("请设置Zookeeper节点路径.");
                else
                    return zookeeperPath;
            }
        }

        /// <summary>
        /// 请求后台接口url
        /// </summary>
        public static string ServicePath
        {
            get
            {
                return ConfigurationManager.AppSettings["ServicePath"];
            }
        }

        public static string PingServicePath
        {
            get
            {
                return ConfigurationManager.AppSettings["PingServicePath"];
            }
        }

        /// <summary>
        /// 机器ID（MachineID）
        /// </summary>
        public static string MachineID
        {
            get
            {
                string machineID = ConfigurationManager.AppSettings["MachineID"];

                if (string.IsNullOrWhiteSpace(machineID))
                    throw new Exception("请设置机器MachineID.");
                else
                    return machineID;
            }
        }

        /// <summary>
        /// 图书馆ID（LibraryID）
        /// </summary>
        public static string LibraryID
        {
            get
            {
                string libraryID = ConfigurationManager.AppSettings["LibraryID"];

                if (string.IsNullOrWhiteSpace(libraryID))
                    throw new Exception("请设置图书馆ID.");
                else
                    return libraryID;
            }
        }

        /// <summary>
        /// 口令
        /// </summary>
        public static string CommonKey
        {
            get
            {
                string commonKey = ConfigurationManager.AppSettings["CommonKey"];

                if (string.IsNullOrWhiteSpace(commonKey))
                    throw new Exception("请设置口令.");
                else
                    return commonKey;
            }
        }

        /// <summary>
        /// 密码口令
        /// </summary>
        public static string LoginKey
        {
            get
            {
                string loginKey = ConfigurationManager.AppSettings["LoginKey"];

                if (string.IsNullOrWhiteSpace(loginKey))
                    throw new Exception("请设置密码口令.");
                else
                    return loginKey;
            }
        }

        /// <summary>
        /// 借书类型
        /// </summary>
        public static int BorrowType
        {
            get
            {
                string showBorrowType = ConfigurationManager.AppSettings["BorrowType"];

                if (string.IsNullOrWhiteSpace(showBorrowType))
                    return 1;
                else
                    return Convert.ToInt32(showBorrowType);
            }
        }

        /// <summary>
        /// 还书类型
        /// </summary>
        public static int ReadBookType
        {
            get
            {
                string readBookType = ConfigurationManager.AppSettings["ReadBookType"];

                if (string.IsNullOrWhiteSpace(readBookType))
                    return 1;
                else
                    return Convert.ToInt32(readBookType);
            }
        }

        /// <summary>
        /// 扫描机器类型
        /// </summary>
        public static int ScanType
        {
            get
            {
                string readBookType = ConfigurationManager.AppSettings["ScanType"];

                if (string.IsNullOrWhiteSpace(readBookType))
                    return 1;
                else
                    return Convert.ToInt32(readBookType);
            }
        }

        /// <summary>
        /// 是否具有注册功能
        /// </summary>
        public static int CanRegister
        {
            get
            {
                string canRegister = ConfigurationManager.AppSettings["CanRegister"];

                if (string.IsNullOrWhiteSpace(canRegister))
                    return 0;
                else
                    return Convert.ToInt32(canRegister);
            }
        }

        /// <summary>
        /// 登录借书是否需要密码
        /// </summary>
        public static int IsNeedPassWord
        {
            get
            {
                string isNeedPassWord = ConfigurationManager.AppSettings["IsNeedPassWord"];

                if (string.IsNullOrWhiteSpace(isNeedPassWord))
                    return 0;
                else
                    return Convert.ToInt32(isNeedPassWord);
            }
        }
        

    }
}
