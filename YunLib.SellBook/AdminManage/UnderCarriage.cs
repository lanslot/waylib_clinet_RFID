using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Yunlib.Common;
using Yunlib.Entity;
using YunLib.BLL;
using YunLib.Common;
using Yunlib.SellBook.Extensions;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Threading;
using Newtonsoft.Json.Linq;
using Yunlib.SellBook.ViewModels;
using System.ComponentModel;
using System.IO.Ports;
using ShenZhen.SmartLife.Control;

namespace Yunlib.SellBook
{
    public partial class UnderCarriage : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string machineId = ConfigManager.MachineID;
        private SmartLifeBroker model = new SmartLifeBroker();
        private string control = ConfigManager.Control;
        private int cmdPort = ConfigManager.CmdPort;

        public int icdev;   // 通讯设备标识符
        public short st;    //函数返回值
        public int Flag { get; set; }
        /// <summary>
        /// 数据总条数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 数据总页数
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// 当前页数
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int PageSize { get; set; }

         public delegate void MyDelegate(int pageSize, int currentPage, string machId, string libraryId, string keyword = null);
         MyDelegate md = null;

        public UnderCarriage()
        {
            InitializeComponent();
            this.md = new MyDelegate(GetData);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        public void BorrowBook_Load(object sender, EventArgs e)
        {
            try
            {
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                CodeOptimize.SolveLightScreen(this);

                DrawListView();

                PageIndex = 1;
                PageSize = 25;
                Invoke(md, PageSize, PageIndex,machineId, libraryId, null);
                //  DGVBindToListView(PageIndex, PageSize);

                listView1.Click += new EventHandler(listView1_Click);
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 绘制ListView
        /// </summary>
        private void DrawListView()
        {
            listView1.View = View.Tile;

            int w = listView1.Width / 5 - 4;
            int h = listView1.Height / 5 + 1;
            listView1.TileSize = new Size(w, h);
            listView1.OwnerDraw = true;
            listView1.Columns.AddRange(new ColumnHeader[]
            {
                    new ColumnHeader(), new ColumnHeader(), new ColumnHeader()
            });
        }

        private void SolveLightScreen()
        {
            //A—Z的字母排序按钮
            lblBtnA.Visible = false;
            lblBtnB.Visible = false;
            lblBtnC.Visible = false;
            lblBtnD.Visible = false;
            lblBtnE.Visible = false;
            lblBtnF.Visible = false;
            lblBtnG.Visible = false;
            lblBtnH.Visible = false;
            lblBtnI.Visible = false;
            lblBtnJ.Visible = false;
            lblBtnK.Visible = false;
            lblBtnL.Visible = false;
            lblBtnM.Visible = false;
            lblBtnN.Visible = false;
            lblBtnO.Visible = false;
            lblBtnP.Visible = false;
            lblBtnQ.Visible = false;
            lblBtnR.Visible = false;
            lblBtnS.Visible = false;
            lblBtnT.Visible = false;
            lblBtnU.Visible = false;
            lblBtnV.Visible = false;
            lblBtnW.Visible = false;
            lblBtnX.Visible = false;
            lblBtnY.Visible = false;
            lblBtnZ.Visible = false;

            //listView1.Visible = false;
            palTop.Visible = false;
            palDGVArea.Visible = false;
            palBottom.Visible = false;
            lblLoading.Visible = true;

            Application.DoEvents();

            //A—Z的字母排序按钮
            lblBtnA.Visible = true;
            lblBtnB.Visible = true;
            lblBtnC.Visible = true;
            lblBtnD.Visible = true;
            lblBtnE.Visible = true;
            lblBtnF.Visible = true;
            lblBtnG.Visible = true;
            lblBtnH.Visible = true;
            lblBtnI.Visible = true;
            lblBtnJ.Visible = true;
            lblBtnK.Visible = true;
            lblBtnL.Visible = true;
            lblBtnM.Visible = true;
            lblBtnN.Visible = true;
            lblBtnO.Visible = true;
            lblBtnP.Visible = true;
            lblBtnQ.Visible = true;
            lblBtnR.Visible = true;
            lblBtnS.Visible = true;
            lblBtnT.Visible = true;
            lblBtnU.Visible = true;
            lblBtnV.Visible = true;
            lblBtnW.Visible = true;
            lblBtnX.Visible = true;
            lblBtnY.Visible = true;
            lblBtnZ.Visible = true;

            //listView1.Visible = true;
            palTop.Visible = true;
            palDGVArea.Visible = true;
            palBottom.Visible = true;
            lblLoading.Visible = false;
        }

        private void DGVBindToListView(int pageIndex, int pageSize)
        {
            DataTable dt = accessBLL.GetBookToDataTable();

            IList<ShowBooks> list = DataTable2List<ShowBooks>.ConvertToModel(dt);

            //获取已预约书籍列表
            List<ShowBooks> orderList = list.Where(x => !string.IsNullOrWhiteSpace(x.IsLend)).ToList();

            foreach (var item in orderList)
            {
                DateTime endTime = CustomExtensions.ConvertIntDateTime(Convert.ToDouble(item.IsLend.Split('_')[1]));
                string barCode = item.IsLend.Split('_')[0];

                LogManager.BorrowRecord("书籍：{0}，转化之后的过期时间：{1}".FormatWith(barCode, endTime.ToString()));
                //预约时间已经超期
                if (endTime <= DateTime.Now)
                {
                    bool res = accessBLL.UpdateLocalBookbyCode(barCode, "");
                    if (res)
                    {
                        //休息一秒，防止加载太快又加载一次
                        Thread.Sleep(1000);
                        BorrowBook_Load(null, null);
                    }
                }
            }

            if (list.Count > 0 && list != null)
            {
                List<ShowBooks> newList = new List<ShowBooks>();

                string query = lblQuery.Text;

                if (!string.IsNullOrWhiteSpace(query))
                    newList = list.Where(x => ChineseToLetter.GetSpellCode(x.BName[0].ToString()) == query).ToList();
                else
                    newList = list.ToList();

                TotalPage = Convert.ToInt32(Math.Ceiling((double)newList.Count / pageSize));

                pBtnLastPage.Enabled = pageIndex <= 1 ? false : true;

                pBtnNextPage.Enabled = pageIndex >= TotalPage ? false : true;

                lblThisPage.Text = pageIndex.ToString();
                lblTotalPage.Text = TotalPage.ToString();
                label2.Text = "/";

                listView1.Clear();

                for (int i = 0; i < newList.Count; i++)
                {
                    if (i >= (pageIndex - 1) * 25 && i < (pageIndex * 25))
                    {
                        listView1.Items.Add(new ListViewItem(new string[] { newList[i].DoorNUM.ToString(), newList[i].BName, newList[i].IsLend }, 0));
                    }
                }

                Application.DoEvents();

                if (pageIndex * 25 > newList.Count)
                {
                    for (int i = 0; i < pageIndex * 25 - newList.Count; i++)
                    {
                        listView1.Items.Add(new ListViewItem());
                    }
                }
            }
        }

        private void pic_BtnTurnBack_Click(object sender, EventArgs e)
        {
            AdminManage.BackMain frm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };
            this.Close();

            frm.Show();
        }

        private void pBtnLastPage_Click(object sender, EventArgs e)
        {
            PageIndex -= 1;
            Flag = 1;
            lblThisPage.Text = PageIndex + "";

            this.listView1.Clear();

            if (string.IsNullOrWhiteSpace(lblQuery.Text))
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
            }
            else
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
            }
        }

        private void pBtnNextPage_Click(object sender, EventArgs e)
        {
            PageIndex += 1;
            Flag = 1;
            lblThisPage.Text = PageIndex + "";

            this.listView1.Clear();

            if (string.IsNullOrWhiteSpace(lblQuery.Text))
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
            }
            else
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
            }
        }

        private void txtBooksCode_MouseDown(object sender, MouseEventArgs e)
        {
            this.txtBooksCode.Clear();

            DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.Cancel)
            {
                //关闭虚拟键盘时,把值传到登录页面的文本框中
                txtBooksCode.Text = DymicNewJP.inputCount;

                if (!string.IsNullOrEmpty(txtBooksCode.Text))
                    pBtn_Search_Click(null, null);
            }
        }

        private void pBtn_Search_Click(object sender, EventArgs e)
        {
            try
            {
                string query = txtBooksCode.Text;

                if (string.IsNullOrWhiteSpace(query))
                {
                    MessageBox.Show("请输入柜门编号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (query.Equals("请输入书柜号"))
                {
                    MessageBox.Show("请输入柜门编号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (query.Length >= 4)
                    {
                        MessageBox.Show("你输入正确的柜门号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        string[] s = new string[1];
                        s[0] = ConfigManager.PingServicePath;
                        bool flag = NetTest.CheckServeStatus(s);
                        //验证网络连接状态，true就发起请求，false不请求
                        if (flag)
                        {
                            var timeStamp = DateTime.Now.Ticks.ToString();

                            var signStr = "{0}{1}{2}{3}".FormatWith(machineId, query, timeStamp, commonKey);
                            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                            var url = "{0}/public/book/bookInfo?machId={1}&machLatNum={2}&timeStamp={3}&sign={4}".FormatWith(
                                ConfigManager.ServicePath, machineId, query, timeStamp, sign);

                            var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                            if (callback["code"].ToString() == "100")
                            {
                                listView1.Clear();

                                List<ShowBooks> newList = new List<ShowBooks>();

                                TotalPage = 1;

                                pBtnLastPage.Enabled = false;

                                pBtnNextPage.Enabled = false;

                                lblThisPage.Text = "1";
                                lblTotalPage.Text = "1";
                                label2.Text = "/";




                                listView1.Items.Add(new ListViewItem(new string[] { query, callback["data"]["title"].ToString(), callback["data"]["status"].ToString(), callback["data"]["bookId"].ToString() }, 0));


                            }
                            else
                            {
                                MessageBox.Show(callback["msg"].ToString());
                            }
                        }
                        else
                        {
                            MessageBox.Show("网络繁忙,请稍后再试！");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("柜号查询报错：" + ex.Message);
            }

        }

        private void pic_BtnTurnBack_MouseMove(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        

        public delegate void AsyncUnderCarriage(object sender, EventArgs e);

        private void listView1_Click(object sender, EventArgs e)
        {
            string servicePath = ConfigManager.ServicePath;

            try
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in this.listView1.SelectedItems)
                    {
                        if (item.SubItems[2].Text == "1")
                        {
                            AlertFrm frm = new AlertFrm { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

                            frm.ShowDialog();
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Text)) //判断柜号是否为空
                            {
                                string doorNum = item.Text;
                                

                                long timeStamp = DateTime.Now.Ticks;

                                string signstr = "{0}{1}{2}{3}".FormatWith(item.SubItems[3].Text, libraryId, timeStamp, commonKey);
                                string sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                                string url = "{0}/public/book/undercarriage?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(servicePath,item.SubItems[3].Text, libraryId, timeStamp, sign);

                                string[] s = new string[1];
                                s[0] = ConfigManager.PingServicePath;
                               
                                bool flag = NetTest.CheckServeStatus(s);
                                
                                //验证网络连接状态，true就发起请求，false不请求
                                if (flag)
                                {
                                    JObject IsLoginSuccess = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                                    

                                    if (IsLoginSuccess["code"].ToString() == "100")
                                    {

                                        var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                                        CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                                        //   MessageBox.Show("下架书籍成功,书籍名称：{0}".FormatWith(book.BName), "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                                        if (string.IsNullOrWhiteSpace(lblQuery.Text))
                                        {
                                            Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
                                        }
                                        else
                                        {
                                            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
                                        }


                                    }
                                    else
                                    {
                                        MessageBox.Show("下架失败");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("下架失败");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                LogManager.ErrorRecord(DateTime.Now+"--书籍下架出错：" + ex.Message);
            }

        }

        private void listView1_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            var subitem = e.Item.SubItems;

            //获得行号
            var y = e.ItemIndex / 5;

            using (Graphics g = e.Graphics)
            {
                Rectangle rect = Rectangle.Empty;

                rect = new Rectangle(e.Bounds.Left - 1, e.Bounds.Top - 3, e.Item.Bounds.Width + 4, e.Bounds.Height + 2);

                //绘制背景色
                if ((y + 1) % 2 == 0)
                {
                    using (LinearGradientBrush brush = new LinearGradientBrush(rect,
                                                                               Color.FromArgb(255, 249, 249, 241),
                                                                               Color.FromArgb(255, 249, 249, 241),
                                                                               LinearGradientMode.Horizontal))
                    {
                        e.Graphics.FillRectangle(brush, rect);
                    }
                }

                g.DrawLine(new Pen(Color.FromArgb(255, 235, 235, 235), 1), e.Item.Bounds.Right + 4, e.Bounds.Top - 3, e.Item.Bounds.Right + 4, e.Bounds.Bottom);

                Graphics graphics = CreateGraphics();

                //判断item是否有值
                if (subitem.Count > 1)
                {
                    SizeF sizeH = graphics.MeasureString(subitem[0].Text, new Font("微软雅黑", 14));

                    float w = (e.Bounds.Width - sizeH.Width) / 2;

                    graphics.Dispose();

                    if (subitem[2].Text=="1")
                    {
                        g.DrawString(subitem[0].Text, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DeepSkyBlue, e.Bounds.Left + w, e.Bounds.Height - 70 + e.Bounds.Height * y);
                    }
                    else
                    {
                        g.DrawString(subitem[0].Text, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DarkSeaGreen, e.Bounds.Left + w, e.Bounds.Height - 70 + e.Bounds.Height * y);
                    }
                    //绘制书柜号

                    string subText = subitem[1].Text;

                    Graphics graphics1 = CreateGraphics();

                    SizeF sizeF = graphics1.MeasureString(subText, new Font("微软雅黑", 14));

                    float m = (e.Bounds.Width - sizeF.Width) / 2;

                    graphics1.Dispose();

                    //获得行号
                    var n = e.ItemIndex / 5;

                    if (subText.Length > 7)
                    {
                        subText = subText.Substring(0, 7);
                    }

                    if (subitem[2].Text == "1")
                    {
                        g.DrawString(subText, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DeepSkyBlue, e.Bounds.Left + 30, e.Bounds.Height - 36 + e.Bounds.Height * y);
                    }
                    else
                    {
                        if (m >= 30)
                        {
                            //绘制书名
                            g.DrawString(subText, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.Black, e.Bounds.Left + m, e.Bounds.Height - 36 + e.Bounds.Height * y);
                        }
                        else
                        {
                            //if (subText[0] < 127 && subText[0] > 0)
                            //    subText = "丨" + subText.Replace("?", "");

                            //byte[] strToBytes = Encoding.Default.GetBytes(subText);

                            //string str = Encoding.Default.GetString(strToBytes, 0, 14);

                            //str = str.Replace("?", "") + "...";

                            g.DrawString(subText, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.Black, e.Bounds.Left + 30, e.Bounds.Height - 36 + e.Bounds.Height * y);
                        }
                    }
                }
            }
        }

        #region 字母点击事件

        private void lblBtnAll_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblAll);
            this.lblQuery.Text = "";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
        }

        private void lblBtnA_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnA);
            this.lblQuery.Text = "A";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnB_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnB);
            this.lblQuery.Text = "B";
            PageIndex = 1;


            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnC_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnC);
            this.lblQuery.Text = "C";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnD_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnD);
            this.lblQuery.Text = "D";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnE_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnE);
            this.lblQuery.Text = "E";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnF_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnF);
            this.lblQuery.Text = "F";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnG_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnG);
            this.lblQuery.Text = "G";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnH_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnH);
            this.lblQuery.Text = "H";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnI_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnI);
            this.lblQuery.Text = "I";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnJ_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnJ);
            this.lblQuery.Text = "J";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnK_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnK);
            this.lblQuery.Text = "K";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnL_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnL);
            this.lblQuery.Text = "L";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnM_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnM);
            this.lblQuery.Text = "M";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnN_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnN);
            this.lblQuery.Text = "N";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnO_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnO);
            this.lblQuery.Text = "O";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnP_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnP);
            this.lblQuery.Text = "P";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnQ_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnQ);
            this.lblQuery.Text = "Q";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnR_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnR);
            this.lblQuery.Text = "R";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnS_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnS);
            this.lblQuery.Text = "S";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnT_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnT);
            this.lblQuery.Text = "T";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnU_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnU);
            this.lblQuery.Text = "U";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnV_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnV);
            this.lblQuery.Text = "V";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnW_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnW);
            this.lblQuery.Text = "W";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnX_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnX);
            this.lblQuery.Text = "X";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnY_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnY);
            this.lblQuery.Text = "Y";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void lblBtnZ_Click(object sender, EventArgs e)
        {
            AddBgStyle(lblBtnZ);
            this.lblQuery.Text = "Z";
            PageIndex = 1;
            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }
        #endregion

        #region 公共方法

        private void AddBgStyle(Label label)
        {
            foreach (var item in palTop.Controls)
            {
                if (item is Label)
                {
                    (item as Label).BackColor = new Color();
                }
            }
            label.BackColor = Color.Orange;
        }
        #endregion

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                var str = "";
                var doorNumStr = "";
                foreach (ListViewItem item in this.listView1.Items)
                {
                    if (item.SubItems.Count > 3)
                    {
                        if (!string.IsNullOrWhiteSpace(item.SubItems[3].Text))
                        {
                            str += item.SubItems[3].Text + ",";
                            doorNumStr += item.Text+",";
                        }
                    }
                }

                if(str != "")
                {


                    long timeStamp = DateTime.Now.Ticks;

                    string signstr = "{0}{1}{2}{3}".FormatWith(str, libraryId, timeStamp, commonKey);
                    string sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                    string url = "{0}/public/book/undercarriage?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(ConfigManager.ServicePath, str, libraryId, timeStamp, sign);
                    LogManager.ErrorRecord("str="+str);
                    string[] s = new string[1];
                    s[0] = ConfigManager.PingServicePath;

                    bool flag = NetTest.CheckServeStatus(s);

                    //验证网络连接状态，true就发起请求，false不请求
                    if (flag)
                    {
                        JObject IsLoginSuccess = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                        if (IsLoginSuccess["code"].ToString() == "100")
                        {
                            var ss = doorNumStr.Split(',');
                            for (int i = 0; i < ss.Length; i++)
                            {
                                string s1 = ss[i];
                                if (string.IsNullOrWhiteSpace(s1))
                                {
                                    var list = accessBLL.GetBookByDoorNum(int.Parse(s1));
                                   // CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                                    #region 异步开门
                                    OpenDoorDelegate myDelegate = new OpenDoorDelegate(OpenDoor);

                                    IAsyncResult result = myDelegate.BeginInvoke(list[i].DoorID, list[i].DoorCode, null, null);

                                    myDelegate.EndInvoke(result);
                                    #endregion
                                    Thread.Sleep(500);
                                }
                            }
                            MessageBox.Show("全部下架成功");
                            BorrowBook_Load(null, null);
                        }
                        else
                        {
                            MessageBox.Show("下架失败");
                        }
                    }
                    else
                    {
                        MessageBox.Show("下架失败");
                    }
                }
                      

               
              
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("全部下架出错：".FormatWith(ex.Message));
            }

        }


        public delegate void OpenDoorDelegate(int doorID, int doorCode);

        /// <summary>
        /// 循环打开所有的门
        /// </summary>
        /// <param name="item">Label子集</param>
        private void OpenDoor(int doorId, int doorCode)
        {
            SerialPort port = model.SmartSerialPort;
            //判断序列化的端口，为空就初始化
            if (port == null)
            {
                model.Init(control, cmdPort);
            }
            else
            {
                string str = model.SmartSerialPort.PortName;
                //判断已经序列化的端口是否是扫描端口，不是就初始化扫描端口
                if (str != (control))
                {
                    model.Init(control, cmdPort);
                }
            }

            //打开相应行号的柜门
            bool res = model.OpenDoor(doorId, doorCode, 2);
            
        }



        private void GetData(int pageSize, int currentPage, string machId, string libraryId, string keyword = null)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}{4}{5}{6}".FormatWith(currentPage, keyword, libraryId, machId, pageSize, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/query/page?pageSize={1}&currentPage={2}&machId={3}&libraryId={4}&keyword={5}&timeStamp={6}&sign={7}".FormatWith(
                    ConfigManager.ServicePath, pageSize, currentPage, machId, libraryId, keyword, timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                listView1.Clear();
                if (res["code"].ToString() == "100")
                {
                    string str = res["data"]["totalPage"].ToString();
                    List<ListViewModel> list = res["data"]["data"].ToJson().ToList<ListViewModel>();
                    lblTotalPage.Text = str;

                    if (list.Count > 0 && list != null)
                    {
                        List<ShowBooks> newList = new List<ShowBooks>();

                        TotalPage = int.Parse(str);

                        pBtnLastPage.Enabled = currentPage <= 1 ? false : true;

                        pBtnNextPage.Enabled = currentPage >= TotalPage ? false : true;

                        lblThisPage.Text = currentPage.ToString();
                        lblTotalPage.Text = TotalPage.ToString();
                        label2.Text = "/";



                        for (int i = 0; i < list.Count; i++)
                        {
                            //if (i >= (currentPage - 1) * 25 && i < (currentPage * 25))
                            //{
                            listView1.Items.Add(new ListViewItem(new string[] { list[i].machLatNum.ToString(), list[i].title, list[i].status ,list[i].bookId}, 0));
                            //}
                        }

                        Application.DoEvents();

                        if (PageIndex * 25 > list.Count)
                        {
                            for (int i = 0; i < currentPage * 25 - list.Count; i++)
                            {
                                listView1.Items.Add(new ListViewItem());
                            }
                        }
                    }
                }
                else
                {
                    LogManager.BorrowRecord("{0}--BorrowBook请求错误返回：{1}".FormatWith(DateTime.Now, res.ToString()));
                }
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("{0}--BorrowBook请求报错：{1}".FormatWith(DateTime.Now, ex.Message));
            }
        }
    }
}
