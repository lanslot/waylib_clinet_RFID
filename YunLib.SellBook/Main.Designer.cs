﻿using System.Windows.Forms;

namespace Yunlib.SellBook
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pBtnBorrow = new System.Windows.Forms.PictureBox();
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblLastOp = new System.Windows.Forms.Label();
            this.picOpenCode = new System.Windows.Forms.PictureBox();
            this.lblNetTest = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(7, 14);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 30);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(729, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(258, 57);
            this.panel1.TabIndex = 4;
            // 
            // pBtnBorrow
            // 
            this.pBtnBorrow.BackColor = System.Drawing.Color.Transparent;
            this.pBtnBorrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBtnBorrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnBorrow.Image = ((System.Drawing.Image)(resources.GetObject("pBtnBorrow.Image")));
            this.pBtnBorrow.Location = new System.Drawing.Point(339, 330);
            this.pBtnBorrow.Name = "pBtnBorrow";
            this.pBtnBorrow.Size = new System.Drawing.Size(254, 168);
            this.pBtnBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnBorrow.TabIndex = 2;
            this.pBtnBorrow.TabStop = false;
            this.pBtnBorrow.WaitOnLoad = true;
            this.pBtnBorrow.Click += new System.EventHandler(this.pBtnBorrow_Click);
            this.pBtnBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnBorrow_MouseDown);
            this.pBtnBorrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pBtnBorrow_MouseUp);
            // 
            // timerLogin
            // 
            this.timerLogin.Enabled = true;
            this.timerLogin.Interval = 1000;
            this.timerLogin.Tick += new System.EventHandler(this.timerLogin_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(50, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 10;
            this.label1.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Yunlib.SellBook.Properties.Resources.时间图标;
            this.pictureBox2.Location = new System.Drawing.Point(699, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // lblLastOp
            // 
            this.lblLastOp.AutoSize = true;
            this.lblLastOp.BackColor = System.Drawing.Color.Transparent;
            this.lblLastOp.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblLastOp.ForeColor = System.Drawing.Color.Red;
            this.lblLastOp.Location = new System.Drawing.Point(368, 39);
            this.lblLastOp.Name = "lblLastOp";
            this.lblLastOp.Size = new System.Drawing.Size(0, 17);
            this.lblLastOp.TabIndex = 43;
            this.lblLastOp.Visible = false;
            // 
            // picOpenCode
            // 
            this.picOpenCode.BackColor = System.Drawing.Color.Transparent;
            this.picOpenCode.Image = global::Yunlib.SellBook.Properties.Resources.kaiguima1;
            this.picOpenCode.Location = new System.Drawing.Point(35, 601);
            this.picOpenCode.Name = "picOpenCode";
            this.picOpenCode.Size = new System.Drawing.Size(144, 138);
            this.picOpenCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picOpenCode.TabIndex = 36;
            this.picOpenCode.TabStop = false;
            this.picOpenCode.Click += new System.EventHandler(this.picOpenCode_Click);
            this.picOpenCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpenCode_MouseDown);
            // 
            // lblNetTest
            // 
            this.lblNetTest.AutoSize = true;
            this.lblNetTest.BackColor = System.Drawing.Color.Transparent;
            this.lblNetTest.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblNetTest.ForeColor = System.Drawing.Color.Red;
            this.lblNetTest.Location = new System.Drawing.Point(333, 234);
            this.lblNetTest.Name = "lblNetTest";
            this.lblNetTest.Size = new System.Drawing.Size(0, 31);
            this.lblNetTest.TabIndex = 44;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(828, 601);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 109);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(785, 718);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 21);
            this.label2.TabIndex = 46;
            this.label2.Text = "关注公众账号获取更多信息";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.SellBook.Properties.Resources.bj;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblNetTest);
            this.Controls.Add(this.lblLastOp);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.picOpenCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pBtnBorrow);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Transparent;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Panel panel1;
        private PictureBox pBtnBorrow;
        private Timer timerLogin;
        private Label label1;
        private PictureBox pictureBox2;
        private Label lblLastOp;
        private PictureBox picOpenCode;
        private Label lblNetTest;
        private PictureBox pictureBox1;
        private Label label2;
    }
}