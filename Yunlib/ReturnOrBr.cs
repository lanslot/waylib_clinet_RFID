﻿using System;
using System.Drawing;
using System.Text;
using Yunlib.Entity;
using YunLib.Common;
using Yunlib.Common;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using Yunlib.Extensions;
using ShenZhen.SmartLife.Control;
using YunLib.BLL;
using System.Data;
using System.Collections.Generic;
using System.Threading;
using System.IO.Ports;
using YunLib.ReadCard;
using NPOI.Util.Collections;

namespace Yunlib
{
    public partial class ReturnOrBr : Form
    {
        //获取服务器url
        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;

        private int cmdPort = ConfigManager.CmdPort;
        private string control = ConfigManager.Control;

        private IReadCard read ;
        //定义全局的控制变量
        private SmartLifeBroker model;
        private bool IsNeedReadCard = true;
        
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        private Form2 f;

       
        public ReturnOrBr()
        {
            InitializeComponent();
            if(ConfigManager.ReadBookType == 1)
            {
                model = new SmartLifeBroker();
                model.OnScanedDataEvent += OnScanedDataEvent;
            }
            else
            {
                read = ObjectCreator.GetReadClass();
            }

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }


        private void ReturnOrBr_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;

            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

         
            try
            {
                if (ConfigManager.ReadBookType == 1)
                {
                    if (model.IsCanUsed(ConfigManager.Scan, cmdPort))
                    {
                        model.Init(ConfigManager.Scan, cmdPort);
                    }
                    pictureBox1.Image = Properties.Resources.Scan22;
                }
                Fun1 = Request;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("ReturnorBr_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        string orderMsg = string.Empty;
        private void IsOutTimeTurnback()
        {
            try
            {
                DataTable dt = accessBLL.GetOutTimeBooks();
                orderMsg += "DataTable dt = accessBLL.GetOutTimeBooks()|";
                IList<LocalBooks> list = null;
                if (dt != null && dt.Rows.Count > 0)
                {
                    list = DataTable2List<LocalBooks>.ConvertToModel(dt);

                    if (list != null && list.Count > 0)
                    {
                        orderMsg += "list = DataTable2List<LocalBooks>.ConvertToModel(dt)|";
                        foreach (var item in list)
                        {
                            string s = list.ToJson().ToString();
                            orderMsg += "foreach (var item in list)|" + s;
                            DateTime endTime = CustomExtensions.ConvertIntDateTime(Convert.ToDouble(item.IsLend.Split('_')[1]));

                            //DataTable dt = accessBLL.GetOutTimeBooks() | list = DataTable2List<LocalBooks>.ConvertToModel(dt) |foreach (var item in list)|

                            //预约还书时间已经超期
                            if (endTime <= DateTime.Now)
                            {
                                bool res = accessBLL.UpdateBooksByid(item.ID);
                                if (res)
                                {
                                    ////休息一秒，防止加载太快又加载一次
                                    //Thread.Sleep(1000);
                                    //ReturnOrBr_Load(null, null);

                                    //取消了超期的预约书柜的信息
                                }
                            }
                        }
                    }
                    else
                    {
                        orderMsg += "无预约书柜超期的书籍！|";
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("还书窗体加载事件，判断预约的书柜是否过期!错误信息：{0},返回信息：{1}".FormatWith(ex.Message, orderMsg));
            }
        }

        private void LoadControlPic()
        {
            pictureBox1.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Scan22.png");
            pictureBox3.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Split11.png");
            pictureBox2.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Write_in111.png");
            pic_BtnTurnBack.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/pic_btn_turnBack.png");
        }
        

       

        #region 控件事件函数
        private void pic_BtnTurnBack_Click(object sender, EventArgs e)
        {
            
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }
        
        int i = 0;
        public string barCode = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                int s = int.Parse(this.label1.Text);

                s -= 1;
                this.label1.Text = s.ToString();

                if (s == 0)
                {

                    //停止计时器
                    timer1.Enabled = false;
                    timer1.Stop();

                    //调回上一窗体
                    //Main frm = Singleton<Main>.CreateInstance();
                    Main frm = new Main();
                    frm.MdiParent = ParentForm;
                    frm.Dock = DockStyle.Fill;
                    IsNeedReadCard = false;
                    this.Close();

                    frm.Show();

                }

               
                if (IsNeedReadCard)
                {
                    if (ConfigManager.ReadBookType == 2)
                    {
                        barCode = read.ReadRFID(ConfigManager.Pos, ConfigManager.IMPort);
                    }
                    else if (ConfigManager.ReadBookType == 3)
                    {
                        barCode = read.ReadUHFRFID(ConfigManager.UHF, ConfigManager.IMPort);
                    
                    }
                    else
                    {
                        if (ConfigManager.ScanType == 1)
                        {
                            if (i % 9 == 0)
                            {
                                //打开扫描器
                                model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(barCode))
                    {
                        txtBarCode.Text = barCode;
                        lblErrMsg.Text = "";
                        IsNeedReadCard = false;
                    }

                }
                i++;
            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }
       
        // 鼠标点入的时候,弹出虚拟键盘
        private void txtBarCode_MouseDown(object sender, MouseEventArgs e)
        {
            ////DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            //frm.ShowDialog();

            //if (frm.DialogResult == DialogResult.Cancel)
            //{
            //    //关闭虚拟键盘时,把值传到登录页面的文本框中
            //    txtBarCode.Text = DymicNewJP.inputCount;
            //}
            //DymicJP frm = new DymicJP { TopMost = true, StartPosition = FormStartPosition.Manual };

            //frm.getTextHandler = getValue;//将方法赋给委托对象
            //frm.Show();
            //txtBarCode.Focus();
            //this.Activate();
            CodeOptimize.form.Show();
            
            txtBarCode.Focus();

            this.Activate();
        }

        public void getValue(string strV)
        {
            if (strV == "Backspace")
            {
                string str = txtBarCode.Text;
                txtBarCode.Text = str.Substring(0, str.Length - 1);
            }
            else if (strV == "Clear")
            {
                this.txtBarCode.Text = "";
            }
            else
            {
                this.txtBarCode.Text += strV;
            }
        }

        JObject responseData;
        int doorNum = 0;
        int doorId = 0;
        int doorCode = 0;
        string url;

       

        public static System.DateTime ConvertIntDateTime(double d)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            DateTime dt = System.DateTime.MinValue;
            dt = startTime.AddSeconds(d);
            return dt;
        }


        private void GetBooksInfo2(string barCode, int doorNum, int doorId, int doorCode, JObject callback)
        {
            if (!string.IsNullOrEmpty(callback["data"].ToString()))
            {
                JToken datas = callback["data"];

                JToken bookLendInfo = datas["bookLendInfo"];
                JToken bookDesc = datas["bookDesc"];

                if (bookLendInfo.ToJson() == null || bookLendInfo.ToJson() == "null")
                {
                    lblErrMsg.Text = "无该书借阅记录，若有问题请联系管理员！";
                }
                else if (bookDesc.ToJson() == null || bookDesc.ToJson() == "null")
                {
                    lblErrMsg.Text = "该书不属于本设备哟，请检查是否拿对图书！";
                }
                else
                {

                    BooksInfo bmodel = new BooksInfo();

                    bmodel.BarCode = barCode;
                    bmodel.BooksName = bookDesc["title"].ToString() != null ? bookDesc["title"].ToString() : "";
                    bmodel.Author = bookDesc["author"].ToString() != null ? bookDesc["author"].ToString() : "";
                    bmodel.Pulisher = bookDesc["publisher"].ToString() != null ? bookDesc["publisher"].ToString() : "";
                    bmodel.PulishTime = bookDesc["pubdate"].ToString() != null ? bookDesc["pubdate"].ToString() : "";
                    bmodel.BorrowDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["createdTime"]));
                    bmodel.ShouldReturnDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["updatedTime"]));

                    bmodel.DoorNum = doorNum;
                    bmodel.DoorID = doorId;
                    bmodel.DoorCode = doorCode;
                    bmodel.CoverUrl = !string.IsNullOrWhiteSpace(bookDesc["coverUrl"].ToString()) ? bookDesc["coverUrl"].ToString() : "";

                   // LogManager.BorrowRecord("预约还书柜号：{0}，DoorId：{1}，DoorCode:{2}".FormatWith(doorNum, doorId, doorCode));

                    timer1.Enabled = false;
                    timer1.Stop();

                    //有此书,就跳转到还书详情窗体。
                    //并且把书,借书人等相关信息传过去

                    ReturnOrBrDetail Frm = new ReturnOrBrDetail { MdiParent = ParentForm, Dock = DockStyle.Fill, bookModel = bmodel };

                    ReturnOrBrDetail.overplusDyas = datas["day"].ToString();

                    this.Close();

                    Frm.Show();
                   
                }
            }
        }

        private JObject GetBooksInfo(string barCode)
        {
            var timeStamp = DateTime.Now.Ticks.ToString();

            var signStr = "{0}{1}{2}{3}".FormatWith(barCode, libraryId, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}/public/book/lendinfo?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(
                servicePath, barCode, libraryId, timeStamp, sign);

            var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();
            return callback;
        }


        #endregion

        #region 窗体扩展事件
        private void OnScanedDataEvent(object source, byte[] scanFrame)
        {
            string barCode = Encoding.ASCII.GetString(scanFrame);

            txtBarCode.Text = barCode;
            IsNeedReadCard = false;
        }


        #endregion

        private void ReturnOrBr_FormClosing(object sender, FormClosingEventArgs e)
        {
            //停止计时器
            timer1.Enabled = false;
            timer1.Stop();
            if(ConfigManager.ReadBookType == 1)
            {
                model.Dispose();
            }
              
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
               
                //获取条形码
                string barCode = txtBarCode.Text.Trim();

                //条形码为空时提示用户请输入
                if (string.IsNullOrWhiteSpace(barCode))
                {
                    lblErrMsg.Text = "请输入条形码！";
                }
                else
                {


                    //根据条形码,判断是否是预约还书的书
                    DataTable book = accessBLL.GetBookLikeBarCode(barCode);

                    IList<LocalBooks> list = null;
                    if (book != null && book.Rows.Count > 0)
                    {
                        list = DataTable2List<LocalBooks>.ConvertToModel(book);

                        if (list != null && list.Count > 0)
                        {
                            //不为空说明是预约还书了的
                            doorNum = list[0].DoorNUM;
                            doorId = list[0].DoorID;
                            doorCode = list[0].DoorCode;

                            string[] s = new string[1];
                            s[0] = ConfigManager.PingServicePath;
                            bool flag = NetTest.CheckServeStatus(s);
                            //验证网络连接状态，true就发起请求，false不请求
                            if (flag)
                            {
                                var timeStamp = DateTime.Now.Ticks.ToString();

                                var signStr = "{0}{1}{2}{3}".FormatWith(barCode, libraryId, timeStamp, commonKey);
                                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                                var url = "{0}/public/book/lendinfo?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(
                                    servicePath, barCode, libraryId, timeStamp, sign);

                                var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                                responseData = callback;

                                if (callback["code"].ToString() == "100")
                                {
                                    GetBooksInfo2(barCode, doorNum, doorId, doorCode, callback);
                                }
                                else
                                {
                                    lblErrMsg.Text = callback["msg"].ToString();
                                }
                            }
                            else
                            {
                                lblErrMsg.Text = "网络繁忙,请稍后再试！";
                            }

                        }
                        else
                        {
                            lblErrMsg.Text = "书籍不属于图书馆或者书籍已归还";
                        }
                    }
                    else
                    {

                        string[] s = new string[1];
                        s[0] = ConfigManager.PingServicePath;
                        bool flag = NetTest.CheckServeStatus(s);
                        //验证网络连接状态，true就发起请求，false不请求
                        if (flag)
                        {

                            var timeStamp = DateTime.Now.Ticks.ToString();

                            var signStr = "{0}{1}{2}{3}".FormatWith(barCode, libraryId, timeStamp, commonKey);
                            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());
                            url = "{0}/public/book/lendinfo?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(
                                servicePath, barCode, libraryId, timeStamp, sign);

                            if (t == null)
                            {
                                t = new Thread(Run);
                                t.Start();
                            }
                            if (t.ThreadState == ThreadState.Suspended) // 如果被挂起了，就唤醒
                            {
                                t.Resume();
                            }


                        }
                        else
                        {
                            lblErrMsg.Text = "网络繁忙,请稍后再试！";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("调用得到的信息：{1}，错误原因：{0}".FormatWith(ex.Message, responseData.ToJson()));
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// 因为控件的Invoke方法需要接收委托变量，因此需要定义委托和委托变量
        /// 定义一个委托，接收一个参数
        /// </summary>
        /// <param name="msg"></param>
        public delegate void DelegateFun(string msg);
        /// <summary>
        /// 定义一个委托变量
        /// 这个委托变量，需要初始化指定具体的方法；然后传递给控件的Invoke方法调用。
        /// </summary>
        public DelegateFun Fun1;


        /// <summary>
        /// 定义一个线程，处理数据，并更新界面
        /// </summary>
        private Thread t = null;

        // 结束执行


        // 具体做事情的方法
        public void Run()
        {

            //Invoke指： 在拥有控件的基础窗口句柄的线程上，用指定的参数列表执行指定委托。
            //Invoke的参数是一个委托类型，因此必须定义委托变量 
            this.Invoke(Fun1, url);

        }

        private void Request(string url)
        {
            try
            {
                LogManager.ErrorRecord("开始线程123");
                var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                responseData = callback;

                if (callback["code"].ToString() == "100")
                {
                    if (!string.IsNullOrEmpty(callback["data"].ToString()))
                    {
                        JToken datas = callback["data"];

                        JToken bookLendInfo = datas["bookLendInfo"];
                        JToken bookDesc = datas["bookDesc"];

                        if (bookDesc.ToJson() == null || bookDesc.ToJson() == "null")
                        {
                            lblErrMsg.Text = "该书不属于本设备哟，请检查是否拿对图书！";
                        }
                        else
                        {

                            BooksInfo bmodel = new BooksInfo();

                            bmodel.BarCode = txtBarCode.Text.Trim();
                            bmodel.BooksName = bookDesc["title"].ToString() != null ? bookDesc["title"].ToString() : "";
                            bmodel.Author = bookDesc["author"].ToString() != null ? bookDesc["author"].ToString() : "";
                            bmodel.Pulisher = bookDesc["publisher"].ToString() != null ? bookDesc["publisher"].ToString() : "";
                            bmodel.PulishTime = bookDesc["pubdate"].ToString() != null ? bookDesc["pubdate"].ToString() : "";
                            if (bookLendInfo.HasValues)
                            {
                                if (!string.IsNullOrWhiteSpace(bookLendInfo["createdTime"].ToString()))
                                {
                                    bmodel.BorrowDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["createdTime"]));
                                }
                                if (!string.IsNullOrWhiteSpace(bookLendInfo["updatedTime"].ToString()))
                                {
                                    bmodel.ShouldReturnDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["updatedTime"]));
                                }
                            }
                            bmodel.DoorNum = doorNum;
                            bmodel.DoorID = doorId;
                            bmodel.DoorCode = doorCode; 

                            bmodel.CoverUrl = !string.IsNullOrWhiteSpace(bookDesc["coverUrl"].ToString()) ? bookDesc["coverUrl"].ToString() : "";

                            LogManager.BorrowRecord("预约还书柜号：{0}，DoorId：{1}，DoorCode:{2}".FormatWith(doorNum, doorId, doorCode));



                            //有此书,就跳转到还书详情窗体。
                            //并且把书,借书人等相关信息传过去

                            ReturnOrBrDetail Frm = new ReturnOrBrDetail { MdiParent = ParentForm, Dock = DockStyle.Fill, bookModel = bmodel };
                            if (datas["day"].HasValues)
                            {
                                ReturnOrBrDetail.overplusDyas = datas["day"].ToString();
                            }
                            this.Close();

                            Frm.Show();
                            timer1.Enabled = false;
                            timer1.Stop();
                            //}

                        }
                    }
                }
                else
                {
                    lblErrMsg.Text = callback["msg"].ToString();
                }
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                lblErrMsg.Text = "网络错误，请稍后在试";
                LogManager.ErrorRecord("还书请求流通信息出现错误，错误原因：{0}".FormatWith(ex.Message));
            }
            IsNeedReadCard = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            txtBarCode.Focus();
            f.Show();
            this.Activate();
        }
    }
}
