﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.ReadCard.HF
{
    public class SSSE32
    {
        //打开端口 

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Open")]
        public static extern int ICC_Reader_Open(int port, int buod);



        //关闭端口
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Close")]
        public static extern int ICC_Reader_Close(int ReaderHandle);

        [DllImport("SSSE32.dll", EntryPoint = "ICC_PosBeep")]
        public static extern int ICC_PosBeep(int ReaderHandle, byte time);/*蜂鸣*/

        //注意：输入的是12位的密钥，例如12个f，但是password必须是6个字节的密钥，需要用StrToHex函数处理。
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Authentication_Pass")]
        public static extern int PICC_Reader_Authentication_Pass(int ReaderHandle,
                                                                    byte Mode,
                                                                    byte SecNr,
                                                                    byte[] PassWord);
        //读卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Read")]
        public static extern int PICC_Reader_Read(int ReaderHandle, byte Addr,
                                                        byte[] Data);
        //写卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Write")]
        public static extern int PICC_Reader_Write(int ReaderHandle, byte Addr,
                                                        byte[] Data);

        //将字符命令流转为16进制流
        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(StringBuilder strIn, int len, Byte[] HexOut);

        //将16进制流命令转为字符流
        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(Byte[] strIn, int inLen,
                                                StringBuilder strOut);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_PowerOnTypeA")]
        public static extern int PICC_Reader_PowerOnTypeA(int ReaderHandle, byte[] Response);//上电 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_pre_PowerOn")]
        public static extern int ICC_Reader_pre_PowerOn(int ReaderHandle, byte SLOT, byte[] Response);//上电 返回数据长度 失败小于0
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Application")]
        public static extern int ICC_Reader_Application(int ReaderHandle, byte SLOT, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Application")]
        public static extern int PICC_Reader_Application(int ReaderHandle, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        //HD100身份证
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ReadIDMsg")]
        public static extern int PICC_Reader_ReadIDMsg(int RHandle, StringBuilder pBmpFile, StringBuilder pName, StringBuilder pSex, StringBuilder pNation, StringBuilder pBirth, StringBuilder pAddress, StringBuilder pCertNo, StringBuilder pDepartment, StringBuilder pEffectData, StringBuilder pExpire, StringBuilder pErrMsg);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ID_ReadUID")]
        public static extern int PICC_Reader_ID_ReadUID(int fd, byte[] UID);//获取身份证UID, 8Byte

        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(byte[] strIn, int inLen, byte[] strOut);


        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(byte[] strIn, int inLen, byte[] strOut);

        /*############ 15693 卡操作函数#################*/
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Inventory")]
        public static extern int PICC_Reader_Inventory(int ReaderHandle, byte[] Response_APDU);//此命令通过防冲突用于得到读卡区域内所有卡片的序列号
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_15693_Read")]
        public static extern int PICC_Reader_15693_Read(int ReaderHandle, byte blk_add, byte[] Response_APDU);//用来读取1个扇区的值
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_15693_Write")]
        public static extern int PICC_Reader_15693_Write(int ReaderHandle, byte blk_add, byte[] data,
                                            byte[] Response_APDU);//对一个块进行写操作（每次只能写一个块）
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_API")]
        public static extern int PICC_Reader_API(int ReaderHandle, byte[] data, byte[] Response_APDU);//Data[0]	0代表写API,1代表锁API
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_DSFID")]
        public static extern int PICC_Reader_DSFID(int ReaderHandle, byte[] data, byte[] Response_APDU);//Data[0]	0代表写DSFID,1代表锁DSFID
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_LockDataBlock")]
        public static extern int PICC_Reader_LockDataBlock(int ReaderHandle, byte blk_add, byte[] Response_APDU);//用于锁定块内容。注意：此过程不可逆（不能解锁）块锁定后内容不能在修改。
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_SystemInfor")]
        public static extern int PICC_Reader_SystemInfor(int ReaderHandle, byte[] Response_APDU);//用于得到卡的详细信息
 
    }
}
