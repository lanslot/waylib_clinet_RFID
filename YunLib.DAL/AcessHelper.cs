﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.Entity;

namespace Yunlib.DAL
{
    public class AcessHelper
    {
        public static readonly string ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}\\Data\\LocalBooks.accdb;Persist Security Info=False;".FormatWith(Environment.CurrentDirectory);

        public static DataSet ExecuteDataSet(string sqlText)
        {
            OleDbConnection conn;

            conn = new OleDbConnection(ConnectionString);

            OleDbDataAdapter da = new OleDbDataAdapter(sqlText, conn);
            DataSet ds = new DataSet("ds");
            da.Fill(ds, "Localbooks");

            return ds;
        }

        public static Boolean ExecuteNonQuery(string sqlText)
        {
            try
            {
                OleDbConnection conn;
                conn = new OleDbConnection(ConnectionString);
                conn.Open();
                OleDbCommand da = new OleDbCommand();
                da.CommandText = sqlText;
                da.Connection = conn;
                int line = da.ExecuteNonQuery();
                conn.Close();
                if (line > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }


    }
}
