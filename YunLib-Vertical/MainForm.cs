﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.DAL;
using YunLib.Common;
using YunLib_Vertical.AdminManager;

namespace YunLib_Vertical
{
    public partial class MainForm : Form
    {
        private string IsStop=ConfigurationManager.AppSettings["IsStop"];
        public MainForm()
        {
            InitializeComponent();
            if(IsStop == "1")
            {
                picBorrow.Enabled = false;
                picRegister.Enabled = false;
                picReturn.Enabled = false;
                picOpenCode.Enabled = false;
                label1.Visible = true;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (ConfigManager.CanRegister == 1)
            {
                picRegister.Visible = true;
            }
        }

        #region 按钮样式
        private void picBorrow_MouseUp(object sender, MouseEventArgs e)
        {
            picBorrow.Image = Properties.Resources.jie_12_03;
        }

        private void picBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            picBorrow.Image = Properties.Resources.jie_12_03_1;
        }

        private void picReturn_MouseDown(object sender, MouseEventArgs e)
        {
            picReturn.Image = Properties.Resources.jie_12_05_1;
        }

        private void picReturn_MouseUp(object sender, MouseEventArgs e)
        {
            picReturn.Image = Properties.Resources.jie_12_05;
        }

        private void picRegister_MouseDown(object sender, MouseEventArgs e)
        {
            picRegister.Image = Properties.Resources.jie_12_13_1;
        }

        private void picRegister_MouseUp(object sender, MouseEventArgs e)
        {
            picRegister.Image = Properties.Resources.jie_12_13;
        }

       

        private void picOpenCode_MouseDown(object sender, MouseEventArgs e)
        {
            picOpenCode.Image = Properties.Resources.jie_12_10_1;
        }

        private void picOpenCode_MouseUp(object sender, MouseEventArgs e)
        {
            picOpenCode.Image = Properties.Resources.jie_12_10;
        }
        #endregion

        private void picOpenCode_Click(object sender, EventArgs e)
        {
            kGForm frm = new kGForm();
            frm.sType = "1";
            frm.ShowDialog();
        }

        private void picBorrow_Click(object sender, EventArgs e)
        {
            try
            {
                BooKListForm frm = new BooKListForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                this.Close();
                frm.Show();
            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }

        private void picReturn_Click(object sender, EventArgs e)
        {
            try
            {
                ScanForm frm = new ScanForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                this.Close();
                frm.Show();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }

        }

        private void picRegister_Click(object sender, EventArgs e)
        {
            RegisterForm frm = new RegisterForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            panel_click_count++;
            if (panel_click_count == 10)
            {
                kGForm frm = new kGForm();
                frm.sType = "2";
                frm.ShowDialog();

                if (frm.DialogResult == DialogResult.Cancel)
                {
                    if (kGForm.isAdmin)
                    {
                        BackMain bf = new BackMain();
                        bf.MdiParent = this.ParentForm;
                        bf.Parent = this.Parent;
                        this.Close();
                        bf.Show();
                    }
                }
            }
        }
        int panel_click_count = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
           if( DateTime.Now.Second%5 == 0)
            {
                panel_click_count = 0;
            }
        }

       
    }
}
