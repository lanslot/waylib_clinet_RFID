﻿namespace YunLib_Vertical
{
    partial class BookDescForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookDescForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBName = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblPulisher = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMachLatNum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPublishTime = new System.Windows.Forms.Label();
            this.lable2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAbstracts = new System.Windows.Forms.Label();
            this.picBorrow = new System.Windows.Forms.PictureBox();
            this.lblType = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.picBack = new System.Windows.Forms.PictureBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.picLine1 = new System.Windows.Forms.PictureBox();
            this.picLine2 = new System.Windows.Forms.PictureBox();
            this.picline3 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picline3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(331, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 414);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::YunLib_Vertical.Properties.Resources.nopic;
            this.pictureBox1.Location = new System.Drawing.Point(103, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 281);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(126, 558);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "书   名：";
            // 
            // lblBName
            // 
            this.lblBName.AutoSize = true;
            this.lblBName.BackColor = System.Drawing.Color.Transparent;
            this.lblBName.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBName.ForeColor = System.Drawing.Color.White;
            this.lblBName.Location = new System.Drawing.Point(224, 558);
            this.lblBName.Name = "lblBName";
            this.lblBName.Size = new System.Drawing.Size(0, 28);
            this.lblBName.TabIndex = 2;
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.BackColor = System.Drawing.Color.Transparent;
            this.lblAuthor.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAuthor.ForeColor = System.Drawing.Color.White;
            this.lblAuthor.Location = new System.Drawing.Point(738, 558);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(0, 28);
            this.lblAuthor.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(615, 558);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "作       者：";
            // 
            // lblPulisher
            // 
            this.lblPulisher.AutoSize = true;
            this.lblPulisher.BackColor = System.Drawing.Color.Transparent;
            this.lblPulisher.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPulisher.ForeColor = System.Drawing.Color.White;
            this.lblPulisher.Location = new System.Drawing.Point(224, 597);
            this.lblPulisher.Name = "lblPulisher";
            this.lblPulisher.Size = new System.Drawing.Size(0, 28);
            this.lblPulisher.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(126, 597);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 28);
            this.label6.TabIndex = 5;
            this.label6.Text = "出版社：";
            // 
            // lblMachLatNum
            // 
            this.lblMachLatNum.AutoSize = true;
            this.lblMachLatNum.BackColor = System.Drawing.Color.Transparent;
            this.lblMachLatNum.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMachLatNum.ForeColor = System.Drawing.Color.White;
            this.lblMachLatNum.Location = new System.Drawing.Point(224, 636);
            this.lblMachLatNum.Name = "lblMachLatNum";
            this.lblMachLatNum.Size = new System.Drawing.Size(0, 28);
            this.lblMachLatNum.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(126, 636);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 28);
            this.label8.TabIndex = 7;
            this.label8.Text = "柜门号：";
            // 
            // lblPublishTime
            // 
            this.lblPublishTime.AutoSize = true;
            this.lblPublishTime.BackColor = System.Drawing.Color.Transparent;
            this.lblPublishTime.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPublishTime.ForeColor = System.Drawing.Color.White;
            this.lblPublishTime.Location = new System.Drawing.Point(738, 597);
            this.lblPublishTime.Name = "lblPublishTime";
            this.lblPublishTime.Size = new System.Drawing.Size(0, 28);
            this.lblPublishTime.TabIndex = 10;
            // 
            // lable2
            // 
            this.lable2.AutoSize = true;
            this.lable2.BackColor = System.Drawing.Color.Transparent;
            this.lable2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable2.ForeColor = System.Drawing.Color.White;
            this.lable2.Location = new System.Drawing.Point(615, 597);
            this.lable2.Name = "lable2";
            this.lable2.Size = new System.Drawing.Size(117, 28);
            this.lable2.TabIndex = 9;
            this.lable2.Text = "出版日期：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(126, 773);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 28);
            this.label12.TabIndex = 11;
            this.label12.Text = "简   介：";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.lblAbstracts);
            this.panel2.Location = new System.Drawing.Point(131, 813);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(766, 100);
            this.panel2.TabIndex = 12;
            // 
            // lblAbstracts
            // 
            this.lblAbstracts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAbstracts.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAbstracts.ForeColor = System.Drawing.Color.White;
            this.lblAbstracts.Location = new System.Drawing.Point(0, 0);
            this.lblAbstracts.Name = "lblAbstracts";
            this.lblAbstracts.Size = new System.Drawing.Size(766, 100);
            this.lblAbstracts.TabIndex = 0;
            // 
            // picBorrow
            // 
            this.picBorrow.Image = ((System.Drawing.Image)(resources.GetObject("picBorrow.Image")));
            this.picBorrow.Location = new System.Drawing.Point(454, 1034);
            this.picBorrow.Name = "picBorrow";
            this.picBorrow.Size = new System.Drawing.Size(220, 60);
            this.picBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBorrow.TabIndex = 13;
            this.picBorrow.TabStop = false;
            this.picBorrow.Click += new System.EventHandler(this.pBtnBorrow_Click);
            this.picBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBorrow_MouseDown);
            this.picBorrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picBorrow_MouseUp);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.BackColor = System.Drawing.Color.Transparent;
            this.lblType.Location = new System.Drawing.Point(88, 451);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 12);
            this.lblType.TabIndex = 14;
            this.lblType.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // picBack
            // 
            this.picBack.Image = global::YunLib_Vertical.Properties.Resources.hui_w;
            this.picBack.Location = new System.Drawing.Point(968, 1007);
            this.picBack.Name = "picBack";
            this.picBack.Size = new System.Drawing.Size(100, 100);
            this.picBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBack.TabIndex = 15;
            this.picBack.TabStop = false;
            this.picBack.Click += new System.EventHandler(this.picBack_Click);
            this.picBack.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseDown);
            this.picBack.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseUp);
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblBarcode.Location = new System.Drawing.Point(52, 451);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(0, 12);
            this.lblBarcode.TabIndex = 16;
            this.lblBarcode.Visible = false;
            // 
            // picLine1
            // 
            this.picLine1.Image = global::YunLib_Vertical.Properties.Resources.xuxian_1;
            this.picLine1.Location = new System.Drawing.Point(99, 518);
            this.picLine1.Name = "picLine1";
            this.picLine1.Size = new System.Drawing.Size(895, 2);
            this.picLine1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLine1.TabIndex = 17;
            this.picLine1.TabStop = false;
            // 
            // picLine2
            // 
            this.picLine2.Image = global::YunLib_Vertical.Properties.Resources.xuxian_2;
            this.picLine2.Location = new System.Drawing.Point(99, 732);
            this.picLine2.Name = "picLine2";
            this.picLine2.Size = new System.Drawing.Size(894, 3);
            this.picLine2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLine2.TabIndex = 18;
            this.picLine2.TabStop = false;
            // 
            // picline3
            // 
            this.picline3.Image = global::YunLib_Vertical.Properties.Resources.xuxian_3;
            this.picline3.Location = new System.Drawing.Point(99, 937);
            this.picline3.Name = "picline3";
            this.picline3.Size = new System.Drawing.Size(894, 2);
            this.picline3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picline3.TabIndex = 19;
            this.picline3.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Magenta;
            this.label2.Location = new System.Drawing.Point(1047, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 28);
            this.label2.TabIndex = 22;
            this.label2.Text = "秒";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCount.ForeColor = System.Drawing.Color.Magenta;
            this.lblCount.Location = new System.Drawing.Point(1005, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(48, 28);
            this.lblCount.TabIndex = 21;
            this.lblCount.Text = "120";
            // 
            // BookDescForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.di;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1080, 1129);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.picline3);
            this.Controls.Add(this.picLine2);
            this.Controls.Add(this.picLine1);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.picBack);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.picBorrow);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblPublishTime);
            this.Controls.Add(this.lable2);
            this.Controls.Add(this.lblMachLatNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblPulisher);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblBName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BookDescForm";
            this.Text = "BookDescForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BookDescForm_FormClosing);
            this.Load += new System.EventHandler(this.BookDescForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBorrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picline3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBName;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPulisher;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMachLatNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPublishTime;
        private System.Windows.Forms.Label lable2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAbstracts;
        private System.Windows.Forms.PictureBox picBorrow;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox picBack;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.PictureBox picLine1;
        private System.Windows.Forms.PictureBox picLine2;
        private System.Windows.Forms.PictureBox picline3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCount;
    }
}