﻿using System;
using System.Text;
using YunLib.BLL;
using ZooKeeperNet;
using Yunlib.Common;
using YunLib.Common;
using Yunlib.Entity;
using Newtonsoft.Json.Linq;
using Yunlib.Extensions;
using ShenZhen.SmartLife.Control;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Yunlib
{
    public partial class Watcher : Form, IWatcher
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        public ReaderLogin readerLoginFrm;
        public BorrSuccess borrSuccessFrm;

        public void Process(WatchedEvent @event)
        {
            try
            {
                LogManager.BorrowRecord(" 收到节点事件通知：{0}，内容： {1}".FormatWith(DateTime.Now, @event.State));

                if (@event.Type == EventType.NodeDataChanged)
                {
                    string rDataStr = ZookeeperCallBack(ConfigManager.ZookeeperPath, ConfigManager.MachineID);

                    LogManager.BorrowRecord(" 节点数据修改时间：{0}，内容：{1}".FormatWith(DateTime.Now, rDataStr));

                    string[] dataArr = rDataStr.Split('-');

                    switch (dataArr[0])
                    {
                        case "1":
                            //借书成功打开柜门
                            HttpBorrSuccess(rDataStr, dataArr);
                            break;
                        case "2":
                            //还书成功打开柜门
                            HttpReturnSuccess(dataArr);
                            break;
                        case "8":
                            //还书失败 Zookeeper返回的状态
                            LogManager.ErrorRecord("【还书】开门失败，错误信息：{0}".FormatWith(rDataStr));
                            break;
                        case "9":
                            //借书失败 Zookeeper返回的状态
                            LogManager.ErrorRecord("【借书】开门失败，错误信息：{0}".FormatWith(rDataStr));
                            break;
                        default:
                            break;
                    }
                }

                //重新建立连接
                if (@event.State == KeeperState.Expired || @event.State == KeeperState.Disconnected)
                {
                    ZooKeeperClient.createNode();
                    LogManager.BorrowRecord("**** 重新建立连接时间：{0}".FormatWith(DateTime.Now));
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("{0}".FormatWith(ex.Message));
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private string ZookeeperCallBack(string zookeeperPath, string machineID)
        {
            //节点路径+/+机器编号
            string path = zookeeperPath + "/" + machineID;

            //获取节点修改后的数据，绑定监听
            byte[] data = ZooKeeperClient.zk.GetData(path, ZooKeeperClient.wc, null);

            //获取Zookeeper返回的字符串
            return Encoding.Default.GetString(data);
        }

        private void HttpReturnSuccess(string[] sdata)
        {
            //获得空书柜集合
            IList<LocalBooks> currentModel = accessBLL.GetNullBookCase();

            var back = WebHelper.HttpWebRequest("{0}/public/book/bookdesc?bookId={1}".FormatWith(ConfigManager.ServicePath, sdata[1].ToString())).ToJObject();

            if (back["code"].ToString() == "100")
            {
                JToken datas = back["data"];
                LocalBooks model = new LocalBooks
                {
                    BarCode = sdata[1].ToString(),
                    BName = datas["title"].ToString() ?? "",
                    Image = datas["coverUrl"].ToString() ?? "",
                    Introduction = datas["abstract"].ToString() ?? "",
                    Press = datas["publisher"].ToString() ?? "",
                    PublicationDate = datas["pubdate"].ToString() ?? "",
                    Writer = datas["author"].ToString() ?? "",
                    DoorID = currentModel[0].DoorID,
                    DoorNUM = currentModel[0].DoorNUM
                };

                bool res = accessBLL.FillBookToAcess(model);

                if (res)
                {
                    //自动释放资源
                    using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                    {
                        //初始化控制台串口
                        consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                        //打开相应行号的柜门
                        consoleModel.OpenDoor(currentModel[0].DoorID, currentModel[0].DoorCode);

                        LogManager.BorrowRecord("【还书】成功开门，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                    }
                }
                else
                {
                    //Zookeeper返回成功，但是本机执行录入数据
                    LogManager.ErrorRecord("【还书】开门失败，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                }
            }
            else
            {
                //code 返回的不是101
                LogManager.ErrorRecord("【还书】返回信息——code:{0}  msg:{1}  datetime:{2}".FormatWith(back["code"].ToString(), back["msg"].ToString(), DateTime.Now));
            }
        }

        private void HttpBorrSuccess(string rDataStr, string[] sdata)
        {
            
            var currentModel = accessBLL.GetBookByBarCode(sdata[1]);

            LocalBooks model = new LocalBooks
            {
                DoorID = currentModel.DoorID,
                DoorCode = currentModel.DoorCode
            };

            //清除本地数据
            bool res = accessBLL.FillBookToAcess(model);

            if (res)
            {
                //自动释放资源
                using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                {
                    //初始化控制台串口
                    consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                    //打开相应行号的柜门
                    consoleModel.OpenDoor(currentModel.DoorID, currentModel.DoorCode);

                    LogManager.BorrowRecord("【借书】成功开门，{0}{1}*****{2},返回的信息：{3}".FormatWith(currentModel.DoorID, currentModel.DoorCode, DateTime.Now, rDataStr));

                    BorrSuccess frm = new Yunlib.BorrSuccess { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    readerLoginFrm.Close();

                    frm.Show();

                    ////停止ReaderLogin中的Timer1控件
                    //readerLoginFrm.timer1.Stop();

                    //readerLoginFrm.timer2.Stop();

                    //readerLoginFrm.Close();

                    ////跳转到借书成功界面
                    //borrSuccessFrm.Show();
                }
            }
            else
            {
                //Zookeeper返回成功，但是本机执行录入数据失败
                LogManager.ErrorRecord("【借书】开门失败，{0}{1}*****{2},返回的信息：{3}".FormatWith(currentModel.DoorID, currentModel.DoorCode, DateTime.Now, rDataStr));
            }
        }
    }
}
