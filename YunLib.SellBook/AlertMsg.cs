﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib.SellBook
{
    public partial class AlertMsg : Form
    {
        public AlertMsg(string Message)
        {
            InitializeComponent();

            lblAlertMsg.Text = Message;
        }
        public AlertMsg()
        {
            InitializeComponent();

            lblAlertMsg.Text = "";
        }

        public void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
