﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class RegisterForm : Form
    {
        private bool ISNeedReadCard = true;

        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;
        public delegate void MyDelegate(string address, string dzCode, string gender, string name);
        MyDelegate md = null;
     
        public RegisterForm()
        {
            InitializeComponent();
        //    md = new MyDelegate(getData);

        }
        bool yzmFlag = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

                int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;
                this.lblDaoJiShi.Text = second.ToString();

                if (second <= 0)
                {
                    //断开连接
                    

                    // Main frm = Singleton<Main>.CreateInstance();
                    Main frm = new Main();

                    frm.MdiParent = ParentForm;
                    frm.Dock = DockStyle.Fill;
                    timer1.Enabled = false;
                    timer1.Stop();
                    this.Close();

                    frm.Show();
                }
                if (ISNeedReadCard)
                {
                    List<string> list = HDReadCard.ReadIDCardID(ConfigManager.Pos, ConfigManager.IMPort);
                    if (list.Count > 0)
                    {
                        ISNeedReadCard = false;
                        lblName.Text = list[0];
                        lblSex.Text = list[1];
                        lblIdCard.Text = list[2];
                        lblAddress.Text = list[3];
                        picZP.ImageLocation = list[5];
                        lblPwd.Text = list[2].Substring(8, 6);
                        if (!string.IsNullOrWhiteSpace(lblPhoneErr.Text))
                        {
                            lblPhoneErr.Text = "";
                        }

                    }
                }
                if (yzmFlag)
                {
                    int YZMsecond = Convert.ToInt32(btnYZM.Text) - 1;
                    this.btnYZM.Text = YZMsecond.ToString();


                    if(YZMsecond == 0)
                    {
                        yzmFlag = false;
                        btnYZM.Text = "获取验证码";
                        btnYZM.Enabled = true;
                        btnYZM.BackColor = Color.LightSeaGreen;
                    }

                }
                

            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }

        private void pic_BtnTurnBack_Click(object sender, EventArgs e)
        {
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void RegisterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //停止计时器
            timer1.Enabled = false;
            timer1.Stop();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtPhone.Text))
                {
                    lblPhoneErr.Text = "手机号码不能为空";
                }
                else if (string.IsNullOrWhiteSpace(txtYZM.Text))
                {
                    lblCodeErr.Text = "验证码不能为空";
                }
                else
                {
                    getData(lblAddress.Text, lblIdCard.Text, lblSex.Text, lblName.Text, txtPhone.Text, txtYZM.Text);
                }
               
            }
            catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }

        public void getData(string address,string dzCode,string gender,string name,string phone,string yzm)
        {
            try
            {
                button1.Enabled = false;
                var timeStamp = DateTime.Now.Ticks.ToString();
                var signStr = "{0}{1}{2}{3}{4}{5}".FormatWith(dzCode, machineId,yzm, timeStamp,phone, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());
                LogManager.WriteLogs("注册请求", "加密前signStr:"+signStr+"加密后 sign:"+sign, LogFile.Record);
                var url = "{0}public/idCard/register?address={1}&dzCode={2}&gender={3}&machineId={4}&realName={5}&uMobile={6}&smsCode={7}&timeStamp={8}&sign={9}".FormatWith(servicePath, address,dzCode,gender,machineId,name,phone,yzm,timeStamp,sign);
                LogManager.WriteLogs("注册请求url", url, LogFile.Record);
                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                LogManager.WriteLogs("注册请求", res.ToString(), LogFile.Record);
                if (res["code"].ToString() == "100")
                {
                    bool flag = res["data"]["foregift"].ToBool();;
                    if (flag && !string.IsNullOrWhiteSpace(res["data"]["codeUrl"].ToString()))
                    {
                       // res["data"]["codeUrl"].ToString(), res["data"]["outTradeNo"].ToString()
                       

                        PayForm frm = new PayForm { MdiParent = ParentForm, Dock = DockStyle.Fill };
                        frm.url = res["data"]["codeUrl"].ToString();
                        frm.outTradeNo = res["data"]["outTradeNo"].ToString();
                        frm.money = res["data"]["money"].ToString();
                        this.Close();
                        frm.Show();

                    }
                    else
                    {
                        RegisterSuccessForm frm = new RegisterSuccessForm { MdiParent = ParentForm, Dock = DockStyle.Fill };
                      
                        this.Close();
                        frm.Show();
                    }
                }else
                {
                    RegisterFailForm frm = new RegisterFailForm { MdiParent = ParentForm, Dock = DockStyle.Fill }; ;

                    this.Close();
                    frm.Show();
                   
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }


      

     

        private void txtPhone_MouseDown(object sender, MouseEventArgs e)
        {
            if (ISNeedReadCard)
            {
                lblPhoneErr.Text = "请先将身份证放到身份证读卡区";
            }
            else
            {
                DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

                frm.ShowDialog();

                if (frm.DialogResult == DialogResult.Cancel)
                {
                    //关闭虚拟键盘时,把值传到登录页面的文本框中
                    txtPhone.Text = DymicNewJP.inputCount;
                    // pBtnLogin_Click(null, null);
                }
            }
        }

        private void txtPhone_MouseLeave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPhone.Text))
                txtPhone.Text = "※手机号码不能为空";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string str = "SMS_84685375";
            var timeStamp = DateTime.Now.Ticks.ToString();
            var signStr = "{0}{1}{2}{3}".FormatWith(str,timeStamp, txtPhone.Text, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}public/users/sendSMSCode?smsType={1}&uMobile={2}&timeStamp={3}&sign={4}".FormatWith(servicePath, str, txtPhone.Text,  timeStamp, sign);
            var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();
            btnYZM.Text = "60";
            yzmFlag = true;
            btnYZM.Enabled = false;
            btnYZM.BackColor = Color.LightSteelBlue;
        }

        private void txtYZM_MouseDown(object sender, MouseEventArgs e)
        {
            DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.Cancel)
            {
                //关闭虚拟键盘时,把值传到登录页面的文本框中
                txtYZM.Text = DymicNewJP.inputCount;
                // pBtnLogin_Click(null, null);
            }
        }
    }
}
