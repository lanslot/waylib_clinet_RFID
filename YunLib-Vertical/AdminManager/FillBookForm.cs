﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib.ReadCard;

namespace YunLib_Vertical.AdminManager
{
    public partial class FillBookForm : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        private int cmdPort = ConfigManager.CmdPort;
        private string control = ConfigManager.Control;
        private bool IsNeedReadCard = true;
        //定义全局的控制变量

        private IReadCard read;
        //定义全局的控制变量
        private SmartLifeBroker model;

        private JPForm jp;

        public FillBookForm()
        {
            InitializeComponent();
            jp = new JPForm();
            if (ConfigManager.ReadBookType == 1)
            {
                model = new SmartLifeBroker();
                model.OnScanedDataEvent += OnScanedDataEvent;
              

            }
            else
            {
                read = ObjectCreator.GetReadClass();

            }
        }

        #region 窗体扩展事件
        private void OnScanedDataEvent(object source, byte[] scanFrame)
        {
            string barCode = Encoding.ASCII.GetString(scanFrame);

            txtBarCode.Text = barCode;
            IsNeedReadCard = false;
        }


        #endregion


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        private void FillBookForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (ConfigManager.ReadBookType == 1)
                {
                    if (model.IsCanUsed(ConfigManager.Scan, cmdPort))
                    {
                        model.Init(ConfigManager.Scan, cmdPort);
                    }
                    panel1.BackgroundImage = Properties.Resources.huanshu_tm_03;
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs("书籍上架加载错误",ex.StackTrace,LogFile.Error);
                BackMain frm = new BackMain();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                this.Close();
                frm.Show();
            }
        }

        private void picSure_Click(object sender, EventArgs e)
        {
            string barCode = txtBarCode.Text;
            if (string.IsNullOrWhiteSpace(barCode))
            {
                lblErr.Text = "请输入条形码！";
            }
            else
            {
                Func<string> longTask = new Func<string>(delegate ()
                {
                    //  模拟长时间任务
                    ///  Thread.Sleep(2000);
                    return FillBook(barCode);
                    //  返回任务结果：5
                });
                //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                longTask.BeginInvoke(ar =>
                {
                    //  使用EndInvoke获取到任务结果（5）
                    string result = longTask.EndInvoke(ar);

                    //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                    //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                    if (result != null)
                    {
                        Invoke(new Action(() =>

                       endFillBook(result)

                        ));
                    }
                }, null);
                IsNeedReadCard = true;
            }
        }

        #region http请求
        private string FillBook(string barCode)
        {
            try
            {
                long timeStamp = DateTime.Now.Ticks;

                string signStr = GetSign(barCode, timeStamp);
                string urlStr = GetUrl(timeStamp, signStr, barCode);



                string backData = WebHelper.HttpWebRequest(urlStr, "", true);
                return backData;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("书籍上架请求错误", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void endFillBook(string result)
        {
            if (string.IsNullOrWhiteSpace(result))
            {
                lblErr.Text = "网络错误，请稍后再试";
                return;
            }
            var backData = result.ToJObject();
            if (backData["code"].ToString() == "100")
            {
                string doorNum = backData["data"]["machLatNum"].ToString();
                var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                LogManager.BorrowRecord("机器上书开柜，doorid={0},doorcode={1},doorNum={2}".FormatWith(list[0].DoorID, list[0].DoorCode, list[0].DoorNUM));
                lblErr.Text = "读取成功，请将书放入{0}储物格!".FormatWith(list[0].DoorNUM);
                lblErr.ForeColor = Color.Green;

            }
            else
            {
                //lblErrMsg.Text = "加入失败，请重新扫描!";
                lblErr.Text = backData["msg"].ToString();
                lblErr.ForeColor = Color.Red;
            }

            txtBarCode.Text = string.Empty;


            if (ConfigManager.ScanType == 1&&ConfigManager.ReadBookType == 1)
            {
                //打开扫描器
                model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);

            }

        }

        private string GetSign(string barCode, long timeStamp)
        {
            StringBuilder signStr = new StringBuilder();
            signStr.Append("{0}".FormatWith(barCode));
            signStr.Append("{0}".FormatWith(ConfigManager.LibraryID));
            signStr.Append("{0}".FormatWith(ConfigManager.MachineID));
            signStr.Append("{0}".FormatWith(timeStamp));
            signStr.Append("{0}".FormatWith(ConfigManager.CommonKey));

            return EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr.ToString()).ToLower());
        }

        private string GetUrl(long timeStamp, string sign, string barCode)
        {
            StringBuilder urlStr = new StringBuilder();
            urlStr.Append("{0}/public/book/syn/shelvesservice".FormatWith(ConfigManager.ServicePath));
            urlStr.Append("?bookId={0}".FormatWith(barCode));
            urlStr.Append("&timeStamp={0}".FormatWith(timeStamp));
            urlStr.Append("&machineId={0}".FormatWith(ConfigManager.MachineID));
            urlStr.Append("&sign={0}".FormatWith(sign));
            urlStr.Append("&libraryId={0}".FormatWith(ConfigManager.LibraryID));
            return urlStr.ToString();
        }


        #endregion

        private void picBack_Click(object sender, EventArgs e)
        {
            IsNeedReadCard = false;
            BackMain frm = new BackMain();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;

            this.Close();
            frm.Show();
        }

        #region 按钮样式
        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }

        private void picSure_MouseUp(object sender, MouseEventArgs e)
        {
            picSure.Image = Properties.Resources.sure1;
        }

        private void picSure_MouseDown(object sender, MouseEventArgs e)
        {
            picSure.Image = Properties.Resources.sure2;
        }

        #endregion

        private void txtBarCode_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(240, 1560);
            jp.Show();
            txtBarCode.Focus();

            this.Activate();
        }

        private void FillBookForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();
            if (ConfigManager.ReadBookType == 1)
            {
                model.Dispose();
            }
            jp.Close();

        }
        int i = 0;
        string barCode = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            if (time <= 0)
            {
                BackMain frm = new BackMain();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                this.Close();
                frm.Show();
            }



            if (IsNeedReadCard)
            {
                string str = "";
                if (ConfigManager.ReadBookType == 2)
                {
                    barCode = read.ReadRFID(ConfigManager.Pos, ConfigManager.IMPort);

                    str = barCode;
                }
                else if (ConfigManager.ReadBookType == 3)
                {
                    barCode = read.ReadUHFRFID(ConfigManager.UHF, ConfigManager.IMPort);

                    str = barCode;

                }

                if (!string.IsNullOrWhiteSpace(str))
                {
                    txtBarCode.Text = str;
                    IsNeedReadCard = false;
                }


            }
            i++;
        }

    }
}
