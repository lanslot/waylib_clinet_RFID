﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using Yunlib_Vertical.Extensions;
using YunLib_Vertical.AdminManager;

namespace YunLib_Vertical
{
    public partial class MDIForm : Form
    {
        private Socket_wrapper ms = null;
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        private string ipStr = ConfigManager.ZooKeeperIP;
        public MDIForm()
        {
            InitializeComponent();
            var ss = ipStr.Split(':');
            //ms = new Socket_wrapper(ss[0], int.Parse(ss[1]));
            // ms.onReceiveMessage += onReceiveMessage;
        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {

        }

        private void MDIForm_Load(object sender, EventArgs e)
        {
            //  Play();
            //Thread threadclient = new Thread(getSocket);
            //threadclient.IsBackground = true;
            //threadclient.Start();

            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            MainForm frm = new MainForm();
            frm.MdiParent = this;
            frm.Parent = this.palMain;
            frm.Show();
        }
        private bool IsExit = false;
        //private void getSocket()
        //{
        //    while (!IsExit)
        //    {
        //        string rDataStr = ms.socket_receive(new byte[1024]);
        //        if (!string.IsNullOrWhiteSpace(rDataStr))
        //        {
        //            string[] dataArr = rDataStr.Split('-');
        //            LogManager.WriteLogs("Socket接收信息", rDataStr, LogFile.Record);

        //            switch (dataArr[0])
        //            {
        //                case "1":
        //                    try
        //                    {
        //                        //借书成功打开柜门
        //                        HttpBorrSuccess(rDataStr, dataArr);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
        //                    }
        //                    break;
        //                case "2":
        //                    try
        //                    {
        //                        //还书成功打开柜门
        //                        //  HttpReturnSuccess(dataArr);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
        //                    }
        //                    break;
        //                case "3":
        //                    //  HttpOrderBorr(dataArr[1], dataArr[2]);
        //                    break;
        //                //预约还书
        //                case "4":
        //                    //  HttpOrderReturn(dataArr[1], dataArr[2], dataArr[3]);
        //                    break;
        //                //预约上架
        //                case "5":
        //                    //   HttpOrderShelves(dataArr[1], dataArr);
        //                    break;
        //                //取消预约借书
        //                case "7":
        //                    //   HttpCancelOrderBorr(dataArr[1]);
        //                    break;
        //                //取消预约还书
        //                case "8":
        //                    //还书失败 Zookeeper返回的状态
        //                    //  HttpCancelOrderReturn(dataArr[1]);
        //                    break;
        //                case "9":
        //                    //借书失败 Zookeeper返回的状态
        //                    LogManager.ErrorRecord("【借书】开门失败，错误信息：{0}".FormatWith(rDataStr));
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //    }

        //}

        #region 播放视频
        //private void Play()
        //{
        //    try
        //    {
        //        string Path = System.Environment.CurrentDirectory + @"\\Movie"; //音乐或视频路径
        //        axWindowsMediaPlayer1.settings.setMode("loop", true);//设置为循环播放
        //        DirectoryInfo dir = new DirectoryInfo(Path);
        //        List<string> fileName = new List<string>(); //存放文件名.
        //        IWMPMedia media;
        //        //循环文件至List列表
        //        foreach (FileInfo f in dir.GetFiles())
        //        {
        //            fileName.Add(f.Name);
        //        }


        //        foreach (string item in fileName)
        //        {
        //            string FileName = Path + "\\" + item;
        //            media = axWindowsMediaPlayer1.newMedia(FileName); //参数为歌曲路径

        //            axWindowsMediaPlayer1.currentPlaylist.appendItem(media);//文件添加到当前列表 
        //        }
        //        axWindowsMediaPlayer1.Ctlcontrols.play(); //开始播放
        //    }catch(Exception ex)
        //    {
        //        LogManager.WriteLogs("MDI播放视频出错", ex.StackTrace, LogFile.Error);
        //    }
        //}
        #endregion

        private bool isNeedInit = true;

        int count = 0;
        int count2 = 0;
        int count1 = 0;


        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("yyyy-MM-dd   hh:mm:ss");
            if(count2 == 5)
            {
                pictureBox1.Image = Properties.Resources.pic1;
            }

            if (count2 == 10)
            {
                pictureBox1.Image = Properties.Resources.pic2;
            }

            if (count2 == 15)
            {
                pictureBox1.Image = Properties.Resources.pic3;
                count2 = 0;
               
            }


            //if(count2 == 0)
            //{
            //    this.TopMost = true;
            //}

            if (count  >= 600)
            {
                panel_click_count = 0;
               
            }

            if(panel_click_count > 0)
            {
                count++;
            }
            count2++;
        }

        #region socket事件
        private void onReceiveMessage(object source, string rDataStr)
        {
            //   string barCode = Encoding.ASCII.GetString(scanFrame);

            if (!string.IsNullOrWhiteSpace(rDataStr))
            {
                string[] dataArr = rDataStr.Split('-');
                LogManager.WriteLogs("Socket接收信息", rDataStr, LogFile.Record);

                switch (dataArr[0])
                {
                    case "1":
                        try
                        {
                            //借书成功打开柜门
                            HttpBorrSuccess(rDataStr, dataArr);
                        }
                        catch (Exception ex)
                        {
                            LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                        }
                        break;
                    case "2":
                        try
                        {
                            //还书成功打开柜门
                          //  HttpReturnSuccess(dataArr);
                        }
                        catch (Exception ex)
                        {
                            LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                        }
                        break;
                    case "3":
                      //  HttpOrderBorr(dataArr[1], dataArr[2]);
                        break;
                    //预约还书
                    case "4":
                      //  HttpOrderReturn(dataArr[1], dataArr[2], dataArr[3]);
                        break;
                    //预约上架
                    case "5":
                     //   HttpOrderShelves(dataArr[1], dataArr);
                        break;
                    //取消预约借书
                    case "7":
                     //   HttpCancelOrderBorr(dataArr[1]);
                        break;
                    //取消预约还书
                    case "8":
                        //还书失败 Zookeeper返回的状态
                      //  HttpCancelOrderReturn(dataArr[1]);
                        break;
                    case "9":
                        //借书失败 Zookeeper返回的状态
                        LogManager.ErrorRecord("【借书】开门失败，错误信息：{0}".FormatWith(rDataStr));
                        break;
                    default:
                        break;
                }
            }
        }

        private void HttpBorrSuccess(string rDataStr, string[] sdata)
        {
            //传的多本书，就有逗号隔开
            if (sdata[2].Contains(","))
            {
                List<string> doorNum = new List<string>();
                sdata[2].Split(',').Each(x => doorNum.Add(x.ToString()));
                doorNum.Each(y => BorrBookOpen(rDataStr, y));
            }
            else
            {
                string doorNum = sdata[2];
                BorrBookOpen(rDataStr, doorNum);
            }
        }

        private void BorrBookOpen(string rDataStr, string doorNum)
        {
            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));

            LocalBooks model = new LocalBooks
            {
                DoorID = list[0].DoorID,
                DoorCode = list[0].DoorCode,
                IsLend = ""
            };


            //自动释放资源
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                //初始化控制台串口
                consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                ////打开相应行号的柜门
                consoleModel.OpenDoor(list[0].DoorID, list[0].DoorCode, 2);

                LogManager.BorrowRecord("【借书】成功开门，{0}{1}*****{2},返回的信息：{3}".FormatWith(list[0].DoorID, list[0].DoorCode, DateTime.Now, rDataStr));
                try
                {
                    this.BeginInvoke(new Change(Settext), doorNum);
                }
                catch (Exception ex)
                {
                    LogManager.ErrorRecord("【界面跳转失败】，{0}".FormatWith(ex.Message));
                }
            }

        }
        delegate void Change(string text);
        private void Settext(string doorNum)
        {

            try
            {
                SuccessForm frm = new SuccessForm();
                frm.Title = "借 阅 成 功";
                frm.Message = "请到" + doorNum + "号柜门取走图书，完成后请关闭柜门";
                frm.MdiParent = this; frm.Parent = palMain;

                foreach(Control item in palMain.Controls)
                {
                    if(item is Form)
                    {
                        item.Dispose();
                    }
                }
               
                frm.Show();
            }catch(Exception ex)
            {
                LogManager.WriteLogs("【界面跳转失败2】",ex.StackTrace,LogFile.Error);
            }

        }

    #endregion

    private void timer2_Tick(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;

            if (time.ToShortTimeString() == "09:00:00" && time.Day == 1)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                DirectoryInfo directory = new System.IO.DirectoryInfo(LogAddress);
                directory.Delete(true);

            }

        
            

            if ( isNeedInit)
            {
                string[] s = new string[1];
                s[0] = ConfigManager.PingServicePath;
                if (NetTest.CheckServeStatus(s))
                {
                    //  MessageBox.Show("初始化InitZookeeper");
                    bool initOk = ZooKeeperClient.createNode();
                    //   bool initOk = ms.socket_receive();
                    if (initOk)
                    {
                        isNeedInit = false;
                        //timer2.Enabled = true;
                        //timer2.Start();
                        LogManager.WriteLogs("创建节点成功！", DateTime.Now.ToString() + ":创建节点成功！", LogFile.Record);
                    }
                }
            }

            //string str = "03-" + DateTime.Now.Ticks;
            //ms.socket_send(str);
        }

        private void MDIForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsExit = true;
        }
        int panel_click_count = 0;
        private void panel1_Click(object sender, EventArgs e)
        {
            panel_click_count++;
            if (panel_click_count == 10)
            {
                kGForm frm = new kGForm();
                frm.sType = "2";
                frm.ShowDialog();

                if (frm.DialogResult == DialogResult.Cancel)
                {
                    if (kGForm.isAdmin)
                    {
                        palMain.Controls.Clear();
                        BackMain bf = new BackMain();
                        bf.MdiParent = this;
                        bf.Parent = this.palMain;
                       
                        frm.Close();
                        bf.Show();
                    }
                }
                panel_click_count = 0;
            }
        }
    }
}
