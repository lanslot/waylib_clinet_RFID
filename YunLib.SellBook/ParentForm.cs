﻿using System;
using YunLib.BLL;
using System.Data;
using System.Text;
using YunLib.Common;
using Yunlib.Common;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System.Runtime.InteropServices;
using System.Configuration;
using Yunlib.Entity;
using System.Collections.Generic;
using System.IO;
using Yunlib.SellBook.Extensions;

namespace Yunlib.SellBook
{
    
    public partial class ParentForm : Form
    {
        private const int GWL_STYLE = -16;
        private const int GWL_EXSTYLE = -20;
        private const int WS_BORDER = 0x00800000;
        private const int WS_EX_CLIENTEDGE = 0x00000200;
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_NOOWNERZORDER = 0x0200;


        // Win32 方法    
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetWindowLong(IntPtr hWnd, int Index);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SetWindowLong(IntPtr hWnd, int Index, int Value);

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter,
            int X, int Y, int cx, int cy, uint uFlags);
        
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        public ParentForm()
        {
            InitializeComponent();
        }

        public static int IsInitOpen = 0;
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        
        public void ParentFrom_Load(object sender, EventArgs e)
        {
            try
            {
                //去调mdi边框
                for (int i = 0; i < this.Controls.Count; i++)
                {
                    var mdiClientForm = this.Controls[i] as MdiClient;
                    if (mdiClientForm == null) continue;
                    // 找到了mdi客户区    
                    // 取得客户区的边框    
                    int style = GetWindowLong(mdiClientForm.Handle, GWL_STYLE);
                    int exStyle = GetWindowLong(mdiClientForm.Handle, GWL_EXSTYLE);
                    style &= ~WS_BORDER;
                    exStyle &= ~WS_EX_CLIENTEDGE;

                    // 调用win32设定样式    
                    SetWindowLong(mdiClientForm.Handle, GWL_STYLE, style);
                    SetWindowLong(mdiClientForm.Handle, GWL_EXSTYLE, exStyle);

                    // 更新客户区    
                    SetWindowPos(mdiClientForm.Handle, IntPtr.Zero, 0, 0, 0, 0,
                        SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                        SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
                    UpdateStyles();
                    break;
                }

                DateTime start = DateTime.Now;
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;




                Main mainFrm = new Main();
                mainFrm.MdiParent = this;
                mainFrm.Show();
                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("ParentForm_Load 运行时间{0}".FormatWith(ts.TotalSeconds));

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                DialogResult dr = MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
               
                if (dr == DialogResult.OK)
                {
                    //通过进程名关闭进程
                    CodeOptimize.KillProcess("Yunlib");
                }
                else
                {
                    CodeOptimize.KillProcess("RsYunlib");
                    CodeOptimize.KillProcess("Yunlib");
                }

            }
        }

        
        int i = 0;
        public static int isOpenLight = 0;
        public static bool isNeedInit = true;
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string[] s = new string[1];
            s[0] = ConfigManager.PingServicePath;

            bool flag = NetTest.CheckServeStatus(s);
            if (dt.Hour == 1 && dt.Minute == 0 && dt.Second == 0)
            {
                Application.Exit();
            }
             
            if (flag && isNeedInit)
            {
                //  MessageBox.Show("初始化InitZookeeper");
                bool initOk = ZooKeeperClient.createNode();
             //   bool initOk = ms.socket_receive();
                if (initOk)
                {
                    isNeedInit = false;
                    //timer2.Enabled = true;
                    //timer2.Start();
                    LogManager.WriteLogs("创建节点成功！", DateTime.Now.ToString() + ":创建节点成功！", LogFile.Record);
                }
            }

            i++;
            if (i >= 10)
            {
               // ClearMemory();
                i = 0;
            }
        }

        #region 内存回收
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize")]
        public static extern int SetProcessWorkingSetSize(IntPtr process, int minSize, int maxSize);
        /// <summary>
        /// 释放内存
        /// </summary>
        public static void ClearMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
        }
        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;

            if(time.ToShortTimeString() == "09:00:00" && time.Day == 1) {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                DirectoryInfo directory = new System.IO.DirectoryInfo(LogAddress);
                directory.Delete(true);

            }
            

        } 

    
       

     

       

      

        #region 扩展
    
        

     

     
        #endregion

    }
}
