﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using YunLib.Common;

namespace Yunlib.Extensions
{
    public class DAMControl
    {
        private static SerialPort comm = new SerialPort();
        int errrcvcnt = 0;




        public bool OpenDO(int io)
        {

            byte[] info = CModbusDll.WriteDO(Convert.ToInt16("254"), io - 1, true);
            byte[] rst = sendinfo(info);
            if (rst == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CloseDO(int io)
        {

            byte[] info = CModbusDll.WriteDO(Convert.ToInt16("254"), io - 1, false);
            byte[] rst = sendinfo(info);
            if (rst == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        private byte[] sendinfo(byte[] info)
        {
            if (comm == null)
            {
                comm = new SerialPort();
                // return null;
            }

            if (comm.IsOpen == false)
            {
                OpenSerialPort();
                // return null;
            }
            try
            {
                byte[] data = new byte[2048];
                int len = 0;

                comm.Write(info, 0, info.Length);
                //  DebugInfo("发送", info, info.Length);

                try
                {
                    Thread.Sleep(50);
                    Stream ns = comm.BaseStream;
                    ns.ReadTimeout = 50;
                    len = ns.Read(data, 0, 2048);

                    // DebugInfo("接收", data, len);
                }
                catch (Exception)
                {
                    return null;
                }
                errrcvcnt = 0;
                return analysisRcv(data, len);
            }
            catch (Exception)
            {

            }
            return null;
        }

        private byte[] analysisRcv(byte[] src, int len)
        {
            if (len < 6) return null;
            if (src[0] != Convert.ToInt16("254")) return null;

            switch (src[1])
            {
                case 0x01:
                    if (CMBRTU.CalculateCrc(src, src[2] + 5) == 0x00)
                    {
                        byte[] dst = new byte[src[2]];
                        for (int i = 0; i < src[2]; i++)
                            dst[i] = src[3 + i];
                        return dst;
                    }
                    break;
                case 0x02:
                    if (CMBRTU.CalculateCrc(src, src[2] + 5) == 0x00)
                    {
                        byte[] dst = new byte[src[2]];
                        for (int i = 0; i < src[2]; i++)
                            dst[i] = src[3 + i];
                        return dst;
                    }
                    break;
                case 0x04:
                    if (CMBRTU.CalculateCrc(src, src[2] + 5) == 0x00)
                    {
                        byte[] dst = new byte[src[2]];
                        for (int i = 0; i < src[2]; i++)
                            dst[i] = src[3 + i];
                        return dst;
                    }
                    break;
                case 0x05:
                    if (CMBRTU.CalculateCrc(src, 8) == 0x00)
                    {
                        byte[] dst = new byte[1];
                        dst[0] = src[4];
                        return dst;
                    }
                    break;
                case 0x0f:
                    if (CMBRTU.CalculateCrc(src, 8) == 0x00)
                    {
                        byte[] dst = new byte[1];
                        dst[0] = 1;
                        return dst;
                    }
                    break;
            }
            return null;
        }


        /// <summary>
        /// 打开串口
        /// </summary>
        public bool OpenSerialPort()
        {

            //关闭时点击，则设置好端口，波特率后打开
            try
            {
                comm.PortName = ConfigurationManager.AppSettings["Bookcase"]; ; //串口名 COM1
                comm.BaudRate = 9600; //波特率  9600
                comm.DataBits = 8; // 数据位 8
                comm.ReadBufferSize = 4096;
                comm.StopBits = StopBits.One;
                comm.Parity = Parity.None;
                comm.Open();
            }
            catch (Exception ex)
            {

                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                //现实异常信息给客户。
                LogManager.ErrorRecord("初始化端口报错:" + ex.Message);
                return false;
            }
            return true;
        }
    }
}
