﻿namespace Yunlib
{
    partial class RegisterFailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pic_btnTurnBack = new System.Windows.Forms.PictureBox();
            this.lblSecond = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOnceBorrowErrMsg = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pic_btnTurnBack
            // 
            this.pic_btnTurnBack.BackColor = System.Drawing.Color.Transparent;
            this.pic_btnTurnBack.Image = global::Yunlib.Properties.Resources.btnX;
            this.pic_btnTurnBack.Location = new System.Drawing.Point(461, 671);
            this.pic_btnTurnBack.Name = "pic_btnTurnBack";
            this.pic_btnTurnBack.Size = new System.Drawing.Size(62, 63);
            this.pic_btnTurnBack.TabIndex = 21;
            this.pic_btnTurnBack.TabStop = false;
            this.pic_btnTurnBack.Click += new System.EventHandler(this.pic_btnTurnBack_Click);
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = true;
            this.lblSecond.BackColor = System.Drawing.Color.White;
            this.lblSecond.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblSecond.ForeColor = System.Drawing.Color.Green;
            this.lblSecond.Location = new System.Drawing.Point(499, 599);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(22, 17);
            this.lblSecond.TabIndex = 19;
            this.lblSecond.Text = "10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(458, 599);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "倒计时      秒";
            // 
            // lblOnceBorrowErrMsg
            // 
            this.lblOnceBorrowErrMsg.AutoSize = true;
            this.lblOnceBorrowErrMsg.BackColor = System.Drawing.Color.White;
            this.lblOnceBorrowErrMsg.Font = new System.Drawing.Font("微软雅黑", 23F);
            this.lblOnceBorrowErrMsg.ForeColor = System.Drawing.Color.Red;
            this.lblOnceBorrowErrMsg.Location = new System.Drawing.Point(317, 423);
            this.lblOnceBorrowErrMsg.Name = "lblOnceBorrowErrMsg";
            this.lblOnceBorrowErrMsg.Size = new System.Drawing.Size(0, 40);
            this.lblOnceBorrowErrMsg.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Yunlib.Properties.Resources.fail;
            this.pictureBox1.Location = new System.Drawing.Point(194, 184);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(618, 471);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Location = new System.Drawing.Point(620, 46);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(26, 31);
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(652, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 57);
            this.panel1.TabIndex = 23;
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // RegisterFailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pic_btnTurnBack);
            this.Controls.Add(this.lblSecond);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblOnceBorrowErrMsg);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RegisterFailForm";
            this.Text = "RegisterFailForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RegisterFailForm_FormClosed);
            this.Load += new System.EventHandler(this.RegisterFailForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pic_btnTurnBack;
        private System.Windows.Forms.Label lblSecond;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOnceBorrowErrMsg;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDataTimeNow;
    }
}