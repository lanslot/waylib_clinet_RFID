﻿namespace YunLib_Vertical
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.picBorrow = new System.Windows.Forms.PictureBox();
            this.picReturn = new System.Windows.Forms.PictureBox();
            this.picOpenCode = new System.Windows.Forms.PictureBox();
            this.picRegister = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegister)).BeginInit();
            this.SuspendLayout();
            // 
            // picBorrow
            // 
            this.picBorrow.BackColor = System.Drawing.Color.Transparent;
            this.picBorrow.Image = global::YunLib_Vertical.Properties.Resources.jie_12_03;
            this.picBorrow.Location = new System.Drawing.Point(200, 250);
            this.picBorrow.Margin = new System.Windows.Forms.Padding(0);
            this.picBorrow.Name = "picBorrow";
            this.picBorrow.Size = new System.Drawing.Size(223, 294);
            this.picBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBorrow.TabIndex = 0;
            this.picBorrow.TabStop = false;
            this.picBorrow.Click += new System.EventHandler(this.picBorrow_Click);
            this.picBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBorrow_MouseDown);
            this.picBorrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picBorrow_MouseUp);
            // 
            // picReturn
            // 
            this.picReturn.BackColor = System.Drawing.Color.Transparent;
            this.picReturn.Image = ((System.Drawing.Image)(resources.GetObject("picReturn.Image")));
            this.picReturn.Location = new System.Drawing.Point(645, 250);
            this.picReturn.Margin = new System.Windows.Forms.Padding(0);
            this.picReturn.Name = "picReturn";
            this.picReturn.Size = new System.Drawing.Size(228, 290);
            this.picReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picReturn.TabIndex = 1;
            this.picReturn.TabStop = false;
            this.picReturn.Click += new System.EventHandler(this.picReturn_Click);
            this.picReturn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picReturn_MouseDown);
            this.picReturn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picReturn_MouseUp);
            // 
            // picOpenCode
            // 
            this.picOpenCode.BackColor = System.Drawing.Color.Transparent;
            this.picOpenCode.Image = ((System.Drawing.Image)(resources.GetObject("picOpenCode.Image")));
            this.picOpenCode.Location = new System.Drawing.Point(78, 900);
            this.picOpenCode.Margin = new System.Windows.Forms.Padding(0);
            this.picOpenCode.Name = "picOpenCode";
            this.picOpenCode.Size = new System.Drawing.Size(137, 158);
            this.picOpenCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picOpenCode.TabIndex = 2;
            this.picOpenCode.TabStop = false;
            this.picOpenCode.Click += new System.EventHandler(this.picOpenCode_Click);
            this.picOpenCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpenCode_MouseDown);
            this.picOpenCode.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picOpenCode_MouseUp);
            // 
            // picRegister
            // 
            this.picRegister.BackColor = System.Drawing.Color.Transparent;
            this.picRegister.Image = ((System.Drawing.Image)(resources.GetObject("picRegister.Image")));
            this.picRegister.Location = new System.Drawing.Point(879, 900);
            this.picRegister.Margin = new System.Windows.Forms.Padding(0);
            this.picRegister.Name = "picRegister";
            this.picRegister.Size = new System.Drawing.Size(125, 157);
            this.picRegister.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picRegister.TabIndex = 3;
            this.picRegister.TabStop = false;
            this.picRegister.Click += new System.EventHandler(this.picRegister_Click);
            this.picRegister.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picRegister_MouseDown);
            this.picRegister.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picRegister_MouseUp);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(879, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(333, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(459, 62);
            this.label1.TabIndex = 6;
            this.label1.Text = "系统维护，暂停使用";
            this.label1.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.di;
            this.ClientSize = new System.Drawing.Size(1080, 1100);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picRegister);
            this.Controls.Add(this.picOpenCode);
            this.Controls.Add(this.picReturn);
            this.Controls.Add(this.picBorrow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBorrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegister)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBorrow;
        private System.Windows.Forms.PictureBox picReturn;
        private System.Windows.Forms.PictureBox picOpenCode;
        private System.Windows.Forms.PictureBox picRegister;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
    }
}