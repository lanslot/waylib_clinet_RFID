﻿using System.Collections.Generic;
using Yunlib.Entity;

namespace Yunlib.SellBook.Extensions
{
    public class Cart
    {
        /// <summary>
        /// 购物车中的书籍编号数组
        /// </summary>
        public static string BooksNumArr { get; set; }

        /// <summary>
        /// 是否可以加入购物车
        /// </summary>
        public static bool IsCanAddCart { get; set; }

        /// <summary>
        /// 书的信息（key：条形码  value：书柜号）
        /// </summary>
        public static Dictionary<string, string> BooksInfo { get; set; }

        /// <summary>
        /// 购物车中的
        /// </summary>
        public static List<LocalBooks> localList { get; set; }
    }
}
