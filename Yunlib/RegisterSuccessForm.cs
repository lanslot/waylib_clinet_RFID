﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib
{
    public partial class RegisterSuccessForm : Form
    {
        public RegisterSuccessForm()
        {
            InitializeComponent();
        }

      

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDataTimeNow.Text = DateTime.Now.ToString();
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";
            if (int.Parse(lblSecond.Text) < 0)
            {
                timer1.Enabled = false;

                //返回1Main加载时不执行开灯操作
                // //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;

                //返回主界面
                //Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void pic_btnTurnBack_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();

            //返回主界面
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void RegisterSuccessForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void RegisterSuccessForm_Load(object sender, EventArgs e)
        {

        }
    }
}
