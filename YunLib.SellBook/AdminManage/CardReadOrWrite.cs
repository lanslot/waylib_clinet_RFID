﻿using ContorTest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.SellBook.Extensions;
using YunLib.Common;

namespace Yunlib.SellBook.AdminManage
{
    public partial class CardReadOrWrite : Form
    {
        public int icdev;   //communication equipment remark
        public short st;    //function return data
        private int accountCount = ConfigManager.AccountCount;
        private HDReadCard hdc = new HDReadCard();
        public CardReadOrWrite()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            string msg = hdc.ReadCardIM(ConfigManager.Pos, ConfigManager.IMPort);
            if (string.IsNullOrWhiteSpace(msg))
            {
                listInfo.Items.Add("读卡失败!");
            }
            else
            {
                listInfo.Items.Add("读卡成功："+ msg);
            }


        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            string msg = hdc.WriteCardIM(ConfigManager.Pos, ConfigManager.IMPort,textBox1.Text);
            if (string.IsNullOrWhiteSpace(msg))
            {
                listInfo.Items.Add("写卡失败!");
            }
            else
            {
                listInfo.Items.Add("写卡成功：" + msg);
                ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "AccountCount", msg.Length.ToString());
                ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "BooksManager", msg);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listInfo.Items.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Close();

            frm.Show();
        }

        private void CardReadOrWrite_Load(object sender, EventArgs e)
        {
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            textBox1.Text = ConfigManager.BooksManager;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
    }
}
