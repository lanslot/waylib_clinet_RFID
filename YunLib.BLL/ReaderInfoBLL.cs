﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;

namespace YunLib.BLL
{
    public class ReaderInfoBLL
    {
        ReaderInfoDAL DAL = new ReaderInfoDAL();

        public string GetUserNo(string rdid)
        {
            return DAL.GetUserNo(rdid);
        }
       
    }
}
