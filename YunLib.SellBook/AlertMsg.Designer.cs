﻿namespace Yunlib.SellBook
{
    partial class AlertMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAlertMsg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblAlertMsg
            // 
            this.lblAlertMsg.AutoSize = true;
            this.lblAlertMsg.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.lblAlertMsg.ForeColor = System.Drawing.Color.White;
            this.lblAlertMsg.Location = new System.Drawing.Point(25, 52);
            this.lblAlertMsg.Name = "lblAlertMsg";
            this.lblAlertMsg.Size = new System.Drawing.Size(0, 25);
            this.lblAlertMsg.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gray;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 23F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(242, -8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 42);
            this.label1.TabIndex = 6;
            this.label1.Text = "×\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // AlertMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(279, 129);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAlertMsg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlertMsg";
            this.Text = "AlertMsg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAlertMsg;
        private System.Windows.Forms.Label label1;
    }
}