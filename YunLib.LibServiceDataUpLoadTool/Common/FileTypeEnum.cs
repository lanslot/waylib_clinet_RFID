﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.LibServiceDataUpLoadTool.Common
{
    public enum FileTypeEnum
    {
        /// <summary>
        /// 流通数据
        /// </summary>
        [Description("FACT_LOAN")]
        FACT_LOAN = 0,
        /// <summary>
        /// 读者数据
        /// </summary>
        [Description("READER")]
        READER = 1,
        /// <summary>
        /// 馆藏数据
        /// </summary>
        [Description("HOLDING")]
        HOLDING = 2,
        /// <summary>
        /// 书目信息
        /// </summary>
        [Description("BIBLIOS")]
        BIBLIOS = 3,
        /// <summary>
        /// 分馆信息
        /// </summary>
        [Description("P_LIBCODE")]
        P_LIBCODE = 4,
        /// <summary>
        /// 馆藏地点
        /// </summary>
        [Description("P_LOCAL")]
        P_LOCAL = 5,
    }
}
