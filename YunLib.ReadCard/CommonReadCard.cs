﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.Common;
using YunLib.ReadCard.HF;

namespace YunLib.ReadCard
{
    /// <summary>
    /// 公共读卡类
    /// </summary>
    public class CommonReadCard : IReadCard
    {


        public string ReadIDCard(string port, int Baud)
        {
            //该函数封装了两种不同协议的身份证操作，根据情况选择
            //HD100读身份证
            StringBuilder pBmpFile = new StringBuilder(100);
            StringBuilder pFingerData = new StringBuilder(1025);
            StringBuilder pBmpData = new StringBuilder(77725);
            StringBuilder pBase64Data = new StringBuilder(6025);
            StringBuilder pName = new StringBuilder(100);
            StringBuilder pSex = new StringBuilder(100);
            StringBuilder pNation = new StringBuilder(100);
            StringBuilder pBirth = new StringBuilder(100);
            StringBuilder pAddress = new StringBuilder(400);
            StringBuilder pCertNo = new StringBuilder(100);
            StringBuilder pDepartment = new StringBuilder(100);
            StringBuilder pEffectData = new StringBuilder(100);
            StringBuilder pExpire = new StringBuilder(100);
            StringBuilder pData = new StringBuilder(100);
            StringBuilder pErrMsg = new StringBuilder(100);
            StringBuilder strUID = new StringBuilder(100);
            byte[] uid = new byte[20];

            string str = System.Environment.CurrentDirectory;
            pBmpFile.Append(str);
            pBmpFile.Append(@"\zp.bmp");


            int Rhandle;
            int ret;

            //  String a = textBox1.Text;
            int p = int.Parse(port.Substring(3));

            Rhandle = SSSE32.ICC_Reader_Open(p, Baud);
            if (Rhandle < 0)
            {
                // //listInfo.Items.Add("连接失败!");
                LogManager.ErrorRecord("身份证读卡连接失败,端口：" + port);
                SSSE32.ICC_Reader_Close(Rhandle);
                return "";
            }
            else
            {
                // //listInfo.Items.Add("连接成功!:" + Rhandle);
            }
            //该函数获取身份证信息的同时保存照片到指定路径
            ret = SSSE32.PICC_Reader_ReadIDMsg(Rhandle, pBmpFile, pName, pSex, pNation, pBirth, pAddress, pCertNo, pDepartment, pEffectData, pExpire, pErrMsg);
            //  ret = PICC_Reader_ID_ReadUID(Rhandle, uid);
            SSSE32.ICC_Reader_Close(Rhandle);
            if (ret != 0)
            {
                //读卡失败
                SSSE32.ICC_Reader_Close(Rhandle);
                // //listInfo.Items.Add("读卡失败!");
                LogManager.ErrorRecord("身份证读卡失败");
                return "";
            }
            else
            {
                ////listInfo.Items.Add("姓名：" + pName);
                ////listInfo.Items.Add("性别：" + pSex);
                ////listInfo.Items.Add("民族：" + pNation);
                ////listInfo.Items.Add("身份证号：" + pCertNo);
                ////listInfo.Items.Add("地址：" + pAddress);

                return pCertNo.ToString();
            }
            //  HexToStr(uid, 8, strUID);
            // ICC_Reader_Close(Rhandle);
        }


        public string ReadIM(string port, int Baud)
        {
            try
            {
                //M1卡操作示例
                int Rhandle;
                int ret;
                int p = int.Parse(port.Substring(3));

                Rhandle = SSSE32.ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    //listInfo.Items.Add("M1连接失败!");

                    return "";
                }
                else
                {
                    //listInfo.Items.Add("M1连接成功!:" + Rhandle);
                }
                // ret = ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用



                byte mode = 0x60;//认证KeyA时为0x60，认证KeyB时为0x61，此处测试用了KeyA
                byte secNr = 0x0c;// 扇区号0~15, 此处测试认证的为1扇区
                byte[] password = new byte[7];//1扇区的秘钥，此处为默认12个 f
                password[0] = 0xff;
                password[1] = 0xff;
                password[2] = 0xff;
                password[3] = 0xff;
                password[4] = 0xff;
                password[5] = 0xff;
                //   password[6] = 0x00;
                ret = SSSE32.PICC_Reader_Authentication_Pass(Rhandle, mode, 0, password);
                //  ret = PICC_Reader_Authentication_Pass(Rhandle, mode, secNr, password);
                if (ret != 0)
                {
                    //秘钥认证失败
                    //listInfo.Items.Add("秘钥认证失败:" + ret);
                    SSSE32.ICC_Reader_Close(Rhandle);
                    return "";
                }
                byte[] hexData = new byte[17];
                //48h是块号（范围0~63），因为秘钥认证的是1扇区所以只能读取1扇区的块，4~7块
                ret = SSSE32.PICC_Reader_Read(Rhandle, 0, hexData);
                //   ret = PICC_Reader_Read(Rhandle, Addr, hexData);
                byte[] RecData = new byte[34];
                if (ret != 0)
                {
                    //读卡失败了
                    //listInfo.Items.Add("读卡失败了");
                    SSSE32.ICC_Reader_Close(Rhandle);
                    return "";
                }
                //else
                //{
                //    //listInfo.Items.Add("读卡成功了lengh:" + hexData.Length);

                //    HexToStr(hexData, 17, RecData);
                //    //listInfo.Items.Add("读卡成功了:" + Encoding.Default.GetString(RecData));
                //}

                SSSE32.ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                SSSE32.ICC_Reader_Close(Rhandle);

                var data = hexData.Take(4).ToArray();
                string str = BitConverter.ToString(data);

                var ss = str.Split('-');
                string str1 = "";
                for (int i = ss.Length - 1; i >= 0; i--)
                {
                    str1 += ss[i];
                }
                long num = long.Parse(str1, System.Globalization.NumberStyles.HexNumber);
                string newStr = num.ToString();
                string sStr = "";
                if (newStr.Length < 10)
                {
                    for (int j = 0; j < 10 - newStr.Length; j++)
                    {
                        sStr += "0";
                    }
                }

                return sStr + newStr;
            }
            catch (Exception ex)
            {
                //listInfo.Items.Add(ex.Message);

                LogManager.ErrorRecord("{0}-读IM卡出现错误：{1}".FormatWith(DateTime.Now, ex.Message));
                return "";
            }
        }

        public string ReadRFID(string port, int Baud)
        {
            try
            {
                //15693卡操作示例
                int Rhandle;
                int ret;

                //连接读卡器
                int p = int.Parse(port.Substring(3));

                Rhandle = SSSE32.ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    // listInfo.Items.Add("连接失败!");

                    return "";
                }
                //else
                //{
                //    //listInfo.Items.Add("连接成功!:" + Rhandle);
                //}
                byte[] cardId = new byte[8];
                ret = SSSE32.PICC_Reader_Inventory(Rhandle, cardId);

                if (ret <= 0)
                {
                    // listInfo.Items.Add("寻卡失败!ret=" + ret);
                    SSSE32.ICC_Reader_Close(Rhandle);
                    return "";
                }
                //else
                //{
                //    listInfo.Items.Add("寻卡成功!ret=" + ret + "**卡号：" + BitConverter.ToString(cardId));
                //}
                byte[] hexData = new byte[4];
                byte[] rec = new byte[40];
                int len = 0;
                for (int i = 0; i <= 7; i++)
                {
                    ret = SSSE32.PICC_Reader_15693_Read(Rhandle, (byte)i, hexData);
                    if (ret <= 0)
                    {
                        //  listInfo.Items.Add("读卡" + i + "块失败!ret=" + ret);
                        SSSE32.ICC_Reader_Close(Rhandle);
                        return "";
                    }
                    else
                    {
                        // listInfo.Items.Add("读卡" + i + "块成功!ret:" + ret + "***数据：" + BitConverter.ToString(hexData));
                        //读标签成功处理数据
                        hexData.CopyTo(rec, len);
                        len += 4;
                    }
                }
                //byte[] data = new byte[2];
                //byte[] data1 = new byte[4];

                //data[0] = 0;
                //data[1] = 0;
                //SSSE32.PICC_Reader_API(Rhandle, data, data1);


                SSSE32.ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                SSSE32.ICC_Reader_Close(Rhandle);

                int strLen = (int)rec[2];


                string buffStr = BitConverter.ToString(rec).Replace("-", string.Empty);
                LogManager.WriteLogs("str", buffStr, LogFile.Record);
                int startIndex = 0;
                int oid = 0;
                Dictionary<int, string> dic = new Dictionary<int, string>();

                while (startIndex < buffStr.Length)
                {
                    oid = 0;
                    string message = getMessage(buffStr, ref startIndex, ref oid);
                    LogManager.WriteLogs("" + oid, message, LogFile.Record);
                    if (message == "0")
                    {
                        break;
                    }
                    else
                    {
                        if (!dic.ContainsKey(oid))
                        {
                            dic.Add(oid, message);
                        }
                        else
                        {
                            dic[oid] = message;
                        }
                    }
                }

                string barCode = EncryptForBit5.DecryptToBit5(dic[1]);


                return barCode;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        /// 
        /// 数字转字母
        /// 
        /// 要转换成字母的数字（数字范围在闭区间[65,90]）
        /// 
        private string NunToChar(int number)
        {
            if (65 <= number && 90 >= number)
            {
                System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
                byte[] btNumber = new byte[] { (byte)number };
                return asciiEncoding.GetString(btNumber);
            }
            return "数字不在转换范围内";
        }

        /// <summary>
        /// 拆分数据段
        /// </summary>
        /// <param name="bufferStr">数据断</param>
        /// <param name="startIndex">开始位置</param>
        /// <param name="oid">数据段索引</param>
        /// <returns></returns>
        private string getMessage(string bufferStr, ref int startIndex, ref int oid)
        {
            try
            {
                //获取前导字符串
                string head = bufferStr.Substring(startIndex, 2);
                //16进制字符串转换为10进制
                int headInt = int.Parse(head, System.Globalization.NumberStyles.HexNumber);
                //10进制转换为2进制字符串
                string headBinary = Convert.ToString(headInt, 2);
                string str = "00000000";
                //如果字符串不够八位数则补齐8位
                if (headBinary.Length < 8)
                {
                    headBinary = str.Substring(0, 8 - headBinary.Length) + headBinary;
                }

                //获取本数据断数据类型
                oid = Convert.ToInt32(headBinary.Substring(4), 2);
                string qdf = headBinary.Substring(1, 3);
                string lengthStr = "";
                string message = "";
                int lenth = 0;
                int cover = 0;
                if (headBinary.Substring(0, 1) == "1")//判断是否有补位符
                {
                    //截取当前数据断的数据长度
                    lengthStr = bufferStr.Substring(startIndex + 4, 2);

                    lenth = int.Parse(lengthStr, System.Globalization.NumberStyles.HexNumber);

                    cover = int.Parse(bufferStr.Substring(startIndex + 2, 2), System.Globalization.NumberStyles.HexNumber);

                    message = bufferStr.Substring(startIndex + 6, lenth * 2);

                    startIndex += 6 + lenth * 2 + cover * 2;
                }
                else
                {
                    lengthStr = bufferStr.Substring(startIndex + 2, 2);

                    lenth = int.Parse(lengthStr, System.Globalization.NumberStyles.HexNumber);

                    message = bufferStr.Substring(startIndex + 4, lenth * 2);

                    startIndex += 4 + lenth * 2;
                }
                string backStr = "";
                if (qdf == "001")
                {
                    backStr = DataCompression.DecryptToInteger(message);
                }
                else if (qdf == "010")
                {
                    backStr = DataCompression.DecryptToNum(message);
                }
                else if (qdf == "011")
                {
                    backStr = DataCompression.DecryptTo5BIT(message);
                }
                else if (qdf == "100")
                {
                    backStr = DataCompression.DecryptTo6BIT(message);
                }
                else if (qdf == "101")
                {
                    backStr = DataCompression.DecryptTo7BIT(message);
                }
                else if (qdf == "110")
                {
                    backStr = DataCompression.DecryptTo8BIT(message);
                }
                else
                {
                    backStr = message;
                }

                return backStr;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("获取数据段出错", ex.StackTrace, LogFile.Error);
                return "0";
            }
        }



        public string ReadUHFRFID(string port, int Baud)
        {
            throw new NotImplementedException();
        }
    }
}
