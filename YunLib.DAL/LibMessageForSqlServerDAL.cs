﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.Common;

namespace YunLib.DAL
{
    public class LibMessageForSqlServerDAL
    {
        /// <summary>
        /// 获取流通数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_FACT_LOAN(DateTime start, DateTime end)
        {
            string sql = @"select CASE a.操作类型 WHEN 'J' THEN '30001' WHEN 'H' THEN '30002' ELSE '30003' END AS 日志类型代码,a.读者条码,a.操作员,b.主键码,a.条形码,CASE a.操作类型 WHEN 'H' THEN '' else 处理日期 end as 文献借阅日期,CASE a.操作类型 WHEN 'H' THEN 处理日期 else '' end as 文献归还日期,c.部门名称,'TSG' as 馆代码,a.处理时间,'0' as 是否读者证,'1' as 数据项1  from 流通日志 a inner join 馆藏典藏库 b on a.条形码 = b.条形码 inner join 馆藏地址定义 c on b.馆键码 = c.馆键码 where a.处理时间 >'{0}' and a.处理时间 < '{1}'".FormatWith(start, end);

            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取馆藏数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_HOLDING(DateTime start, DateTime end)
        {
            string sql = @"select a.主键码,a.条形码,b.索书号,a.处理日期 as 入藏时间,'' as 流通时间,'TSG' as 文献所属馆代码,a.馆藏地址 as 文献所属馆藏地址,'TSG' as 文献所在馆代码,a.馆藏地址 as 文献所在馆藏地址,ISNULL(a.实洋,'') as 单价 from 馆藏典藏库 a inner join 馆藏书目库 b on a.主键码 = b.主键码 where a.处理日期 > '{0}' and a.处理日期<'{1}'".FormatWith(start, end);
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;

        }

        /// <summary>
        /// 获取读者数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_READER(DateTime start, DateTime end)
        {
            string sql = @"SELECT 读者条码,姓名, CASE 性别 WHEN '女' THEN '0' WHEN '男' THEN '1' ELSE '2' END AS sex,isnull(LEFT(身份证号,6),'') as idCard,发证日期,发证日期,ISNULL(SUBSTRING(身份证号,7,8),'') as 出生日期,'1' as 操作员,CASE ISNULL(挂失注销,'有效') WHEN '有效' THEN '1' WHEN '挂失' THEN '3' WHEN '注销' THEN '4' WHEN '暂停' THEN '5' ELSE '6' END AS dzstatus FROM 读者库 WHERE 发证日期 >'{0}' AND 发证日期 < '{1}'".FormatWith(start, end);
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取书目信息
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_BIBLIOS(DateTime start, DateTime end)
        {
            string sql = @"select 主键码,题名,isnull(责任者,''),isnull(标准编码,''),isnull(出版者,''),isnull(出版日期,''),'' as 定价,'1' as 书目类型,处理日期 from 馆藏书目库 where 处理日期 > '{0}' and  处理日期 < '{1}' and 题名 is not null".FormatWith(start, end);
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取分馆信息
        /// </summary>
        /// <returns></returns>
        public DataTable Get_P_LIBCODE()
        {

            string sql = @"select 单位代码,单位名,单位名 from 系统单位定义";
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取馆藏地点
        /// </summary>
        /// <returns></returns>
        public DataTable Get_P_LOCAL()
        {
            string sql = @"select 馆藏地址,'TSG' AS 分馆代码,部门名称 from 馆藏地址定义";
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

    }
}
