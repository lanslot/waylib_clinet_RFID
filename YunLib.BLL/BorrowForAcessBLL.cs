﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;
using Yunlib.Entity;

namespace YunLib.BLL
{
    public class BorrowForAcessBLL
    {
        BorrowForAcess DAL = new BorrowForAcess();

        public DataTable GetBookToDataTable()
        {
            return DAL.GetBookToDataTable();
        }

        public DataTable GetBookByDoorID(int doorId)
        {
            return DAL.GetBookByDoorID(doorId);
        }

        public IList<LocalBooks> GetBookByDoorNum(int doorNum)
        {
            return DAL.GetBookByDoorNum(doorNum);
        }

        public IList<LocalBooks> GetNullBookCase()
        {
            return DAL.GetNullBookCase();
        }

        public bool FillBookToAcess(LocalBooks lb)
        {
            return DAL.FillBookToAcess(lb);
        }
        

        public LocalBooks GetBookByBarCode(string barCode)
        {
            return DAL.GetBookByBarCode(barCode);
        }

        public DataTable GetBookLikeBarCode(string barCode)
        {
            return DAL.GetBookLikeBarCode(barCode);
        }

        public int GetTotalCount()
        {
            return DAL.GetTotalCount();
        }

        public DataTable GetBookToDataTableByPaging(int pageIndex, int pageSize)
        {
            return DAL.GetBookToDataTableByPaging(pageIndex, pageSize);
        }

        public DataTable GetOutTimeBooks()
        {
            return DAL.GetOutTimeBooks();
        }

        public DataTable GetMachineInitData()
        {
            return DAL.GetMachineInitData();
        }

        public bool DeleteBookByID(LocalBooks lb) {
            return DAL.DeleteBookByID(lb);
        }


        public bool UpdateLocalBookbyCode(string barCode, string isLend)
        {
            return DAL.UpdateLocalBookbyCode(barCode, isLend);
        }

        public bool UpdateBooksByid(int ID)
        {
            return DAL.UpdateBooksByid(ID);
        }

        public bool UpdateLocalBookbydoornum(string barTime, int doorNum)
        {
            return DAL.UpdateLocalBookbydoornum(barTime, doorNum);
        }
    }
}
