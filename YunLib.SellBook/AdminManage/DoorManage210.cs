﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Yunlib.Entity;
using YunLib.BLL;
using YunLib.Common;

namespace Yunlib.SellBook.AdminManage
{
    public partial class DoorManage210 : Form
    {
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();

        public DoorManage210()
        {
            InitializeComponent();
        }

        Label label = null;

        List<string> doorNum = new List<string>();
        private void DoorManage210_Load(object sender, EventArgs e)
        {
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            for (int i = 1; i <= 6; i++)
            {
                //i是行号
                for (int j = 0; j <= 39; j++)
                {
                    //j是柜门号
                    label = new Label();
                    label.AutoSize = false;
                    label.TextAlign = ContentAlignment.MiddleCenter;
                    label.Size = new Size(50, 70);
                    label.Font = new Font("微软雅黑", 15);
                    label.BorderStyle = BorderStyle.FixedSingle;
                    if (j < 20)
                    {
                        label.Location = new Point((j % 20) * 50, (i * 2 - 1) * 70);
                    }
                    else
                    {
                        label.Location = new Point((j % 20) * 50, (i * 2) * 70);
                    }

                    string rowNum = i.ToString();
                    string colNum = (j + 1).ToString();
                    if (colNum.Length < 2)
                    {
                        colNum = "0" + colNum;
                    }
                    label.Text = rowNum + colNum;

                    doorNum.Add(label.Text);

                    //如果是210个格子的就执行这个事件
                    StringBuilder noNeed = new StringBuilder();
                    noNeed.Append("401,403,405,407,409,411,413,415,417,419,");
                    noNeed.Append("501,503,505,507,509,511,513,515,517,519,");
                    noNeed.Append("601,603,605,607,609,611,613,615,617,619");
                    var noNeedElement = noNeed.ToString().Split(',');
                    if (noNeedElement.Contains(label.Text))
                    {
                        label.Text = string.Empty;
                        label.Enabled = false;
                    }

                    this.Controls.Add(label);
                }
            }

            //为所有的控件动态绑定单击事件
            foreach (var item in Controls)
            {
                //遍历所有的Label
                if (item is Label)
                {
                    (item as Label).Click += (click_sender, click_e) =>
                    {
                        //这里写处理程序，这段代码写在构造函数，或者formload里面
                        int doorNum = Convert.ToInt32((item as Label).Text);
                        IList<LocalBooks> model = BLL.GetBookByDoorNum(doorNum);

                        //自动释放资源
                        using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                        {
                            //初始化控制台串口
                            consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                            //打开相应行号的柜门
                            bool res = consoleModel.OpenDoor(model[0].DoorID, model[0].DoorCode,4);

                            if (res)
                            {
                                //开门成功
                                (item as Label).BackColor = Color.Green;
                            }
                            else
                            {
                                //开门成功
                                (item as Label).BackColor = Color.Red;
                            }
                        }
                    };
                }
            }
        }

        private void btnOpenDoorAll_Click(object sender, EventArgs e)
        {
            //为所有的控件动态绑定单击事件
            foreach (var item in Controls)
            {
                //遍历所有的Label
                if (item is Label)
                {
                    if (string.IsNullOrEmpty((item as Label).Text))
                    {
                        continue;
                    }
                    else
                    {
                        #region 异步开门
                        OpenDoorDelegate myDelegate = new OpenDoorDelegate(OpenDoor);

                        IAsyncResult result = myDelegate.BeginInvoke(item, null, null);

                        myDelegate.EndInvoke(result);
                        #endregion
                    }
                }
            }
        }

        public delegate void OpenDoorDelegate(object item);

        /// <summary>
        /// 循环打开所有的门
        /// </summary>
        /// <param name="item">Label子集</param>
        private void OpenDoor(object item)
        {
            int doorNum = Convert.ToInt32((item as Label).Text);
            IList<LocalBooks> model = BLL.GetBookByDoorNum(doorNum);
            //自动释放资源
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                var com = ConfigurationManager.AppSettings["Control"];
                //初始化控制台串口
                consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                Thread.Sleep(500);

                //打开相应行号的柜门
                bool res = consoleModel.OpenDoor(model[0].DoorID, model[0].DoorCode,4);

                if (res)
                {
                    //开门成功
                    (item as Label).BackColor = Color.Green;
                }
                else
                {
                    //开门失败
                    (item as Label).BackColor = Color.Red;
                }
            }
        }

        private void btnReturnMain_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }
    }
}
