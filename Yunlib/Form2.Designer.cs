﻿namespace Yunlib
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.lbl_M = new System.Windows.Forms.Label();
            this.lbl_N = new System.Windows.Forms.Label();
            this.lbl_B = new System.Windows.Forms.Label();
            this.lbl_V = new System.Windows.Forms.Label();
            this.lbl_C = new System.Windows.Forms.Label();
            this.lbl_X = new System.Windows.Forms.Label();
            this.lbl_Z = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lbl_BACKSPACE = new System.Windows.Forms.Label();
            this.lbl_L = new System.Windows.Forms.Label();
            this.lbl_K = new System.Windows.Forms.Label();
            this.lbl_J = new System.Windows.Forms.Label();
            this.lbl_H = new System.Windows.Forms.Label();
            this.lbl_G = new System.Windows.Forms.Label();
            this.lbl_F = new System.Windows.Forms.Label();
            this.lbl_D = new System.Windows.Forms.Label();
            this.lbl_S = new System.Windows.Forms.Label();
            this.lbl_A = new System.Windows.Forms.Label();
            this.lbl_P = new System.Windows.Forms.Label();
            this.lbl_O = new System.Windows.Forms.Label();
            this.lbl_I = new System.Windows.Forms.Label();
            this.lbl_U = new System.Windows.Forms.Label();
            this.lbl_Y = new System.Windows.Forms.Label();
            this.lbl_T = new System.Windows.Forms.Label();
            this.lbl_R = new System.Windows.Forms.Label();
            this.lbl_E = new System.Windows.Forms.Label();
            this.lbl_W = new System.Windows.Forms.Label();
            this.lbl_Q = new System.Windows.Forms.Label();
            this.lbl_0 = new System.Windows.Forms.Label();
            this.lbl_9 = new System.Windows.Forms.Label();
            this.lbl_8 = new System.Windows.Forms.Label();
            this.lbl_7 = new System.Windows.Forms.Label();
            this.lbl_6 = new System.Windows.Forms.Label();
            this.lbl_5 = new System.Windows.Forms.Label();
            this.lbl_4 = new System.Windows.Forms.Label();
            this.lbl_3 = new System.Windows.Forms.Label();
            this.lbl_2 = new System.Windows.Forms.Label();
            this.lbl_1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.label39, 9, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_M, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_N, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_B, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_V, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_C, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_X, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_Z, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label30, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_BACKSPACE, 9, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_L, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_K, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_J, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_H, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_G, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_F, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_D, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_S, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_A, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_P, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_O, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_I, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_U, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_Y, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_T, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_R, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_E, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_W, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_Q, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_0, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(900, 200);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label39
            // 
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label39.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label39.Location = new System.Drawing.Point(809, 146);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 49);
            this.label39.TabIndex = 79;
            this.label39.Text = "关闭";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label39.Click += new System.EventHandler(this.label39_Click);
            // 
            // lbl_M
            // 
            this.lbl_M.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_M.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_M.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_M.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_M.Location = new System.Drawing.Point(631, 146);
            this.lbl_M.Name = "lbl_M";
            this.lbl_M.Size = new System.Drawing.Size(83, 49);
            this.lbl_M.TabIndex = 77;
            this.lbl_M.Text = "M";
            this.lbl_M.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_N
            // 
            this.lbl_N.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_N.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_N.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_N.Location = new System.Drawing.Point(542, 146);
            this.lbl_N.Name = "lbl_N";
            this.lbl_N.Size = new System.Drawing.Size(83, 49);
            this.lbl_N.TabIndex = 76;
            this.lbl_N.Text = "N";
            this.lbl_N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_B
            // 
            this.lbl_B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_B.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_B.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_B.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_B.Location = new System.Drawing.Point(453, 146);
            this.lbl_B.Name = "lbl_B";
            this.lbl_B.Size = new System.Drawing.Size(83, 49);
            this.lbl_B.TabIndex = 75;
            this.lbl_B.Text = "B";
            this.lbl_B.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_V
            // 
            this.lbl_V.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_V.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_V.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_V.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_V.Location = new System.Drawing.Point(364, 146);
            this.lbl_V.Name = "lbl_V";
            this.lbl_V.Size = new System.Drawing.Size(83, 49);
            this.lbl_V.TabIndex = 74;
            this.lbl_V.Text = "V";
            this.lbl_V.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_C
            // 
            this.lbl_C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_C.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_C.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_C.Location = new System.Drawing.Point(275, 146);
            this.lbl_C.Name = "lbl_C";
            this.lbl_C.Size = new System.Drawing.Size(83, 49);
            this.lbl_C.TabIndex = 73;
            this.lbl_C.Text = "C";
            this.lbl_C.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_X
            // 
            this.lbl_X.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_X.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_X.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_X.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_X.Location = new System.Drawing.Point(186, 146);
            this.lbl_X.Name = "lbl_X";
            this.lbl_X.Size = new System.Drawing.Size(83, 49);
            this.lbl_X.TabIndex = 72;
            this.lbl_X.Text = "X";
            this.lbl_X.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Z
            // 
            this.lbl_Z.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Z.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Z.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Z.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Z.Location = new System.Drawing.Point(97, 146);
            this.lbl_Z.Name = "lbl_Z";
            this.lbl_Z.Size = new System.Drawing.Size(83, 49);
            this.lbl_Z.TabIndex = 71;
            this.lbl_Z.Text = "Z";
            this.lbl_Z.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label30.Location = new System.Drawing.Point(8, 146);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 49);
            this.label30.TabIndex = 70;
            this.label30.Text = "大写";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label30.Click += new System.EventHandler(this.label30_Click);
            // 
            // lbl_BACKSPACE
            // 
            this.lbl_BACKSPACE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_BACKSPACE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_BACKSPACE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_BACKSPACE.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_BACKSPACE.Location = new System.Drawing.Point(809, 99);
            this.lbl_BACKSPACE.Name = "lbl_BACKSPACE";
            this.lbl_BACKSPACE.Size = new System.Drawing.Size(83, 47);
            this.lbl_BACKSPACE.TabIndex = 69;
            this.lbl_BACKSPACE.Text = "←退格";
            this.lbl_BACKSPACE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_L
            // 
            this.lbl_L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_L.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_L.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_L.Location = new System.Drawing.Point(720, 99);
            this.lbl_L.Name = "lbl_L";
            this.lbl_L.Size = new System.Drawing.Size(83, 47);
            this.lbl_L.TabIndex = 68;
            this.lbl_L.Text = "L";
            this.lbl_L.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_K
            // 
            this.lbl_K.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_K.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_K.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_K.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_K.Location = new System.Drawing.Point(631, 99);
            this.lbl_K.Name = "lbl_K";
            this.lbl_K.Size = new System.Drawing.Size(83, 47);
            this.lbl_K.TabIndex = 67;
            this.lbl_K.Text = "K";
            this.lbl_K.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_J
            // 
            this.lbl_J.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_J.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_J.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_J.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_J.Location = new System.Drawing.Point(542, 99);
            this.lbl_J.Name = "lbl_J";
            this.lbl_J.Size = new System.Drawing.Size(83, 47);
            this.lbl_J.TabIndex = 66;
            this.lbl_J.Text = "J";
            this.lbl_J.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_H
            // 
            this.lbl_H.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_H.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_H.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_H.Location = new System.Drawing.Point(453, 99);
            this.lbl_H.Name = "lbl_H";
            this.lbl_H.Size = new System.Drawing.Size(83, 47);
            this.lbl_H.TabIndex = 65;
            this.lbl_H.Text = "H";
            this.lbl_H.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_G
            // 
            this.lbl_G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_G.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_G.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_G.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_G.Location = new System.Drawing.Point(364, 99);
            this.lbl_G.Name = "lbl_G";
            this.lbl_G.Size = new System.Drawing.Size(83, 47);
            this.lbl_G.TabIndex = 64;
            this.lbl_G.Text = "G";
            this.lbl_G.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_F
            // 
            this.lbl_F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_F.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_F.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_F.Location = new System.Drawing.Point(275, 99);
            this.lbl_F.Name = "lbl_F";
            this.lbl_F.Size = new System.Drawing.Size(83, 47);
            this.lbl_F.TabIndex = 63;
            this.lbl_F.Text = "F";
            this.lbl_F.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_D
            // 
            this.lbl_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_D.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_D.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_D.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_D.Location = new System.Drawing.Point(186, 99);
            this.lbl_D.Name = "lbl_D";
            this.lbl_D.Size = new System.Drawing.Size(83, 47);
            this.lbl_D.TabIndex = 62;
            this.lbl_D.Text = "D";
            this.lbl_D.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_S
            // 
            this.lbl_S.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_S.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_S.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_S.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_S.Location = new System.Drawing.Point(97, 99);
            this.lbl_S.Name = "lbl_S";
            this.lbl_S.Size = new System.Drawing.Size(83, 47);
            this.lbl_S.TabIndex = 61;
            this.lbl_S.Text = "S";
            this.lbl_S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_A
            // 
            this.lbl_A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_A.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_A.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_A.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_A.Location = new System.Drawing.Point(8, 99);
            this.lbl_A.Name = "lbl_A";
            this.lbl_A.Size = new System.Drawing.Size(83, 47);
            this.lbl_A.TabIndex = 60;
            this.lbl_A.Text = "A";
            this.lbl_A.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_P
            // 
            this.lbl_P.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_P.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_P.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_P.Location = new System.Drawing.Point(809, 52);
            this.lbl_P.Name = "lbl_P";
            this.lbl_P.Size = new System.Drawing.Size(83, 47);
            this.lbl_P.TabIndex = 59;
            this.lbl_P.Text = "P";
            this.lbl_P.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_O
            // 
            this.lbl_O.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_O.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_O.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_O.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_O.Location = new System.Drawing.Point(720, 52);
            this.lbl_O.Name = "lbl_O";
            this.lbl_O.Size = new System.Drawing.Size(83, 47);
            this.lbl_O.TabIndex = 58;
            this.lbl_O.Text = "O";
            this.lbl_O.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_I
            // 
            this.lbl_I.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_I.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_I.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_I.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_I.Location = new System.Drawing.Point(631, 52);
            this.lbl_I.Name = "lbl_I";
            this.lbl_I.Size = new System.Drawing.Size(83, 47);
            this.lbl_I.TabIndex = 57;
            this.lbl_I.Text = "I";
            this.lbl_I.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_U
            // 
            this.lbl_U.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_U.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_U.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_U.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_U.Location = new System.Drawing.Point(542, 52);
            this.lbl_U.Name = "lbl_U";
            this.lbl_U.Size = new System.Drawing.Size(83, 47);
            this.lbl_U.TabIndex = 56;
            this.lbl_U.Text = "U";
            this.lbl_U.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Y
            // 
            this.lbl_Y.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Y.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Y.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Y.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Y.Location = new System.Drawing.Point(453, 52);
            this.lbl_Y.Name = "lbl_Y";
            this.lbl_Y.Size = new System.Drawing.Size(83, 47);
            this.lbl_Y.TabIndex = 55;
            this.lbl_Y.Text = "Y";
            this.lbl_Y.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_T
            // 
            this.lbl_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_T.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_T.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_T.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_T.Location = new System.Drawing.Point(364, 52);
            this.lbl_T.Name = "lbl_T";
            this.lbl_T.Size = new System.Drawing.Size(83, 47);
            this.lbl_T.TabIndex = 54;
            this.lbl_T.Text = "T";
            this.lbl_T.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_R
            // 
            this.lbl_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_R.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_R.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_R.Location = new System.Drawing.Point(275, 52);
            this.lbl_R.Name = "lbl_R";
            this.lbl_R.Size = new System.Drawing.Size(83, 47);
            this.lbl_R.TabIndex = 53;
            this.lbl_R.Text = "R";
            this.lbl_R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_E
            // 
            this.lbl_E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_E.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_E.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_E.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_E.Location = new System.Drawing.Point(186, 52);
            this.lbl_E.Name = "lbl_E";
            this.lbl_E.Size = new System.Drawing.Size(83, 47);
            this.lbl_E.TabIndex = 52;
            this.lbl_E.Text = "E";
            this.lbl_E.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_W
            // 
            this.lbl_W.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_W.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_W.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_W.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_W.Location = new System.Drawing.Point(97, 52);
            this.lbl_W.Name = "lbl_W";
            this.lbl_W.Size = new System.Drawing.Size(83, 47);
            this.lbl_W.TabIndex = 51;
            this.lbl_W.Text = "W";
            this.lbl_W.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Q
            // 
            this.lbl_Q.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Q.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_Q.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Q.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Q.Location = new System.Drawing.Point(8, 52);
            this.lbl_Q.Name = "lbl_Q";
            this.lbl_Q.Size = new System.Drawing.Size(83, 47);
            this.lbl_Q.TabIndex = 50;
            this.lbl_Q.Text = "Q";
            this.lbl_Q.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_0
            // 
            this.lbl_0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_0.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_0.Location = new System.Drawing.Point(809, 5);
            this.lbl_0.Name = "lbl_0";
            this.lbl_0.Size = new System.Drawing.Size(83, 47);
            this.lbl_0.TabIndex = 49;
            this.lbl_0.Text = "0";
            this.lbl_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_9
            // 
            this.lbl_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_9.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_9.Location = new System.Drawing.Point(720, 5);
            this.lbl_9.Name = "lbl_9";
            this.lbl_9.Size = new System.Drawing.Size(83, 47);
            this.lbl_9.TabIndex = 48;
            this.lbl_9.Text = "9";
            this.lbl_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_8
            // 
            this.lbl_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_8.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_8.Location = new System.Drawing.Point(631, 5);
            this.lbl_8.Name = "lbl_8";
            this.lbl_8.Size = new System.Drawing.Size(83, 47);
            this.lbl_8.TabIndex = 47;
            this.lbl_8.Text = "8";
            this.lbl_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_7
            // 
            this.lbl_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_7.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_7.Location = new System.Drawing.Point(542, 5);
            this.lbl_7.Name = "lbl_7";
            this.lbl_7.Size = new System.Drawing.Size(83, 47);
            this.lbl_7.TabIndex = 46;
            this.lbl_7.Text = "7";
            this.lbl_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_6
            // 
            this.lbl_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_6.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_6.Location = new System.Drawing.Point(453, 5);
            this.lbl_6.Name = "lbl_6";
            this.lbl_6.Size = new System.Drawing.Size(83, 47);
            this.lbl_6.TabIndex = 45;
            this.lbl_6.Text = "6";
            this.lbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_5
            // 
            this.lbl_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_5.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_5.Location = new System.Drawing.Point(364, 5);
            this.lbl_5.Name = "lbl_5";
            this.lbl_5.Size = new System.Drawing.Size(83, 47);
            this.lbl_5.TabIndex = 44;
            this.lbl_5.Text = "5";
            this.lbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_4
            // 
            this.lbl_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_4.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_4.Location = new System.Drawing.Point(275, 5);
            this.lbl_4.Name = "lbl_4";
            this.lbl_4.Size = new System.Drawing.Size(83, 47);
            this.lbl_4.TabIndex = 43;
            this.lbl_4.Text = "4";
            this.lbl_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_3
            // 
            this.lbl_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_3.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_3.Location = new System.Drawing.Point(186, 5);
            this.lbl_3.Name = "lbl_3";
            this.lbl_3.Size = new System.Drawing.Size(83, 47);
            this.lbl_3.TabIndex = 42;
            this.lbl_3.Text = "3";
            this.lbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_2
            // 
            this.lbl_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_2.Location = new System.Drawing.Point(97, 5);
            this.lbl_2.Name = "lbl_2";
            this.lbl_2.Size = new System.Drawing.Size(83, 47);
            this.lbl_2.TabIndex = 41;
            this.lbl_2.Text = "2";
            this.lbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_2.Click += new System.EventHandler(this.lbl_2_Click);
            // 
            // lbl_1
            // 
            this.lbl_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_1.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_1.Location = new System.Drawing.Point(8, 5);
            this.lbl_1.Name = "lbl_1";
            this.lbl_1.Size = new System.Drawing.Size(83, 47);
            this.lbl_1.TabIndex = 40;
            this.lbl_1.Text = "1";
            this.lbl_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 200);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(50, 550);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form2";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lbl_M;
        private System.Windows.Forms.Label lbl_N;
        private System.Windows.Forms.Label lbl_B;
        private System.Windows.Forms.Label lbl_V;
        private System.Windows.Forms.Label lbl_C;
        private System.Windows.Forms.Label lbl_X;
        private System.Windows.Forms.Label lbl_Z;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbl_BACKSPACE;
        private System.Windows.Forms.Label lbl_L;
        private System.Windows.Forms.Label lbl_K;
        private System.Windows.Forms.Label lbl_J;
        private System.Windows.Forms.Label lbl_H;
        private System.Windows.Forms.Label lbl_G;
        private System.Windows.Forms.Label lbl_F;
        private System.Windows.Forms.Label lbl_D;
        private System.Windows.Forms.Label lbl_S;
        private System.Windows.Forms.Label lbl_A;
        private System.Windows.Forms.Label lbl_P;
        private System.Windows.Forms.Label lbl_O;
        private System.Windows.Forms.Label lbl_I;
        private System.Windows.Forms.Label lbl_U;
        private System.Windows.Forms.Label lbl_Y;
        private System.Windows.Forms.Label lbl_T;
        private System.Windows.Forms.Label lbl_R;
        private System.Windows.Forms.Label lbl_E;
        private System.Windows.Forms.Label lbl_W;
        private System.Windows.Forms.Label lbl_Q;
        private System.Windows.Forms.Label lbl_0;
        private System.Windows.Forms.Label lbl_9;
        private System.Windows.Forms.Label lbl_8;
        private System.Windows.Forms.Label lbl_7;
        private System.Windows.Forms.Label lbl_6;
        private System.Windows.Forms.Label lbl_5;
        private System.Windows.Forms.Label lbl_4;
        private System.Windows.Forms.Label lbl_3;
        private System.Windows.Forms.Label lbl_2;
        private System.Windows.Forms.Label lbl_1;
    }
}