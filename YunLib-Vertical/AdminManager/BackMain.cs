﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib_Vertical.AdminManager
{
    public partial class BackMain : Form
    {
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        public BackMain()
        {
            InitializeComponent();
        }

        private void BackMain_Load(object sender, EventArgs e)
        {
            if(ConfigManager.IsNeedInitData == "0")
            {
                button1.Visible = false;
                button2.Visible = false;
                button5.Visible = true;
            }
            else
            {
                button1.Visible = true;
                button2.Visible = true;
                button5.Visible = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.Enabled = false;
            bool flag = InitData();
            if (true)
            {
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "IsNeedInitData", "1"))
                {
                    MessageBox.Show("修改配置文件失败！");

                }
                else
                {
                    MessageBox.Show("数据同步成功！");
                    button1.Visible = true;
                    button2.Visible = true;

                    button5.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("数据同步失败！");
                button5.Enabled = true;
            }
          
        }

        private bool InitData()
        {
            try
            {
                var servicePath = ConfigManager.ServicePath;
                var libraryId = ConfigManager.LibraryID;
                var commonKey = ConfigManager.CommonKey;

                var timeStamp = DateTime.Now.Ticks;

                var signStr = "{0}{1}{2}".FormatWith(libraryId, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                DataTable dt = BLL.GetMachineInitData();

                string jsonStr = JsonHelper.ToJson(dt).Replace("DoorNUM", "machLatNum").Replace("BarCode", "bookId ");
                //string jsonStr = GetData();
                JObject jo = new JObject();
                jo.Add("machineId", ConfigManager.MachineID);
                jo.Add("machineLattice", jsonStr);

                JObject joNew = new JObject();
                joNew.Add("json", jo.ToJson());

                JToken jtoken = JToken.Parse(joNew.ToJson());

                string jsonStr1 = "json=" + jtoken.ToString().Replace("\\\\\\\"", "\"").Replace("\\\"", "\"").Replace("\\", "").Replace("r", "").Replace("\"{", "{").Replace("\"[", "[").Replace("\"]", "]").Replace("]\"", "]").Replace("}\"", "}").Replace(" ", "");


                var url = "{0}public/machine/putMachine?timeStamp={1}&libraryId={2}&sign={3}".FormatWith(servicePath, timeStamp, libraryId, sign);
                var back = WebHelper.HttpWebRequest(url, jsonStr1, Encoding.GetEncoding("utf-8"), true).ToJObject();

                if (back["code"].ToString() == "100")
                {
                    MessageBox.Show("数据同步成功");
                    return true;
                }
                else
                {
                    MessageBox.Show("数据同步化失败");
                    LogManager.ErrorRecord("{0}-数据同步失败：{1}".FormatWith(DateTime.Now, back.ToString()));
                    return false;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("数据同步化失败");
                LogManager.BorrowRecord("{0}--数据同步失败：{1}".FormatWith(DateTime.Now, ex.Message));
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FillBookForm frm = new FillBookForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UnderCarriageForm frm = new UnderCarriageForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            InitForm frm = new InitForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DoorManagerForm frm = new DoorManagerForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }
    }
}
