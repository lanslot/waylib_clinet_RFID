﻿namespace Yunlib
{
    partial class UnderCarriage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnderCarriage));
            this.lblQuery = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBooksCode = new System.Windows.Forms.TextBox();
            this.pBtn_Search = new System.Windows.Forms.PictureBox();
            this.lblBtnA = new System.Windows.Forms.Label();
            this.lblAll = new System.Windows.Forms.Label();
            this.lblBtnC = new System.Windows.Forms.Label();
            this.lblBtnB = new System.Windows.Forms.Label();
            this.lblBtnE = new System.Windows.Forms.Label();
            this.lblBtnD = new System.Windows.Forms.Label();
            this.lblBtnF = new System.Windows.Forms.Label();
            this.lblBtnG = new System.Windows.Forms.Label();
            this.lblBtnJ = new System.Windows.Forms.Label();
            this.lblBtnH = new System.Windows.Forms.Label();
            this.lblBtnQ = new System.Windows.Forms.Label();
            this.lblBtnK = new System.Windows.Forms.Label();
            this.lblBtnR = new System.Windows.Forms.Label();
            this.lblBtnI = new System.Windows.Forms.Label();
            this.lblBtnM = new System.Windows.Forms.Label();
            this.lblBtnL = new System.Windows.Forms.Label();
            this.lblBtnO = new System.Windows.Forms.Label();
            this.lblBtnN = new System.Windows.Forms.Label();
            this.lblBtnP = new System.Windows.Forms.Label();
            this.lblBtnU = new System.Windows.Forms.Label();
            this.lblBtnV = new System.Windows.Forms.Label();
            this.lblBtnS = new System.Windows.Forms.Label();
            this.lblBtnT = new System.Windows.Forms.Label();
            this.lblBtnY = new System.Windows.Forms.Label();
            this.lblBtnZ = new System.Windows.Forms.Label();
            this.lblBtnW = new System.Windows.Forms.Label();
            this.lblBtnX = new System.Windows.Forms.Label();
            this.palTop = new System.Windows.Forms.Panel();
            this.pBtnNextPage = new System.Windows.Forms.PictureBox();
            this.pic_BtnTurnBack = new System.Windows.Forms.PictureBox();
            this.pBtnLastPage = new System.Windows.Forms.PictureBox();
            this.lblThisPage = new System.Windows.Forms.Label();
            this.lblTotalPage = new System.Windows.Forms.Label();
            this.palBottom = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLoading = new System.Windows.Forms.Label();
            this.palDGVArea = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pBtn_Search)).BeginInit();
            this.palTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnNextPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_BtnTurnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnLastPage)).BeginInit();
            this.palBottom.SuspendLayout();
            this.palDGVArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblQuery
            // 
            this.lblQuery.AutoSize = true;
            this.lblQuery.BackColor = System.Drawing.Color.Transparent;
            this.lblQuery.Location = new System.Drawing.Point(305, 55);
            this.lblQuery.Name = "lblQuery";
            this.lblQuery.Size = new System.Drawing.Size(0, 12);
            this.lblQuery.TabIndex = 9;
            this.lblQuery.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(592, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "柜号检索";
            // 
            // txtBooksCode
            // 
            this.txtBooksCode.BackColor = System.Drawing.Color.White;
            this.txtBooksCode.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.txtBooksCode.ForeColor = System.Drawing.Color.Silver;
            this.txtBooksCode.Location = new System.Drawing.Point(680, 34);
            this.txtBooksCode.Multiline = true;
            this.txtBooksCode.Name = "txtBooksCode";
            this.txtBooksCode.Size = new System.Drawing.Size(230, 38);
            this.txtBooksCode.TabIndex = 11;
            this.txtBooksCode.Text = "请输入书柜号";
            this.txtBooksCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtBooksCode_MouseDown);
            // 
            // pBtn_Search
            // 
            this.pBtn_Search.BackColor = System.Drawing.Color.Transparent;
            this.pBtn_Search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBtn_Search.Image = global::Yunlib.Properties.Resources.pBtnSearch;
            this.pBtn_Search.Location = new System.Drawing.Point(912, 34);
            this.pBtn_Search.Name = "pBtn_Search";
            this.pBtn_Search.Size = new System.Drawing.Size(84, 38);
            this.pBtn_Search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pBtn_Search.TabIndex = 12;
            this.pBtn_Search.TabStop = false;
            this.pBtn_Search.Click += new System.EventHandler(this.pBtn_Search_Click);
            // 
            // lblBtnA
            // 
            this.lblBtnA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnA.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnA.ForeColor = System.Drawing.Color.White;
            this.lblBtnA.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnA.Location = new System.Drawing.Point(66, 10);
            this.lblBtnA.Name = "lblBtnA";
            this.lblBtnA.Size = new System.Drawing.Size(35, 50);
            this.lblBtnA.TabIndex = 2;
            this.lblBtnA.Text = "A";
            this.lblBtnA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnA.Click += new System.EventHandler(this.lblBtnA_Click);
            // 
            // lblAll
            // 
            this.lblAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAll.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Bold);
            this.lblAll.ForeColor = System.Drawing.Color.White;
            this.lblAll.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblAll.Location = new System.Drawing.Point(31, 9);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(35, 50);
            this.lblAll.TabIndex = 2;
            this.lblAll.Text = "全";
            this.lblAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAll.Click += new System.EventHandler(this.lblBtnAll_Click);
            // 
            // lblBtnC
            // 
            this.lblBtnC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnC.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnC.ForeColor = System.Drawing.Color.White;
            this.lblBtnC.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnC.Location = new System.Drawing.Point(138, 9);
            this.lblBtnC.Name = "lblBtnC";
            this.lblBtnC.Size = new System.Drawing.Size(35, 50);
            this.lblBtnC.TabIndex = 3;
            this.lblBtnC.Text = "C";
            this.lblBtnC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnC.Click += new System.EventHandler(this.lblBtnC_Click);
            // 
            // lblBtnB
            // 
            this.lblBtnB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnB.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnB.ForeColor = System.Drawing.Color.White;
            this.lblBtnB.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnB.Location = new System.Drawing.Point(102, 9);
            this.lblBtnB.Name = "lblBtnB";
            this.lblBtnB.Size = new System.Drawing.Size(35, 50);
            this.lblBtnB.TabIndex = 4;
            this.lblBtnB.Text = "B";
            this.lblBtnB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnB.Click += new System.EventHandler(this.lblBtnB_Click);
            // 
            // lblBtnE
            // 
            this.lblBtnE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnE.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnE.ForeColor = System.Drawing.Color.White;
            this.lblBtnE.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnE.Location = new System.Drawing.Point(210, 9);
            this.lblBtnE.Name = "lblBtnE";
            this.lblBtnE.Size = new System.Drawing.Size(35, 50);
            this.lblBtnE.TabIndex = 5;
            this.lblBtnE.Text = "E";
            this.lblBtnE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnE.Click += new System.EventHandler(this.lblBtnE_Click);
            // 
            // lblBtnD
            // 
            this.lblBtnD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnD.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnD.ForeColor = System.Drawing.Color.White;
            this.lblBtnD.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnD.Location = new System.Drawing.Point(174, 9);
            this.lblBtnD.Name = "lblBtnD";
            this.lblBtnD.Size = new System.Drawing.Size(35, 50);
            this.lblBtnD.TabIndex = 6;
            this.lblBtnD.Text = "D";
            this.lblBtnD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnD.Click += new System.EventHandler(this.lblBtnD_Click);
            // 
            // lblBtnF
            // 
            this.lblBtnF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnF.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnF.ForeColor = System.Drawing.Color.White;
            this.lblBtnF.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnF.Location = new System.Drawing.Point(246, 9);
            this.lblBtnF.Name = "lblBtnF";
            this.lblBtnF.Size = new System.Drawing.Size(35, 50);
            this.lblBtnF.TabIndex = 7;
            this.lblBtnF.Text = "F";
            this.lblBtnF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnF.Click += new System.EventHandler(this.lblBtnF_Click);
            // 
            // lblBtnG
            // 
            this.lblBtnG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnG.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnG.ForeColor = System.Drawing.Color.White;
            this.lblBtnG.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnG.Location = new System.Drawing.Point(282, 9);
            this.lblBtnG.Name = "lblBtnG";
            this.lblBtnG.Size = new System.Drawing.Size(35, 50);
            this.lblBtnG.TabIndex = 8;
            this.lblBtnG.Text = "G";
            this.lblBtnG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnG.Click += new System.EventHandler(this.lblBtnG_Click);
            // 
            // lblBtnJ
            // 
            this.lblBtnJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnJ.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnJ.ForeColor = System.Drawing.Color.White;
            this.lblBtnJ.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnJ.Location = new System.Drawing.Point(390, 9);
            this.lblBtnJ.Name = "lblBtnJ";
            this.lblBtnJ.Size = new System.Drawing.Size(35, 50);
            this.lblBtnJ.TabIndex = 9;
            this.lblBtnJ.Text = "J";
            this.lblBtnJ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnJ.Click += new System.EventHandler(this.lblBtnJ_Click);
            // 
            // lblBtnH
            // 
            this.lblBtnH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnH.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnH.ForeColor = System.Drawing.Color.White;
            this.lblBtnH.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnH.Location = new System.Drawing.Point(318, 9);
            this.lblBtnH.Name = "lblBtnH";
            this.lblBtnH.Size = new System.Drawing.Size(35, 50);
            this.lblBtnH.TabIndex = 10;
            this.lblBtnH.Text = "H";
            this.lblBtnH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnH.Click += new System.EventHandler(this.lblBtnH_Click);
            // 
            // lblBtnQ
            // 
            this.lblBtnQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnQ.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnQ.ForeColor = System.Drawing.Color.White;
            this.lblBtnQ.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnQ.Location = new System.Drawing.Point(642, 9);
            this.lblBtnQ.Name = "lblBtnQ";
            this.lblBtnQ.Size = new System.Drawing.Size(35, 50);
            this.lblBtnQ.TabIndex = 11;
            this.lblBtnQ.Text = "Q";
            this.lblBtnQ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnQ.Click += new System.EventHandler(this.lblBtnQ_Click);
            // 
            // lblBtnK
            // 
            this.lblBtnK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnK.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnK.ForeColor = System.Drawing.Color.White;
            this.lblBtnK.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnK.Location = new System.Drawing.Point(426, 9);
            this.lblBtnK.Name = "lblBtnK";
            this.lblBtnK.Size = new System.Drawing.Size(35, 50);
            this.lblBtnK.TabIndex = 12;
            this.lblBtnK.Text = "K";
            this.lblBtnK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnK.Click += new System.EventHandler(this.lblBtnK_Click);
            // 
            // lblBtnR
            // 
            this.lblBtnR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnR.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnR.ForeColor = System.Drawing.Color.White;
            this.lblBtnR.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnR.Location = new System.Drawing.Point(678, 9);
            this.lblBtnR.Name = "lblBtnR";
            this.lblBtnR.Size = new System.Drawing.Size(35, 50);
            this.lblBtnR.TabIndex = 13;
            this.lblBtnR.Text = "R";
            this.lblBtnR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnR.Click += new System.EventHandler(this.lblBtnR_Click);
            // 
            // lblBtnI
            // 
            this.lblBtnI.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnI.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnI.ForeColor = System.Drawing.Color.White;
            this.lblBtnI.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnI.Location = new System.Drawing.Point(354, 9);
            this.lblBtnI.Name = "lblBtnI";
            this.lblBtnI.Size = new System.Drawing.Size(35, 50);
            this.lblBtnI.TabIndex = 14;
            this.lblBtnI.Text = "I";
            this.lblBtnI.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnI.Click += new System.EventHandler(this.lblBtnI_Click);
            // 
            // lblBtnM
            // 
            this.lblBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnM.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnM.ForeColor = System.Drawing.Color.White;
            this.lblBtnM.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnM.Location = new System.Drawing.Point(498, 9);
            this.lblBtnM.Name = "lblBtnM";
            this.lblBtnM.Size = new System.Drawing.Size(35, 50);
            this.lblBtnM.TabIndex = 15;
            this.lblBtnM.Text = "M";
            this.lblBtnM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnM.Click += new System.EventHandler(this.lblBtnM_Click);
            // 
            // lblBtnL
            // 
            this.lblBtnL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnL.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnL.ForeColor = System.Drawing.Color.White;
            this.lblBtnL.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnL.Location = new System.Drawing.Point(462, 9);
            this.lblBtnL.Name = "lblBtnL";
            this.lblBtnL.Size = new System.Drawing.Size(35, 50);
            this.lblBtnL.TabIndex = 16;
            this.lblBtnL.Text = "L";
            this.lblBtnL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnL.Click += new System.EventHandler(this.lblBtnL_Click);
            // 
            // lblBtnO
            // 
            this.lblBtnO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnO.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnO.ForeColor = System.Drawing.Color.White;
            this.lblBtnO.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnO.Location = new System.Drawing.Point(570, 9);
            this.lblBtnO.Name = "lblBtnO";
            this.lblBtnO.Size = new System.Drawing.Size(35, 50);
            this.lblBtnO.TabIndex = 17;
            this.lblBtnO.Text = "O";
            this.lblBtnO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnO.Click += new System.EventHandler(this.lblBtnO_Click);
            // 
            // lblBtnN
            // 
            this.lblBtnN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnN.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnN.ForeColor = System.Drawing.Color.White;
            this.lblBtnN.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnN.Location = new System.Drawing.Point(534, 9);
            this.lblBtnN.Name = "lblBtnN";
            this.lblBtnN.Size = new System.Drawing.Size(35, 50);
            this.lblBtnN.TabIndex = 18;
            this.lblBtnN.Text = "N";
            this.lblBtnN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnN.Click += new System.EventHandler(this.lblBtnN_Click);
            // 
            // lblBtnP
            // 
            this.lblBtnP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnP.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnP.ForeColor = System.Drawing.Color.White;
            this.lblBtnP.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnP.Location = new System.Drawing.Point(606, 9);
            this.lblBtnP.Name = "lblBtnP";
            this.lblBtnP.Size = new System.Drawing.Size(35, 50);
            this.lblBtnP.TabIndex = 19;
            this.lblBtnP.Text = "P";
            this.lblBtnP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnP.Click += new System.EventHandler(this.lblBtnP_Click);
            // 
            // lblBtnU
            // 
            this.lblBtnU.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnU.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnU.ForeColor = System.Drawing.Color.White;
            this.lblBtnU.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnU.Location = new System.Drawing.Point(786, 9);
            this.lblBtnU.Name = "lblBtnU";
            this.lblBtnU.Size = new System.Drawing.Size(35, 50);
            this.lblBtnU.TabIndex = 20;
            this.lblBtnU.Text = "U";
            this.lblBtnU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnU.Click += new System.EventHandler(this.lblBtnU_Click);
            // 
            // lblBtnV
            // 
            this.lblBtnV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnV.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnV.ForeColor = System.Drawing.Color.White;
            this.lblBtnV.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnV.Location = new System.Drawing.Point(822, 9);
            this.lblBtnV.Name = "lblBtnV";
            this.lblBtnV.Size = new System.Drawing.Size(35, 50);
            this.lblBtnV.TabIndex = 21;
            this.lblBtnV.Text = "V";
            this.lblBtnV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnV.Click += new System.EventHandler(this.lblBtnV_Click);
            // 
            // lblBtnS
            // 
            this.lblBtnS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnS.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnS.ForeColor = System.Drawing.Color.White;
            this.lblBtnS.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnS.Location = new System.Drawing.Point(714, 9);
            this.lblBtnS.Name = "lblBtnS";
            this.lblBtnS.Size = new System.Drawing.Size(35, 50);
            this.lblBtnS.TabIndex = 22;
            this.lblBtnS.Text = "S";
            this.lblBtnS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnS.Click += new System.EventHandler(this.lblBtnS_Click);
            // 
            // lblBtnT
            // 
            this.lblBtnT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnT.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnT.ForeColor = System.Drawing.Color.White;
            this.lblBtnT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnT.Location = new System.Drawing.Point(750, 9);
            this.lblBtnT.Name = "lblBtnT";
            this.lblBtnT.Size = new System.Drawing.Size(35, 50);
            this.lblBtnT.TabIndex = 23;
            this.lblBtnT.Text = "T";
            this.lblBtnT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnT.Click += new System.EventHandler(this.lblBtnT_Click);
            // 
            // lblBtnY
            // 
            this.lblBtnY.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnY.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnY.ForeColor = System.Drawing.Color.White;
            this.lblBtnY.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnY.Location = new System.Drawing.Point(930, 9);
            this.lblBtnY.Name = "lblBtnY";
            this.lblBtnY.Size = new System.Drawing.Size(35, 50);
            this.lblBtnY.TabIndex = 24;
            this.lblBtnY.Text = "Y";
            this.lblBtnY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnY.Click += new System.EventHandler(this.lblBtnY_Click);
            // 
            // lblBtnZ
            // 
            this.lblBtnZ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnZ.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnZ.ForeColor = System.Drawing.Color.White;
            this.lblBtnZ.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnZ.Location = new System.Drawing.Point(966, 9);
            this.lblBtnZ.Name = "lblBtnZ";
            this.lblBtnZ.Size = new System.Drawing.Size(35, 50);
            this.lblBtnZ.TabIndex = 25;
            this.lblBtnZ.Text = "Z";
            this.lblBtnZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnZ.Click += new System.EventHandler(this.lblBtnZ_Click);
            // 
            // lblBtnW
            // 
            this.lblBtnW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnW.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnW.ForeColor = System.Drawing.Color.White;
            this.lblBtnW.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnW.Location = new System.Drawing.Point(858, 9);
            this.lblBtnW.Name = "lblBtnW";
            this.lblBtnW.Size = new System.Drawing.Size(35, 50);
            this.lblBtnW.TabIndex = 26;
            this.lblBtnW.Text = "W";
            this.lblBtnW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnW.Click += new System.EventHandler(this.lblBtnW_Click);
            // 
            // lblBtnX
            // 
            this.lblBtnX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBtnX.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lblBtnX.ForeColor = System.Drawing.Color.White;
            this.lblBtnX.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblBtnX.Location = new System.Drawing.Point(894, 9);
            this.lblBtnX.Name = "lblBtnX";
            this.lblBtnX.Size = new System.Drawing.Size(35, 50);
            this.lblBtnX.TabIndex = 27;
            this.lblBtnX.Text = "X";
            this.lblBtnX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBtnX.Click += new System.EventHandler(this.lblBtnX_Click);
            // 
            // palTop
            // 
            this.palTop.BackColor = System.Drawing.Color.Transparent;
            this.palTop.Controls.Add(this.lblBtnX);
            this.palTop.Controls.Add(this.lblBtnW);
            this.palTop.Controls.Add(this.lblBtnZ);
            this.palTop.Controls.Add(this.lblBtnY);
            this.palTop.Controls.Add(this.lblBtnT);
            this.palTop.Controls.Add(this.lblBtnS);
            this.palTop.Controls.Add(this.lblBtnV);
            this.palTop.Controls.Add(this.lblBtnU);
            this.palTop.Controls.Add(this.lblBtnP);
            this.palTop.Controls.Add(this.lblBtnN);
            this.palTop.Controls.Add(this.lblBtnO);
            this.palTop.Controls.Add(this.lblBtnL);
            this.palTop.Controls.Add(this.lblBtnM);
            this.palTop.Controls.Add(this.lblBtnI);
            this.palTop.Controls.Add(this.lblBtnR);
            this.palTop.Controls.Add(this.lblBtnK);
            this.palTop.Controls.Add(this.lblBtnQ);
            this.palTop.Controls.Add(this.lblBtnH);
            this.palTop.Controls.Add(this.lblBtnJ);
            this.palTop.Controls.Add(this.lblBtnG);
            this.palTop.Controls.Add(this.lblBtnF);
            this.palTop.Controls.Add(this.lblBtnD);
            this.palTop.Controls.Add(this.lblBtnE);
            this.palTop.Controls.Add(this.lblBtnB);
            this.palTop.Controls.Add(this.lblBtnC);
            this.palTop.Controls.Add(this.lblAll);
            this.palTop.Controls.Add(this.lblBtnA);
            this.palTop.Location = new System.Drawing.Point(1, 101);
            this.palTop.Name = "palTop";
            this.palTop.Size = new System.Drawing.Size(1004, 68);
            this.palTop.TabIndex = 6;
            // 
            // pBtnNextPage
            // 
            this.pBtnNextPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnNextPage.Image = global::Yunlib.Properties.Resources.Next_page1;
            this.pBtnNextPage.Location = new System.Drawing.Point(358, 32);
            this.pBtnNextPage.Name = "pBtnNextPage";
            this.pBtnNextPage.Size = new System.Drawing.Size(158, 50);
            this.pBtnNextPage.TabIndex = 0;
            this.pBtnNextPage.TabStop = false;
            this.pBtnNextPage.Click += new System.EventHandler(this.pBtnNextPage_Click);
            // 
            // pic_BtnTurnBack
            // 
            this.pic_BtnTurnBack.Image = ((System.Drawing.Image)(resources.GetObject("pic_BtnTurnBack.Image")));
            this.pic_BtnTurnBack.Location = new System.Drawing.Point(826, 32);
            this.pic_BtnTurnBack.Name = "pic_BtnTurnBack";
            this.pic_BtnTurnBack.Size = new System.Drawing.Size(159, 50);
            this.pic_BtnTurnBack.TabIndex = 0;
            this.pic_BtnTurnBack.TabStop = false;
            this.pic_BtnTurnBack.Click += new System.EventHandler(this.pic_BtnTurnBack_Click);
            this.pic_BtnTurnBack.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pic_BtnTurnBack_MouseMove);
            // 
            // pBtnLastPage
            // 
            this.pBtnLastPage.BackgroundImage = global::Yunlib.Properties.Resources.Last_page1;
            this.pBtnLastPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnLastPage.Location = new System.Drawing.Point(39, 32);
            this.pBtnLastPage.Name = "pBtnLastPage";
            this.pBtnLastPage.Size = new System.Drawing.Size(158, 50);
            this.pBtnLastPage.TabIndex = 0;
            this.pBtnLastPage.TabStop = false;
            this.pBtnLastPage.Click += new System.EventHandler(this.pBtnLastPage_Click);
            // 
            // lblThisPage
            // 
            this.lblThisPage.AutoSize = true;
            this.lblThisPage.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.lblThisPage.ForeColor = System.Drawing.Color.White;
            this.lblThisPage.Location = new System.Drawing.Point(237, 42);
            this.lblThisPage.Name = "lblThisPage";
            this.lblThisPage.Size = new System.Drawing.Size(0, 27);
            this.lblThisPage.TabIndex = 1;
            // 
            // lblTotalPage
            // 
            this.lblTotalPage.AutoSize = true;
            this.lblTotalPage.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.lblTotalPage.ForeColor = System.Drawing.Color.White;
            this.lblTotalPage.Location = new System.Drawing.Point(294, 42);
            this.lblTotalPage.Name = "lblTotalPage";
            this.lblTotalPage.Size = new System.Drawing.Size(0, 27);
            this.lblTotalPage.TabIndex = 1;
            // 
            // palBottom
            // 
            this.palBottom.BackColor = System.Drawing.Color.Transparent;
            this.palBottom.Controls.Add(this.button1);
            this.palBottom.Controls.Add(this.lblTotalPage);
            this.palBottom.Controls.Add(this.label2);
            this.palBottom.Controls.Add(this.lblThisPage);
            this.palBottom.Controls.Add(this.pBtnLastPage);
            this.palBottom.Controls.Add(this.pic_BtnTurnBack);
            this.palBottom.Controls.Add(this.pBtnNextPage);
            this.palBottom.Location = new System.Drawing.Point(1, 625);
            this.palBottom.Name = "palBottom";
            this.palBottom.Size = new System.Drawing.Size(1004, 102);
            this.palBottom.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(595, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 50);
            this.button1.TabIndex = 2;
            this.button1.Text = "一 键 下 架";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(271, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 27);
            this.label2.TabIndex = 1;
            // 
            // lblLoading
            // 
            this.lblLoading.AutoSize = true;
            this.lblLoading.BackColor = System.Drawing.Color.Transparent;
            this.lblLoading.Font = new System.Drawing.Font("微软雅黑", 45F);
            this.lblLoading.ForeColor = System.Drawing.Color.White;
            this.lblLoading.Location = new System.Drawing.Point(373, 311);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(306, 78);
            this.lblLoading.TabIndex = 13;
            this.lblLoading.Text = "Loading...";
            // 
            // palDGVArea
            // 
            this.palDGVArea.BackColor = System.Drawing.Color.Transparent;
            this.palDGVArea.Controls.Add(this.listView1);
            this.palDGVArea.Location = new System.Drawing.Point(1, 175);
            this.palDGVArea.Name = "palDGVArea";
            this.palDGVArea.Size = new System.Drawing.Size(1004, 452);
            this.palDGVArea.TabIndex = 22;
            // 
            // listView1
            // 
            this.listView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listView1.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.listView1.Location = new System.Drawing.Point(32, 10);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1004, 434);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView1_DrawItem);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Location = new System.Drawing.Point(14, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 67);
            this.panel2.TabIndex = 23;
            // 
            // UnderCarriage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.palDGVArea);
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.pBtn_Search);
            this.Controls.Add(this.txtBooksCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblQuery);
            this.Controls.Add(this.palBottom);
            this.Controls.Add(this.palTop);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UnderCarriage";
            this.Text = "BorrowBook";
            this.Load += new System.EventHandler(this.BorrowBook_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBtn_Search)).EndInit();
            this.palTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBtnNextPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_BtnTurnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnLastPage)).EndInit();
            this.palBottom.ResumeLayout(false);
            this.palBottom.PerformLayout();
            this.palDGVArea.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblQuery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBooksCode;
        private System.Windows.Forms.PictureBox pBtn_Search;
        private System.Windows.Forms.Label lblBtnA;
        private System.Windows.Forms.Label lblAll;
        private System.Windows.Forms.Label lblBtnC;
        private System.Windows.Forms.Label lblBtnB;
        private System.Windows.Forms.Label lblBtnE;
        private System.Windows.Forms.Label lblBtnD;
        private System.Windows.Forms.Label lblBtnF;
        private System.Windows.Forms.Label lblBtnG;
        private System.Windows.Forms.Label lblBtnJ;
        private System.Windows.Forms.Label lblBtnH;
        private System.Windows.Forms.Label lblBtnQ;
        private System.Windows.Forms.Label lblBtnK;
        private System.Windows.Forms.Label lblBtnR;
        private System.Windows.Forms.Label lblBtnI;
        private System.Windows.Forms.Label lblBtnM;
        private System.Windows.Forms.Label lblBtnL;
        private System.Windows.Forms.Label lblBtnO;
        private System.Windows.Forms.Label lblBtnN;
        private System.Windows.Forms.Label lblBtnP;
        private System.Windows.Forms.Label lblBtnU;
        private System.Windows.Forms.Label lblBtnV;
        private System.Windows.Forms.Label lblBtnS;
        private System.Windows.Forms.Label lblBtnT;
        private System.Windows.Forms.Label lblBtnY;
        private System.Windows.Forms.Label lblBtnZ;
        private System.Windows.Forms.Label lblBtnW;
        private System.Windows.Forms.Label lblBtnX;
        private System.Windows.Forms.Panel palTop;
        private System.Windows.Forms.PictureBox pBtnNextPage;
        private System.Windows.Forms.PictureBox pic_BtnTurnBack;
        private System.Windows.Forms.PictureBox pBtnLastPage;
        private System.Windows.Forms.Label lblThisPage;
        private System.Windows.Forms.Label lblTotalPage;
        private System.Windows.Forms.Panel palBottom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.Panel palDGVArea;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
    }
}