﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Yunlib.DAL
{
    public class BorrowDAL
    {
        /// <summary>
        /// 获取书名
        /// </summary>
        /// <returns></returns>
        public DataTable GetBookName()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat(@"SELECT TOP 50 * FROM [dbo].[读者库]");
            DataTable dt = SqlServerHelper.ExecuteDataTable(strSql.ToString());
            return dt;
        }
    }
}
