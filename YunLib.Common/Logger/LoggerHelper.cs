﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.IO;
using System.Diagnostics;

namespace Yunlib.Common
{
    public class LoggerHelper
    {
        static log4net.ILog loginfo;
        static log4net.ILog logerror;
        static log4net.ILog logoprtator;

        [DebuggerStepThrough]
        static LoggerHelper()
        {
            Assembly asm = Assembly.GetExecutingAssembly();//读取嵌入式资源
            using (Stream sm = asm.GetManifestResourceStream("Yunlib.Common.Logger.log4net.config"))
            {
                log4net.Config.XmlConfigurator.Configure(sm);
            }

            loginfo = log4net.LogManager.GetLogger("loginfo");
            logerror = log4net.LogManager.GetLogger("logerror");
            logoprtator = log4net.LogManager.GetLogger("logoperator");
        }

        [DebuggerStepThrough]
        public static void Error(string ErrorMsg, Exception ex = null)
        {
            if (ex != null)
            {
                logerror.Error(ErrorMsg, ex);
            }
            else
            {
                logerror.Error(ErrorMsg);
            }
        }

        [DebuggerStepThrough]
        public static void Info(string Msg, Exception ex = null)
        {
            if (ex != null)
            {
                loginfo.Info(Msg, ex);
            }
            else
            {
                loginfo.Info(Msg);
            }
        }

        [DebuggerStepThrough]
        public static void LogOperator(string Msg, Exception ex = null)
        {
            if (ex != null)
            {
                logoprtator.Info(Msg, ex);
            }
            else
            {
                logoprtator.Info(Msg);
            }
        }
    }
}