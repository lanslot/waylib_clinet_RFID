﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YunLib.Common;

namespace Yunlib.Extensions
{
   public class CodeOptimize
    {

        public static void OpenDoor(int doorId, int doorCode)
        {
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);
                consoleModel.OpenDoor(doorId, doorCode, 2);
            }
        }
        
    }
}
