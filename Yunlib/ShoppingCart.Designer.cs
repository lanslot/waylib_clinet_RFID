﻿namespace Yunlib
{
    partial class ShoppingCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTotalPage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblThisPage = new System.Windows.Forms.Label();
            this.picBtnLastPage = new System.Windows.Forms.PictureBox();
            this.picBtnNextPage = new System.Windows.Forms.PictureBox();
            this.picBtnClear = new System.Windows.Forms.PictureBox();
            this.picBtnYJBorror = new System.Windows.Forms.PictureBox();
            this.picBtnReturn = new System.Windows.Forms.PictureBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picBtnLastPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnNextPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnYJBorror)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTotalPage
            // 
            this.lblTotalPage.AutoSize = true;
            this.lblTotalPage.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPage.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblTotalPage.ForeColor = System.Drawing.Color.White;
            this.lblTotalPage.Location = new System.Drawing.Point(182, 653);
            this.lblTotalPage.Name = "lblTotalPage";
            this.lblTotalPage.Size = new System.Drawing.Size(0, 31);
            this.lblTotalPage.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(156, 653);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 31);
            this.label2.TabIndex = 3;
            // 
            // lblThisPage
            // 
            this.lblThisPage.AutoSize = true;
            this.lblThisPage.BackColor = System.Drawing.Color.Transparent;
            this.lblThisPage.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblThisPage.ForeColor = System.Drawing.Color.White;
            this.lblThisPage.Location = new System.Drawing.Point(125, 653);
            this.lblThisPage.Name = "lblThisPage";
            this.lblThisPage.Size = new System.Drawing.Size(0, 31);
            this.lblThisPage.TabIndex = 4;
            // 
            // picBtnLastPage
            // 
            this.picBtnLastPage.BackColor = System.Drawing.Color.Transparent;
            this.picBtnLastPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnLastPage.Image = global::Yunlib.Properties.Resources.shangyiye;
            this.picBtnLastPage.Location = new System.Drawing.Point(54, 647);
            this.picBtnLastPage.Name = "picBtnLastPage";
            this.picBtnLastPage.Size = new System.Drawing.Size(50, 45);
            this.picBtnLastPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnLastPage.TabIndex = 24;
            this.picBtnLastPage.TabStop = false;
            this.picBtnLastPage.Click += new System.EventHandler(this.picBtnLastPage_Click);
            // 
            // picBtnNextPage
            // 
            this.picBtnNextPage.BackColor = System.Drawing.Color.Transparent;
            this.picBtnNextPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnNextPage.Image = global::Yunlib.Properties.Resources.xiayiye;
            this.picBtnNextPage.Location = new System.Drawing.Point(232, 647);
            this.picBtnNextPage.Name = "picBtnNextPage";
            this.picBtnNextPage.Size = new System.Drawing.Size(50, 45);
            this.picBtnNextPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnNextPage.TabIndex = 25;
            this.picBtnNextPage.TabStop = false;
            this.picBtnNextPage.Click += new System.EventHandler(this.picBtnNextPage_Click);
            // 
            // picBtnClear
            // 
            this.picBtnClear.BackColor = System.Drawing.Color.Transparent;
            this.picBtnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnClear.Image = global::Yunlib.Properties.Resources.qingkong;
            this.picBtnClear.Location = new System.Drawing.Point(463, 646);
            this.picBtnClear.Name = "picBtnClear";
            this.picBtnClear.Size = new System.Drawing.Size(160, 53);
            this.picBtnClear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnClear.TabIndex = 25;
            this.picBtnClear.TabStop = false;
            this.picBtnClear.Click += new System.EventHandler(this.picBtnClear_Click);
            // 
            // picBtnYJBorror
            // 
            this.picBtnYJBorror.BackColor = System.Drawing.Color.Transparent;
            this.picBtnYJBorror.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnYJBorror.Image = global::Yunlib.Properties.Resources.yijianjieyue;
            this.picBtnYJBorror.Location = new System.Drawing.Point(650, 646);
            this.picBtnYJBorror.Name = "picBtnYJBorror";
            this.picBtnYJBorror.Size = new System.Drawing.Size(160, 53);
            this.picBtnYJBorror.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnYJBorror.TabIndex = 25;
            this.picBtnYJBorror.TabStop = false;
            this.picBtnYJBorror.Click += new System.EventHandler(this.picBtnYJBorror_Click);
            // 
            // picBtnReturn
            // 
            this.picBtnReturn.BackColor = System.Drawing.Color.Transparent;
            this.picBtnReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnReturn.Image = global::Yunlib.Properties.Resources.btn_ReturnBack11;
            this.picBtnReturn.Location = new System.Drawing.Point(839, 646);
            this.picBtnReturn.Name = "picBtnReturn";
            this.picBtnReturn.Size = new System.Drawing.Size(160, 53);
            this.picBtnReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnReturn.TabIndex = 25;
            this.picBtnReturn.TabStop = false;
            this.picBtnReturn.Click += new System.EventHandler(this.picBtnReturn_Click);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(26, 126);
            this.listView1.Name = "listView1";
            this.listView1.Scrollable = false;
            this.listView1.Size = new System.Drawing.Size(957, 480);
            this.listView1.TabIndex = 26;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listView1_ItemCheck);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(100, 100);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ShoppingCart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.picBtnReturn);
            this.Controls.Add(this.picBtnYJBorror);
            this.Controls.Add(this.picBtnClear);
            this.Controls.Add(this.picBtnNextPage);
            this.Controls.Add(this.picBtnLastPage);
            this.Controls.Add(this.lblTotalPage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblThisPage);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ShoppingCart";
            this.Text = "ShoppingCart";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ShoppingCart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBtnLastPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnNextPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnYJBorror)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnReturn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotalPage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblThisPage;
        private System.Windows.Forms.PictureBox picBtnLastPage;
        private System.Windows.Forms.PictureBox picBtnNextPage;
        private System.Windows.Forms.PictureBox picBtnClear;
        private System.Windows.Forms.PictureBox picBtnYJBorror;
        private System.Windows.Forms.PictureBox picBtnReturn;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ImageList imageList1;
    }
}