﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YunLib.Common;

namespace Yunlib.DAL
{
    public class ReaderInfoDAL
    {
        SqlConnection conn = new SqlConnection(SqlServerHelper.ConnectionString);
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="rdid">读者条码</param>
        /// <param name="rdpwd">读者登录密码</param>
        /// <returns></returns>
        public string GetUserNo(string rdid)
        {
            try
            {
                StringBuilder sqlStr = new StringBuilder();
                sqlStr.AppendFormat(@" SELECT OUTID FROM BASE_CUSTOMERS");
                sqlStr.AppendFormat(@" WHERE");
                sqlStr.AppendFormat(@" (SCARDSNR='{0}')", rdid);

                IDataReader dr = SqlServerHelper.ExecuteReader(sqlStr.ToString(), conn);
                string str = "";
                if (dr.Read())
                {
                    str = dr["OUTID"].ToString();
                }
                return str;
            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return "";
            }
            finally
            {
                conn.Close();
            }
        }



        
    }
}
