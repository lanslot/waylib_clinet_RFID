﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.Common;

namespace YunLib_Vertical
{
    public partial class RegisterForm : Form
    {
        private bool ISNeedReadCard = true;

        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;

        private JPForm jp;
        public RegisterForm()
        {
            InitializeComponent();
            jp = new JPForm();
        }

       

       

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtPhone.Text))
            {
                lblMsg.Text = "手机号码不能为空";
                return;
            }
            pictureBox1.Image = Properties.Resources.duanxinD;
            string str = "SMS_84685375";
            var timeStamp = DateTime.Now.Ticks.ToString();
            var signStr = "{0}{1}{2}{3}".FormatWith(str, timeStamp, txtPhone.Text, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}public/users/sendSMSCode?smsType={1}&uMobile={2}&timeStamp={3}&sign={4}".FormatWith(servicePath, str, txtPhone.Text, timeStamp, sign);
            var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();

            pictureBox1.Enabled = false;
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }

        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            this.Close();
            frm.Show();
        }

        

        #region 按钮样式
private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBox2.Image = Properties.Resources.sure2;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox2.Image = Properties.Resources.sure1;
        }
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Properties.Resources.duanxinD;
        }

        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                int time = int.Parse(lblCount.Text);
                time--;
                lblCount.Text = time.ToString();
                if (time <= 0)
                {
                    MainForm frm = new MainForm();
                    frm.MdiParent = ParentForm;
                    frm.Parent = this.Parent;
                    frm.Dock = DockStyle.Fill;

                    this.Close();

                    frm.Show();
                }

                if (ISNeedReadCard)
                {
                    List<string> list = HDReadCard.ReadIDCardID(ConfigManager.Pos, ConfigManager.IMPort);
                    if (list.Count > 0)
                    {
                        ISNeedReadCard = false;
                        lblRealName.Text = list[0];
                        lblSex.Text = list[1];
                        lblIDCard.Text = list[2];
                        lblAddress.Text = list[3];
                        picZP.ImageLocation = list[5];
                        lblPWD.Text = list[2].Substring(8, 6);
                        if (!string.IsNullOrWhiteSpace(lblMsg.Text))
                        {
                            lblMsg.Text = "";
                        }

                    }
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs("注册timer出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtPhone.Text))
                {
                    lblMsg.Text = "手机号码不能为空";
                }
                else if (string.IsNullOrWhiteSpace(txtYZM.Text))
                {
                    lblMsg.Text = "验证码不能为空";
                }
                else
                {
                    {
                        Func<string> longTask = new Func<string>(delegate ()
                        {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return getData(lblAddress.Text, lblIDCard.Text, lblSex.Text, lblRealName.Text, txtPhone.Text, txtYZM.Text);
                        //  返回任务结果：5
                    });
                        //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                        //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                        longTask.BeginInvoke(ar =>
                        {
                        //  使用EndInvoke获取到任务结果（5）
                        string result = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                            {
                                Invoke(new Action(() =>

                               ReturnData(result)

                                ));
                            }
                        }, null);
                    }
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs("注册请求报错", ex.StackTrace, LogFile.Error);
            }
        }
        public string getData(string address, string dzCode, string gender, string name, string phone, string yzm)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();
                var signStr = "{0}{1}{2}{3}{4}{5}".FormatWith(dzCode, machineId, yzm, timeStamp, phone, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());
                LogManager.WriteLogs("注册请求", "加密前signStr:" + signStr + "加密后 sign:" + sign, LogFile.Record);
                var url = "{0}public/idCard/register?address={1}&dzCode={2}&gender={3}&machineId={4}&realName={5}&uMobile={6}&smsCode={7}&timeStamp={8}&sign={9}".FormatWith(servicePath, address, dzCode, gender, machineId, name, phone, yzm, timeStamp, sign);
                LogManager.WriteLogs("注册请求url", url, LogFile.Record);
                var res = WebHelper.HttpWebRequest(url, "", true);
                return res;
               
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void ReturnData(string result)
        {
            if (string.IsNullOrWhiteSpace(result)){
                FailForm frm = new FailForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                frm.Title = "注册失败";
                frm.Message = "网络错误，请稍后再试";
                this.Close();
                frm.Show();
            }
            var res = result.ToJObject();
            if (res["code"].ToString() == "100")
            {
                bool flag = res["data"]["foregift"].ToBool(); ;
                if (flag && !string.IsNullOrWhiteSpace(res["data"]["codeUrl"].ToString()))
                {
                    // res["data"]["codeUrl"].ToString(), res["data"]["outTradeNo"].ToString()


                    //PayForm frm = new PayForm { MdiParent = ParentForm, Dock = DockStyle.Fill };
                    //frm.url = res["data"]["codeUrl"].ToString();
                    //frm.outTradeNo = res["data"]["outTradeNo"].ToString();
                    //frm.money = res["data"]["money"].ToString();
                    //this.Close();
                    //frm.Show();

                }
                else
                {
                    SuccessForm frm = new SuccessForm();
                   
                    frm.MdiParent = this.ParentForm;
                    frm.Parent = this.Parent;
                    frm.Title = "注册成功";

                    this.Close();
                    frm.Show();
                }
            }
            else
            {
                FailForm frm = new FailForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                frm.Title = "注册失败";
                frm.Message = res["msg"].ToString();
                this.Close();
                frm.Show();

            }
        }

        private void txtPhone_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox tb = sender as TextBox;
            jp.Location = new Point(155, 1100);
            jp.Show();
            tb.Focus();

            this.Activate();
        }
    }
}
