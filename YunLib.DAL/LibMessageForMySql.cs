﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.Common;

namespace YunLib.DAL
{
   public class LibMessageForMySql
    {
        /// <summary>
        /// 获取流通数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_FACT_LOAN(DateTime start,DateTime end)
        {
            long startInt = CustomExtensions.ConvertDateTimeInt(start) / 1000;
            long endInt = CustomExtensions.ConvertDateTimeInt(end) / 1000;
            string sql = @"SELECT CASE lend_status WHEN 1 THEN '30001' WHEN 2 THEN '30002' ELSE '30003' END AS lendstatus ,dz_code,op_user,book_id,barcode,site_name,tsg_site_code,DATE_FORMAT(FROM_UNIXTIME(add_time),'%Y-%m-%d %H:%i:%s')as addtime,
  DATE_FORMAT(FROM_UNIXTIME(must_time), '%Y-%m-%d %H:%i:%s') as musttime,DATE_FORMAT(FROM_UNIXTIME(IF(end_time > 0, end_time, add_time)), '%Y-%m-%d %H:%i:%s') as realTime from lib_lend where IF(end_time>0,end_time,add_time) > {0} and IF(end_time>0,end_time,add_time) < {1}".FormatWith(startInt,endInt);
            
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:"+dt.Rows.Count+"行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取馆藏数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_HOLDING(DateTime start, DateTime end)
        {
            long startInt = CustomExtensions.ConvertDateTimeInt(start) / 1000;
            long endInt = CustomExtensions.ConvertDateTimeInt(end) / 1000;
            string sql = @"SELECT book_id,barcode,calino,DATE_FORMAT(FROM_UNIXTIME(add_time),'%Y-%m-%d %H:%i:%s')as addtime,IF(edit_time>0,DATE_FORMAT(FROM_UNIXTIME(edit_time),'%Y-%m-%d %H:%i:%s'),edit_time)as edittime,tsg_code_has,tsg_site_code_has,tsg_code,tsg_site_code,price from lib_dck where add_time > {0} and add_time<{1}".FormatWith(startInt, endInt);
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;

        }

        /// <summary>
        /// 获取读者数据
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_READER(DateTime start,DateTime end)
        {
            long startInt = CustomExtensions.ConvertDateTimeInt(start) / 1000;
            long endInt = CustomExtensions.ConvertDateTimeInt(end) / 1000;
            string sql = @"SELECT dz_code,real_name, CASE gender WHEN '女' THEN '0' WHEN '男' THEN '1' ELSE '2' END AS sex,IF(TRIM(cred_num) != '',LEFT(cred_num,6) ,'') AS crednum,DATE_FORMAT(FROM_UNIXTIME(dz_add_time),'%Y-%m-%d %H:%i:%s')as add_time,DATE_FORMAT(FROM_UNIXTIME(beg_time),'%Y-%m-%d %H:%i:%s')as begtime,birthday,op_user,CASE dz_status WHEN '有效' THEN '1' WHEN '挂失' THEN '3' WHEN '注销' THEN '4' WHEN '暂停' THEN '5' ELSE '6' END AS dzstatus FROM lib_dz where dz_add_time > {0} and dz_add_time<{1}".FormatWith(startInt, endInt);
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取书目信息
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public DataTable Get_BIBLIOS(DateTime start, DateTime end)
        {
            long startInt = CustomExtensions.ConvertDateTimeInt(start) / 1000;
            long endInt = CustomExtensions.ConvertDateTimeInt(end) / 1000;
            string sql = @"SELECT book_id,title,firstauthor,isbn,publisher,pubdate,price_ms,doctype FROM lib_book  where catatime > {0} and catatime<{1}".FormatWith(startInt, endInt);
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

        /// <summary>
        /// 获取分馆信息
        /// </summary>
        /// <returns></returns>
        public DataTable Get_P_LIBCODE()
        {
            
            string sql = @"SELECT tsg_code,tsg_name,tsg_name as tsgName FROM lib_tsg";
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }

       /// <summary>
       /// 获取馆藏地点
       /// </summary>
       /// <returns></returns>
        public DataTable Get_P_LOCAL()
        {
            string sql = @"SELECT tsg_site_code,tsg_code,site_name FROM lib_tsg_site";
            DataTable dt = NewMySqlHelper.ExecuteDataTable(sql);
            LogManager.WriteLogs("sql:" + dt.Rows.Count + "行数据", sql, LogFile.SQL);
            return dt;
        }


    }
}
