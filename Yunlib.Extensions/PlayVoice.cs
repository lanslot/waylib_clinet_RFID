﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib.Extensions
{
    /// <summary>
    /// 播放声音
    /// </summary>
    public class PlayVoice
    {
        public static void play(string fileName)
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = Application.StartupPath + @"\Voice\"+ fileName+".wav";

            player.Load();
            player.Play();
        }
    }
}
