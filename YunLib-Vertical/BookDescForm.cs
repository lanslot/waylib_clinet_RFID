﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.Common;

namespace YunLib_Vertical
{
    public partial class BookDescForm : Form
    {
       
        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;
        public int DoorNum { get; set; }

        public delegate void GetDescHandel(string machLatNum);
        GetDescHandel md = null;
        public BookDescForm()
        {
            InitializeComponent();
            this.md = new GetDescHandel(GetBookDesc);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }


        private void BookDescForm_Load(object sender, EventArgs e)
        {
            PlayVoice.play(this.Name);
            Invoke(md, DoorNum + "");
        }

        private void GetBookDesc(string machLatNum)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}{4}".FormatWith(libraryId, machineId, machLatNum, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/query/desc?machId={1}&machLatNum={2}&libraryId={3}&timeStamp={4}&sign={5}".FormatWith(
                    servicePath, machineId, machLatNum, libraryId, timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                if (res["code"].ToString() == "100")
                {
                    var data = res["data"];
                    lblAuthor.Text = data["author"].ToString();
                    lblBName.Text = data["title"].ToString();
                    lblPublishTime.Text = data["pubdate"].ToString();
                    lblPulisher.Text = data["publisher"].ToString();
                    lblAbstracts.Text = data["abstracts"].ToString();
                    lblMachLatNum.Text = DoorNum + "";
                    pictureBox1.ImageLocation = data["coverUrl"].ToString();
                    lblBarcode.Text = data["bookId"].ToString();
                    lblType.Text = data["type"].ToString();
                   

                }
                else
                {
                    LogManager.BorrowRecord("{0}--BookDesc请求错误返回：{1}".FormatWith(DateTime.Now, res.ToString()));
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("BookDesc请求报错", ex.Message,LogFile.Error);
            }
        }

        private void pBtnBorrow_Click(object sender, EventArgs e)
        {
            int borrowTyoe = ConfigManager.BorrowType;
           
            timer1.Enabled = false;
           
            BorrowBookForm rdl = new BorrowBookForm { MdiParent = ParentForm, Parent = Parent, BarCode = lblBarcode.Text, DoorNum = this.lblMachLatNum.Text, BorrowType = lblType.Text };

            this.Close();

            rdl.Show();
           
        }

      

        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }


        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            lblCount.Text = time.ToString();
            if (time <= 0)
            {
                MainForm frm = new MainForm();
                frm.MdiParent = ParentForm;
                frm.Parent = this.Parent;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void BookDescForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;
        }


        #region 按钮样式 

        private void picBorrow_MouseUp(object sender, MouseEventArgs e)
        {
            picBorrow.Image = Properties.Resources.jieyue_w;
        }

        private void picBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            picBorrow.Image = Properties.Resources.jieyue_x;
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }
        #endregion
    }
}
