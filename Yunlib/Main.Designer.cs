﻿using System.Windows.Forms;

namespace Yunlib
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pBtnBorrow = new System.Windows.Forms.PictureBox();
            this.pBtnBorrowOnceOrReturn = new System.Windows.Forms.PictureBox();
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.CreateQRCode = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tmLastOp = new System.Windows.Forms.Timer(this.components);
            this.lblLastOp = new System.Windows.Forms.Label();
            this.picOpenCode = new System.Windows.Forms.PictureBox();
            this.lblNetTest = new System.Windows.Forms.Label();
            this.picRegister = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrowOnceOrReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegister)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(7, 14);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 30);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(729, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(258, 57);
            this.panel1.TabIndex = 4;
            // 
            // pBtnBorrow
            // 
            this.pBtnBorrow.BackColor = System.Drawing.Color.Transparent;
            this.pBtnBorrow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBtnBorrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnBorrow.Image = global::Yunlib.Properties.Resources.借666;
            this.pBtnBorrow.Location = new System.Drawing.Point(180, 329);
            this.pBtnBorrow.Name = "pBtnBorrow";
            this.pBtnBorrow.Size = new System.Drawing.Size(290, 204);
            this.pBtnBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnBorrow.TabIndex = 2;
            this.pBtnBorrow.TabStop = false;
            this.pBtnBorrow.WaitOnLoad = true;
            this.pBtnBorrow.Click += new System.EventHandler(this.pBtnBorrow_Click);
            this.pBtnBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnBorrow_MouseDown);
            // 
            // pBtnBorrowOnceOrReturn
            // 
            this.pBtnBorrowOnceOrReturn.BackColor = System.Drawing.Color.Transparent;
            this.pBtnBorrowOnceOrReturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBtnBorrowOnceOrReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnBorrowOnceOrReturn.Image = global::Yunlib.Properties.Resources.还666;
            this.pBtnBorrowOnceOrReturn.Location = new System.Drawing.Point(570, 329);
            this.pBtnBorrowOnceOrReturn.Name = "pBtnBorrowOnceOrReturn";
            this.pBtnBorrowOnceOrReturn.Size = new System.Drawing.Size(290, 204);
            this.pBtnBorrowOnceOrReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnBorrowOnceOrReturn.TabIndex = 2;
            this.pBtnBorrowOnceOrReturn.TabStop = false;
            this.pBtnBorrowOnceOrReturn.WaitOnLoad = true;
            this.pBtnBorrowOnceOrReturn.Click += new System.EventHandler(this.pBtnBorrowOnceOrReturn_Click);
            this.pBtnBorrowOnceOrReturn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnBorrowOnceOrReturn_MouseDown);
            // 
            // timerLogin
            // 
            this.timerLogin.Enabled = true;
            this.timerLogin.Interval = 2000;
            this.timerLogin.Tick += new System.EventHandler(this.timerLogin_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Location = new System.Drawing.Point(13, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 67);
            this.panel2.TabIndex = 9;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(50, 198);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 17);
            this.label1.TabIndex = 10;
            this.label1.Visible = false;
            // 
            // CreateQRCode
            // 
            this.CreateQRCode.Interval = 600000;
            this.CreateQRCode.Tick += new System.EventHandler(this.CreateQRCode_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox2.Location = new System.Drawing.Point(699, 39);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // tmLastOp
            // 
            this.tmLastOp.Enabled = true;
            this.tmLastOp.Interval = 1000;
            this.tmLastOp.Tick += new System.EventHandler(this.tmLastOp_Tick);
            // 
            // lblLastOp
            // 
            this.lblLastOp.AutoSize = true;
            this.lblLastOp.BackColor = System.Drawing.Color.Transparent;
            this.lblLastOp.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblLastOp.ForeColor = System.Drawing.Color.Red;
            this.lblLastOp.Location = new System.Drawing.Point(368, 39);
            this.lblLastOp.Name = "lblLastOp";
            this.lblLastOp.Size = new System.Drawing.Size(0, 17);
            this.lblLastOp.TabIndex = 43;
            this.lblLastOp.Visible = false;
            // 
            // picOpenCode
            // 
            this.picOpenCode.BackColor = System.Drawing.Color.Transparent;
            this.picOpenCode.Image = global::Yunlib.Properties.Resources.kaiguima1;
            this.picOpenCode.Location = new System.Drawing.Point(35, 601);
            this.picOpenCode.Name = "picOpenCode";
            this.picOpenCode.Size = new System.Drawing.Size(144, 138);
            this.picOpenCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picOpenCode.TabIndex = 36;
            this.picOpenCode.TabStop = false;
            this.picOpenCode.Click += new System.EventHandler(this.picOpenCode_Click);
            this.picOpenCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picOpenCode_MouseDown);
            // 
            // lblNetTest
            // 
            this.lblNetTest.AutoSize = true;
            this.lblNetTest.BackColor = System.Drawing.Color.Transparent;
            this.lblNetTest.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblNetTest.ForeColor = System.Drawing.Color.Red;
            this.lblNetTest.Location = new System.Drawing.Point(333, 234);
            this.lblNetTest.Name = "lblNetTest";
            this.lblNetTest.Size = new System.Drawing.Size(0, 31);
            this.lblNetTest.TabIndex = 44;
            // 
            // picRegister
            // 
            this.picRegister.BackColor = System.Drawing.Color.Transparent;
            this.picRegister.Image = global::Yunlib.Properties.Resources._417674956899281670;
            this.picRegister.Location = new System.Drawing.Point(210, 601);
            this.picRegister.Name = "picRegister";
            this.picRegister.Size = new System.Drawing.Size(144, 138);
            this.picRegister.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picRegister.TabIndex = 46;
            this.picRegister.TabStop = false;
            this.picRegister.Visible = false;
            this.picRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.DarkGray;
            this.label2.Location = new System.Drawing.Point(524, 723);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(463, 25);
            this.label2.TabIndex = 48;
            this.label2.Text = "客服电话：四川云图信息技术有限公司 400 028 0339";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(318, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 46);
            this.label4.TabIndex = 53;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(12, 415);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(146, 138);
            this.btnLogin.TabIndex = 47;
            this.btnLogin.Text = "人 脸 绑 定";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Visible = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.background888;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.picRegister);
            this.Controls.Add(this.lblNetTest);
            this.Controls.Add(this.lblLastOp);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.picOpenCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pBtnBorrowOnceOrReturn);
            this.Controls.Add(this.pBtnBorrow);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrowOnceOrReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOpenCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRegister)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Panel panel1;
        private PictureBox pBtnBorrow;
        private PictureBox pBtnBorrowOnceOrReturn;
        private Timer timerLogin;
        private Panel panel2;
        private Label label1;
        private Timer CreateQRCode;
        private PictureBox pictureBox2;
        private Timer tmLastOp;
        private Label lblLastOp;
        private PictureBox picOpenCode;
        private Label lblNetTest;
        private PictureBox picRegister;
        private Label label2;
        private Label label4;
        private Button btnLogin;
    }
}