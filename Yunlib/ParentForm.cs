﻿using System;
using YunLib.BLL;
using System.Data;
using System.Text;
using YunLib.Common;
using Yunlib.Common;
using System.Threading;
using System.Diagnostics;
using Yunlib.Extensions;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System.Runtime.InteropServices;
using System.Configuration;
using Yunlib.Entity;
using System.Collections.Generic;
using System.IO;

namespace Yunlib
{
    
    public partial class ParentForm : Form
    {
        private const int GWL_STYLE = -16;
        private const int GWL_EXSTYLE = -20;
        private const int WS_BORDER = 0x00800000;
        private const int WS_EX_CLIENTEDGE = 0x00000200;
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOZORDER = 0x0004;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_NOOWNERZORDER = 0x0200;


        // Win32 方法    
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetWindowLong(IntPtr hWnd, int Index);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SetWindowLong(IntPtr hWnd, int Index, int Value);

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter,
            int X, int Y, int cx, int cy, uint uFlags);

        private MySocket ms = new MySocket("119.23.251.40", 6001);
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        public ParentForm()
        {
            InitializeComponent();
            ms.onReceiveMessage += onReceiveMessage;
        }

        public static int IsInitOpen = 0;
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        
        public void ParentFrom_Load(object sender, EventArgs e)
        {
            try
            {
                //去调mdi边框
                for (int i = 0; i < this.Controls.Count; i++)
                {
                    var mdiClientForm = this.Controls[i] as MdiClient;
                    if (mdiClientForm == null) continue;
                    // 找到了mdi客户区    
                    // 取得客户区的边框    
                    int style = GetWindowLong(mdiClientForm.Handle, GWL_STYLE);
                    int exStyle = GetWindowLong(mdiClientForm.Handle, GWL_EXSTYLE);
                    style &= ~WS_BORDER;
                    exStyle &= ~WS_EX_CLIENTEDGE;

                    // 调用win32设定样式    
                    SetWindowLong(mdiClientForm.Handle, GWL_STYLE, style);
                    SetWindowLong(mdiClientForm.Handle, GWL_EXSTYLE, exStyle);

                    // 更新客户区    
                    SetWindowPos(mdiClientForm.Handle, IntPtr.Zero, 0, 0, 0, 0,
                        SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                        SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
                    UpdateStyles();
                    break;
                }

                DateTime start = DateTime.Now;
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;




                Main mainFrm = new Main();
                mainFrm.MdiParent = this;
                mainFrm.Show();
                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("ParentForm_Load 运行时间{0}".FormatWith(ts.TotalSeconds));

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                DialogResult dr = MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
               
                if (dr == DialogResult.OK)
                {
                    //通过进程名关闭进程
                    CodeOptimize.KillProcess("Yunlib");
                }
                else
                {
                    CodeOptimize.KillProcess("RsYunlib");
                    CodeOptimize.KillProcess("Yunlib");
                }

            }
        }

        
        int i = 0;
        public static int isOpenLight = 0;
        public static bool isNeedInit = true;
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string[] s = new string[1];
            s[0] = ConfigManager.PingServicePath;

            bool flag = NetTest.CheckServeStatus(s);
            if (dt.Hour == 1 && dt.Minute == 0 && dt.Second == 0)
            {
                Application.Exit();
            }
             
            if (flag && isNeedInit)
            {
                //  MessageBox.Show("初始化InitZookeeper");
                bool initOk = ZooKeeperClient.createNode();
             //   bool initOk = ms.socket_receive();
                if (initOk)
                {
                    isNeedInit = false;
                    //timer2.Enabled = true;
                    //timer2.Start();
                    LogManager.WriteLogs("创建节点成功！", DateTime.Now.ToString() + ":创建节点成功！", LogFile.Record);
                }
            }

            i++;
            if (i >= 10)
            {
               // ClearMemory();
                i = 0;
            }
        }

        #region 内存回收
        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize")]
        public static extern int SetProcessWorkingSetSize(IntPtr process, int minSize, int maxSize);
        /// <summary>
        /// 释放内存
        /// </summary>
        public static void ClearMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
        }
        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;

            if(time.ToShortTimeString() == "09:00:00" && time.Day == 1) {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                DirectoryInfo directory = new System.IO.DirectoryInfo(LogAddress);
                directory.Delete(true);

            }

            //string str = "03-" + time.Ticks;
            //ms.socket_send(str);

        } 

        private void onReceiveMessage(object source, string rDataStr)
        {
            //   string barCode = Encoding.ASCII.GetString(scanFrame);

            if (!string.IsNullOrWhiteSpace(rDataStr))
            {
                string[] dataArr = rDataStr.Split('-');
                LogManager.WriteLogs("Socket接收信息", rDataStr, LogFile.Record);

                switch (dataArr[0])
                {
                    case "1":
                        try
                        {
                            //借书成功打开柜门
                            HttpBorrSuccess(rDataStr, dataArr);
                        }
                        catch (Exception ex)
                        {
                            LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                        }
                        break;
                    case "2":
                        try
                        {
                            //还书成功打开柜门
                            HttpReturnSuccess(dataArr);
                        }
                        catch (Exception ex)
                        {
                            LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                        }
                        break;
                    case "3":
                        HttpOrderBorr(dataArr[1], dataArr[2]);
                        break;
                    //预约还书
                    case "4":
                        HttpOrderReturn(dataArr[1], dataArr[2], dataArr[3]);
                        break;
                    //预约上架
                    case "5":
                        HttpOrderShelves(dataArr[1], dataArr);
                        break;
                    //取消预约借书
                    case "7":
                        HttpCancelOrderBorr(dataArr[1]);
                        break;
                    //取消预约还书
                    case "8":
                        //还书失败 Zookeeper返回的状态
                        HttpCancelOrderReturn(dataArr[1]);
                        break;
                    case "9":
                        //借书失败 Zookeeper返回的状态
                        LogManager.ErrorRecord("【借书】开门失败，错误信息：{0}".FormatWith(rDataStr));
                        break;
                    default:
                        break;
                }
            }
        }


        private void HttpReturnSuccess(string[] sdata)
        {
            //获得空书柜集合
            IList<LocalBooks> currentModel = accessBLL.GetNullBookCase();

            var back = WebHelper.HttpWebRequest("{0}/public/book/bookdesc?bookId={1}".FormatWith(ConfigManager.ServicePath, sdata[1].ToString())).ToJObject();

            if (back["code"].ToString() == "100")
            {
                JToken datas = back["data"];
                LocalBooks model = new LocalBooks
                {
                    BarCode = sdata[1].ToString(),
                    BName = datas["title"].ToString() ?? "",
                    Image = datas["coverUrl"].ToString() ?? "",
                    Introduction = datas["abstract"].ToString() ?? "",
                    Press = datas["publisher"].ToString() ?? "",
                    PublicationDate = datas["pubdate"].ToString() ?? "",
                    Writer = datas["author"].ToString() ?? "",
                    DoorID = currentModel[0].DoorID,
                    DoorNUM = currentModel[0].DoorNUM
                };

                bool res = accessBLL.FillBookToAcess(model);

                if (res)
                {
                    //自动释放资源
                    using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                    {
                        //初始化控制台串口
                        consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                        //打开相应行号的柜门
                        consoleModel.OpenDoor(currentModel[0].DoorID, currentModel[0].DoorCode, 4);

                        LogManager.BorrowRecord("【还书】成功开门，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                    }
                }
                else
                {
                    //Zookeeper返回成功，但是本机执行录入数据
                    LogManager.ErrorRecord("【还书】开门失败，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                }
            }
            else
            {
                //code 返回的不是101
                LogManager.ErrorRecord("【还书】返回信息——code:{0}  msg:{1}  datetime:{2}".FormatWith(back["code"].ToString(), back["msg"].ToString(), DateTime.Now));
            }
        }

        private void HttpBorrSuccess(string rDataStr, string[] sdata)
        {
            //传的多本书，就有逗号隔开
            if (sdata[2].Contains(","))
            {
                List<string> doorNum = new List<string>();
                sdata[2].Split(',').Each(x => doorNum.Add(x.ToString()));
                doorNum.Each(y => BorrBookOpen(rDataStr, y));
            }
            else
            {
                string doorNum = sdata[2];
                BorrBookOpen(rDataStr, doorNum);
            }
        }

        private void HttpOrderBorr(string barCode, string timeStamp)
        {
            try
            {
                barCode = barCode.Replace(ConfigManager.LibraryID, "");
                string isLend = barCode + "_" + timeStamp;
                bool res = accessBLL.UpdateLocalBookbyCode(barCode, isLend);
                if (res)
                {
                    LogManager.BorrowRecord("预约图书成功，图书BarCode:{0},过期日期:{1}".FormatWith(barCode, timeStamp));
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("预约借书错误信息：{0}".FormatWith(ex.Message));
            }
        }

        private void HttpCancelOrderBorr(string barCode)
        {
            barCode = barCode.Replace(ConfigManager.LibraryID, "");
            bool res = accessBLL.UpdateLocalBookbyCode(barCode, "");
            if (res)
            {
                LogManager.BorrowRecord("取消预约图书成功，图书BarCode:{0},取消时间:{1}".FormatWith(barCode, DateTime.Now.ToString()));
            }
        }

        private void HttpOrderReturn(string doorNum, string timeStamp, string barCode)
        {
            string barTime = barCode + "_" + timeStamp;

            bool res = accessBLL.UpdateLocalBookbydoornum(barTime, Convert.ToInt32(doorNum));
            if (res)
            {
                LogManager.BorrowRecord("预约还书成功，预约格子编号:{0},过期日期:{1},书BarCode:{2}".FormatWith(doorNum, timeStamp, barCode));
            }
            else
            {
                LogManager.BorrowRecord("预约还书失败，预约格子编号:{0},过期日期:{1},书BarCode:{2}".FormatWith(doorNum, timeStamp, barCode));
            }
        }

        private void HttpCancelOrderReturn(string doorNUM)
        {
            bool res = accessBLL.UpdateLocalBookbydoornum("", Convert.ToInt32(doorNUM));
            if (res)
            {
                LogManager.BorrowRecord("取消预约还书成功，取消预约格子编号:{0},".FormatWith(doorNUM));
            }
            else
            {
                LogManager.BorrowRecord("取消预约还书失败，取消预约格子编号:{0},".FormatWith(doorNUM));
            }
        }

        private void HttpOrderShelves(string barCode, string[] dataArr)
        {
            try
            {
                FillBooks(barCode);
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【预约上架】预约上架失败，错误信息:{2} --- 预约上架书籍BarCode:{0}，返回信息{1}".FormatWith(barCode, dataArr, ex.Message));
            }
        }

        #region 扩展
        private void BorrBookOpen(string rDataStr, string doorNum)
        {
            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
            
            LocalBooks model = new LocalBooks
            {
                DoorID = list[0].DoorID,
                DoorCode = list[0].DoorCode,
                IsLend = ""
            };


            //自动释放资源
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                //初始化控制台串口
                //consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                ////打开相应行号的柜门
                //consoleModel.OpenDoor(list[0].DoorID, list[0].DoorCode, 2);

                LogManager.BorrowRecord("【借书】成功开门，{0}{1}*****{2},返回的信息：{3}".FormatWith(list[0].DoorID, list[0].DoorCode, DateTime.Now, rDataStr));
                try
                {
                    //ReaderLogin.ScanDoorNum = list[0].DoorNUM;
                    //ReaderLogin.readerLogin.Close();
                    //  this.MdiChildren.Each(f => f.Close());

                    Form[] fs = this.MdiChildren;
                    foreach (var f in fs)
                    {
                        if (f.InvokeRequired)
                            this.Invoke(new Action<Form>(s => { s.Close(); }), f);
                    }

                    BorrSuccess frm = new BorrSuccess(list[0].DoorNUM + "");

                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action<Form>(s => { frm.MdiParent = s; frm.Dock = DockStyle.Fill; frm.timer1.Enabled = true; frm.Show(); }), this);
                    }
                  //  frm.Show();
                }
                catch (Exception ex)
                {
                    LogManager.ErrorRecord("【界面跳转失败】，{0}".FormatWith(ex.Message));
                }
            }

        }

        public void FillBooks(string barCode)
        {
            var list = accessBLL.GetNullBookCase();
            var localModel = accessBLL.GetBookByBarCode(barCode);
            if (string.IsNullOrEmpty(barCode))
            {
                LogManager.BorrowRecord("【预约上架】，BarCode为空！");
            }
            else
            {
                long timeStamp = DateTime.Now.Ticks;
                string signStr = GetSign(barCode, list[0].DoorNUM, timeStamp);
                string urlStr = GetUrl(timeStamp, signStr, barCode, list[0].DoorNUM);

                JObject backData = WebHelper.HttpWebRequest(urlStr, "", true).ToJObject();

                if (backData["code"].ToString() == "100")
                {
                    JToken datas = backData["data"];
                    LocalBooks model = new LocalBooks();
                    model.BarCode = barCode;
                    model.BName = datas["title"].ToString();
                    model.Image = datas["coverUrl"].ToString();
                    model.Introduction = datas["abstracts"].ToString();
                    model.Press = datas["publisher"].ToString();
                    model.PublicationDate = datas["pubdate"].ToString();
                    model.Writer = datas["author"].ToString();
                    model.DoorID = list[0].DoorID;
                    model.DoorCode = list[0].DoorCode;

                    accessBLL.FillBookToAcess(model);

                    CodeOptimize.OpenDoor(model.DoorID, model.DoorCode);

                    LogManager.BorrowRecord("【预约上架】，上架书籍成功,书籍名称：《{0}》".FormatWith(model.BName));
                }
                else
                {
                    LogManager.BorrowRecord("【预约上架】，上架书籍失败,书籍名称：《{0}》".FormatWith(backData["data"]["title"].ToString()));
                }
            }
        }

        private string GetSign(string barCode, int doorNum, long timeStamp)
        {
            StringBuilder signStr = new StringBuilder();
            signStr.Append("{0}".FormatWith(barCode));
            signStr.Append("{0}".FormatWith(ConfigManager.LibraryID));
            signStr.Append("{0}".FormatWith(doorNum));
            signStr.Append("{0}".FormatWith(ConfigManager.MachineID));
            signStr.Append("{0}".FormatWith(timeStamp));
            signStr.Append("{0}".FormatWith(ConfigManager.CommonKey));

            return EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr.ToString()).ToLower());
        }

        private string GetUrl(long timeStamp, string sign, string barCode, int doorNum)
        {
            StringBuilder urlStr = new StringBuilder();
            urlStr.Append("{0}/public/book/syn/shelvesservice".FormatWith(ConfigManager.ServicePath));
            urlStr.Append("?bookId={0}".FormatWith(barCode));
            urlStr.Append("&timeStamp={0}".FormatWith(timeStamp));
            urlStr.Append("&machineId={0}".FormatWith(ConfigManager.MachineID));
            urlStr.Append("&machLatNum={0}".FormatWith(doorNum));
            urlStr.Append("&sign={0}".FormatWith(sign));
            urlStr.Append("&libraryId={0}".FormatWith(ConfigManager.LibraryID));
            return urlStr.ToString();
        }
        #endregion

    }
}
