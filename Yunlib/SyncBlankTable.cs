﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib.Entity;

namespace Yunlib
{
    public partial class SyncBlankTable : Form
    {
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        public SyncBlankTable()
        {
            InitializeComponent();
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitData()
        {
            var servicePath = ConfigManager.ServicePath;
            var libraryId = ConfigManager.LibraryID;
            var commonKey = ConfigManager.CommonKey;

            var timeStamp = DateTime.Now.Ticks;

            var signStr = "{0}{1}{2}".FormatWith(libraryId, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            DataTable dt = BLL.GetMachineInitData();

            string jsonStr = JsonHelper.ToJson(dt).Replace("DoorNUM", "machLatNum").Replace("BarCode", "bookId ");

            JObject jo = new JObject();
            jo.Add("machineId", ConfigManager.MachineID);
            jo.Add("machineLattice", jsonStr);

            JObject joNew = new JObject();
            joNew.Add("json", jo.ToJson());

            JToken jtoken = JToken.Parse(joNew.ToJson());

            string jsonStr1 = "json=" + jtoken.ToString().Replace("\\\\\\\"", "\"").Replace("\\\"", "\"").Replace("\\", "").Replace("r", "").Replace("\"{", "{").Replace("\"[", "[").Replace("\"]", "]").Replace("]\"", "]").Replace("}\"", "}").Replace(" ", "");

            try
            {
                var url = "{0}/public/machine/putMachine?timeStamp={1}&libraryId={2}&sign={3}".FormatWith(servicePath, timeStamp, libraryId, sign);
                var back = WebHelper.HttpWebRequest(url, jsonStr1, Encoding.GetEncoding("utf-8"), true).ToJObject();

                if (back["code"].ToString() == "100")
                {
                    MessageBox.Show("数据同步成功!");
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("数据同步化失败");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("同步失败，失败原因：" + ex.Message);
            }
        }
    }
}
