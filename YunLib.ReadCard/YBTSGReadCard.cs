﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;
using YunLib.Common;
using YunLib.ReadCard.UHF;

namespace YunLib.ReadCard
{
    /// <summary>
    /// 宜宾图书馆读卡功能
    /// </summary>
    public class YBTSGReadCard : IReadCard
    {
        private byte fComAdr = 0xff; //当前操作的ComAdr
        private int ferrorcode;
        private byte fBaud;
        private int fCmdRet = 30; //所有执行指令的返回值
        private byte[] fOperEPC = new byte[100];
        private byte[] fPassWord = new byte[4];
        private byte[] fOperID_6B = new byte[10];
        private int frmcomportindex;
        private byte Target = 0;
        private byte InAnt = 0;
        private byte Scantime = 0;
        private byte FastFlag = 0;
        private byte Qvalue = 0;
        private byte Session = 0;
        private byte TIDFlag = 0;
        public static byte antinfo = 0;
        private int AA_times = 0;
        private string strlog = "";
        public string ReadIDCard(string port, int Baud)
        {
            throw new NotImplementedException();
        }

        public string ReadIM(string port, int Baud)
        {
            throw new NotImplementedException();
        }
        int FrmPortIndex = 0;
        public string ReadRFID(string port, int Baud)
        {
            try
            {
                int portNum = int.Parse(port.Substring(3));
                // int FrmPortIndex = 0;
                string strException = string.Empty;
                fBaud = Convert.ToByte(3);
                if (fBaud > 2)
                    fBaud = Convert.ToByte(fBaud + 2);
                fComAdr = 255;//广播地址打开设备
                fCmdRet = RWDev.OpenComPort(portNum, ref fComAdr, fBaud, ref FrmPortIndex);
                if (fCmdRet != 0)
                {
                    string strLog = "连接读写器失败，失败原因： " + GetReturnCodeDesc(fCmdRet);

                    return "";
                }
                else
                {
                    frmcomportindex = FrmPortIndex;

                }
                return inventory();
            }catch(Exception ex)
            {
                LogManager.BorrowRecord("超高频读条码错误：" + ex.Message);
                return "";
                
            }
        }

        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public static string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        private string GetReturnCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "操作成功";
                case 0x01:
                    return "询查时间结束前返回";
                case 0x02:
                    return "指定的询查时间溢出";
                case 0x03:
                    return "本条消息之后，还有消息";
                case 0x04:
                    return "读写模块存储空间已满";
                case 0x05:
                    return "访问密码错误";
                case 0x09:
                    return "销毁密码错误";
                case 0x0a:
                    return "销毁密码不能为全0";
                case 0x0b:
                    return "电子标签不支持该命令";
                case 0x0c:
                    return "对该命令，访问密码不能为全0";
                case 0x0d:
                    return "电子标签已经被设置了读保护，不能再次设置";
                case 0x0e:
                    return "电子标签没有被设置读保护，不需要解锁";
                case 0x10:
                    return "有字节空间被锁定，写入失败";
                case 0x11:
                    return "不能锁定";
                case 0x12:
                    return "已经锁定，不能再次锁定";
                case 0x13:
                    return "参数保存失败,但设置的值在读写模块断电前有效";
                case 0x14:
                    return "无法调整";
                case 0x15:
                    return "询查时间结束前返回";
                case 0x16:
                    return "指定的询查时间溢出";
                case 0x17:
                    return "本条消息之后，还有消息";
                case 0x18:
                    return "读写模块存储空间已满";
                case 0x19:
                    return "电子不支持该命令或者访问密码不能为0";
                case 0x1A:
                    return "标签自定义功能执行错误";
                case 0xF8:
                    return "检测天线错误";
                case 0xF9:
                    return "命令执行出错";
                case 0xFA:
                    return "有电子标签，但通信不畅，无法操作";
                case 0xFB:
                    return "无电子标签可操作";
                case 0xFC:
                    return "电子标签返回错误代码";
                case 0xFD:
                    return "命令长度错误";
                case 0xFE:
                    return "不合法的命令";
                case 0xFF:
                    return "参数错误";
                case 0x30:
                    return "通讯错误";
                case 0x31:
                    return "CRC校验错误";
                case 0x32:
                    return "返回数据长度有错误";
                case 0x33:
                    return "通讯繁忙，设备正在执行其他指令";
                case 0x34:
                    return "繁忙，指令正在执行";
                case 0x35:
                    return "端口已打开";
                case 0x36:
                    return "端口已关闭";
                case 0x37:
                    return "无效句柄";
                case 0x38:
                    return "无效端口";
                case 0xEE:
                    return "读写器处于主动模式";
                default:
                    return "";
            }
        }
        private string GetErrorCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "其它错误";
                case 0x03:
                    return "存储器超限或不被支持的PC值";
                case 0x04:
                    return "存储器锁定";
                case 0x0b:
                    return "电源不足";
                case 0x0f:
                    return "非特定错误";
                default:
                    return "";
            }
        }

        private string inventory()
        {
            try
            {
                byte Ant = 0;
                int CardNum = 0;
                int Totallen = 0;
                int EPClen, m;
                byte[] EPC = new byte[50000];
                int CardIndex;
                string temps, temp;
                temp = "";
                string sEPC;
                byte MaskMem = 0;
                byte[] MaskAdr = new byte[2];
                byte MaskLen = 0;
                byte[] MaskData = new byte[100];
                byte MaskFlag = 0;
                byte AdrTID = 0;
                byte LenTID = 0;
                AdrTID = 0;
                LenTID = 6;
                MaskFlag = 0;
                int cbtime = System.Environment.TickCount;
                CardNum = 0;
                fCmdRet = RWDev.Inventory_G2(ref fComAdr, Qvalue, Session, MaskMem, MaskAdr, MaskLen, MaskData, MaskFlag, AdrTID, LenTID, TIDFlag, Target, InAnt, Scantime, FastFlag, EPC, ref Ant, ref Totallen, ref CardNum, frmcomportindex);

                 strlog = "询查命令： " + GetReturnCodeDesc(fCmdRet);

                ///////////设置网络断线重连
                if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4))//代表已查找结束，
                {
                    byte[] daw = new byte[Totallen];
                    Array.Copy(EPC, daw, Totallen);
                    temps = ByteArrayToHexString(daw);
                    m = 0;
                    if (CardNum == 0)
                    {
                        if (Session > 1)
                            AA_times = AA_times + 1;
                        CloseCOM();
                        return "";
                    }
                    //antstr = Convert.ToString(Ant, 2).PadLeft(4, '0');
                    for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                    {
                        EPClen = daw[m] + 1;
                        temp = temps.Substring(m * 2 + 2, EPClen * 2);
                        sEPC = temp.Substring(0, temp.Length - 2);
                        string RSSI = Convert.ToInt32(temp.Substring(temp.Length - 2, 2), 16).ToString();
                        m = m + EPClen + 1;
                        if (sEPC.Length != (EPClen - 1) * 2)
                        {
                            CloseCOM();
                            return "";
                        }
                        //  text_WriteData.Text = sEPC;
                        int len = int.Parse(sEPC.Substring(0, 2));
                        string newEpc = sEPC.Substring(2, len);
                        byte[] bEpc = HexStringToByteArray(newEpc);
                        //text_WriteData.Text =  Encoding.ASCII.GetString(bEpc);
                        string strlog = "读标签成功，值：" + Encoding.ASCII.GetString(bEpc);
                        // WriteLog(lrtxtLog, strlog, 0);
                        CloseCOM();
                        return Encoding.ASCII.GetString(bEpc);
                    }
                }
                CloseCOM();
                return "";
            }catch(Exception ex)
            {
                CloseCOM();
                LogManager.WriteLogs("超高频读卡错误", ex.Message, LogFile.Error);
                return "";
            }
            finally
            {
                LogManager.WriteLogs("读卡记录", strlog, LogFile.Record);
            }

        }

        private void CloseCOM()
        {
            if (frmcomportindex > 0)
                fCmdRet = RWDev.CloseSpecComPort(frmcomportindex);
            if (fCmdRet == 0) frmcomportindex = -1;
        }

        public string ReadUHFRFID(string port, int Baud)
        {
            try
            {
                int portNum = int.Parse(port.Substring(3));
                // int FrmPortIndex = 0;
                string strException = string.Empty;
                fBaud = Convert.ToByte(3);
                if (fBaud > 2)
                    fBaud = Convert.ToByte(fBaud + 2);
                fComAdr = 255;//广播地址打开设备
                fCmdRet = RWDev.OpenComPort(portNum, ref fComAdr, fBaud, ref FrmPortIndex);
                if (fCmdRet != 0)
                {
                    strlog = "连接读写器失败，失败原因： " + GetReturnCodeDesc(fCmdRet);

                    return "";
                }
                else
                {
                    frmcomportindex = FrmPortIndex;

                }
                return inventory();
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("超高频读条码错误：" + ex.Message);
                return "";

            }
            finally
            {
                LogManager.WriteLogs("读卡记录", strlog, LogFile.Record);
            }
        }
    }
}
