﻿//#######################
//#                     #
//#    馆藏书目库       #
//#                     #
//#######################
//Author:纪钟磊
//CreateTime:2017/6/23

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Entity
{
    public class BooksInfo
    {
        /// <summary>
        /// 库键码
        /// </summary>
        public short KuKeyCode { get; set; }

        /// <summary>
        /// 主键码
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 题名
        /// </summary>
        public string BooksName { get; set; }

        /// <summary>
        /// 题目缩写
        /// </summary>
        public string BooksNameAbb { get; set; }

        /// <summary>
        /// 语种
        /// </summary>
        public string Languages { get; set; }

        /// <summary>
        /// 版次
        /// </summary>
        public string Revision { get; set; }

        /// <summary>
        /// 责任者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 出版者
        /// </summary>
        public string Pulisher { get; set; }

        /// <summary>
        /// 出版地
        /// </summary>
        public string PulishAddress { get; set; }

        /// <summary>
        /// 出版日期
        /// </summary>
        public string PulishTime { get; set; }

        /// <summary>
        /// 标准编码
        /// </summary>
        public string EncodeRule { get; set; }

        /// <summary>
        /// 索书号
        /// </summary>
        public string SuoBookNum { get; set; }

        /// <summary>
        /// marc
        /// </summary>
        public string marc { get; set; }

        /// <summary>
        /// 册数
        /// </summary>
        public short CeCount { get; set; }

        /// <summary>
        /// 可外借数
        /// </summary>
        public int CanBorrowCount { get; set; }

        /// <summary>
        /// 已外借数
        /// </summary>
        public short BorrowCountED { get; set; }

        /// <summary>
        /// 预约数
        /// </summary>
        public short OrderCount { get; set; }

        /// <summary>
        /// 图象页数
        /// </summary>
        public short PicPageCount { get; set; }

        /// <summary>
        /// 卷标
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 操作员
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// 修改人员
        /// </summary>
        public string UpdatePerson { get; set; }

        /// <summary>
        /// 处理日期
        /// </summary>
        public DateTime DealTime { get; set; }

        /// <summary>
        /// 上月外借册数
        /// </summary>
        public short LastMBorrowCount { get; set; }

        /// <summary>
        /// 本月外借册数
        /// </summary>
        public short ThisMBorrowCount { get; set; }

        /// <summary>
        /// 去年外借册数
        /// </summary>
        public short LastYBorrowCount { get; set; }

        /// <summary>
        /// 今年外借册数  
        /// </summary>
        public short ThisYBorrowCount { get; set; }

        /// <summary>
        /// 累计外借册数
        /// </summary>
        public int TotalBorrowCount { get; set; }

        #region 外表关联字段
        /// <summary>
        /// 条形码
        /// </summary>
        public string BarCode { get; set; }

        /// <summary>
        /// 读者条码
        /// </summary>
        public string ReaderCode { get; set; }

        /// <summary>
        /// 外借时间
        /// </summary>
        public DateTime BorrowDate { get; set; }

        /// <summary>
        /// 应归还时间
        /// </summary>
        public DateTime ShouldReturnDate { get; set; }

        /// <summary>
        /// 读者姓名
        /// </summary>
        public string ReaderName { get; set; }

        /// <summary>
        /// 级别代码
        /// </summary>
        public string ClassCode { get; set; }

        /// <summary>
        /// 单位代码
        /// </summary>
        public string CellCode { get; set; }

        /// <summary>
        /// 续借次数
        /// </summary>
        public short AllowOnceBorrowCount { get; set; }

        /// <summary>
        /// 续借期限天数
        /// </summary>
        public short OnceBorrowTerm { get; set; }

        /// <summary>
        /// 当前续借次数
        /// </summary>
        public short BorrowCountNow { get; set; }

        public int DoorID { get; set; }

        public int DoorNum { get; set; }

        public int DoorCode { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string CoverUrl { get; set; }
        #endregion
    }
}
