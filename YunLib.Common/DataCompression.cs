﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.Common
{
    /// <summary>
    /// ISO28560数据加密
    /// </summary>
    public class DataCompression
    {
        /// <summary>
        /// 整形加密-首字符不能为0，前导字节001
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptToInteger(string msg)
        {
            long s4 = long.Parse(msg);
            string encryptBarCode = Convert.ToString(s4, 16).ToUpper();
            if (encryptBarCode.Length % 2 != 0)
            {
                encryptBarCode = "0" + encryptBarCode;
            }

            return encryptBarCode;
        }
        /// <summary>
        /// 整形解密，前导字节001
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptToInteger(string msg)
        {
            long num = Int64.Parse(msg, System.Globalization.NumberStyles.HexNumber);
            return num.ToString();
        }

        /// <summary>
        /// 数字压缩-首字符可以为0，前导字节010
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptToNum(string msg)
        {
            string backStr = "";
            byte[] b = Encoding.UTF8.GetBytes(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 4);
            if (list.Count % 2 != 0)
            {
                string st = "1111";
                list.Add(st);
            }
            List<string> list2 = new List<string>();
            for (int i = 0; i < list.Count / 2; i++)
            {
                list2.Add(list[i * 2] + list[i * 2 + 1]);
                string str = string.Format("{0:X}", Convert.ToInt32(list[i * 2] + list[i * 2 + 1], 2));
                if (str.Length == 1)
                {
                    str = "0" + str;
                }
                backStr += str;

            }


            return backStr;
        }

        /// <summary>
        /// 数字压缩解密-首字符可以为0，前导字节010
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptToNum(string msg)
        {
            string str = msg.Substring(msg.Length - 1);

            if (str == "F")
            {
                msg = msg.Substring(0, msg.Length - 1);
            }

            return msg;
        }

        /// <summary>
        /// 5bit加密，前导字节011
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptTo5BIT(string msg)
        {
            byte[] b = Encoding.UTF8.GetBytes(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 5);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }
            string st = "00000000";
            string back = "";
            if (binaryStr.Length % 8 != 0)
            {
                binaryStr += st.Substring(0, 8 - binaryStr.Length % 8);

            }

            for (int i = 0; i < binaryStr.Length / 8; i++)
            {
                string st1 = string.Format("{0:X}", Convert.ToInt32(binaryStr.Substring(i * 8, 8), 2));
                if (st1.Length == 1)
                {
                    st1 = "0" + st1;
                }
                back += st1;
            }

            //byte[] backByte = new byte[binaryStr.Length / 8];
            //backByte = ByteArrayAndBinary.BinaryStr2ByteArray(binaryStr);

            //string back = Encoding.UTF8.GetString(backByte);


            return back;
        }

        /// <summary>
        /// 5bit解密，前导字节011
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptTo5BIT(string msg)
        {
            byte[] b = ByteArrayAndBinary.HexStringToByteArray(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 8);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }

            bool flag = true;
            while (flag)
            {
                string st = binaryStr.Substring(binaryStr.Length - 1);
                if (st == "0")
                {
                    binaryStr = binaryStr.Remove(binaryStr.Length - 1);
                }
                else
                {
                    flag = false;
                }
            }

            List<string> binaryList = new List<string>();
            string newBinaryStr = "";
            for (int i = 0; i < binaryStr.Length / 5; i++)
            {
                string st1 = binaryStr.Substring(i * 5, 5);
                newBinaryStr += "010" + st1;
            }

            byte[] byteBack = ByteArrayAndBinary.BinaryStr2ByteArray(newBinaryStr);

            string backString = Encoding.UTF8.GetString(byteBack);


            return backString;
        }

        /// <summary>
        /// 6bit加密，前导字节100
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptTo6BIT(string msg)
        {
            byte[] b = Encoding.UTF8.GetBytes(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 6);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }
            string st = "100000";
            string back = "";
            if (binaryStr.Length % 8 != 0)
            {
                binaryStr += st.Substring(0, 8 - binaryStr.Length % 8);

            }

            for (int i = 0; i < binaryStr.Length / 8; i++)
            {
                string st1 = string.Format("{0:X}", Convert.ToInt32(binaryStr.Substring(i * 8, 8), 2));
                if (st1.Length == 1)
                {
                    st1 = "0" + st1;
                }
                back += st1;
            }

            //byte[] backByte = new byte[binaryStr.Length / 8];
            //backByte = ByteArrayAndBinary.BinaryStr2ByteArray(binaryStr);

            //string back = Encoding.UTF8.GetString(backByte);


            return back;
        }
        /// <summary>
        /// 6bit解密，前导字节100
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptTo6BIT(string msg)
        {
            byte[] b = ByteArrayAndBinary.HexStringToByteArray(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 8);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }

            if (binaryStr.Length % 6 != 0)
            {
                binaryStr = binaryStr.Substring(0, ((int)(binaryStr.Length / 6)) * 6);
            }
            else
            {
                string s = binaryStr.Substring((int)(binaryStr.Length) / 6 - 1, 6);
                if (s == "100000")
                {
                    binaryStr = binaryStr.Substring(0, (int)(binaryStr.Length) / 6 - 1);
                }
            }



            List<string> binaryList = new List<string>();
            string newBinaryStr = "";
            for (int i = 0; i < binaryStr.Length / 6; i++)
            {
                string st1 = binaryStr.Substring(i * 6, 6);
                if (st1.Substring(0, 1) == "1")
                {
                    newBinaryStr += "00" + st1;
                }
                else
                {
                    newBinaryStr += "01" + st1;
                }

            }

            byte[] byteBack = ByteArrayAndBinary.BinaryStr2ByteArray(newBinaryStr);

            string backString = Encoding.UTF8.GetString(byteBack);


            return backString;
        }

        /// <summary>
        /// 7bit加密，前导字节101
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptTo7BIT(string msg)
        {
            byte[] b = Encoding.UTF8.GetBytes(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 7);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }
            string st = "11111111";
            string back = "";
            if (binaryStr.Length % 8 != 0)
            {
                binaryStr += st.Substring(0, 8 - binaryStr.Length % 8);

            }

            for (int i = 0; i < binaryStr.Length / 8; i++)
            {
                string st1 = string.Format("{0:X}", Convert.ToInt32(binaryStr.Substring(i * 8, 8), 2));
                if (st1.Length == 1)
                {
                    st1 = "0" + st1;
                }
                back += st1;
            }

            //byte[] backByte = new byte[binaryStr.Length / 8];
            //backByte = ByteArrayAndBinary.BinaryStr2ByteArray(binaryStr);

            //string back = Encoding.UTF8.GetString(backByte);


            return back;
        }
        /// <summary>
        /// 7bit解密，前导字节101
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptTo7BIT(string msg)
        {
            byte[] b = ByteArrayAndBinary.HexStringToByteArray(msg);

            List<string> list = ByteArrayAndBinary.ByteArray2BinaryStrByBitLenth(b, 8);
            string binaryStr = "";
            for (int i = 0; i < list.Count; i++)
            {
                binaryStr += list[i];
            }

            if (binaryStr.Length % 7 != 0)
            {
                binaryStr = binaryStr.Substring(0, (binaryStr.Length - binaryStr.Length % 7));
            }
            else
            {
                string s = binaryStr.Substring((int)(binaryStr.Length) / 7 - 1, 7);
                if (s == "1111111")
                {
                    binaryStr = binaryStr.Substring(0, (int)(binaryStr.Length) / 7 - 1);
                }
            }



            List<string> binaryList = new List<string>();
            string newBinaryStr = "";
            for (int i = 0; i < binaryStr.Length / 7; i++)
            {
                string st1 = binaryStr.Substring(i * 7, 7);

                newBinaryStr += "0" + st1;
            }

            byte[] byteBack = ByteArrayAndBinary.BinaryStr2ByteArray(newBinaryStr);

            string backString = Encoding.UTF8.GetString(byteBack);


            return backString;
        }

        /// <summary>
        /// 8bit加密，前导字节110
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string EncryptTo8BIT(string msg)
        {
            return msg;
        }
        /// <summary>
        /// 8bit解密，前导字节110
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string DecryptTo8BIT(string msg)
        {
            return msg;
        }

    }
}
