﻿using HUlitity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilities;
using Yunlib.Common;
using Yunlib.SellBook.Extensions;
using YunLib.Common;

namespace Yunlib.SellBook
{
    public partial class DymicNewJP : Form
    {
        public DymicNewJP()
        {
            InitializeComponent();
        }

        public static string inputCount = string.Empty;
        Hmethod method = new Hmethod();

        private void btn1_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtContent.Text += method.CallBackNum("num9");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            txtContent.Text += "0";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtContent.Text = string.Empty;
        }

        private void btnSure_Click(object sender, EventArgs e)
        {
            inputCount = txtContent.Text;
            txtContent.Clear();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (txtContent.Text.Length <= 0)
            {
                txtContent.Text = string.Empty;
            }
            else
            {
                txtContent.Text = txtContent.Text.Substring(0, txtContent.Text.Length - 1);
            }
        }

        private void DymicNewJP_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            

            Hmethod.MagicalEvent();

            //键盘点击相应按钮,txtBox显示相应值
            this.KeyDown += new KeyEventHandler(method.KeyBoard_KeyDown);
            txtContent.KeyDown += new KeyEventHandler(method.KeyBoard_KeyDown);
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("DymicNewJP_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void picBtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            txtContent.Clear();
            this.Close();
        }

        private void btnCancel_DoubleClick(object sender, EventArgs e)
        {
            btnCancel_Click(null, null);
        }

        private void btn1_DoubleClick(object sender, EventArgs e)
        {
            btn1_Click(null, null);
        }

        private void btn2_DoubleClick(object sender, EventArgs e)
        {
            btn2_Click(null, null);
        }

        private void btn3_DoubleClick(object sender, EventArgs e)
        {
            btn3_Click(null, null);
        }

        private void btn4_DoubleClick(object sender, EventArgs e)
        {
            btn4_Click(null, null);
        }

        private void btn5_DoubleClick(object sender, EventArgs e)
        {
            btn5_Click(null, null);
        }

        private void btn6_DoubleClick(object sender, EventArgs e)
        {
            btn6_Click(null, null);
        }

        private void btn7_DoubleClick(object sender, EventArgs e)
        {
            btn7_Click(null, null);
        }

        private void btn8_DoubleClick(object sender, EventArgs e)
        {
            btn8_Click(null, null);
        }

        private void btn9_DoubleClick(object sender, EventArgs e)
        {
            btn9_Click(null, null);
        }

        private void btn00_DoubleClick(object sender, EventArgs e)
        {
            btn00_Click(null, null);
        }

        private void btn_MouseDown(object sender, MouseEventArgs e)
        {
            Label btn = (Label)sender;
            btn.BackColor = Color.FromArgb(11, 37, 91);
            btn.ForeColor = Color.White;
            btn.Cursor = Cursors.Hand;
        }

        private void btn6_MouseUp(object sender, MouseEventArgs e)
        {
            Thread.Sleep(80);

            Label btn = (Label)sender;
            btn.BackColor = Color.White;
            btn.ForeColor = Color.Black;
        }
    }
}
