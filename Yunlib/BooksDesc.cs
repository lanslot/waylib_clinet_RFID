﻿using System;
using YunLib.BLL;
using Yunlib.Common;
using Yunlib.Entity;
using Yunlib.Extensions;
using System.Windows.Forms;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YunLib.Common;
using System.IO;
using System.Drawing;
using System.Net;
using System.Threading;

namespace Yunlib
{
    public partial class BooksDesc : Form
    {
        BorrowForAcessBLL borrowForAcessBLL = new BorrowForAcessBLL();

        public static string IsFirstAdd = "0";

        public delegate void MyDelegate(object sender, EventArgs e);

        private IList<LocalBooks> list = null;

        public delegate void GetDescHandel(string machLatNum);
        GetDescHandel md = null;

        public int DoorNum { get; set; }

        public BooksDesc()
        {
            InitializeComponent();
            this.md = new GetDescHandel(GetBookDesc);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;
        private string GetBookBarcode(int DoorNum)
        {
            var list = borrowForAcessBLL.GetBookByDoorNum(DoorNum);
            return list[0].BarCode;
        }

        private Image GetBooksPic(string resource, string fileName)
        {
            try
            {
                if (File.Exists(Application.StartupPath + "/" + fileName))
                {
                    //存在文件
                    return Image.FromFile(fileName);
                }
                else
                {
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(resource, fileName);
                    return Image.FromFile(fileName);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("图片路径：{0}---图片名称：{1}获取网络图片资源出错，错误信息：{2}".FormatWith(resource, fileName, ex.Message));
                throw;
            }
        }

        public void AsyncShowBookPic(string coverUrl, string title)
        {
            DelPicShow myDelegeate = new DelPicShow(GetBooksPic);

            IAsyncResult result = myDelegeate.BeginInvoke(coverUrl, title, null, null);

            this.pictureBox1.BackgroundImage = myDelegeate.EndInvoke(result);
        }

        string barCode = string.Empty;

        public void BooksDesc_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;
                pBtnBorrow.Enabled = true;

                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                //CodeOptimize.SolveLightScreen(this);

                // barCode = GetBookBarcode(DoorNum);
                Invoke(md, DoorNum+"");
                //string abStract = string.Empty;
                //string coverUrl = string.Empty;
                //string title = string.Empty;

                //pBtnBorrow.Enabled = false;

                //list = borrowForAcessBLL.GetBookByDoorNum(DoorNum);
                //if (!string.IsNullOrWhiteSpace(list[0].StandardCode))
                //{
                //    AsyncShowBookPic(list[0].StandardCode, list[0].BName);
                //}

                //if (list.Count > 0 && list != null)
                //{
                //    ControlValue();

                //    pBtnBorrow.Enabled = true;
                //}

             PlayVoice.play(this.Name);

                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("BookDesc_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                list = borrowForAcessBLL.GetBookByDoorNum(DoorNum);
                ControlValue();
                pBtnBorrow.Enabled = true;
                this.pictureBox1.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Cover_NoBook1.png");
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }

        private void GetBookDesc(string machLatNum)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}{4}".FormatWith(libraryId, machineId, machLatNum, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/query/desc?machId={1}&machLatNum={2}&libraryId={3}&timeStamp={4}&sign={5}".FormatWith(
                    servicePath, machineId, machLatNum, libraryId,  timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();
             
                if (res["code"].ToString() == "100")
                {
                    var data = res["data"];
                    lblAuthor.Text = data["author"].ToString();
                    lblBName.Text = data["title"].ToString();
                    lblPublishTime.Text = data["pubdate"].ToString();
                    lblPulisher.Text = data["publisher"].ToString();
                    lblAbstracts.Text = data["abstracts"].ToString();
                    lblMachLatNum.Text = DoorNum+"";
                    pictureBox1.ImageLocation = data["coverUrl"].ToString();
                    label1.Text = data["bookId"].ToString();
                    lblPrice.Text = data["realPrice"].ToString();
                    lblType.Text = data["type"].ToString();
                    if (data["type"].ToString() == "2")
                    {
                        lblP.Visible = true;
                        lblPrice.Visible = true;
                    }

                }
                else
                {
                    LogManager.BorrowRecord("{0}--BookDesc请求错误返回：{1}".FormatWith(DateTime.Now, res.ToString()));
                }
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("{0}--BookDesc请求报错：{1}".FormatWith(DateTime.Now, ex.Message));
            }
        }

        public delegate Image DelPicShow(string coverUrl, string url);

        /// <summary>
        /// 控件赋值
        /// </summary>
        private void ControlValue()
        {
            LocalBooks lb = list[0];
            this.lblBName.Text = lb.BName;
            this.lblAuthor.Text = lb.Writer;
            this.lblPulisher.Text = lb.Press;
            this.lblPublishTime.Text = lb.PublicationDate;
            this.lblMachLatNum.Text = lb.DoorNUM.ToString();
            this.lblAbstracts.Text = lb.Classify;
            this.label1.Text = lb.BarCode;
            this.label1.Hide();
        }

        //点击借阅时的事件
        private void pBtnBorrow_Click(object sender, EventArgs e)
        {
            int borrowTyoe = ConfigManager.BorrowType;
            this.pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Borrow_noLog1.png");
            timer1.Enabled = false;
            if (borrowTyoe == 1)
            {
                ReaderLogin rdl = new ReaderLogin { MdiParent = ParentForm, Dock = DockStyle.Fill, BarCode = label1.Text, DoorNum = this.lblMachLatNum.Text, BorrowType = lblType.Text };

                this.Close();

                rdl.Show();
            }
            else if(borrowTyoe == 0)
            {
                ReaderLoginT rdl = new ReaderLoginT { MdiParent = ParentForm, Dock = DockStyle.Fill, BarCode = label1.Text, DoorNum = this.lblMachLatNum.Text, BorrowType = lblType.Text };

                this.Close();

                rdl.Show();
            }
            else
            {
                ReaderLogin3 rdl = new ReaderLogin3 { MdiParent = ParentForm, Dock = DockStyle.Fill, BarCode = label1.Text, DoorNum = this.lblMachLatNum.Text, BorrowType = lblType.Text };

                this.Close();

                rdl.Show();
            }
        }

        //返回上一页
        private void pBtnTurnBack_Click(object sender, EventArgs e)
        {
            this.pBtnTurnBack.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/btn_ReturnBack2.png");
            timer1.Enabled = false;

            BorrowBook frm = new BorrowBook();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        //加入购物车
        private void picShopCart_Click(object sender, EventArgs e)
        {
            ShoppingCart frm = new ShoppingCart { MdiParent = ParentForm, Dock = DockStyle.Fill, DoorNumArr = Cart.BooksNumArr };

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm:ss");
        }

        private void pBtnBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            this.pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Borrow_noLog2.png");
        }

        private void pBtnTurnBack_MouseDown(object sender, MouseEventArgs e)
        {
            this.pBtnTurnBack.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/btn_ReturnBack22.png");
        }

        private void palBackground_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
