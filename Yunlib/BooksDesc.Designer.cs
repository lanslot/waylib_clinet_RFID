﻿namespace Yunlib
{
    partial class BooksDesc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BooksDesc));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pBtnTurnBack = new System.Windows.Forms.PictureBox();
            this.pBtnBorrow = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAbstracts = new System.Windows.Forms.Label();
            this.lblPulisher = new System.Windows.Forms.Label();
            this.lblMachLatNum = new System.Windows.Forms.Label();
            this.lblPublishTime = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.palBackground = new System.Windows.Forms.Panel();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblP = new System.Windows.Forms.Label();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblType = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnTurnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).BeginInit();
            this.panel2.SuspendLayout();
            this.palBackground.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "jiarujieyueche.png");
            this.imageList1.Images.SetKeyName(1, "yizaijieyueche.png");
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Location = new System.Drawing.Point(654, 37);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(26, 31);
            this.pictureBox4.TabIndex = 52;
            this.pictureBox4.TabStop = false;
            // 
            // pBtnTurnBack
            // 
            this.pBtnTurnBack.BackColor = System.Drawing.Color.Transparent;
            this.pBtnTurnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnTurnBack.Image = ((System.Drawing.Image)(resources.GetObject("pBtnTurnBack.Image")));
            this.pBtnTurnBack.Location = new System.Drawing.Point(723, 632);
            this.pBtnTurnBack.Name = "pBtnTurnBack";
            this.pBtnTurnBack.Size = new System.Drawing.Size(174, 74);
            this.pBtnTurnBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnTurnBack.TabIndex = 51;
            this.pBtnTurnBack.TabStop = false;
            this.pBtnTurnBack.Click += new System.EventHandler(this.pBtnTurnBack_Click);
            this.pBtnTurnBack.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnTurnBack_MouseDown);
            // 
            // pBtnBorrow
            // 
            this.pBtnBorrow.BackColor = System.Drawing.Color.Transparent;
            this.pBtnBorrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnBorrow.Image = ((System.Drawing.Image)(resources.GetObject("pBtnBorrow.Image")));
            this.pBtnBorrow.Location = new System.Drawing.Point(495, 632);
            this.pBtnBorrow.Name = "pBtnBorrow";
            this.pBtnBorrow.Size = new System.Drawing.Size(174, 74);
            this.pBtnBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnBorrow.TabIndex = 49;
            this.pBtnBorrow.TabStop = false;
            this.pBtnBorrow.Click += new System.EventHandler(this.pBtnBorrow_Click);
            this.pBtnBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnBorrow_MouseDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.lblAbstracts);
            this.panel2.Location = new System.Drawing.Point(453, 489);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(431, 119);
            this.panel2.TabIndex = 48;
            // 
            // lblAbstracts
            // 
            this.lblAbstracts.BackColor = System.Drawing.Color.Transparent;
            this.lblAbstracts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAbstracts.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblAbstracts.ForeColor = System.Drawing.Color.White;
            this.lblAbstracts.Location = new System.Drawing.Point(0, 0);
            this.lblAbstracts.Name = "lblAbstracts";
            this.lblAbstracts.Size = new System.Drawing.Size(431, 119);
            this.lblAbstracts.TabIndex = 21;
            // 
            // lblPulisher
            // 
            this.lblPulisher.AutoSize = true;
            this.lblPulisher.BackColor = System.Drawing.Color.Transparent;
            this.lblPulisher.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPulisher.ForeColor = System.Drawing.Color.White;
            this.lblPulisher.Location = new System.Drawing.Point(564, 239);
            this.lblPulisher.Name = "lblPulisher";
            this.lblPulisher.Size = new System.Drawing.Size(0, 28);
            this.lblPulisher.TabIndex = 36;
            // 
            // lblMachLatNum
            // 
            this.lblMachLatNum.AutoSize = true;
            this.lblMachLatNum.BackColor = System.Drawing.Color.Transparent;
            this.lblMachLatNum.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMachLatNum.ForeColor = System.Drawing.Color.Yellow;
            this.lblMachLatNum.Location = new System.Drawing.Point(564, 337);
            this.lblMachLatNum.Name = "lblMachLatNum";
            this.lblMachLatNum.Size = new System.Drawing.Size(0, 28);
            this.lblMachLatNum.TabIndex = 37;
            // 
            // lblPublishTime
            // 
            this.lblPublishTime.AutoSize = true;
            this.lblPublishTime.BackColor = System.Drawing.Color.Transparent;
            this.lblPublishTime.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPublishTime.ForeColor = System.Drawing.Color.White;
            this.lblPublishTime.Location = new System.Drawing.Point(564, 290);
            this.lblPublishTime.Name = "lblPublishTime";
            this.lblPublishTime.Size = new System.Drawing.Size(0, 28);
            this.lblPublishTime.TabIndex = 38;
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.BackColor = System.Drawing.Color.Transparent;
            this.lblAuthor.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAuthor.ForeColor = System.Drawing.Color.White;
            this.lblAuthor.Location = new System.Drawing.Point(564, 199);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(0, 28);
            this.lblAuthor.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(430, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 28);
            this.label3.TabIndex = 41;
            this.label3.Text = "出版社：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(448, 437);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 28);
            this.label8.TabIndex = 42;
            this.label8.Text = "简介：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(408, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 28);
            this.label5.TabIndex = 44;
            this.label5.Text = "出版日期：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(448, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 28);
            this.label6.TabIndex = 45;
            this.label6.Text = "书名：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(328, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 30);
            this.label1.TabIndex = 46;
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(448, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 28);
            this.label2.TabIndex = 47;
            this.label2.Text = "作者：";
            // 
            // lblBName
            // 
            this.lblBName.AutoSize = true;
            this.lblBName.BackColor = System.Drawing.Color.Transparent;
            this.lblBName.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBName.ForeColor = System.Drawing.Color.White;
            this.lblBName.Location = new System.Drawing.Point(564, 155);
            this.lblBName.Name = "lblBName";
            this.lblBName.Size = new System.Drawing.Size(0, 28);
            this.lblBName.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(430, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 28);
            this.label4.TabIndex = 43;
            this.label4.Text = "柜门号：";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // palBackground
            // 
            this.palBackground.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.palBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.palBackground.Controls.Add(this.lblType);
            this.palBackground.Controls.Add(this.lblPrice);
            this.palBackground.Controls.Add(this.label4);
            this.palBackground.Controls.Add(this.lblP);
            this.palBackground.Controls.Add(this.lblDataTimeNow);
            this.palBackground.Controls.Add(this.panel3);
            this.palBackground.Controls.Add(this.pictureBox4);
            this.palBackground.Controls.Add(this.pBtnTurnBack);
            this.palBackground.Controls.Add(this.pBtnBorrow);
            this.palBackground.Controls.Add(this.panel2);
            this.palBackground.Controls.Add(this.lblPulisher);
            this.palBackground.Controls.Add(this.lblMachLatNum);
            this.palBackground.Controls.Add(this.lblPublishTime);
            this.palBackground.Controls.Add(this.lblAuthor);
            this.palBackground.Controls.Add(this.label3);
            this.palBackground.Controls.Add(this.label8);
            this.palBackground.Controls.Add(this.label5);
            this.palBackground.Controls.Add(this.label6);
            this.palBackground.Controls.Add(this.label1);
            this.palBackground.Controls.Add(this.label2);
            this.palBackground.Controls.Add(this.lblBName);
            this.palBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.palBackground.Location = new System.Drawing.Point(0, 0);
            this.palBackground.Name = "palBackground";
            this.palBackground.Size = new System.Drawing.Size(1024, 768);
            this.palBackground.TabIndex = 1;
            this.palBackground.Paint += new System.Windows.Forms.PaintEventHandler(this.palBackground_Paint);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblPrice.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPrice.ForeColor = System.Drawing.Color.Yellow;
            this.lblPrice.Location = new System.Drawing.Point(564, 387);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(0, 28);
            this.lblPrice.TabIndex = 54;
            this.lblPrice.Tag = "";
            this.lblPrice.Visible = false;
            // 
            // lblP
            // 
            this.lblP.AutoSize = true;
            this.lblP.BackColor = System.Drawing.Color.Transparent;
            this.lblP.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblP.ForeColor = System.Drawing.Color.White;
            this.lblP.Location = new System.Drawing.Point(448, 387);
            this.lblP.Name = "lblP";
            this.lblP.Size = new System.Drawing.Size(75, 28);
            this.lblP.TabIndex = 55;
            this.lblP.Text = "价格：";
            this.lblP.Visible = false;
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(703, 35);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 36;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(39, 155);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(345, 439);
            this.panel3.TabIndex = 53;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.ErrorImage = global::Yunlib.Properties.Resources.Cover_NoBook1;
            this.pictureBox1.InitialImage = global::Yunlib.Properties.Resources.Cover_NoBook11;
            this.pictureBox1.Location = new System.Drawing.Point(72, 121);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.BackColor = System.Drawing.Color.Transparent;
            this.lblType.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblType.ForeColor = System.Drawing.Color.White;
            this.lblType.Location = new System.Drawing.Point(590, 116);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 30);
            this.lblType.TabIndex = 56;
            this.lblType.Visible = false;
            // 
            // BooksDesc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.palBackground);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BooksDesc";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.BooksDesc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnTurnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnBorrow)).EndInit();
            this.panel2.ResumeLayout(false);
            this.palBackground.ResumeLayout(false);
            this.palBackground.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pBtnTurnBack;
        private System.Windows.Forms.PictureBox pBtnBorrow;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAbstracts;
        private System.Windows.Forms.Label lblPulisher;
        private System.Windows.Forms.Label lblMachLatNum;
        private System.Windows.Forms.Label lblPublishTime;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBName;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel palBackground;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblP;
        private System.Windows.Forms.Label lblType;
    }
}