﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.ReadCard
{
    public interface IReadCard
    {
        /// <summary>
        /// 读IM卡
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        string ReadIM(string port, int Baud);

        /// <summary>
        /// 读身份证
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        string ReadIDCard(string port, int Baud);

        /// <summary>
        /// 读RFID
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        string ReadRFID(string port, int Baud);

        /// <summary>
        /// 读RFID
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        string ReadUHFRFID(string port, int Baud);
    }
}
