﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.ReadCard
{
    public class ObjectCreator
    {
        public static string readType = ConfigurationManager.AppSettings["CardReadType"];

        /// <summary>
        /// 基于反射创建类
        /// </summary>
        /// <returns></returns>
        public static IReadCard GetReadClass()
        {
            try
            {
                Assembly ss = Assembly.Load("YunLib.ReadCard");
                IReadCard r = (IReadCard)ss.CreateInstance("YunLib.ReadCard." + readType);
                return (IReadCard)Assembly.Load("YunLib.ReadCard").CreateInstance("YunLib.ReadCard." + readType);
            }catch(Exception ex)
            {
                // return new IReadCard();
                throw;
            }
        }
    }
}
