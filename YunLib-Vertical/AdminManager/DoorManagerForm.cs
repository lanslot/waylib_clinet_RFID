﻿using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Entity;
using YunLib.BLL;
using YunLib.Common;
using static System.Windows.Forms.ListView;

namespace YunLib_Vertical.AdminManager
{
    public partial class DoorManagerForm : Form
    {
        private SmartLifeBroker model = new SmartLifeBroker();
        private string control = ConfigManager.Control;
        private int cmdPort = ConfigManager.CmdPort;
        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        public DoorManagerForm()
        {
            InitializeComponent();
        }

        private void DoorManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                BorrowForAcessBLL BLL = new BorrowForAcessBLL();
                DataTable dt = BLL.GetMachineInitData();
                IList<ShowBooks> list = DataTable2List<ShowBooks>.ConvertToModel(dt);

                // listView1.View = View.Tile;
                int h = listView1.Height / 10 + 1;
                //ImageList image = new ImageList();

                //image.ImageSize = new Size(1, h);//这边设置宽和高
                //this.listView1.SmallImageList = image;
                //判断序列化的端口，为空就初始化
                for (int i = 0; i < list.Count; i++)
                {

                    listView1.Items.Add(new ListViewItem(new string[] { list[i].DoorNUM.ToString(), list[i].DoorID.ToString(), list[i].DoorCode.ToString() }, 0));

                }

                model.Init(control, cmdPort);
            }catch(Exception ex)
            {
                LogManager.WriteLogs("DoorManagerForm_Load出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void btnOpenDoorAll_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("确定打开所有空柜", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                BorrowForAcessBLL BLL = new BorrowForAcessBLL();
                DataTable dt = BLL.GetMachineInitData();
                IList<ShowBooks> list = DataTable2List<ShowBooks>.ConvertToModel(dt);
                ListViewItemCollection list1 = listView1.Items;
                for (int i = 0; i < list.Count; i++)
                {
                    #region 异步开门
                    OpenDoorDelegate myDelegate = new OpenDoorDelegate(OpenDoor);

                    IAsyncResult result = myDelegate.BeginInvoke(list[i].DoorID, list[i].DoorCode, list1[i], null, null);

                    myDelegate.EndInvoke(result);
                    //myDelegate.Invoke(list[i].DoorID, list[i].DoorCode, list1[i]);
                    Thread.Sleep(500);
                    #endregion
                }
            }
        }

        public delegate void OpenDoorDelegate(int doorID, int doorCode, object item);

        /// <summary>
        /// 循环打开所有的门
        /// </summary>
        /// <param name="item">Label子集</param>
        private void OpenDoor(int doorId, int doorCode, object item)
        {
            ListViewItem myItem = item as ListViewItem;

            SerialPort port = model.SmartSerialPort;


            //打开相应行号的柜门
            bool res = model.OpenDoor(doorId, doorCode, 2);
            Thread.Sleep(500);
            if (res)
            {
                //开门成功
                myItem.BackColor = Color.Green;
            }
            else
            {
                //开门失败
                myItem.BackColor = Color.Red;
            }

        }

        private void btnReturnMain_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            model.Dispose();
            this.Close();

            frm.Show();
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in this.listView1.SelectedItems)
                    {

                        if (!string.IsNullOrWhiteSpace(item.Text)) //判断柜号是否为空
                        {
                            int doorID = int.Parse(item.SubItems[1].Text);
                            int doorCode = int.Parse(item.SubItems[2].Text);

                            SerialPort port = model.SmartSerialPort;
                            //判断序列化的端口，为空就初始化
                            if (port == null)
                            {
                                model.Init(control, cmdPort);
                            }
                            else
                            {
                                string str = model.SmartSerialPort.PortName;
                                //判断已经序列化的端口是否是扫描端口，不是就初始化扫描端口
                                if (str != (control))
                                {
                                    model.Init(control, cmdPort);
                                }
                            }
                            //打开相应行号的柜门
                            bool res = model.OpenDoor(doorID, doorCode, 1);

                            if (res)
                            {
                                //开门成功
                                item.BackColor = Color.Green;
                            }
                            else
                            {
                                //开门失败
                                item.BackColor = Color.Red;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void DoorManager_FormClosed(object sender, FormClosedEventArgs e)
        {
            model.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("确定打开所有空柜", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return OPenNullDoor();
                //  返回任务结果：5
            });
                //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                longTask.BeginInvoke(ar =>
                {
                //  使用EndInvoke获取到任务结果（5）
                string result1 = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result1 != null)
                    {
                        Invoke(new Action(() =>

                       EndOPenNullDoor(result1)
                        // dataGridView1.DataSource = result

                        ));
                    }
                }, null);
            }
        }

        public string OPenNullDoor()
        {
            string dt = "";
            try
            {
                var time = DateTime.Now.Ticks;
                var signstr = "{0}{1}{2}".FormatWith(machineId, time, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}/public/pubmachine/emptyLattices".FormatWith(servicePath);
                string parameters = "machineId={0}&timeStamp={1}&&sign={2}".FormatWith(machineId, time, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

                //if (res["code"].ToString() == "100")
                //{
                //    dt = res["data"].ToJson().ToString();



                //}
                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("请求开全部空柜错误", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        public void EndOPenNullDoor(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {

                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        var data = result["data"].ToString().ToList<string>();
                        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
                        DataTable dt = BLL.GetMachineInitData();
                        IList<ShowBooks> list = DataTable2List<ShowBooks>.ConvertToModel(dt);
                        ListViewItemCollection list1 = listView1.Items;
                        for (int i = 0; i < list.Count; i++)
                        {
                            for (int j = 0; j < data.Count; j++)
                            {
                                if (list[i].DoorNUM.ToString() == data[j])
                                {
                                    #region 异步开门
                                    OpenDoorDelegate myDelegate = new OpenDoorDelegate(OpenDoor);

                                    IAsyncResult result1 = myDelegate.BeginInvoke(list[i].DoorID, list[i].DoorCode, list1[i], null, null);

                                    myDelegate.EndInvoke(result1);
                                    //myDelegate.Invoke(list[i].DoorID, list[i].DoorCode, list1[i]);
                                    Thread.Sleep(1000);
                                }
                            }
                            #endregion
                        }



                    }
                    else
                    {
                        MessageBox.Show(result["msg"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("刷脸登陆返回数据出错", ex.StackTrace, LogFile.Error);
                // lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }

    }
}
