﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib.SellBook.Extensions
{
    public class Singleton<T> where T : Form, new()
    {
        private static T _instance;
        private static object _lock = new object();

        public static T CreateInstance()
        {
            if (_instance == null)//保证对象初始化之后的所有线程，不需要等待锁
            {
                lock (_lock)//保证一个线程能进去
                {
                    if (_instance == null)//保证对象为空才创建
                        _instance = new T();
                }
            }
            return _instance;
        }
    }
}
