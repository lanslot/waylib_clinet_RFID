﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib.ReadCard;

namespace Yunlib
{
    public partial class ReaderLogin3 : Form
    {
        public static ReaderLogin3 readerLogin;

        public static int ScanDoorNum;

        public string BarCode { get; set; }

        public string DoorNum { get; set; }

        public string MoreBarCode { get; set; }

        public string BorrowType { get; set; }

        public int icdev;   // 通讯设备标识符
        public short st;    //函数返回值

        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        //为了让虚拟键盘输入密码和用户名时显示不同
        public static string Plus = "0";

        //获取服务器url
        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string loginKey = ConfigManager.LoginKey;
        private string url = "";
        private IReadCard read = ObjectCreator.GetReadClass();

        public ReaderLogin3()
        {
            ZooKeeperClient.wc.onReceiveMessage += onReceiveMessage;
            InitializeComponent();
        }

        private void onReceiveMessage(object source, string rDataStr)
        {
            //   string barCode = Encoding.ASCII.GetString(scanFrame);
            this.BeginInvoke((Action)(() =>
            {
                //perform on the UI thread
                BorrSuccess frm = new BorrSuccess(rDataStr) { MdiParent = ParentForm, Dock = DockStyle.Fill };
                CloseTimer();
                ZooKeeperClient.wc.onReceiveMessage -= onReceiveMessage;
                this.Close();

                frm.Show();
            }));
        }

        private void ReaderLogin3_Load(object sender, EventArgs e)
        {
            string msg = string.Empty;

            try
            {
                DateTime start = DateTime.Now;
                Fun1 = Print;
                msg += "readerLogin = this";
                //窗体加载的时候储存，然后把对象传给Watcher
                readerLogin = this;

                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                CodeOptimize.SolveLightScreen(this);

                msg += "LoadCreateQrCode|";
                //生成相应书籍的二维码
                LoadCreateQrCode();

                msg += "ConnIMCard|";
                //IM卡的连接
                //  ConnIMCard();

                lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

                txtAccount.Focus();

                PlayVoice.play(this.Name);
                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("ReaderLogin_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("{0},错误原因：{1}".FormatWith(ex.Message, msg));
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        #region 控件事件函数
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

                int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;
                this.lblDaoJiShi.Text = second.ToString();
                if (second <= 0)
                {
                    //断开连接
                    CloseConn();

                    foreach (Form item in Application.OpenForms)
                    {
                        if (item is DymicNewJP)
                        {
                            (item as DymicNewJP).Close();
                        }
                    }

                    //清除标记,这样再调出虚拟键盘时就不会是密码
                    Plus = "0";

                    CloseTimer();

                    // Main frm = Singleton<Main>.CreateInstance();
                    Main frm = new Main();

                    frm.MdiParent = ParentForm;
                    frm.Dock = DockStyle.Fill;

                    this.Hide();

                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }
        HDReadCard hdr = new HDReadCard();

        private void timer2_Tick_1(object sender, EventArgs e)
        {
            try
            {
                string strID = "";
                if (ConfigManager.ReadCardType == "1")
                {
                    // 读身份证
                    strID = read.ReadIDCard(ConfigManager.Pos, ConfigManager.IMPort);
                }
                else if (ConfigManager.ReadCardType == "2")
                {
                    // 武警学院读IM卡
                    // strID = hdr.ReadCardIMForSCWJ(ConfigManager.Pos, ConfigManager.IMPort);

                    strID = read.ReadIM(ConfigManager.Pos, ConfigManager.IMPort);
                    //  MessageBox.Show(strID);
                    //  读IM卡
                    // strID = hdr.ReadCardIM(ConfigManager.Pos, ConfigManager.IMPort);
                }
                else if (ConfigManager.ReadCardType == "3")
                {

                    strID = read.ReadIM(ConfigManager.Pos, ConfigManager.IMPort);
                    if (string.IsNullOrWhiteSpace(strID))
                    {
                        strID = read.ReadIDCard(ConfigManager.Pos, ConfigManager.IMPort);
                    }

                }
                if (!string.IsNullOrWhiteSpace(strID))
                {
                    txtAccount.Text = strID;
                    if (ConfigManager.IsNeedPassWord == 1)
                    {
                        txtPwd.Text = "111111";
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //断开连接
            CloseConn();

            //清除标记,这样再调出虚拟键盘时就不会是密码
            Plus = "0";

            CloseTimer();

            //BorrowBook frm =Singleton< BorrowBook >.CreateInstance();
            BorrowBook frm = new BorrowBook();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Hide();

            frm.Show();
        }

        private void txtPwd_MouseDown(object sender, MouseEventArgs e)
        {
            ////是“1”就显示为密码框
            //Plus = "1";

            //DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            //frm.ShowDialog();

            //if (frm.DialogResult == DialogResult.Cancel)
            //{
            //    //关闭虚拟键盘时,把值传到登录页面的文本框中
            //    txtPwd.Text = DymicNewJP.inputCount;
            //    // pBtnLogin_Click(null, null);
            //}

            //DymicJP frm = new DymicJP { TopMost = true, StartPosition = FormStartPosition.Manual };

            //frm.getTextHandler = getValue1;//将方法赋给委托对象
            //frm.ShowDialog();
            CodeOptimize.form.Show();

            txtPwd.Focus();

            this.Activate();
        }

        public void getValue1(string strV)
        {
            if (strV == "Backspace")
            {
                string str = txtPwd.Text;
                txtPwd.Text = str.Substring(0, str.Length - 1);
            }
            else if (strV == "Clear")
            {
                this.txtPwd.Text = "";
            }
            else
            {
                this.txtPwd.Text += strV;
            }
        }

        private void txtPwd_MouseLeave(object sender, EventArgs e)
        {
            if (txtPwd.Text != null || txtPwd.Text != "")
                lblPwdErrMsg.Text = "";
        }

        public void getValue(string strV)
        {
            if (strV == "Backspace")
            {
                string str = txtAccount.Text;
                txtAccount.Text = str.Substring(0, str.Length - 1);
            }
            else if (strV == "Clear")
            {
                this.txtAccount.Text = "";
            }
            else
            {
                this.txtAccount.Text += strV;
            }
        }

        private void txtAccount_MouseDown(object sender, MouseEventArgs e)
        {
            //是“1”就显示为普通文本框
            //DymicJP frm = new DymicJP { TopMost = true, StartPosition = FormStartPosition.Manual };

            ////frm.fatherform = this;//将当前窗体赋给fatherform
            ////frm.getTextHandler += new Form2.GetTextHandler(getValue);//给事件赋值（注意：GetText方法的参数必须与GetTextHandler委托的参数一样，方可委托）
            //frm.getTextHandler = getValue;//将方法赋给委托对象
            //frm.ShowDialog();
            //是“1”就显示为密码框
            //Plus = "0";

            //DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            //frm.ShowDialog();

            //if (frm.DialogResult == DialogResult.Cancel)
            //{
            //    关闭虚拟键盘时,把值传到登录页面的文本框中
            //    txtAccount.Text = DymicNewJP.inputCount;
            //     pBtnLogin_Click(null, null);
            //}
            CodeOptimize.form.Show();

            txtAccount.Focus();

            this.Activate();
        }

        private void txtAccount_MouseLeave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtAccount.Text))
                lblAccountErrMsg.Text = "";
        }

        Thread lendThr = null;
        private void pBtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //验证TextBox非空
                if (string.IsNullOrEmpty(txtAccount.Text))
                {
                    lblAccountErrMsg.Text = "请输入读者证号/手机号";
                }
                else if (string.IsNullOrEmpty(txtPwd.Text))
                {
                    lblPwdErrMsg.Text = "请输入密码";
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(MoreBarCode))
                    {
                        //lendThr = new Thread(() =>
                        //{
                        lendBook(BarCode, DoorNum);
                        //});
                        //lendThr.IsBackground = true;
                        //lendThr.Start();
                    }
                    else
                    {
                        var bcs = MoreBarCode.Split(',');

                        //可借书数量大于或者等于要借阅的书籍数量
                        foreach (string str in bcs)
                        {
                            if (!string.IsNullOrWhiteSpace(str))
                            {
                                var strs = str.Split('&');
                                var barcode = strs[0];
                                var doornum = strs[1];

                                lendThr = new Thread(() =>
                                {
                                    lendBook(barcode, doornum);
                                });
                                lendThr.IsBackground = true;
                                lendThr.Start();
                            }
                        }

                        Cart.BooksNumArr = "";

                        //断开连接
                        CloseConn();
                        CloseTimer();

                        //清除标记,这样再调出虚拟键盘时就不会是密码
                        Plus = "0";

                        BorrSuccess frm = new BorrSuccess() { MdiParent = ParentForm, Dock = DockStyle.Fill };

                        this.Hide();

                        frm.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void lendBook(string barCode, string doorNum)
        {
            var uName = txtAccount.Text.Trim();
            var uPwd = txtPwd.Text.Trim();

            var timeStamp = DateTime.Now.Ticks;

            var pwd = "{0}{1}".FormatWith(uPwd, loginKey);
            var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
            var signstr = "{0}{1}{2}{3}{4}{5}{6}{7}".FormatWith(barCode, libraryId, signPwd, doorNum, machineId, timeStamp, uName, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

            url = "{0}/public/auth/machinelogin?uName={1}&timeStamp={2}&libraryPwd={3}&bookId={4}&machineId={5}&machLatNum={6}&sign={7}&libraryId={8}".FormatWith(servicePath, uName, timeStamp, signPwd, barCode, machineId, doorNum, sign, libraryId);

            var loginType = ConfigurationManager.AppSettings["LoginType"].ToString();
            if (loginType == "1")
            {
                url += "&sipPwd={0}".FormatWith(uPwd);
            }
            string[] s = new string[1];
            s[0] = ConfigManager.PingServicePath;
            bool flag = NetTest.CheckServeStatus(s);
            //验证网络连接状态，true就发起请求，false不请求
            if (flag)
            {
                // this.Invoke(Fun1, url);

                // 增加判断，避免每次单击都开辟一个线程
                if (t == null)
                {
                    t = new Thread(Run);
                    t.Start();
                }
                if (t.ThreadState == ThreadState.Suspended) // 如果被挂起了，就唤醒
                {
                    t.Resume();
                }
            }
            else
            {
                lblPwdErrMsg.Text = "网络繁忙，请稍后在试！";
            }
        }

        /// <summary>
        /// 因为控件的Invoke方法需要接收委托变量，因此需要定义委托和委托变量
        /// 定义一个委托，接收一个参数
        /// </summary>
        /// <param name="msg"></param>
        public delegate void DelegateFun(string msg);
        /// <summary>
        /// 定义一个委托变量
        /// 这个委托变量，需要初始化指定具体的方法；然后传递给控件的Invoke方法调用。
        /// </summary>
        public DelegateFun Fun1;


        /// <summary>
        /// 定义一个线程，处理数据，并更新界面
        /// </summary>
        private Thread t = null;

        // 结束执行


        // 具体做事情的方法
        public void Run()
        {

            //Invoke指： 在拥有控件的基础窗口句柄的线程上，用指定的参数列表执行指定委托。
            //Invoke的参数是一个委托类型，因此必须定义委托变量 
            this.Invoke(Fun1, url);

        }


        // 输出日志的方法
        public void Print(string url)
        {
            try
            {
                LogManager.ErrorRecord("开始线程******".FormatWith(DateTime.Now));
                // 新开辟的线程，不能直接调用这个方法。原因是控件只能由创建它的线程调用。
                // 其他线程调用提示错误： 线程间操作无效: 从不是创建控件“richTextBox1”的线程访问它。
                var IsLoginSuccess = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                string errMsg = IsLoginSuccess["msg"].ToString();

                if (IsLoginSuccess["code"].ToString() == "100")
                {


                    //清除标记,这样再调出虚拟键盘时就不会是密码
                    Plus = "0";

                    BorrSuccess frm = new BorrSuccess(DoorNum) { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Close();

                    frm.Show();

                    var list = accessBLL.GetBookByDoorNum(int.Parse(DoorNum.ToString()));

                    CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                    //断开连接
                    CloseConn();
                    CloseTimer();

                }
                else
                {
                    //登录失败
                    lblPwdErrMsg.Text = errMsg;
                }
                t.Abort();
                t = null;
            }
            catch (Exception ex)
            {
                lblPwdErrMsg.Text = "网络繁忙，请稍后再试！";
                LogManager.ErrorRecord("登陆借书请求出错，错误信息:{0}".FormatWith(ex.Message));
            }
        }

        #endregion

        #region 窗体扩展事件
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        //关闭计时器
        private void CloseTimer()
        {
            timer1.Enabled = false;
            timer1.Stop();
            timer2.Enabled = false;
            timer2.Stop();
        }

        // 生成二维码
        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }

        private void LoadCreateQrCode()
        {
            string url = "";
            if (BorrowType == "2")
            {
                url = string.Format("{3}wxpay/redirectUrl?url={3}bookinfo.html?bookId={0}&machineId={1}&machLatNum={2}&libraryId={4}", BarCode, ConfigManager.MachineID, DoorNum, ConfigManager.ServicePath, ConfigManager.LibraryID);
            }
            else
            {
                url = string.Format("{3}wxpay/redirectUrl?url={3}lend-book.html?bookId={0}&machineId={1}&machLatNum={2}&libraryId={4}", BarCode, ConfigManager.MachineID, DoorNum, ConfigManager.ServicePath, ConfigManager.LibraryID);
            }

            Image image = CreateImage(url);
            if (image != null)
            {
                picEncode.Image = image;
            }
        }

        //IM卡的连接事件
  
        //断开连接
        private void CloseConn()
        {
            // st = mt_32dll.close_device(icdev);
            if (st != 0)
            {
                timer2.Enabled = false;
                timer1.Enabled = false;
                label1.Text += "断开连接失败";
            }
            else
            {
                timer2.Enabled = false;
                timer1.Enabled = false;
                //  icdev = 0;
                label1.Text += "断开连接成功";
            }
        }




        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                FaceLogin frm = new FaceLogin { MdiParent = ParentForm, Dock = DockStyle.Fill };
                frm.barCode = BarCode;
                this.Close();

                frm.Show();
                CloseTimer();
            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }
    }
}
