﻿using System.IO;
using System.Drawing;
using YunLib.Common;
using System.Diagnostics;
using ShenZhen.SmartLife.Control;
using System.Windows.Forms;
using System;
using System.Threading;

namespace Yunlib.SellBook.Extensions
{
    public class CodeOptimize
    {
        public static Image InitPicture(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            return Image.FromStream(fs);
        }

        public static void KillProcess(string proccessName)
        {
            Process[] pro = Process.GetProcesses();
            for (int i = 0; i < pro.Length; i++)
            {
                if (pro[i].ProcessName == proccessName)
                {
                    pro[i].Kill();
                }
            }
        }

        public static void OpenDoor(int doorId, int doorCode)
        {
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);
                consoleModel.OpenDoor(doorId, doorCode,2);
            }
        }

        public static void SolveLightScreen(Control c)
        {
            foreach (var items in c.Controls)
            {
                if (items is Panel)
                {
                    (items as Panel).Visible = false;
                    foreach (var item in (items as Panel).Controls)
                    {
                        (item as Control).Visible = false;
                    }
                }
                else
                {
                    (items as Control).Visible = false;
                }
            }

            Application.DoEvents();

            foreach (var items in c.Controls)
            {
                if (items is Panel)
                {
                    (items as Panel).Visible = true;
                    foreach (var item in (items as Panel).Controls)
                    {
                        (item as Control).Visible = true;
                    }
                }
                else
                {
                    (items as Control).Visible = true;
                }
            }
        }

        public static Form2 form = new Form2();
    }
}
