﻿using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class FaceRegister : Form
    {
        Mat matImg;//摄像头图像
        Capture capture;//摄像头对象

        Util.KingFaceDetect kfd;

        int currentFaceFlag = 0;
        Util.KingFaceDetect.faceDetectedObj currentfdo;//点击鼠标时的人脸检测对象

        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string loginKey = ConfigManager.LoginKey;
        private string path = "";
      //  private string token = ConfigManager.FaceToken;

        public FaceRegister()
        {
            InitializeComponent();
            CvInvoke.UseOpenCL = false;
            kfd = new Util.KingFaceDetect();
            try
            {
                capture = new Capture();
                capture.Start();//摄像头开始工作
                capture.ImageGrabbed += frameProcess;//实时获取图像
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }

        private void FaceRegister_Load(object sender, EventArgs e)
        {

        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

            int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;
            this.lblDaoJiShi.Text = second.ToString();
            if(second <= 0)
            {
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            markFaces();
        }

        public static string UrlEncode(string str)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }

            return (sb.ToString());
        }


        /// <summary>
        /// 人脸绑定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFaceRegister_Click(object sender, EventArgs e)
        {
            byte[] b = File.ReadAllBytes(path);
         
            string strbaser64 = Convert.ToBase64String(b);
            string s1 = UrlEncode(strbaser64);
            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return getData(s1);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result != null)
                {
                    Invoke(new Action(() =>

                   changeData(result)
                    // dataGridView1.DataSource = result

                    ));
                }
            }, null);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string uName = txtUserName.Text;
            string uPwd = txtPwd.Text;
            if (string.IsNullOrEmpty(uName))
            {
                lblPwdErrMsg.Text = "请输入读者证号/手机号";
            }
            else if (string.IsNullOrEmpty(uPwd))
            {
                lblPwdErrMsg.Text = "请输入密码";
            }
            else
            {
                Func<string> longTask = new Func<string>(delegate ()
                {
                    //  模拟长时间任务
                    ///  Thread.Sleep(2000);
                    return userLogin(uName,uPwd);
                    //  返回任务结果：5
                });
                //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                longTask.BeginInvoke(ar =>
                {
                    //  使用EndInvoke获取到任务结果（5）
                    string result = longTask.EndInvoke(ar);

                    //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                    //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                    if (result != null)
                    {
                        Invoke(new Action(() =>

                       endLogin(result)
                        // dataGridView1.DataSource = result

                        ));
                    }
                }, null);
            }
        }

        public string  userLogin(string uName,string uPwd)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks;

                var pwd = "{0}{1}".FormatWith(uPwd, loginKey);
                var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                var signstr = "{0}{1}{2}{3}{4}{5}".FormatWith(libraryId, signPwd, machineId, timeStamp, uName, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}/public/face/login?uName={1}&sipPwd={2}&libraryPwd={3}&libraryId={4}&machineId={5}&timeStamp={6}&sign={7}".FormatWith(servicePath, uName, uPwd, signPwd, libraryId, machineId, timeStamp, sign);

                string res = WebHelper.HttpWebRequest(url, "", true);
                return res;
            }catch(Exception ex)
            {
                LogManager.WriteLogs("用户绑定登陆出错", ex.StackTrace, LogFile.Error);
                return "";
            }

        }


        public void endLogin(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {
                    lblPwdErrMsg.Text = "用户绑定登陆出错";
                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        var data = result["data"];
                        if (data == null)
                        {
                            lblPwdErrMsg.Text = "返回用户数据错误";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(data["uName"].ToString()))
                            {
                                lblUName.Text = data["uName"].ToString();
                            }
                            else
                            {
                                lblUName.Text = data["uMobile"].ToString();
                            }
                            if (!string.IsNullOrWhiteSpace(data["userRealName"].ToString()))
                            {
                                lblRealName.Text = data["userRealName"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["uIdcard"].ToString()))
                            {
                                lblIDCard.Text = data["uIdcard"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["gender"].ToString()))
                            {
                                lblSex.Text = data["gender"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["address"].ToString()))
                            {
                                lblAddress.Text = data["address"].ToString();
                            }

                            lblToken.Text = data["token"].ToString();
                            string faceUid = data["faceUid"].ToString();
                            if (!string.IsNullOrWhiteSpace(faceUid))
                            {
                                lblPwdErrMsg.Text = "该账号已绑定人脸";
                            }
                            else
                            {
                                btnFaceRegister.Enabled = true;
                                btnFaceRegister.BackColor = Color.DodgerBlue;
                                lblPwdErrMsg.Text = "账号登录成功";
                            }
                        }
                    }
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs("解析用户绑定登陆返回数据出错", ex.StackTrace, LogFile.Error);
                lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }

        #region 人脸识别
        private void markFaces()
        {
            try
            {
                // Image image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap;
                picShow.Image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap;
                //if (image != null)
                //{
                currentfdo = kfd.GetFaceRectangle(capture.QueryFrame());
                currentFaceFlag = 0;
                getCurrentFaceSample(0);
                capture.Stop();
                
                //}
            }
            catch
            {
            }
        }

        private void getCurrentFaceSample(int i)
        {
            try
            {

                if (currentfdo.facesRectangle.Count > 0)
                {
                   
                    
                    Image<Rgba, byte> result = currentfdo.originalImg.ToImage<Rgba, byte>().Resize(200, 200, Emgu.CV.CvEnum.Inter.Cubic);
                    //  result._EqualizeHist();//灰度直方图均衡化
                    //  sampleBox.Image = result.Bitmap;
                    picSample.Image = result.Bitmap;
                    path = AppDomain.CurrentDomain.BaseDirectory;
                    if (!string.IsNullOrEmpty(path))
                    {
                        path = AppDomain.CurrentDomain.BaseDirectory + "face";
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        
                    }
                    path = path + "\\123.jpg";
                    picSample.Image.Save(path);
                    timer2.Stop();
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("没有检测到人脸");
            }
        }

        public static string ImageToBase64(Image image)
        {
            ImageFormat format = image.RawFormat;
            using (MemoryStream ms = new MemoryStream())
            {
                if (format.Equals(ImageFormat.Jpeg))
                {
                    image.Save(ms, ImageFormat.Jpeg);
                }
                else if (format.Equals(ImageFormat.Png))
                {
                    image.Save(ms, ImageFormat.Png);
                }
                else if (format.Equals(ImageFormat.Bmp))
                {
                    image.Save(ms, ImageFormat.Bmp);
                }
                else if (format.Equals(ImageFormat.Gif))
                {
                    image.Save(ms, ImageFormat.Gif);
                }
                else if (format.Equals(ImageFormat.Icon))
                {
                    image.Save(ms, ImageFormat.Icon);
                }
                byte[] bytes = new byte[ms.Length];
                //Image.Save()会改变MemoryStream的Position，需要重新Seek到Begin
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(bytes, 0, bytes.Length);
                string base64 = Convert.ToBase64String(bytes);
                return base64;
            }

        }


        public void changeData(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {
                    lblPwdErrMsg.Text = "人脸绑定出错";
                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        lblPwdErrMsg.Text = "人脸绑定成功，您可以返回首页开始借书了！";
                    }
                    else
                    {
                        lblPwdErrMsg.Text = "人脸绑定失败";
                        LogManager.WriteLogs("人脸绑定失败", result["msg"].ToString(), LogFile.Record);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("解析人脸绑定返回数据出错", ex.StackTrace, LogFile.Error);
                lblPwdErrMsg.Text = "解析人脸绑定返回数据出错";
            }
        }

        public string getData(string fs)
        {
            string dt = "";

            try
            {
                var time = DateTime.Now.Ticks;
                string token = lblToken.Text;
                if (string.IsNullOrWhiteSpace(token))
                {
                    lblPwdErrMsg.Text = "请先用账号+密码登录";
                    return "";
                }
                var signstr = "{0}{1}{2}".FormatWith(time, token, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/addFace".FormatWith(servicePath);
                string parameters = "image={0}&token={1}&timeStamp={2}&sign={3}".FormatWith(fs, token, time, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

               
                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("人脸绑定出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }



        /// <summary>
        /// 实时获取图像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arg"></param>
        private void frameProcess(object sender, EventArgs arg)
        {
            matImg = new Mat();
            capture.Retrieve(matImg, 0);
            picShow.Image = matImg.Bitmap;
        }





        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void FaceRegister_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
            timer2.Stop();
        }

        private void btnMarkFaces_Click(object sender, EventArgs e)
        {
            capture.Start();
            timer2.Start();
            
        }
    }
}
