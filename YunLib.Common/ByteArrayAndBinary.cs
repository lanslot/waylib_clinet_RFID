﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.Common
{
    /// <summary>
    /// 字节数组和二进制互转
    /// </summary>
    public class ByteArrayAndBinary
    {
        /// <summary>
        /// 二进制样式的字符串转byte数组
        /// </summary>
        /// <param name="binaryStr">二进制样式的字符串</param>
        /// <returns></returns>
        public static byte[] BinaryStr2ByteArray(string binaryStr)
        {
            if (string.IsNullOrEmpty(binaryStr)) binaryStr = string.Empty;

            List<byte> byte_List = new List<byte>();
            var strL = binaryStr.Length / 8;
            if (strL == 0)
                byte_List.Add(0);
            else if (strL > 0 && strL <= 4)
                byte_List.Add(Convert.ToByte(binaryStr, 2));
            else
            {
                var tempStr = string.Empty;
                for (var i = 0; i < strL; i++)
                {

                    tempStr = binaryStr.Substring(i * 8, 8);

                    byte_List.Add(Convert.ToByte(tempStr, 2));
                }
            }

            return byte_List.ToArray();
        }


        /// <summary>
        /// byte数组转二进制样式的字符串集合
        /// </summary>
        /// <param name="byteArr">byte数组</param>
        /// <returns></returns>
        public static List<string> ByteArray2BinaryStr(byte[] byteArr)
        {
            List<string> strList = new List<string>();
            var bl = byteArr.Length;
            if (bl == 0)
                return null;
            else
            {
                var tempStr = string.Empty;
                for (var i = 0; i < bl; i++)
                {
                    tempStr = Convert.ToString(byteArr[i], 2);
                    if (tempStr.Length < 4)
                        tempStr = tempStr.PadLeft(4, '0');
                    strList.Add(tempStr);
                }
            }

            return strList;
        }

        /// <summary>
        /// byte数组转指定长度二进制样式的字符串集合
        /// </summary>
        /// <param name="byteArr">byte数组</param>
        /// <returns></returns>
        public static List<string> ByteArray2BinaryStrByBitLenth(byte[] byteArr, int bitLenth)
        {
            List<string> strList = new List<string>();
            var bl = byteArr.Length;
            if (bl == 0)
                return null;
            else
            {
                var tempStr = string.Empty;
                for (var i = 0; i < bl; i++)
                {
                    tempStr = Convert.ToString(byteArr[i], 2).PadLeft(8, '0');
                    string str = tempStr.Substring(tempStr.Length - bitLenth, bitLenth);
                    strList.Add(str);
                }
            }

            return strList;
        }

        /// <summary>
        /// 16进制转字节数组
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }
    }
}
