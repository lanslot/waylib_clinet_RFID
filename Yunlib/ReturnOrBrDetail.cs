﻿using System;
using YunLib.BLL;
using Yunlib.Entity;
using YunLib.Common;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using Newtonsoft.Json.Linq;
using Yunlib.Extensions;
using Yunlib.Common;
using System.Collections.Generic;
using System.Net;

namespace Yunlib
{
    public partial class ReturnOrBrDetail : Form
    {
        public ReturnOrBrDetail()
        {
            InitializeComponent();
        }

        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        String machineId = ConfigManager.MachineID;
        String libraryId = ConfigManager.LibraryID;
        String commonKey = ConfigManager.CommonKey;
        String servicePath = ConfigManager.ServicePath;

        public BooksInfo bookModel { get; set; }

        public static string overplusDyas { get; set; }

        private JObject GetBooksInfo(string barCode)
        {
            var timeStamp = DateTime.Now.Ticks.ToString();

            var signStr = "{0}{1}{2}{3}".FormatWith(barCode, libraryId, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}/public/book/lendinfo?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(
                servicePath, barCode, libraryId, timeStamp, sign);

            var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();
            return callback;
        }

        private Image GetBooksPic(string resource, string fileName)
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile(resource, fileName);
                return Image.FromFile(fileName);
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("图片路径：{0}---图片名称：{1}获取网络图片资源出错，错误信息：{2}".FormatWith(resource, fileName, ex.Message));
                throw;
            }
        }

        private void ReturnOrBrDetail_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;

                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                string barCode = bookModel.BarCode.Trim();

                //CodeOptimize.SolveLightScreen(this);

                //MessageBox.Show(bookModel.CoverUrl);

                this.pictureBox5.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Cover_NoBook1.png");

                //if (!string.IsNullOrWhiteSpace(bookModel.CoverUrl))
                //{
                //    this.pictureBox1.BackgroundImage = GetBooksPic(bookModel.CoverUrl, DateTime.Now.ToLongTimeString() + ".png");
                //}
                //else
                //{
                //    this.pictureBox1.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Cover_NoBook1.png");
                //}

                //借书时间
                lblBorrowDate.Text = bookModel.BorrowDate.ToString("yyyy-MM-dd HH:mm:ss");
                //已借天数
                lblBorrowDays.Text = (DateTime.Now - bookModel.BorrowDate).Days.ToString() + "天";
                //剩余天数
                lblOverplusDays.Text = (bookModel.ShouldReturnDate - DateTime.Now).Days.ToString() + "天";
                //书名
                lblBooksName.Text = bookModel.BooksName;
                //作者
                lblAuthor.Text = bookModel.Author;

                timer1.Start();
                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("ReturnOeBrDetial_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
            }
            catch (Exception)
            {
                this.pictureBox5.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Cover_NoBook1.png");
                //借书时间
                lblBorrowDate.Text = bookModel.BorrowDate.ToString("yyyy-MM-dd HH:mm:ss");
                //已借天数
                lblBorrowDays.Text = (DateTime.Now - bookModel.BorrowDate).Days.ToString() + "天";
                //剩余天数
                lblOverplusDays.Text = (bookModel.ShouldReturnDate - DateTime.Now).Days.ToString() + "天";
                //书名
                lblBooksName.Text = bookModel.BooksName;
                //作者
                lblAuthor.Text = bookModel.Author;

                timer1.Start();
                throw;
            }
        }

        #region 续借
        private void pBtnOnceBorrowBook_Click(object sender, EventArgs e)
        {
            try
            {
                long timeStamp = DateTime.Now.Ticks;

                //获取输入的条形码
                string barCode = bookModel.BarCode.Trim();

                String signStr = "{0}{1}{2}{3}{4}".FormatWith(barCode, libraryId, machineId, timeStamp, commonKey);
                String sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                String url = "{0}/public/book/booklendtimeservice?bookId={1}&machineId={2}&timeStamp={3}&sign={4}&libraryId={5}".FormatWith(
                    servicePath, barCode, machineId, timeStamp, sign, libraryId);
                string[] s = new string[1];
                s[0] = ConfigManager.PingServicePath;
                bool flag = NetTest.CheckServeStatus(s);
                //验证网络连接状态，true就发起请求，false不请求
                if (flag)
                {
                    var back = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                    if (back["code"].ToString() == "100")
                    {
                        BorrowSuccess model = new BorrowSuccess { bookModel = bookModel, MdiParent = ParentForm, Dock = DockStyle.Fill };

                        this.Close();

                        model.Show();
                    }
                    else
                    {
                        BorrowFailure successModel = new BorrowFailure { bookModel = bookModel, MdiParent = ParentForm, Dock = DockStyle.Fill };

                        this.Close();

                        successModel.Show();
                    }
                }
                else
                {
                    BorrowFailure model = new BorrowFailure { bookModel = bookModel, MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Close();

                    model.Show();
                }
                timer1.Enabled = false;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }
        #endregion

        JObject responseData;
        string url;
        #region 还书
        private void pBtnReturnBook_Click(object sender, EventArgs e)
        {
            try
            {
                Thread t = new Thread(ShowDatatable);
                t.IsBackground = true;
                t.Start();
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("还书出现错误：{0}".FormatWith(ex.Message));
                AlertMsg alert = new AlertMsg("还书出现错误，请联系管理员！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                alert.ShowDialog();
            }
        }

        public delegate void ShowDatatableDelegate();
        /// <summary>
        　　　  /// 如果InitDt放在BindDt之中去实现，同样会造成UI卡顿。因为Invoke中的委托方法会阻塞UI线程
        /// </summary>
        private void ShowDatatable()
        {
            //this.BeginInvoke(new     ShowDatatableDelegate(BindDt),new object[]{dt});
            this.Invoke(new ShowDatatableDelegate(BindDt));
        }

        private void BindDt() {
            try
            {

                //获取输入的条形码
                string barCode = bookModel.BarCode.Trim();


                var timeStamp = DateTime.Now.Ticks;
                var signStr = "{0}{1}{2}{3}{4}".FormatWith(barCode, libraryId, machineId, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/book/backService?bookId={1}&machineId={2}&timeStamp={3}&sign={4}&libraryId={5}".FormatWith(
                    servicePath, barCode, machineId, timeStamp, sign, libraryId);

                string[] s = new string[1];
                s[0] = ConfigManager.PingServicePath;
                bool flag = NetTest.CheckServeStatus(s);
                //验证网络连接状态，true就发起请求，false不请求
                if (flag)
                {
                    var IsStill = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                    responseData = IsStill;

                    if (IsStill["code"].ToInt() != 100)
                    {
                        timer1.Stop();

                        ReturnFailture Frm = new ReturnFailture { MdiParent = ParentForm, Dock = DockStyle.Fill };


                        Frm.errorMessage = IsStill["msg"].ToString();

                        //  MessageBox.Show("还书失败原因：{0}".FormatWith(IsStill["message"].ToString()));

                        this.Close();

                        Frm.Show();
                    }
                    else
                    {
 ReturnSuccess Frm = new ReturnSuccess { MdiParent = ParentForm, Dock = DockStyle.Fill };
                        string doorNum = IsStill["data"]["machLatNum"].ToString();
                        string msg = doorNum + "号柜门";
                        if (doorNum != "9999")
                        {
                           
                            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                            CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                            LogManager.BorrowRecord("还书成功___DoorID:{0},DoorCode:{1}".FormatWith(list[0].DoorID, list[0].DoorCode));
                        }
                        else
                        {
                            msg = "还书箱";
                            DAMControl dam = new DAMControl();
                            dam.OpenDO(1);
                            Frm.IsNeedClose = "1";
                        }
                       
                        Frm.msg = msg;
                        this.Close();

                        Frm.Show();
                    }
                }
                else
                {
                    MessageBox.Show("网络繁忙，请稍后再试！");
                }


            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("后台返回的Json：{1}{0}".FormatWith(ex.Message, responseData.ToJson()));
                AlertMsg alert = new AlertMsg("还书出现错误，请联系管理员！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                alert.ShowDialog();
            }
        }
        
        #endregion

        #region 改变控件样式
        //点击"续借后",变按钮样式
        private void pBtnOnceBorrowBook_MouseDown(object sender, MouseEventArgs e)
        {
            this.pBtnOnceBorrowBook.Image = Image.FromFile(Application.StartupPath + "/Image/btnOnceBorrowBooked1.png");
            this.pBtnOnceBorrowBook.Width = 107;
            this.pBtnOnceBorrowBook.Height = 99;
            this.pBtnOnceBorrowBook.BackgroundImageLayout = ImageLayout.Stretch;
        }

        //点击"还书后",变按钮样式
        private void pBtnReturnBook_MouseDown(object sender, MouseEventArgs e)
        {
            this.pBtnReturnBook.Image = Image.FromFile(Application.StartupPath + "/Image/btnReturnBooked1.png");
            this.pBtnReturnBook.Width = 107;
            this.pBtnReturnBook.Height = 99;
            this.pBtnReturnBook.BackgroundImageLayout = ImageLayout.Stretch;
        }
        #endregion

        #region 窗体扩展事件

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        #endregion

        #region 控件事件函数
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDataTimeNow.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            Main Frm = new Main { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Close();

            Frm.Show();
        }
        #endregion

    }
}
