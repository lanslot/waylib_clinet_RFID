﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;

namespace Yunlib.DAL
{
    public class LogManager
    {
        static object locker = new object();
        /// <summary>
        /// 重要信息写入日志
        /// </summary>
        /// <param name="logs">日志列表，每条日志占一行</param>
        public static void WriteProgramLog(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                if (!Directory.Exists(LogAddress + "\\PRG"))
                {
                    Directory.CreateDirectory(LogAddress + "\\PRG");
                }
                LogAddress = string.Concat(LogAddress, "\\PRG\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, '-',
                 DateTime.Now.Hour, "_program.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void BorrowRecord(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                if (!Directory.Exists(LogAddress + "\\ZooKeeperLog"))
                {
                    Directory.CreateDirectory(LogAddress + "\\ZooKeeperLog");
                }
                LogAddress = string.Concat(LogAddress, "\\ZooKeeperLog\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, '-',
                 DateTime.Now.Hour, "_ZooKeeperRecord.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void ErrorRecord(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                if (!Directory.Exists(LogAddress + "\\ErrorLog"))
                {
                    Directory.CreateDirectory(LogAddress + "\\ErrorLog");
                }
                LogAddress = string.Concat(LogAddress, "\\ErrorLog\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, '-',
                 DateTime.Now.Hour, "_ErrorRecord.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void TimeRecord(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                if (!Directory.Exists(LogAddress + "\\TimeRecordLog"))
                {
                    Directory.CreateDirectory(LogAddress + "\\TimeRecordLog");
                }
                LogAddress = string.Concat(LogAddress, "\\TimeRecordLog\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, '-',
                 DateTime.Now.Hour, "_ErrorRecord.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void WriteLogs(string title, string content, LogFile type)
        {
            lock (locker)
            {
                string path = AppDomain.CurrentDomain.BaseDirectory;
                if (!string.IsNullOrEmpty(path))
                {
                    path = AppDomain.CurrentDomain.BaseDirectory + "log";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    path = path + "\\" + type.GetDescription();

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    path = path + "\\" + DateTime.Now.ToString("yyMM");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    path = path + "\\" + DateTime.Now.ToString("dd HH") + ".txt";
                    if (!File.Exists(path))
                    {
                        FileStream fs = File.Create(path);
                        fs.Close();
                    }
                    if (File.Exists(path))
                    {
                        StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default);
                        sw.WriteLine(DateTime.Now);
                        sw.WriteLine("日志位置：" + title);
                        sw.WriteLine("详情：" + content);
                        sw.WriteLine("----------------------------------------");
                        sw.Close();
                    }
                }
            }
        }
    }

    public enum LogFile
    {
        [Description("Record")]
        Record = 0,
        [Description("Error")]
        Error = 1,
        [Description("SQL")]
        SQL = 2
    }
}
