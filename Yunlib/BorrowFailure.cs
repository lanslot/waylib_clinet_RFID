﻿using System;
using Yunlib.Entity;
using System.Windows.Forms;
using Yunlib.Extensions;
using Yunlib.Common;
using YunLib.Common;

namespace Yunlib
{
    public partial class BorrowFailure : Form
    {
        public BooksInfo bookModel { get; set; }

        public BorrowFailure()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        private void lblOnceBorrowFailtureMsg_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            CodeOptimize.SolveLightScreen(this);
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("BookFailure_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void pic_btnTurnBack_Click(object sender, EventArgs e)
        {
            //返回1Main加载时不执行开灯操作
          // //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;

            timer1.Enabled = false;

            //返回主界面
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";
            if (int.Parse(lblSecond.Text) < 0)
            {
                timer1.Enabled = false;

                //返回1Main加载时不执行开灯操作
              // //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;

                //返回主界面
                //Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void lblSecond_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblOnceBorrowErrMsg_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
