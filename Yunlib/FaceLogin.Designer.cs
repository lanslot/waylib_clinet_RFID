﻿namespace Yunlib
{
    partial class FaceLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblGun = new System.Windows.Forms.Label();
            this.lblMiao = new System.Windows.Forms.Label();
            this.lblDaoJiShi = new System.Windows.Forms.Label();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.picSample = new System.Windows.Forms.PictureBox();
            this.picShow = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.lblUName = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.lblRealName = new System.Windows.Forms.Label();
            this.lblToken = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.btnMarkFaces = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShow)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Location = new System.Drawing.Point(610, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(26, 31);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblGun);
            this.panel1.Controls.Add(this.lblMiao);
            this.panel1.Controls.Add(this.lblDaoJiShi);
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(642, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 57);
            this.panel1.TabIndex = 14;
            // 
            // lblGun
            // 
            this.lblGun.BackColor = System.Drawing.Color.Transparent;
            this.lblGun.Font = new System.Drawing.Font("微软雅黑", 17F);
            this.lblGun.ForeColor = System.Drawing.Color.White;
            this.lblGun.Location = new System.Drawing.Point(221, 13);
            this.lblGun.Name = "lblGun";
            this.lblGun.Size = new System.Drawing.Size(27, 31);
            this.lblGun.TabIndex = 4;
            this.lblGun.Text = "丨";
            // 
            // lblMiao
            // 
            this.lblMiao.BackColor = System.Drawing.Color.Transparent;
            this.lblMiao.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblMiao.ForeColor = System.Drawing.Color.Orange;
            this.lblMiao.Location = new System.Drawing.Point(297, 11);
            this.lblMiao.Name = "lblMiao";
            this.lblMiao.Size = new System.Drawing.Size(36, 31);
            this.lblMiao.TabIndex = 3;
            this.lblMiao.Text = "秒";
            // 
            // lblDaoJiShi
            // 
            this.lblDaoJiShi.BackColor = System.Drawing.Color.Transparent;
            this.lblDaoJiShi.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDaoJiShi.ForeColor = System.Drawing.Color.Orange;
            this.lblDaoJiShi.Location = new System.Drawing.Point(227, 12);
            this.lblDaoJiShi.Name = "lblDaoJiShi";
            this.lblDaoJiShi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDaoJiShi.Size = new System.Drawing.Size(70, 31);
            this.lblDaoJiShi.TabIndex = 3;
            this.lblDaoJiShi.Text = "120";
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::Yunlib.Properties.Resources._7_03;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.btnMarkFaces);
            this.panel2.Controls.Add(this.lblError);
            this.panel2.Controls.Add(this.lblToken);
            this.panel2.Controls.Add(this.lblAddress);
            this.panel2.Controls.Add(this.picSample);
            this.panel2.Controls.Add(this.lblIDCard);
            this.panel2.Controls.Add(this.picShow);
            this.panel2.Controls.Add(this.lblUName);
            this.panel2.Controls.Add(this.lblRealName);
            this.panel2.Controls.Add(this.lblSex);
            this.panel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel2.Location = new System.Drawing.Point(67, 147);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(899, 448);
            this.panel2.TabIndex = 16;
            // 
            // picSample
            // 
            this.picSample.Location = new System.Drawing.Point(681, 31);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(149, 156);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 2;
            this.picSample.TabStop = false;
            // 
            // picShow
            // 
            this.picShow.Location = new System.Drawing.Point(43, 44);
            this.picShow.Name = "picShow";
            this.picShow.Size = new System.Drawing.Size(247, 310);
            this.picShow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picShow.TabIndex = 0;
            this.picShow.TabStop = false;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAddress.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAddress.Location = new System.Drawing.Point(460, 184);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(0, 21);
            this.lblAddress.TabIndex = 51;
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.BackColor = System.Drawing.Color.Transparent;
            this.lblIDCard.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblIDCard.Location = new System.Drawing.Point(460, 135);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(0, 21);
            this.lblIDCard.TabIndex = 50;
            // 
            // lblUName
            // 
            this.lblUName.AutoSize = true;
            this.lblUName.BackColor = System.Drawing.Color.Transparent;
            this.lblUName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUName.Location = new System.Drawing.Point(460, 100);
            this.lblUName.Name = "lblUName";
            this.lblUName.Size = new System.Drawing.Size(0, 21);
            this.lblUName.TabIndex = 49;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.BackColor = System.Drawing.Color.Transparent;
            this.lblSex.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSex.Location = new System.Drawing.Point(460, 65);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(0, 21);
            this.lblSex.TabIndex = 48;
            // 
            // lblRealName
            // 
            this.lblRealName.AutoSize = true;
            this.lblRealName.BackColor = System.Drawing.Color.Transparent;
            this.lblRealName.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRealName.Location = new System.Drawing.Point(460, 31);
            this.lblRealName.Name = "lblRealName";
            this.lblRealName.Size = new System.Drawing.Size(0, 21);
            this.lblRealName.TabIndex = 47;
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.BackColor = System.Drawing.Color.Transparent;
            this.lblToken.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblToken.Location = new System.Drawing.Point(392, 292);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(0, 21);
            this.lblToken.TabIndex = 52;
            this.lblToken.Visible = false;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblError.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblError.Location = new System.Drawing.Point(416, 353);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 27);
            this.lblError.TabIndex = 53;
            // 
            // btnMarkFaces
            // 
            this.btnMarkFaces.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(178)))), ((int)(((byte)(56)))));
            this.btnMarkFaces.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarkFaces.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMarkFaces.ForeColor = System.Drawing.Color.White;
            this.btnMarkFaces.Location = new System.Drawing.Point(67, 374);
            this.btnMarkFaces.Name = "btnMarkFaces";
            this.btnMarkFaces.Size = new System.Drawing.Size(196, 45);
            this.btnMarkFaces.TabIndex = 35;
            this.btnMarkFaces.Text = "重 新 识 别";
            this.btnMarkFaces.UseVisualStyleBackColor = false;
            this.btnMarkFaces.Click += new System.EventHandler(this.btnMarkFaces_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(178)))), ((int)(((byte)(56)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(816, 657);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 45);
            this.button1.TabIndex = 54;
            this.button1.Text = "返     回";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightSlateGray;
            this.button2.Enabled = false;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(452, 225);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(205, 45);
            this.button2.TabIndex = 54;
            this.button2.Text = "确   认";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FaceLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FaceLogin";
            this.Text = "FaceLogin";
            this.Load += new System.EventHandler(this.FaceLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblGun;
        private System.Windows.Forms.Label lblMiao;
        private System.Windows.Forms.Label lblDaoJiShi;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox picShow;
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.Label lblUName;
        private System.Windows.Forms.Label lblRealName;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label lblToken;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Button btnMarkFaces;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}