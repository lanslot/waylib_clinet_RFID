﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YunLib_Vertical
{
    public partial class NumJPForm : Form
    {
        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATE = 0x0003;
        public NumJPForm()
        {
            InitializeComponent();
        }

        private void NumJPForm_Load(object sender, EventArgs e)
        {

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_NOACTIVATE;
                return cp;
            }

        }
        protected override void WndProc(ref Message m)
        {
            //If we're being activated because the mouse clicked on us...
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                //Then refuse to be activated, but allow the click event to pass through (don't use MA_NOACTIVATEEAT)
                m.Result = (IntPtr)MA_NOACTIVATE;
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        #region
       

        private void pic1_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_114;
        }

        private void pic1_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_114;
        }

        private void pic2_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_116;
        }

        private void pic2_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_116;
        }

        private void pic3_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_117;
        }

        private void pic3_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_117;
        }

        private void pic4_MouseDown(object sender, MouseEventArgs e)
        {

            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_121;
        }

        private void pic4_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_121;
        }

        private void pic5_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_122;
        }

        private void pic5_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_122;
        }

        private void pic6_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_123;
        }

        private void pic6_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_123;
        }

        private void pic7_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_127;
        }

        private void pic7_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_127;
        }

        private void pic8_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_128;
        }

        private void pic8_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_128;
        }

        private void pic9_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_129;
        }

        private void pic9_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_129;
        }

        private void picDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_del_2;
        }
        private void picDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_del_1;
        }
        private void pic0_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_134;
        }

        private void pic0_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_134;
        }



        private void picSure_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_135;
        }

        private void picSure_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_135;
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            string strkey = p.Name.Substring(3);

            SendKeys.Send("{" + strkey + "}");
        }
        #endregion

        private void picSure_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
