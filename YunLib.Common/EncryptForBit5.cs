﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.Common
{
    public class EncryptForBit5
    {
        /// <summary>
        /// bit5加密
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string EncryptToBit5(string message)
        {
            byte[] strByte = Encoding.ASCII.GetBytes(message);
            List<string> binaryStrList = new List<string>();
            binaryStrList = ByteArrayAndBinary.ByteArray2BinaryStr(strByte);

            string str = "";
            for (int i = 0; i < binaryStrList.Count; i++)
            {
                string s1 = "00000000";
                string s = binaryStrList[i];
                if (s.Length < 8)
                {
                    s = s1.Substring(0, 8 - s.Length) + s;
                }
                str += s.Substring(2, 6);
            }
            string s2 = "100000";
            if (str.Length % 8 != 0)
            {
                str += s2.Substring(0, 8 - str.Length % 8);
            }

            byte[] newStrByte = ByteArrayAndBinary.BinaryStr2ByteArray(str);
            string backStr = byteToHexStr(newStrByte);


            return backStr;
        }

        /// <summary>
        /// bit5解密
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string DecryptToBit5(string message)
        {
            byte[] buffer = HexStringToByteArray(message);

            List<string> list = new List<string>();
            list = ByteArrayAndBinary.ByteArray2BinaryStr(buffer);
            string binaryStr = "";
            for(int i = 0; i < list.Count; i++)
            {
                string str = list[i];
                string ss = "00000000";
                if(str.Length < 8)
                {
                    str = ss.Substring(0, 8 - str.Length) + str;
                }
                binaryStr += str;
            }

            string newBinaryStr = "";
            List<string> bufferList = new List<string>();
            for(int i = 0; i < binaryStr.Length / 6; i++)
            {
                string str =binaryStr.Substring(i * 6, 6);
                if(str.Substring(0,1) == "1")
                {
                    str = "00" + str;
                }else if(str.Substring(0,1) == "0")
                {
                    str = "01" + str;
                }
                newBinaryStr += str;
            }

            byte[] newStrByte = ByteArrayAndBinary.BinaryStr2ByteArray(newBinaryStr);

            string backStr = Encoding.ASCII.GetString(newStrByte);
            return backStr;
        }
        /// <summary>
        /// 16进制转字节数组
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }

        /// <summary>
        /// 此方法用于将普通字符串转换成16进制的字符串。
        /// </summary>
        /// <param name="_str">要转换的字符串。</param>
        /// <returns></returns>
        public static List<string> StringToHex16String(string _str)
        {
            List<string> list = new List<string>();
            //将字符串转换成字节数组。
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(_str);
            
            for (int i = 0; i < buffer.Length; i++)
            {
                //将每一个字节数组转换成16进制的字符串，以空格相隔开。
                list.Add(Convert.ToString(buffer[i], 16));
            }
            return list;
        }

        /// <summary>
        /// 此方法用于将16进制的字符串转换成普通字符串。
        /// </summary>
        /// <param name="_hex16String">要转换的16进制的字符串。</param>
        /// <returns></returns>
        private static string Hex16StringToString(string _hex16String)
        {
            string result = string.Empty;
            //以空格分割字符串。
            string[] chars = _hex16String.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //声明一个字节数组，其长度等于分割的字符串的长度。
            byte[] buffer = new byte[chars.Length];
            for (int i = 0; i < buffer.Length; i++)
            {
                //将chars数组中的元素转换成字节数组。
                buffer[i] = Convert.ToByte(chars[i], 16);
            }
            //将字节数组转换成字符串。
            result = System.Text.Encoding.UTF8.GetString(buffer);
            //返回。
            return result;
        }

    }
}
