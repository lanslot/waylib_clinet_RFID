﻿using MT3;
using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ShenZhen.SmartLife.Control;
using Yunlib.SellBook.Extensions;
using YunLib.Common;
using Yunlib.Common;
using ThoughtWorks.QRCode.Codec;
using System.Drawing;
using Newtonsoft.Json.Linq;
using YunLib.BLL;
using Yunlib.Entity;
using System.Configuration;
using Yunlib.SellBook.AdminManage;
using System.Data;
using System.Collections.Generic;
using Yunlib.SellBook.Properties;

namespace Yunlib.SellBook
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        String machineId = ConfigManager.MachineID;
        String libraryId = ConfigManager.LibraryID;
        String commonKey = ConfigManager.CommonKey;
        String servicePath = ConfigManager.ServicePath;

        //public static int IsInitOpen = 0;
        

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;
                //Main frm default loading is Max screen
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm:ss");

               
            
               
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseReason s = e.CloseReason;

          
           
           
        }

        #region 窗体扩展事件
        //private void LoadCreateQrCode()
        //{
        //    Image image = CreateImage(string.Format("{0}/wx-manager-putbook.html?libraryId={1}&machineId={2}", ConfigManager.ServicePath, ConfigManager.LibraryID, ConfigManager.MachineID));
        //    if (image != null)
        //    {
        //        picEncode.Image = image;
        //    }
        //}

        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }

      

      

       
        #endregion

        #region 控件事件函数
       

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm:ss");
            string[] s = new string[1];
            s[0] = ConfigManager.PingServicePath;
            bool flag = NetTest.CheckServeStatus(s);
            if (flag)
            {
                if (!string.IsNullOrWhiteSpace(lblNetTest.Text))
                {
                    lblNetTest.Text = "";
                }
                if (!pBtnBorrow.Enabled)
                {
                    pBtnBorrow.Enabled = true;
                }

            }
            else
            {
                if (string.IsNullOrWhiteSpace(lblNetTest.Text))
                {
                    lblNetTest.Text = "网络繁忙，请稍后操作......";
                    
                }
                if (pBtnBorrow.Enabled)
                {
                    pBtnBorrow.Enabled = false;
                }
            }
            
        }

        //续借或者还书
     

        //借书
        private void pBtnBorrow_Click(object sender, EventArgs e)
        {
            try
            {
             
                
                //跳转页面前先关闭主页的计时器
                timerLogin.Enabled = false;

                //先断开当前窗体的连接
              

                // BorrowBook borrowBookFrm = Singleton<BorrowBook>.CreateInstance();
                BorrowBook borrowBookFrm = new BorrowBook { MdiParent = ParentForm, Dock = DockStyle.Fill };

                this.Hide();

                borrowBookFrm.Show();

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        
        #endregion

        private void picBtnSure_Click(object sender, EventArgs e)
        {
            try
            {
                string borrCode = openCode;

                if (string.IsNullOrWhiteSpace(borrCode) || borrCode == "请输入8位数开柜码")
                {
                    AlertMsg alert = new AlertMsg("开柜码不能为空！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                    alert.ShowDialog();
                }
                else
                {
                    JObject callback = GetBooksInfo(borrCode);

                    if (callback["code"].ToString() != "100")
                    {
                        AlertMsg alert = new AlertMsg("请输入正确的开柜码！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                        alert.ShowDialog();
                    }
                    else
                    {
                        string doorNumStr = callback["data"]["lendLat"].ToString();
                        string doorNum = doorNumStr.Replace(machineId, "");

                      //  LocalBooks currentModel = accessBLL.GetBookByBarCode(doorNum);
                        var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                        if (list.Count>0)
                        {

                            CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                            LogManager.ErrorRecord("开柜码借书成功！书籍barCode：{0},书籍名称：{1}".FormatWith(list[0].DoorID, list[0].DoorCode));
                        }
                        else
                        {
                            AlertMsg alert = new AlertMsg("请勿重复输入！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                            alert.ShowDialog();

                            Thread.Sleep(2000);

                            alert.label1_Click(null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("开柜码错误信息：{0}".FormatWith(ex.Message));
                MessageBox.Show("网络繁忙，请稍后再试！", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private JObject GetBooksInfo(string borrCode)
        {
            var timeStamp = DateTime.Now.Ticks.ToString();

            var signStr = "{0}{1}{2}".FormatWith(borrCode, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}/public/book/openCase_2?code={1}&timeStamp={2}&sign={3}".FormatWith(
                servicePath, borrCode, timeStamp, sign);

            var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();

            return callback;
        }

      

      

        

        private void pBtnBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            pBtnBorrow.Image = Resources.z_d_03;
        }

     

        private void picOpenCode_MouseDown(object sender, MouseEventArgs e)
        {
            picOpenCode.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/kaiguima2.png");
        }

        string openCode = string.Empty;
        private void picOpenCode_Click(object sender, EventArgs e)
        {
             picOpenCode.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/kaiguima1.png");

            DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.Cancel)
            {
                //关闭虚拟键盘时,把值传到登录页面的文本框中
                openCode = DymicNewJP.inputCount;

                if (!string.IsNullOrEmpty(openCode))
                {
                    if (openCode.Length == 6)
                    {
                        if (openCode == "020912")
                        {
                            AdminManage.BackMain bm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

                            timerLogin.Enabled = false;

                            this.Hide();

                            bm.Show();
                        }
                        else
                        {
                            bool flag = CheckLogin(openCode);

                            if (!flag)
                            {
                                AlertMsg alert = new AlertMsg("请输入正确的开柜码！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                                alert.ShowDialog();
                            }
                            else
                            {
                                AdminManage.BackMain bm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };
                                
                                timerLogin.Enabled = false;
                                
                                this.Hide();

                                bm.Show();
                            }
                        }
                    }
                    else
                    {
                        picBtnSure_Click(null, null);
                    }
                }
            }
        }

       

        public bool CheckLogin(string pwd)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}".FormatWith(machineId, pwd, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}public/pubmachine/validate/managerLogin?machId={1}&password={2}&timeStamp={3}&sign={4}".FormatWith(
                    servicePath, machineId, pwd, timeStamp, sign);

                var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                if (callback["code"].ToString() == "100")
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void pBtnBorrow_MouseUp(object sender, MouseEventArgs e)
        {
            pBtnBorrow.Image = Resources.z_u_03;
        }
    }
}
