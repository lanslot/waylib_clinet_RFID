﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ContorTest
{
    public class ControlMachine
    {
        private int cmdPort = int.Parse(ConfigurationManager.AppSettings["CmdPort"]);
        private string imPort = ConfigurationManager.AppSettings["IMPort"];
        private string control = ConfigurationManager.AppSettings["Control"];
        private string scan = ConfigurationManager.AppSettings["Scan"];
        private SmartLifeBroker model = new SmartLifeBroker();
        public int icdev;   //communication equipment remark
        public short st;    //function return data

        

        public bool OpenDoor(int doorID,int doorCode)
        {
            model.Init(control, cmdPort);
            bool isOPen = model.OpenDoor(doorID,doorCode,4);
            model.Dispose();
            return isOPen;
        }

        public string ReadIM()
        {
            bool flag = OpenConn();
            //find im card
            byte[] datarecv = new byte[1000];
            byte[] rlen = new byte[8];
            string str = "";
            st = mt_32dll.rf_card(icdev, 0, datarecv, rlen);

            //label1.Text += st == 0 ? "M1卡寻卡成功" : "M1卡寻卡失败";
             str += st == 0 ? "M1卡寻卡成功" : "M1卡寻卡失败****** icdev:"+icdev;
            //valid
            byte[] key = new byte[10];

            key[0] = 0xff;
            key[1] = 0xff;
            key[2] = 0xff;
            key[3] = 0xff;
            key[4] = 0xff;
            key[5] = 0xff;
            st = mt_32dll.rf_authentication_key(icdev, 0, 48, key);

            //label1.Text += st == 0 ? "M1卡密码认证成功" : "M1卡密码认证失败";
            str += st == 0 ? "M1卡密码认证成功" : "M1卡密码认证失败*****";
            //read data
            byte[] sRecData = new byte[20];

            int accountCount = int.Parse(ConfigurationManager.AppSettings["AccountCount"]);
            byte[] RecData = new byte[accountCount];

            st = mt_32dll.rf_read(icdev, 48, sRecData);
            if (st != 0)
            {
                CloseConn();
                return str += st == 0 ? "M1卡密码认证成功" : "M1卡密码认证失败*****";
            }
            else
            {
                mt_32dll.hex_asc(sRecData, RecData, 10);

                //不管蜂鸣器叫不叫,都要执行数据的读取时间
                st = mt_32dll.dev_beep(icdev, 1, 1, 1);
                CloseConn();
                return Encoding.Default.GetString(RecData);
                
            }
        }
        public bool WriteIM(string msg) {
            byte[] key = new byte[10];
            byte[] sRecData = new byte[20];
            byte[] RecData = new byte[20];

            //byte[] WriteData = new byte[5];

            byte[] WriteData1 = new byte[20];
            byte[] d = new byte[40];
            byte[] rlen = new byte[8];
            byte[] datarecv = new byte[1000];
            byte[] datarecvasc = new byte[1000];
            long[] val = new long[10];




            //WriteData1[0] = 0x01;
            //WriteData1[1] = 0x23;
            //WriteData1[2] = 0x45;
            //WriteData1[3] = 0x67;
            //WriteData1[4] = 0x89;
            //WriteData1[5] = 0x00;
            //WriteData1[6] = 0x00;
            //WriteData1[7] = 0x00;
            //WriteData1[8] = 0x00;
            //WriteData1[9] = 0x00;
            //WriteData1[10] = 0x00;
            //WriteData1[11] = 0x00;
            //WriteData1[12] = 0x00;
            //WriteData1[13] = 0x00;
            //WriteData1[14] = 0x00;
            //WriteData1[15] = 0x00;

            st = mt_32dll.rf_card(icdev, 0, datarecv, rlen);
            //if (st != 0)
            //    listInfo.Items.Add("M1卡寻卡失败!");
            //else
            //    listInfo.Items.Add("M1卡寻卡成功!");

            key[0] = 0xff;
            key[1] = 0xff;
            key[2] = 0xff;
            key[3] = 0xff;
            key[4] = 0xff;
            key[5] = 0xff;
            st = mt_32dll.rf_authentication_key(icdev, 0, 48, key);
            //if (st != 0)
            //    listInfo.Items.Add("M1卡密码认证失败!");
            //else
            //    listInfo.Items.Add("M1卡密码认证成功!");


            st = mt_32dll.rf_read(icdev, 48, sRecData);
            //if (st != 0)
            //    listInfo.Items.Add("M1卡读数据失败!");
            //else
            //{
            //    mt_32dll.hex_asc(sRecData, RecData, 10);
            //    listInfo.Items.Add("M1卡读数据成功,数据:" + Encoding.Default.GetString(RecData));
            //};
            
            byte[] WriteData2 = Encoding.Default.GetBytes(msg);

            mt_32dll.asc_hex(WriteData2, WriteData1, 10);
            st = mt_32dll.rf_write(icdev, 48, WriteData1);
            if (st != 0)
                return false;// listInfo.Items.Add("M1卡写数据失败!");
            else
                // listInfo.Items.Add("M1卡写数据成功!");
                return true;

        }

        public bool OpenConn()
        {
            if (icdev > 1)
            {
                st = mt_32dll.close_device(icdev);
            }

            icdev = mt_32dll.open_device(3, 115200);

            if (icdev > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CloseConn()
        {
            st = mt_32dll.close_device(icdev);
            if (st != 0)
            {
                //断开失败
            }
            else
            {
                //断开成功
                icdev = 0;
            }
        }

    }
    #region im卡接口
    public class mt_32dll
    {
        public mt_32dll()
        {
            //

            //
        }

        [DllImport("mt_32.dll", EntryPoint = "open_device", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]

        public static extern int open_device(int nPort, uint ulBaud);

        [DllImport("mt_32.dll", EntryPoint = "close_device", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]

        public static extern Int16 close_device(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "hex_asc", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 hex_asc([MarshalAs(UnmanagedType.LPArray)]byte[] sHex, [MarshalAs(UnmanagedType.LPArray)]byte[] sAsc, ulong ulLength);

        [DllImport("mt_32.dll", EntryPoint = "asc_hex", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 asc_hex([MarshalAs(UnmanagedType.LPArray)]byte[] sAsc, [MarshalAs(UnmanagedType.LPArray)]byte[] sHex, ulong ulLength);

        [DllImport("mt_32.dll", EntryPoint = "ICC_Reset", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ICC_Reset(int icdev, byte nCardSet, [MarshalAs(UnmanagedType.LPArray)]byte[] sAtr, [MarshalAs(UnmanagedType.LPArray)]byte[] nAtrLen);

        [DllImport("mt_32.dll", EntryPoint = "ICC_PowerOn", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ICC_PowerOn(int icdev, byte nCardSet, [MarshalAs(UnmanagedType.LPArray)]byte[] sAtr, [MarshalAs(UnmanagedType.LPArray)]byte[] nAtrLen);

        [DllImport("mt_32.dll", EntryPoint = "ICC_CommandExchange", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ICC_CommandExchange(int icdev, byte nCardSet, [MarshalAs(UnmanagedType.LPArray)]byte[] sCmd, short nCmdLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sResp, [MarshalAs(UnmanagedType.LPArray)]byte[] nRespLen);

        [DllImport("mt_32.dll", EntryPoint = "ICC_PowerOff", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ICC_PowerOff(int icdev, byte nCardSet);

        [DllImport("mt_32.dll", EntryPoint = "ICC_CommandExchange_hex", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ICC_CommandExchange_hex(int icdev, byte nCardSet, [MarshalAs(UnmanagedType.LPArray)]byte[] sCmd, [MarshalAs(UnmanagedType.LPArray)]byte[] sResp);

        [DllImport("mt_32.dll", EntryPoint = "contact_select", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 contact_select(int icdev, byte nCardType);

        [DllImport("mt_32.dll", EntryPoint = "contact_verify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 contact_verify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nCardType);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_is42", SetLastError = true,
            CharSet = CharSet.Auto, ExactSpelling = false,
            CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_is42(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sCardState);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_read(int icdev, byte nAddr, short nDLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sRecData);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_write", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_write(int icdev, byte nAddr, short nWLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sWriteData);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_pwd_check", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_pwd_check(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        //[DllImport("mt_32.dll", EntryPoint = "sle4442_pwd_read", SetLastError = true,
        //     CharSet = CharSet.Auto, ExactSpelling = false,
        //     CallingConvention = CallingConvention.StdCall)]
        //public static extern Int16 sle4442_pwd_read(int icdev,[MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_pwd_modify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_pwd_modify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_probit_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_probit_read(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sProBitData);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_probit_write", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_probit_write(int icdev, byte nAddr, short nWLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sProBitData);

        [DllImport("mt_32.dll", EntryPoint = "sle4442_errcount_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4442_errcount_read(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nErrCount);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_is28", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_is28(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sCardState);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_read(int icdev, short nAddr, short nDLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sRecData);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_write", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_write(int icdev, short nAddr, short nWLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sWriteData);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_pwd_check", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_pwd_check(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_pwd_modify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_pwd_modify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_probit_readdata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_probit_readdata(int icdev, short nAddr, short nDLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sRecData);

        [DllImport("mt_32.dll", EntryPoint = "sle4428_probit_writedata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 sle4428_probit_writedata(int icdev, short nAddr, short nWLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sWriteData);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_pwd_check", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_pwd_check(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_pwd_modify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_pwd_modify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sNewKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_pwd_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_pwd_read(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_epwd_check", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_epwd_check(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_epwd_modify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_epwd_modify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sNewKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_epwd_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_epwd_read(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "open_device", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_epwd_check(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua2_epwd_modify", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_epwd_modify(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sNewKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua2_epwd_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_epwd_read(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sKey);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_pwd_errorcount", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_pwd_errorcount(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nErrorCount);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua2_fusecount", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_fusecount(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nErrorCount);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_probit_clr", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_probit_clr(int icdev, byte nProBitType);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_readdata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_readdata(int icdev, byte nAddr, byte nRLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sRecData);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua2_readdata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_readdata(int icdev, byte nAddr, byte nRLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sRecData);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_clrdata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_clrdata(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua2_clrdata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_clrdata(int icdev, byte nECState);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_Anafuse", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_Anafuse(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "open_device", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_Anafuse_cancel(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_ua1_modifydata", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua1_modifydata(int icdev, byte nAddr, byte nMLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sModifyData);

        [DllImport("mt_32.dll", EntryPoint = "open_device", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_ua2_modifydata(int icdev, byte nECState, byte nAddr, byte nMLen, [MarshalAs(UnmanagedType.LPArray)]byte[] sModifyData);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_fuse_issuerfuse", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_fuse_issuerfuse(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_fuse_ec2enfuse", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_fuse_ec2enfuse(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "at88sc102_is102", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 at88sc102_is102(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] nCardState);

        [DllImport("mt_32.dll", EntryPoint = "OpenCard", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 OpenCard(int icdev, byte mode, [MarshalAs(UnmanagedType.LPArray)]byte[] sAtr, [MarshalAs(UnmanagedType.LPArray)]byte[] snr, [MarshalAs(UnmanagedType.LPArray)]byte[] nAtrLen);

        [DllImport("mt_32.dll", EntryPoint = "ExchangePro", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 ExchangePro(int icdev, [MarshalAs(UnmanagedType.LPArray)]byte[] sAtr, short len, [MarshalAs(UnmanagedType.LPArray)]byte[] Rec, [MarshalAs(UnmanagedType.LPArray)]byte[] nAtrLen);

        [DllImport("mt_32.dll", EntryPoint = "CloseCard", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 CloseCard(int icdev);

        [DllImport("mt_32.dll", EntryPoint = "rf_card", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_card(int icdev, byte mode, [MarshalAs(UnmanagedType.LPArray)]byte[] sAtr, [MarshalAs(UnmanagedType.LPArray)]byte[] len);

        [DllImport("mt_32.dll", EntryPoint = "rf_authentication_key", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_authentication_key(int icdev, byte mode, byte Add, [MarshalAs(UnmanagedType.LPArray)]byte[] key);

        [DllImport("mt_32.dll", EntryPoint = "rf_read", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_read(int icdev, byte Add, [MarshalAs(UnmanagedType.LPArray)]byte[] read);

        [DllImport("mt_32.dll", EntryPoint = "rf_write", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_write(int icdev, byte Add, [MarshalAs(UnmanagedType.LPArray)]byte[] write);

        [DllImport("mt_32.dll", EntryPoint = "rf_initval", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_initval(int icdev, byte Add, long val);

        [DllImport("mt_32.dll", EntryPoint = "rf_increment", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_increment(int icdev, byte Add, long val);

        [DllImport("mt_32.dll", EntryPoint = "rf_decrement", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_decrement(int icdev, byte Add, long val);

        [DllImport("mt_32.dll", EntryPoint = "rf_readval", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 rf_readval(int icdev, byte Add, [MarshalAs(UnmanagedType.LPArray)]long[] val);



        [DllImport("mt_32.dll", EntryPoint = "dev_beep", SetLastError = true,
             CharSet = CharSet.Auto, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
        public static extern Int16 dev_beep(int icdev, byte nMsec, byte nMsec_end, byte nTime);
    }
    #endregion
}
