﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.LibServiceDataUpLoadTool.Common;

namespace YunLib.LibServiceDataUpLoadTool.Common
{
    public class WriteFiles
    {
        static object locker = new object();

        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <param name="type"></param>
        public static bool WriteDataToFile(string path, DataTable dt, FileTypeEnum fileType)
        {
            try
            {
                lock (locker)
                {

                    if (!string.IsNullOrEmpty(path))
                    {
                        //获取全局图书馆编码
                        string libCode = ConfigurationManager.AppSettings["LibCode"];
                        if (string.IsNullOrWhiteSpace(libCode))
                        {
                            LogManager.WriteLogs("数据写入文件出错", "全局图书馆编码不能为空", LogFile.Error);
                            return false;
                        }

                        //文件前缀
                        string filePrefix = (typeof(FileTypeEnum)).GetEnumDesc((int)fileType);

                        var time = DateTime.Now;

                        string fileName = libCode + "_" + filePrefix + "_" + string.Format("{0:yyyyMMdd}", time) + "_" + (CustomExtensions.ConvertDateTimeInt(time) / 1000).ToString().Substring(4) + ".gz";


                        path = path + "\\" + fileName;
                        if (!File.Exists(path))
                        {
                            FileStream fs = File.Create(path);
                            fs.Close();
                        }

                        if (File.Exists(path))
                        {
                            StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default);
                            foreach (DataRow row in dt.Rows)
                            {
                                string str = "";
                                for(int i = 0; i < row.ItemArray.Count(); i++)
                                {
                                    str +="\""+ row[i].ToString() + "\"|";
                                }
                                if (fileType == FileTypeEnum.FACT_LOAN)
                                {
                                    str += "\"1\"|\"0\"";
                                }else if(fileType == FileTypeEnum.BIBLIOS)
                                {
                                    str += "\"1\"";
                                }
                                LogManager.WriteLogs(str, "", LogFile.Record);
                                sw.WriteLine(str);
                            }
                            
                            sw.Close();
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return false;
            }
        }

        public static bool WriteDataToFileForSqlServer(string path, DataTable dt, FileTypeEnum fileType)
        {
            try
            {
                lock (locker)
                {

                    if (!string.IsNullOrEmpty(path))
                    {
                        //获取全局图书馆编码
                        string libCode = ConfigurationManager.AppSettings["LibCode"];
                        if (string.IsNullOrWhiteSpace(libCode))
                        {
                            LogManager.WriteLogs("数据写入文件出错", "全局图书馆编码不能为空", LogFile.Error);
                            return false;
                        }

                        //文件前缀
                        string filePrefix = (typeof(FileTypeEnum)).GetEnumDesc((int)fileType);

                        var time = DateTime.Now;

                        string fileName = libCode + "_" + filePrefix + "_" + string.Format("{0:yyyyMMdd}", time) + "_" + (CustomExtensions.ConvertDateTimeInt(time) / 1000).ToString().Substring(4) + ".gz";


                        path = path + "\\" + fileName;
                        if (!File.Exists(path))
                        {
                            FileStream fs = File.Create(path);
                            fs.Close();
                        }

                        if (File.Exists(path))
                        {
                            StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default);
                            foreach (DataRow row in dt.Rows)
                            {
                                string str = "";
                                for (int i = 0; i < row.ItemArray.Count(); i++)
                                {
                                    str += "\"" + row[i].ToString() + "\"|";
                                }
                               
                                sw.WriteLine(str);
                            }

                            sw.Close();
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return false;
            }
        }
    }
}
