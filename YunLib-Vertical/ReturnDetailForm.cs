﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib_Vertical
{
    public partial class ReturnDetailForm : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        String machineId = ConfigManager.MachineID;
        String libraryId = ConfigManager.LibraryID;
        String commonKey = ConfigManager.CommonKey;
        String servicePath = ConfigManager.ServicePath;

        public BooksInfo bookModel { get; set; }

        public ReturnDetailForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        private void ReturnDetailForm_Load(object sender, EventArgs e)
        {
            try {
                lblBorrowDate.Text = bookModel.BorrowDate.ToString("yyyy-MM-dd HH:mm:ss");
                //已借天数
                lblBorrowDays.Text = (DateTime.Now - bookModel.BorrowDate).Days.ToString() + "天";
                //剩余天数
                lblOverplusDays.Text = (bookModel.ShouldReturnDate - DateTime.Now).Days.ToString() + "天";
                //书名
                lblBooksName.Text = bookModel.BooksName;
                //作者
                lblAuthor.Text = bookModel.Author;

                lblPulisher.Text = bookModel.Pulisher;
                lblPublishTime.Text = bookModel.PulishTime;

                PlayVoice.play(this.Name);
            } catch(Exception ex)
            {

            }
        }

        /// <summary>
        /// 返回事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }



        private void picReBorrow_Click(object sender, EventArgs e)
        {
            //获取输入的条形码
            string barCode = bookModel.BarCode.Trim();

            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return reBorrowBook(barCode);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result != null)
                {
                    Invoke(new Action(() =>

                   endReBorrowBook(result)

                    ));
                }
            }, null);
        }

       
        /// <summary>
        /// 还书
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picReturn_Click(object sender, EventArgs e)
        {
            string barCode = bookModel.BarCode.Trim();
            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return returnBook(barCode);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result != null)
                {
                    Invoke(new Action(() =>

                   endReturnBook(result)

                    ));
                }
            }, null);
        }

        #region HTTP请求
        private string returnBook(string barCode)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks;
                var signStr = "{0}{1}{2}{3}{4}".FormatWith(barCode, libraryId, machineId, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/book/backService?bookId={1}&machineId={2}&timeStamp={3}&sign={4}&libraryId={5}".FormatWith(
                    servicePath, barCode, machineId, timeStamp, sign, libraryId);
                var IsStill = WebHelper.HttpWebRequest(url, "", true);
                return IsStill;
            }catch(Exception ex)
            {
                
                LogManager.WriteLogs("还书http请求出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void endReturnBook(string result)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(result))
                {
                    FailForm frm = new FailForm();
                    frm.MdiParent = this.ParentForm;
                    frm.Parent = this.Parent;
                    frm.Title = "还书失败";
                    frm.Message = "网络不给力，请稍后再试";
                    this.Close();
                    frm.Show();

                }
                else
                {
                    var data = result.ToJObject();

                    if (data["code"].ToString() == "100")
                    {
                        SuccessForm frm = new SuccessForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = this.Parent;
                        frm.Title = "还书成功";
                        frm.Message = "请将书放入" + data["data"]["machLatNum"] + "号格子，然后关闭柜门完成还书操作";
                        string doorNum = data["data"]["machLatNum"].ToString();
                        string msg = doorNum + "号柜门";
                        if (doorNum != "9999")
                        {

                            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                            CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                            
                        }
                        else
                        {
                            frm.Message = "请将书放入还书箱，然后关闭柜门完成还书操作";
                            DAMControl dam = new DAMControl();
                            dam.OpenDO(1);
                            frm.IsNeedClose = "1";
                        }

                       
                        this.Close();

                       frm.Show();
                    }
                    else
                    {
                        FailForm frm = new FailForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = this.Parent;
                        frm.Title = "还书失败";
                        frm.Message = data["msg"].ToString();
                        this.Close();
                        frm.Show();
                    }
                }
            }catch(Exception ex)
            {
                LogManager.WriteLogs("还书http解析出错", ex.StackTrace, LogFile.Error);
                FailForm frm = new FailForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                frm.Title = "还书失败";
                frm.Message = ex.Message;
                this.Close();
                frm.Show();
            }
           
        }

        private string reBorrowBook(string barCode)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks;

                String signStr = "{0}{1}{2}{3}{4}".FormatWith(barCode, libraryId, machineId, timeStamp, commonKey);
                String sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                String url = "{0}/public/book/booklendtimeservice?bookId={1}&machineId={2}&timeStamp={3}&sign={4}&libraryId={5}".FormatWith(
                    servicePath, barCode, machineId, timeStamp, sign, libraryId);
                var IsStill = WebHelper.HttpWebRequest(url, "", true);
                return IsStill;
            }
            catch (Exception ex)
            {

                LogManager.WriteLogs("续借http请求出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void endReBorrowBook(string result)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(result))
                {
                    FailForm frm = new FailForm();
                    frm.MdiParent = this.ParentForm;
                    frm.Parent = this.Parent;
                    frm.Title = "续借失败";
                    frm.Message = "网络不给力，请稍后再试";
                    this.Close();
                    frm.Show();

                }
                else
                {
                    var data = result.ToJObject();

                    if (data["code"].ToString() == "100")
                    {
                        SuccessForm frm = new SuccessForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = this.Parent;
                        frm.Title = "续借成功";
                        frm.Message = "";
                        this.Close();
                        frm.Show();
                    }
                    else
                    {
                        FailForm frm = new FailForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = this.Parent;
                        frm.Title = "续借失败";
                        frm.Message = data["msg"].ToString();
                        this.Close();
                        frm.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("续借http解析出错", ex.StackTrace, LogFile.Error);
                FailForm frm = new FailForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                frm.Title = "续借失败";
                frm.Message = ex.Message;
                this.Close();
                frm.Show();
            }

        }

        #endregion

        #region 按钮样式
        private void picReturn_MouseDown(object sender, MouseEventArgs e)
        {
            picReturn.Image = Properties.Resources.return2;
        }

        private void picReturn_MouseUp(object sender, MouseEventArgs e)
        {
            picReturn.Image = Properties.Resources.return1;
        }

        private void picReBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            picReBorrow.Image = Properties.Resources.reborrow2;
        }

        private void picReBorrow_MouseUp(object sender, MouseEventArgs e)
        {
            picReBorrow.Image = Properties.Resources.reborrow1;
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }
        #endregion
    }
}
