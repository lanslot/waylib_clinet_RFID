﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.ViewModels
{
    public class ListViewModel
    {
        public string machLatNum { get; set; }
        public string bookId { get; set; }
        public string title { get; set; }
        public string coverUrl { get; set; }
        public string status { get; set; }

        public string type { get; set; }
    }
}
