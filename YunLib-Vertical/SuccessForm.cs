﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Extensions;

namespace YunLib_Vertical
{
    public partial class SuccessForm : Form
    {

        public string Title { get; set; }
        public string Message { get; set; }

        public string IsNeedClose { get; set; }
        public SuccessForm()
        {
            InitializeComponent();
        }

        private void SuccessForm_Load(object sender, EventArgs e)
        {
            lblMessage.Text = Message;
            lblTitle.Text = Title;
            if (Title.Contains("借书"))
            {
                PlayVoice.play("BorrSuccess");
            }else if (Title.Contains("续借"))
            {
                PlayVoice.play("BorrowSuccess");
            }
            else
            {
                PlayVoice.play("ReturnSuccess");
            }

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            lblCount.Text = time.ToString();
            if(time <= 0)
            {
                MainForm frm = new MainForm();
                frm.MdiParent = ParentForm;
                frm.Parent = this.Parent;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }

        }

        private void SuccessForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
            timer1.Enabled=true;
            if(IsNeedClose == "1")
            {
                DAMControl dam = new DAMControl();
                dam.CloseDO(1);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
