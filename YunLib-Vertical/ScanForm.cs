﻿using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib.ReadCard;

namespace YunLib_Vertical
{
    public partial class ScanForm : Form
    {
        //获取服务器url
        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;

        private int cmdPort = ConfigManager.CmdPort;
        private string control = ConfigManager.Control;

        private IReadCard read;
        //定义全局的控制变量
        private SmartLifeBroker model;
        private bool IsNeedReadCard = true;
        private JPForm jp;

        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        public ScanForm()
        {
            InitializeComponent();
            jp = new JPForm();
            if (ConfigManager.ReadBookType == 1)
            {
                model = new SmartLifeBroker();
                model.OnScanedDataEvent += OnScanedDataEvent;
            }
            else
            {
                read = ObjectCreator.GetReadClass();
            }
        }

        private void OnScanedDataEvent(object source, byte[] scanFrame)
        {
            string barCode = Encoding.ASCII.GetString(scanFrame);

            txtBarCode.Text = barCode;
            IsNeedReadCard = false;
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }


        private void ScanForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (ConfigManager.ReadBookType == 1)
                {
                    if (model.IsCanUsed(ConfigManager.Scan, cmdPort))
                    {
                        model.Init(ConfigManager.Scan, cmdPort);
                    }
                    panel1.BackgroundImage = Properties.Resources.huanshu_tm_03;
                }
                PlayVoice.play(this.Name);
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void picSure_Click(object sender, EventArgs e)
        {
            string barCode = txtBarCode.Text;
            if (string.IsNullOrWhiteSpace(barCode))
            {
                lblErr.Text = "请输入条形码！";
            }
            else
            {
                Func<string> longTask = new Func<string>(delegate ()
                {
                    //  模拟长时间任务
                    ///  Thread.Sleep(2000);
                    return getData(barCode);
                    //  返回任务结果：5
                });
                //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                longTask.BeginInvoke(ar =>
                {
                    //  使用EndInvoke获取到任务结果（5）
                    string result = longTask.EndInvoke(ar);

                    //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                    //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                    if (result != null)
                    {
                        Invoke(new Action(() =>

                       ReturnData(result)

                        ));
                    }
                }, null);
            }

        }

        #region HTTP 请求
        private string getData(string barcode)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}".FormatWith(barcode, libraryId, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());
                string url = "{0}/public/book/lendinfo?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(
                    servicePath, barcode, libraryId, timeStamp, sign);
                var callback = WebHelper.HttpWebRequest(url, "", true);

                return callback;
            }
            catch(Exception ex)
            {
                LogManager.WriteLogs("还书扫描请求数据出错", ex.StackTrace, LogFile.Error);
                return "";
            }
           
        }

        private void ReturnData(string result)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(result))
                {
                    lblErr.Text = "网络错误，请稍后再试！";
                    return;
                }
                var callback = result.ToJObject();
                if (callback["code"].ToString() == "100")
                {
                    if (!string.IsNullOrEmpty(callback["data"].ToString()))
                    {
                        JToken datas = callback["data"];

                        JToken bookLendInfo = datas["bookLendInfo"];
                        JToken bookDesc = datas["bookDesc"];

                        if (bookDesc.ToJson() == null || bookDesc.ToJson() == "null")
                        {
                            lblErr.Text = "该书不属于本设备哟，请检查是否拿对图书！";
                        }
                        else
                        {

                            BooksInfo bmodel = new BooksInfo();

                            bmodel.BarCode = txtBarCode.Text.Trim();
                            bmodel.BooksName = bookDesc["title"].ToString() != null ? bookDesc["title"].ToString() : "";
                            bmodel.Author = bookDesc["author"].ToString() != null ? bookDesc["author"].ToString() : "";
                            bmodel.Pulisher = bookDesc["publisher"].ToString() != null ? bookDesc["publisher"].ToString() : "";
                            bmodel.PulishTime = bookDesc["pubdate"].ToString() != null ? bookDesc["pubdate"].ToString() : "";
                            if (bookLendInfo.HasValues)
                            {
                                if (!string.IsNullOrWhiteSpace(bookLendInfo["createdTime"].ToString()))
                                {
                                    bmodel.BorrowDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["createdTime"]));
                                }
                                if (!string.IsNullOrWhiteSpace(bookLendInfo["updatedTime"].ToString()))
                                {
                                    bmodel.ShouldReturnDate = DateTime.Parse("1970-01-01 08:00:00").AddMilliseconds((double)(bookLendInfo["updatedTime"]));
                                }
                            }
                            //bmodel.DoorNum = doorNum;
                            //bmodel.DoorID = doorId;
                            //bmodel.DoorCode = doorCode;
                            bmodel.CoverUrl = !string.IsNullOrWhiteSpace(bookDesc["coverUrl"].ToString()) ? bookDesc["coverUrl"].ToString() : "";

                            //有此书,就跳转到还书详情窗体。
                            //并且把书,借书人等相关信息传过去

                            //ReturnOrBrDetail Frm = new ReturnOrBrDetail { MdiParent = ParentForm, Dock = DockStyle.Fill, bookModel = bmodel };
                            //if (datas["day"].HasValues)
                            //{
                            //    ReturnOrBrDetail.overplusDyas = datas["day"].ToString();
                            //}
                            //this.Close();

                            //Frm.Show();

                            ReturnDetailForm frm = new ReturnDetailForm();
                            frm.MdiParent = this.ParentForm;
                            frm.Parent = this.Parent;
                            frm.bookModel = bmodel;
                            this.Close();
                            frm.Show();
                            timer1.Enabled = false;
                            timer1.Stop();
                            //}

                        }
                    }
                }
                else
                {
                    lblErr.Text = callback["msg"].ToString();
                }
            }catch(Exception ex)
            {
                lblErr.Text = ex.StackTrace;
                LogManager.WriteLogs("解析流通信息错误", ex.StackTrace, LogFile.Error);
            }
        }
        #endregion

        /// <summary>
        /// 返回事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        #region 按钮样式
        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }

        private void picSure_MouseUp(object sender, MouseEventArgs e)
        {
            picSure.Image = Properties.Resources.sure1;
        }

        private void picSure_MouseDown(object sender, MouseEventArgs e)
        {
            picSure.Image = Properties.Resources.sure2;
        }

        #endregion
        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                i++;
                int time = int.Parse(lblCount.Text);
                time--;
                lblCount.Text = time.ToString();
                if (time <= 0)
                {
                    MainForm frm = new MainForm();
                    frm.MdiParent = ParentForm;
                    frm.Parent = this.Parent;
                    frm.Dock = DockStyle.Fill;

                    this.Close();

                    frm.Show();
                }
                string barCode = "";
                if (IsNeedReadCard)
                {
                    if (ConfigManager.ReadBookType == 2)
                    {
                        barCode = read.ReadRFID(ConfigManager.Pos, ConfigManager.IMPort);
                    }
                    else if (ConfigManager.ReadBookType == 3)
                    {
                        barCode = read.ReadUHFRFID(ConfigManager.UHF, ConfigManager.IMPort);

                    }
                    else
                    {
                        if (ConfigManager.ScanType == 1)
                        {
                            if (i % 9 == 0)
                            {
                                //打开扫描器
                                model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(barCode))
                    {
                        txtBarCode.Text = barCode;
                        lblErr.Text = "";
                        IsNeedReadCard = false;
                    }

                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("读条码出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void ScanForm_FormClosed(object sender, FormClosedEventArgs e)
        {
           // model.OnScanedDataEvent -= OnScanedDataEvent;
            //停止计时器
            timer1.Enabled = false;
            timer1.Stop();
            if (ConfigManager.ReadBookType == 1)
            {
                model.Dispose();
            }
            jp.Close();
        }

        private void txtBarCode_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(240,1560);
            jp.Show();
            txtBarCode.Focus();

            this.Activate();
        }
    }
}
