﻿namespace Yunlib.AdminManage
{
    partial class LocalBookManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.picBtnReturn = new System.Windows.Forms.PictureBox();
            this.picBtnDelete = new System.Windows.Forms.PictureBox();
            this.picBtnNextPage = new System.Windows.Forms.PictureBox();
            this.picBtnLastPage = new System.Windows.Forms.PictureBox();
            this.lblTotalPage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblThisPage = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IsChecked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Writer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Press = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PublicationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoorNUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnNextPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnLastPage)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // picBtnReturn
            // 
            this.picBtnReturn.BackColor = System.Drawing.Color.Transparent;
            this.picBtnReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnReturn.Image = global::Yunlib.Properties.Resources.btn_ReturnBack11;
            this.picBtnReturn.Location = new System.Drawing.Point(815, 653);
            this.picBtnReturn.Name = "picBtnReturn";
            this.picBtnReturn.Size = new System.Drawing.Size(160, 53);
            this.picBtnReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnReturn.TabIndex = 31;
            this.picBtnReturn.TabStop = false;
            this.picBtnReturn.Click += new System.EventHandler(this.picBtnReturn_Click);
            // 
            // picBtnDelete
            // 
            this.picBtnDelete.BackColor = System.Drawing.Color.Transparent;
            this.picBtnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnDelete.Image = global::Yunlib.Properties.Resources.Delete;
            this.picBtnDelete.Location = new System.Drawing.Point(626, 653);
            this.picBtnDelete.Name = "picBtnDelete";
            this.picBtnDelete.Size = new System.Drawing.Size(160, 53);
            this.picBtnDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnDelete.TabIndex = 32;
            this.picBtnDelete.TabStop = false;
            this.picBtnDelete.Click += new System.EventHandler(this.picBtnDelete_Click);
            // 
            // picBtnNextPage
            // 
            this.picBtnNextPage.BackColor = System.Drawing.Color.Transparent;
            this.picBtnNextPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnNextPage.Image = global::Yunlib.Properties.Resources.xiayiye;
            this.picBtnNextPage.Location = new System.Drawing.Point(237, 654);
            this.picBtnNextPage.Name = "picBtnNextPage";
            this.picBtnNextPage.Size = new System.Drawing.Size(50, 45);
            this.picBtnNextPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnNextPage.TabIndex = 34;
            this.picBtnNextPage.TabStop = false;
            this.picBtnNextPage.Click += new System.EventHandler(this.picBtnNextPage_Click);
            // 
            // picBtnLastPage
            // 
            this.picBtnLastPage.BackColor = System.Drawing.Color.Transparent;
            this.picBtnLastPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBtnLastPage.Image = global::Yunlib.Properties.Resources.shangyiye;
            this.picBtnLastPage.Location = new System.Drawing.Point(30, 654);
            this.picBtnLastPage.Name = "picBtnLastPage";
            this.picBtnLastPage.Size = new System.Drawing.Size(50, 45);
            this.picBtnLastPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBtnLastPage.TabIndex = 30;
            this.picBtnLastPage.TabStop = false;
            this.picBtnLastPage.Click += new System.EventHandler(this.picBtnLastPage_Click);
            // 
            // lblTotalPage
            // 
            this.lblTotalPage.AutoSize = true;
            this.lblTotalPage.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPage.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblTotalPage.ForeColor = System.Drawing.Color.White;
            this.lblTotalPage.Location = new System.Drawing.Point(166, 654);
            this.lblTotalPage.Name = "lblTotalPage";
            this.lblTotalPage.Size = new System.Drawing.Size(0, 31);
            this.lblTotalPage.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(140, 654);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 31);
            this.label2.TabIndex = 28;
            // 
            // lblThisPage
            // 
            this.lblThisPage.AutoSize = true;
            this.lblThisPage.BackColor = System.Drawing.Color.Transparent;
            this.lblThisPage.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblThisPage.ForeColor = System.Drawing.Color.White;
            this.lblThisPage.Location = new System.Drawing.Point(109, 654);
            this.lblThisPage.Name = "lblThisPage";
            this.lblThisPage.Size = new System.Drawing.Size(0, 31);
            this.lblThisPage.TabIndex = 29;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(100, 100);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(30, 122);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(945, 492);
            this.panel1.TabIndex = 35;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 50;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsChecked,
            this.ID,
            this.BName,
            this.BarCode,
            this.Writer,
            this.Press,
            this.PublicationDate,
            this.DoorNUM});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(945, 492);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // IsChecked
            // 
            this.IsChecked.FalseValue = "";
            this.IsChecked.HeaderText = "选择";
            this.IsChecked.Name = "IsChecked";
            this.IsChecked.ReadOnly = true;
            this.IsChecked.TrueValue = "DoorNUM";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // BName
            // 
            this.BName.DataPropertyName = "BName";
            this.BName.HeaderText = "书名";
            this.BName.Name = "BName";
            this.BName.ReadOnly = true;
            this.BName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BarCode
            // 
            this.BarCode.DataPropertyName = "BarCode";
            this.BarCode.HeaderText = "条形码";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            this.BarCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Writer
            // 
            this.Writer.DataPropertyName = "Writer";
            this.Writer.HeaderText = "作者";
            this.Writer.Name = "Writer";
            this.Writer.ReadOnly = true;
            this.Writer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Press
            // 
            this.Press.DataPropertyName = "Press";
            this.Press.HeaderText = "出版社";
            this.Press.Name = "Press";
            this.Press.ReadOnly = true;
            this.Press.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PublicationDate
            // 
            this.PublicationDate.DataPropertyName = "PublicationDate";
            this.PublicationDate.HeaderText = "出版日期";
            this.PublicationDate.Name = "PublicationDate";
            this.PublicationDate.ReadOnly = true;
            this.PublicationDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DoorNUM
            // 
            this.DoorNUM.DataPropertyName = "DoorNUM";
            this.DoorNUM.HeaderText = "柜号";
            this.DoorNUM.Name = "DoorNUM";
            this.DoorNUM.ReadOnly = true;
            // 
            // LocalBookManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picBtnReturn);
            this.Controls.Add(this.picBtnDelete);
            this.Controls.Add(this.picBtnNextPage);
            this.Controls.Add(this.picBtnLastPage);
            this.Controls.Add(this.lblTotalPage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblThisPage);
            this.Name = "LocalBookManager";
            this.Text = "LocalBookManager";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.LocalBookManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBtnReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnNextPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnLastPage)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox picBtnReturn;
        private System.Windows.Forms.PictureBox picBtnDelete;
        private System.Windows.Forms.PictureBox picBtnNextPage;
        private System.Windows.Forms.PictureBox picBtnLastPage;
        private System.Windows.Forms.Label lblTotalPage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblThisPage;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Writer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Press;
        private System.Windows.Forms.DataGridViewTextBoxColumn PublicationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DoorNUM;
    }
}