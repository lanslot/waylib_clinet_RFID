﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Yunlib.Entity;
using YunLib.Common;

namespace Yunlib.DAL
{
    public class ReturnOrBrDAL
    {
        SqlConnection conn = new SqlConnection(SqlServerHelper.ConnectionString);
        /// <summary>
        /// 通过条形码获取书和借阅信息详情
        /// </summary>
        /// <returns></returns>
        public BooksInfo GetBooksInfoByBarcode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            //申明读者条码,允许续借次数,续借期限
            strSql.AppendLine(@" DECLARE @RDID varchar(50),");
            strSql.AppendLine(@"         @AllowOnceBorrowCount int,");
            strSql.AppendLine(@"         @OnceBorrowTerm int,");
            strSql.AppendLine(@"         @BorrowCountNow int");

            //通过书的条形码查询出读者条码
            strSql.AppendLine(@" SELECT @RDID= d.读者条码");
            strSql.AppendLine(@" FROM [YunLibDB5].[dbo].[馆藏书目库] a");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[采购库] b on a.主键码 = b.主键码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[馆藏典藏库] c on b.子键码 = c.子键码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[流通库] d on d.条形码=c.条形码");
            strSql.AppendFormat(@" WHERE c.条形码='{0}'", barCode);

            //查询当前已续借次数
            strSql.AppendLine(@" SELECT @BorrowCountNow=COUNT(*) FROM 流通日志");
            strSql.AppendLine(@" WHERE 读者条码= @RDID");
            strSql.AppendLine(@" AND 操作类型='X'");

            //通过读者条码查询出该读者允许续借的次数和期限
            strSql.AppendLine(@" SELECT DISTINCT @AllowOnceBorrowCount=a.允许续借次数,");
            strSql.AppendLine(@"                 @OnceBorrowTerm=a.续借期限");
            strSql.AppendLine(@" FROM [YunLibDB5].[dbo].[流通参数定义] a");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[读者库] b on a.单位代码=b.单位代码");
            strSql.AppendLine(@" AND a.级别代码=b.级别代码");
            strSql.AppendLine(@" WHERE b.读者条码=@RDID");

            strSql.AppendLine(@" SELECT DISTINCT a.题名 as BooksName,");
            strSql.AppendLine(@"        e.姓名 as ReaderName,");
            strSql.AppendLine(@"        e.级别代码 as ClassCode, ");
            strSql.AppendLine(@"        e.单位代码 as CellCode,");
            strSql.AppendLine(@"        e.可外借 as CanBorrowCount,");
            strSql.AppendLine(@"        e.已外借 as BorrowEDCount,");
            strSql.AppendLine(@"        @AllowOnceBorrowCount as AllowOnceBorrowCount,");
            strSql.AppendLine(@"        @OnceBorrowTerm as OnceBorrowTerm,");
            strSql.AppendLine(@"        @BorrowCountNow as BorrowCountNow,");
            strSql.AppendLine(@"        a.索书号 as SuoBookNum,");
            strSql.AppendLine(@"        c.条形码 as BarCode,");
            strSql.AppendLine(@"        d.读者条码 as ReaderCode,");
            strSql.AppendLine(@"        d.外借时间 as BorrowDate,");
            strSql.AppendLine(@"        d.应归还时间 as ShouldReturnDate");
            strSql.AppendLine(@" FROM [YunLibDB5].[dbo].[馆藏书目库] a");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[采购库] b on a.主键码 = b.主键码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[馆藏典藏库] c on b.子键码 = c.子键码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[流通库] d on d.条形码=c.条形码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[读者库] e on e.读者条码=d.读者条码");
            strSql.AppendLine(@" LEFT JOIN [YunLibDB5].[dbo].[流通日志] f on  f.读者条码=d.读者条码");
            strSql.AppendFormat(@" WHERE c.条形码='{0}'", barCode);

            BooksInfo model = new BooksInfo();

            //获取数据
            IDataReader dr = SqlServerHelper.ExecuteReader(strSql.ToString(), conn);

            if (dr.Read())
            {
                model.ReaderName = dr["ReaderName"].ToString();
                model.BorrowCountNow = Convert.ToInt16(dr["BorrowCountNow"].ToString());
                model.CanBorrowCount = (dr["CanBorrowCount"].ToString() == "") ? 0 : Convert.ToInt32(dr["CanBorrowCount"].ToString());
                model.BorrowCountED = (dr["BorrowEDCount"].ToString() == "") ? (short)0 : Convert.ToInt16(dr["BorrowEDCount"].ToString());
                model.AllowOnceBorrowCount = (dr["AllowOnceBorrowCount"].ToString() == "") ? (short)0 : Convert.ToInt16(dr["AllowOnceBorrowCount"].ToString());
                model.OnceBorrowTerm = (dr["OnceBorrowTerm"].ToString() == "") ? (short)0 : Convert.ToInt16(dr["OnceBorrowTerm"].ToString());
                model.BooksName = dr["BooksName"].ToString();
                model.SuoBookNum = dr["SuoBookNum"].ToString();
                model.BarCode = dr["BarCode"].ToString();
                model.ReaderCode = dr["ReaderCode"].ToString();
                model.BorrowDate = (dr["BorrowDate"].ToString() == "") ? DateTime.Parse("1990-1-1") : Convert.ToDateTime(dr["BorrowDate"].ToString());
                model.ShouldReturnDate = (dr["ShouldReturnDate"].ToString() == "") ? DateTime.Parse("1990-1-1") : Convert.ToDateTime(dr["ShouldReturnDate"].ToString());
            }
            conn.Close();
            return model;
        }

        /// <summary>
        /// 续借成功后,添加记录到流通日志中
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool InsertLTKOneInfo(ChangeLogInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" INSERT INTO [YunLibDB5].[dbo].[流通日志]");
            strSql.AppendLine(@"        (操作类型,");
            strSql.AppendLine(@"         条形码,");
            strSql.AppendLine(@"         登录号,");
            strSql.AppendLine(@"         读者条码,");
            strSql.AppendLine(@"         处理时间,");
            strSql.AppendLine(@"         赔罚款,");
            strSql.AppendLine(@"         操作员,");
            strSql.AppendLine(@"         主键码,");
            strSql.AppendLine(@"         流通库室键码,");
            strSql.AppendLine(@"         签到键码");
            strSql.AppendLine(@"         )");
            strSql.AppendLine(@" VALUES");
            strSql.AppendFormat(@"       ('{0}','{1}','{2}','{3}','{4}',{5},{6},{7},{8},{9})", model.OperateType, model.BarCode, model.LoginAccount, model.ReaderCode, model.DealTime, model.Penalty, model.Operator, model.ID, model.LTKID, model.QDID);

            return SqlServerHelper.ExecuteNonQuery(strSql.ToString()) > 0;
        }

        /// <summary>
        /// 通过条形码获取未还书籍
        /// </summary>
        /// <returns></returns>
        public ChangeKuInfo GetBorrowBooksInfoByBarcode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT [条形码] as BarCode,");
            strSql.AppendLine(@"        [登录号] as LoginAccount,");
            strSql.AppendLine(@"        [读者条码] as ReaderCode,");
            strSql.AppendLine(@"        [外借时间] as BorrowTime,");
            strSql.AppendLine(@"        [应归还时间] as ShouldReturnTime,");
            strSql.AppendLine(@"        [主键码] as ID,");
            strSql.AppendLine(@"        [虚拟库室] as DummyKu,");
            strSql.AppendLine(@"        [续借次数] as OnceBorrowCount,");
            strSql.AppendLine(@"        [处理时间] as DealTime,");
            strSql.AppendLine(@"        [流通库室键码] as LTKSID,");
            strSql.AppendLine(@"        [签到键码] as QDID");
            strSql.AppendLine(@" FROM [YunLibDB5].[dbo].[流通库]");
            strSql.AppendFormat(@" WHERE [条形码]='{0}'", barCode);

            IDataReader dr = SqlServerHelper.ExecuteReader(strSql.ToString(), conn);

            ChangeKuInfo model = null;

            if (dr.Read())
            {
                model = new ChangeKuInfo();
                model.BarCode = dr["BarCode"].ToString();
                model.LoginAccount = dr["LoginAccount"].ToString();
                model.ReaderCode = dr["ReaderCode"].ToString();
                model.BorrowTime = Convert.ToDateTime(dr["BorrowTime"].ToString());
                model.ShouldReturnTime = Convert.ToDateTime(dr["ShouldReturnTime"].ToString());
                model.ID = Convert.ToInt32(dr["ID"].ToString());
                model.DummyKu = Convert.ToInt32(dr["DummyKu"].ToString());
                model.OnceBorrowCount = Convert.ToInt32(dr["OnceBorrowCount"].ToString());
            }

            conn.Close();
            return model;
        }

        public bool DeleteChangeLogInfoByBarCode(ChangeLogInfo model)
        {
            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine(@" DELETE FROM [YunLibDB5].[dbo].[流通库]");
            sqlStr.AppendFormat(@" WHERE 条形码='{0}'", model.BarCode);

            bool insertCmd = new ReturnOrBrDAL().InsertLTKOneInfo(model);
            if (!insertCmd)
            {
                //如果新增流通日志失败,就直接返回false
                return false;
            }

            bool deleteCmd = SqlServerHelper.ExecuteNonQuery(sqlStr.ToString()) > 0;

            bool res = false;

            //如果新增日志成功,但是删除流通库失败,就把新增成功的日志删除掉
            if (insertCmd && !deleteCmd)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine(@" DELETE FROM 流通日志");
                builder.AppendLine(@" WHERE ");
                builder.AppendLine(@" 处理时间=(");
                builder.AppendLine(@"           SELECT 处理时间 FROM 流通日志");
                builder.AppendLine(@"           WHERE ");
                builder.AppendFormat(@"         读者条码='{0}' ORDER BY 处理时间 DESC) ", model.ReaderCode);
                builder.AppendFormat(@" AND 读者条码='{0}'", model.ReaderCode);

                res = SqlServerHelper.ExecuteNonQuery(builder.ToString()) > 0;
            }
            //删除新增的流通日志成功,但是却没有操作成功
            if (res)
            {
                return false;
            }

            //如果新增流通日志和删除流通库都成功,才返回操作成功
            if (deleteCmd && insertCmd)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetOnceBorrowCount(string rdid)
        {
            StringBuilder strSql = new StringBuilder();

            //查询当前已续借次数
            strSql.AppendLine(@" SELECT COUNT(*) FROM 流通日志");
            strSql.AppendFormat(@" WHERE 读者条码='{0}'", rdid);
            strSql.AppendLine(@" AND 操作类型='X'");

            return SqlServerHelper.getSingleInt(strSql.ToString());

        }

        /// <summary>
        /// 根据条形码查询书籍
        /// </summary>
        /// <returns></returns>
        public LocalBooks GetBookByBarCode(string barCode)
        {
            string sqlText = "select c.条形码 as BarCode,a.题名 as BName,a.责任者 as Writer,a.出版者 as Press,a.出版日期 as PublicationDate,a.索书号 as Classify from 馆藏书目库" +
                " a left join 采购库 b on a.主键码 = b.主键码 left join 馆藏典藏库 c on  b.子键码 = c.子键码 where 条形码 = '" + barCode + "'";

            DataTable dt = SqlServerHelper.ExecuteDataTable(sqlText);

            IList<LocalBooks> list = DataTable2List<LocalBooks>.ConvertToModel(dt);

            if (list.Count > 0 && list != null)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 流通库插入数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AddCirculation(ChangeKuInfo model)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine(@"INSERT INTO [YunLibDB5].[dbo].[流通库]
                   ([条形码]
                   ,[登录号]
                   ,[读者条码]
                   ,[外借时间]
                   ,[应归还时间]
                   ,[主键码]
                   ,[虚拟库室]
                   ,[续借次数])
             VALUES ");
            str.AppendFormat(@" ('{0}','{1}','{2}','{3}','{4}',{5},{6},{7})", model.BarCode, model.LoginAccount, model.ReaderCode, model.BorrowTime, model.ShouldReturnTime,
                    model.ID, 1, model.OnceBorrowCount);

            int i = SqlServerHelper.ExecuteNonQuery(str.ToString());
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
