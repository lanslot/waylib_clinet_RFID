﻿namespace YunLib_Vertical
{
    partial class ReturnDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReturnDetailForm));
            this.lblCount = new System.Windows.Forms.Label();
            this.picline3 = new System.Windows.Forms.PictureBox();
            this.picLine2 = new System.Windows.Forms.PictureBox();
            this.picLine1 = new System.Windows.Forms.PictureBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.picBack = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblType = new System.Windows.Forms.Label();
            this.picReBorrow = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPublishTime = new System.Windows.Forms.Label();
            this.lblMachLatNum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPulisher = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblBooksName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lable2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblOverplusDays = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBorrowDays = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblBorrowDate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.picReturn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picline3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReBorrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCount.ForeColor = System.Drawing.Color.Magenta;
            this.lblCount.Location = new System.Drawing.Point(979, 10);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(48, 28);
            this.lblCount.TabIndex = 43;
            this.lblCount.Text = "120";
            // 
            // picline3
            // 
            this.picline3.Image = global::YunLib_Vertical.Properties.Resources.xuxian_3;
            this.picline3.Location = new System.Drawing.Point(73, 948);
            this.picline3.Name = "picline3";
            this.picline3.Size = new System.Drawing.Size(894, 2);
            this.picline3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picline3.TabIndex = 42;
            this.picline3.TabStop = false;
            // 
            // picLine2
            // 
            this.picLine2.Image = global::YunLib_Vertical.Properties.Resources.xuxian_2;
            this.picLine2.Location = new System.Drawing.Point(73, 743);
            this.picLine2.Name = "picLine2";
            this.picLine2.Size = new System.Drawing.Size(894, 3);
            this.picLine2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLine2.TabIndex = 41;
            this.picLine2.TabStop = false;
            // 
            // picLine1
            // 
            this.picLine1.Image = global::YunLib_Vertical.Properties.Resources.xuxian_1;
            this.picLine1.Location = new System.Drawing.Point(73, 529);
            this.picLine1.Name = "picLine1";
            this.picLine1.Size = new System.Drawing.Size(895, 2);
            this.picLine1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLine1.TabIndex = 40;
            this.picLine1.TabStop = false;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblBarcode.Location = new System.Drawing.Point(26, 462);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(0, 12);
            this.lblBarcode.TabIndex = 39;
            this.lblBarcode.Visible = false;
            // 
            // picBack
            // 
            this.picBack.Image = global::YunLib_Vertical.Properties.Resources.hui_w;
            this.picBack.Location = new System.Drawing.Point(942, 1018);
            this.picBack.Name = "picBack";
            this.picBack.Size = new System.Drawing.Size(100, 100);
            this.picBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBack.TabIndex = 38;
            this.picBack.TabStop = false;
            this.picBack.Click += new System.EventHandler(this.picBack_Click);
            this.picBack.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseDown);
            this.picBack.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.BackColor = System.Drawing.Color.Transparent;
            this.lblType.Location = new System.Drawing.Point(62, 462);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 12);
            this.lblType.TabIndex = 37;
            this.lblType.Visible = false;
            // 
            // picReBorrow
            // 
            this.picReBorrow.Image = global::YunLib_Vertical.Properties.Resources.reborrow1;
            this.picReBorrow.Location = new System.Drawing.Point(250, 1018);
            this.picReBorrow.Name = "picReBorrow";
            this.picReBorrow.Size = new System.Drawing.Size(220, 60);
            this.picReBorrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picReBorrow.TabIndex = 36;
            this.picReBorrow.TabStop = false;
            this.picReBorrow.Click += new System.EventHandler(this.picReBorrow_Click);
            this.picReBorrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picReBorrow_MouseDown);
            this.picReBorrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picReBorrow_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Magenta;
            this.label2.Location = new System.Drawing.Point(1021, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 28);
            this.label2.TabIndex = 44;
            this.label2.Text = "秒";
            // 
            // lblPublishTime
            // 
            this.lblPublishTime.AutoSize = true;
            this.lblPublishTime.BackColor = System.Drawing.Color.Transparent;
            this.lblPublishTime.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPublishTime.ForeColor = System.Drawing.Color.White;
            this.lblPublishTime.Location = new System.Drawing.Point(712, 629);
            this.lblPublishTime.Name = "lblPublishTime";
            this.lblPublishTime.Size = new System.Drawing.Size(0, 28);
            this.lblPublishTime.TabIndex = 33;
            // 
            // lblMachLatNum
            // 
            this.lblMachLatNum.AutoSize = true;
            this.lblMachLatNum.BackColor = System.Drawing.Color.Transparent;
            this.lblMachLatNum.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMachLatNum.ForeColor = System.Drawing.Color.White;
            this.lblMachLatNum.Location = new System.Drawing.Point(195, 689);
            this.lblMachLatNum.Name = "lblMachLatNum";
            this.lblMachLatNum.Size = new System.Drawing.Size(0, 28);
            this.lblMachLatNum.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(97, 689);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 28);
            this.label8.TabIndex = 30;
            this.label8.Text = "编   号：";
            // 
            // lblPulisher
            // 
            this.lblPulisher.AutoSize = true;
            this.lblPulisher.BackColor = System.Drawing.Color.Transparent;
            this.lblPulisher.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPulisher.ForeColor = System.Drawing.Color.White;
            this.lblPulisher.Location = new System.Drawing.Point(193, 629);
            this.lblPulisher.Name = "lblPulisher";
            this.lblPulisher.Size = new System.Drawing.Size(0, 28);
            this.lblPulisher.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(100, 629);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 28);
            this.label6.TabIndex = 28;
            this.label6.Text = "出版社：";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.BackColor = System.Drawing.Color.Transparent;
            this.lblAuthor.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAuthor.ForeColor = System.Drawing.Color.White;
            this.lblAuthor.Location = new System.Drawing.Point(712, 569);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(0, 28);
            this.lblAuthor.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(589, 569);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 28);
            this.label4.TabIndex = 26;
            this.label4.Text = "作       者：";
            // 
            // lblBooksName
            // 
            this.lblBooksName.AutoSize = true;
            this.lblBooksName.BackColor = System.Drawing.Color.Transparent;
            this.lblBooksName.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBooksName.ForeColor = System.Drawing.Color.White;
            this.lblBooksName.Location = new System.Drawing.Point(198, 569);
            this.lblBooksName.Name = "lblBooksName";
            this.lblBooksName.Size = new System.Drawing.Size(0, 28);
            this.lblBooksName.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(100, 569);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 28);
            this.label1.TabIndex = 24;
            this.label1.Text = "书   名：";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::YunLib_Vertical.Properties.Resources.nopic;
            this.pictureBox1.Location = new System.Drawing.Point(103, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 281);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lable2
            // 
            this.lable2.AutoSize = true;
            this.lable2.BackColor = System.Drawing.Color.Transparent;
            this.lable2.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lable2.ForeColor = System.Drawing.Color.White;
            this.lable2.Location = new System.Drawing.Point(589, 629);
            this.lable2.Name = "lable2";
            this.lable2.Size = new System.Drawing.Size(117, 28);
            this.lable2.TabIndex = 32;
            this.lable2.Text = "出版日期：";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(305, 69);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 414);
            this.panel1.TabIndex = 23;
            // 
            // lblOverplusDays
            // 
            this.lblOverplusDays.AutoSize = true;
            this.lblOverplusDays.BackColor = System.Drawing.Color.Transparent;
            this.lblOverplusDays.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblOverplusDays.ForeColor = System.Drawing.Color.White;
            this.lblOverplusDays.Location = new System.Drawing.Point(208, 893);
            this.lblOverplusDays.Name = "lblOverplusDays";
            this.lblOverplusDays.Size = new System.Drawing.Size(0, 28);
            this.lblOverplusDays.TabIndex = 50;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(94, 893);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 28);
            this.label5.TabIndex = 49;
            this.label5.Text = "剩余天数：";
            // 
            // lblBorrowDays
            // 
            this.lblBorrowDays.AutoSize = true;
            this.lblBorrowDays.BackColor = System.Drawing.Color.Transparent;
            this.lblBorrowDays.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBorrowDays.ForeColor = System.Drawing.Color.White;
            this.lblBorrowDays.Location = new System.Drawing.Point(198, 833);
            this.lblBorrowDays.Name = "lblBorrowDays";
            this.lblBorrowDays.Size = new System.Drawing.Size(0, 28);
            this.lblBorrowDays.TabIndex = 48;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(97, 833);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 28);
            this.label9.TabIndex = 47;
            this.label9.Text = "已借天数：";
            // 
            // lblBorrowDate
            // 
            this.lblBorrowDate.AutoSize = true;
            this.lblBorrowDate.BackColor = System.Drawing.Color.Transparent;
            this.lblBorrowDate.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBorrowDate.ForeColor = System.Drawing.Color.White;
            this.lblBorrowDate.Location = new System.Drawing.Point(198, 773);
            this.lblBorrowDate.Name = "lblBorrowDate";
            this.lblBorrowDate.Size = new System.Drawing.Size(0, 28);
            this.lblBorrowDate.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(97, 773);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 28);
            this.label11.TabIndex = 45;
            this.label11.Text = "借阅时间：";
            // 
            // picReturn
            // 
            this.picReturn.Image = global::YunLib_Vertical.Properties.Resources.return1;
            this.picReturn.Location = new System.Drawing.Point(610, 1018);
            this.picReturn.Name = "picReturn";
            this.picReturn.Size = new System.Drawing.Size(220, 60);
            this.picReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picReturn.TabIndex = 51;
            this.picReturn.TabStop = false;
            this.picReturn.Click += new System.EventHandler(this.picReturn_Click);
            this.picReturn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picReturn_MouseDown);
            this.picReturn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picReturn_MouseUp);
            // 
            // ReturnDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.di;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1080, 1129);
            this.Controls.Add(this.picReturn);
            this.Controls.Add(this.lblOverplusDays);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblBorrowDays);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblBorrowDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.picline3);
            this.Controls.Add(this.picLine2);
            this.Controls.Add(this.picLine1);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.picBack);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.picReBorrow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPublishTime);
            this.Controls.Add(this.lblMachLatNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblPulisher);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblBooksName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lable2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReturnDetailForm";
            this.Text = "ReturnDetailForm";
            this.Load += new System.EventHandler(this.ReturnDetailForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picline3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReBorrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picReturn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.PictureBox picline3;
        private System.Windows.Forms.PictureBox picLine2;
        private System.Windows.Forms.PictureBox picLine1;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.PictureBox picBack;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.PictureBox picReBorrow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPublishTime;
        private System.Windows.Forms.Label lblMachLatNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPulisher;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBooksName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lable2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblOverplusDays;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBorrowDays;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblBorrowDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox picReturn;
    }
}