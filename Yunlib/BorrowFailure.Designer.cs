﻿namespace Yunlib
{
    partial class BorrowFailure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblOnceBorrowErrMsg = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSecond = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pic_btnTurnBack = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOnceBorrowErrMsg
            // 
            this.lblOnceBorrowErrMsg.AutoSize = true;
            this.lblOnceBorrowErrMsg.BackColor = System.Drawing.Color.White;
            this.lblOnceBorrowErrMsg.Font = new System.Drawing.Font("微软雅黑", 23F);
            this.lblOnceBorrowErrMsg.ForeColor = System.Drawing.Color.Red;
            this.lblOnceBorrowErrMsg.Location = new System.Drawing.Point(324, 402);
            this.lblOnceBorrowErrMsg.Name = "lblOnceBorrowErrMsg";
            this.lblOnceBorrowErrMsg.Size = new System.Drawing.Size(389, 40);
            this.lblOnceBorrowErrMsg.TabIndex = 1;
            this.lblOnceBorrowErrMsg.Text = "该书籍续借次数已达上限！";
            this.lblOnceBorrowErrMsg.Click += new System.EventHandler(this.lblOnceBorrowErrMsg_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Yunlib.Properties.Resources.pic_OnceBorrowFail1;
            this.pictureBox1.Location = new System.Drawing.Point(201, 163);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(618, 471);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(361, 512);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "— 还了书，再看看别的吧！ —";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = true;
            this.lblSecond.BackColor = System.Drawing.Color.White;
            this.lblSecond.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblSecond.ForeColor = System.Drawing.Color.Green;
            this.lblSecond.Location = new System.Drawing.Point(506, 578);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(22, 17);
            this.lblSecond.TabIndex = 6;
            this.lblSecond.Text = "10";
            this.lblSecond.Click += new System.EventHandler(this.lblSecond_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(465, 578);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "倒计时      秒";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // pic_btnTurnBack
            // 
            this.pic_btnTurnBack.BackColor = System.Drawing.Color.Transparent;
            this.pic_btnTurnBack.Image = global::Yunlib.Properties.Resources.btnX;
            this.pic_btnTurnBack.Location = new System.Drawing.Point(470, 640);
            this.pic_btnTurnBack.Name = "pic_btnTurnBack";
            this.pic_btnTurnBack.Size = new System.Drawing.Size(62, 63);
            this.pic_btnTurnBack.TabIndex = 8;
            this.pic_btnTurnBack.TabStop = false;
            this.pic_btnTurnBack.Click += new System.EventHandler(this.pic_btnTurnBack_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BorrowFailure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pic_btnTurnBack);
            this.Controls.Add(this.lblSecond);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblOnceBorrowErrMsg);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BorrowFailure";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.lblOnceBorrowFailtureMsg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblOnceBorrowErrMsg;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSecond;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pic_btnTurnBack;
        private System.Windows.Forms.Timer timer1;
    }
}