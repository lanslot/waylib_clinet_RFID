﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class BorrSuccess : Form
    {
        public BorrSuccess()
        {
            InitializeComponent();
        }

        public BorrSuccess(string doorNum)
        {
            InitializeComponent();
            lblDoorNum.Text = doorNum;
           
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        private void BorrSuccess_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            timer1.Start();
         PlayVoice.play(this.Name);
            CodeOptimize.SolveLightScreen(this);
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("BorrSuccess_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //返回1Main加载时不执行开灯操作
            //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;
            timer1.Enabled = false;
            timer1.Stop();

            //返回主界面
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";
            if (int.Parse(lblSecond.Text) < 0)
            {
                //返回1Main加载时不执行开灯操作
               //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;

                timer1.Enabled = false;

                //返回主界面
                //Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblDoorNum_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
