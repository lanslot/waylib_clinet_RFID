﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.DAL
{
    
    /// <summary> 
    /// 标识表名、主键等信息特性类 
    /// </summary> 
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class TableInfoAttribute : Attribute
    {
        /// <summary> 
        /// 数据库表名 
        /// </summary> 
        public string TableName { get; set; }

        /// <summary> 
        /// 主键名称 
        /// </summary> 
        public string PrimaryKey { get; set; }

        public TableInfoAttribute()
        { 
        }

        public TableInfoAttribute(string tableName, string primaryKey)
        {
            this.TableName = tableName;
            this.PrimaryKey = primaryKey;
        }
    }

    /// <summary> 
    /// 标识表名、主键等信息特性类 
    /// </summary> 
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class ColumnAttribute : Attribute
    {
        /// <summary> 
        /// 列名 
        /// </summary> 
        public string ColumnName { get; set; }

        public ColumnAttribute(string columnName)
        {
            this.ColumnName = columnName;
        }
    }
}
