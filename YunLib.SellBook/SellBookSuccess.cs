﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.SellBook.Extensions;
using YunLib.Common;

namespace Yunlib.SellBook
{
    public partial class SellBookSuccess : Form
    {
      
        public SellBookSuccess(string doorNum)
        {
            InitializeComponent();
            lblDoorNum.Text = doorNum;
        }

        private void SellBookSuccess_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            timer1.Start();

            CodeOptimize.SolveLightScreen(this);
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("BorrSuccess_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
            timer1.Stop();

            //返回主界面
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";
            if (int.Parse(lblSecond.Text) < 0)
            {
                //返回1Main加载时不执行开灯操作

                timer1.Enabled = false;

                //返回主界面
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
            
        }
    }
}
