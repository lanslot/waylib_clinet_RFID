﻿namespace YunLib_Vertical
{
    partial class FaceLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSure = new System.Windows.Forms.Button();
            this.btnMarkFaces = new System.Windows.Forms.Button();
            this.lblToken = new System.Windows.Forms.Label();
            this.picSample = new System.Windows.Forms.PictureBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblIDCard = new System.Windows.Forms.Label();
            this.lblUName = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.lblRealName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picShow = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPwdErrMsg = new System.Windows.Forms.Label();
            this.picFaceRegister = new System.Windows.Forms.PictureBox();
            this.pictLogin = new System.Windows.Forms.PictureBox();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.picBack = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShow)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFaceRegister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::YunLib_Vertical.Properties.Resources.renlian_u;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.btnSure);
            this.panel1.Controls.Add(this.btnMarkFaces);
            this.panel1.Controls.Add(this.lblToken);
            this.panel1.Controls.Add(this.picSample);
            this.panel1.Controls.Add(this.lblAddress);
            this.panel1.Controls.Add(this.lblIDCard);
            this.panel1.Controls.Add(this.lblUName);
            this.panel1.Controls.Add(this.lblSex);
            this.panel1.Controls.Add(this.lblRealName);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.picShow);
            this.panel1.Location = new System.Drawing.Point(180, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(700, 581);
            this.panel1.TabIndex = 0;
            // 
            // btnSure
            // 
            this.btnSure.BackColor = System.Drawing.Color.LightGray;
            this.btnSure.Enabled = false;
            this.btnSure.FlatAppearance.BorderSize = 0;
            this.btnSure.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnSure.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnSure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSure.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSure.ForeColor = System.Drawing.Color.White;
            this.btnSure.Location = new System.Drawing.Point(384, 440);
            this.btnSure.Name = "btnSure";
            this.btnSure.Size = new System.Drawing.Size(248, 45);
            this.btnSure.TabIndex = 37;
            this.btnSure.Text = "确    定";
            this.btnSure.UseVisualStyleBackColor = false;
            this.btnSure.Click += new System.EventHandler(this.btnSure_Click);
            // 
            // btnMarkFaces
            // 
            this.btnMarkFaces.BackColor = System.Drawing.Color.Cyan;
            this.btnMarkFaces.FlatAppearance.BorderSize = 0;
            this.btnMarkFaces.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkTurquoise;
            this.btnMarkFaces.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkTurquoise;
            this.btnMarkFaces.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarkFaces.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMarkFaces.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnMarkFaces.Location = new System.Drawing.Point(54, 440);
            this.btnMarkFaces.Name = "btnMarkFaces";
            this.btnMarkFaces.Size = new System.Drawing.Size(196, 45);
            this.btnMarkFaces.TabIndex = 36;
            this.btnMarkFaces.Text = "重 新 识 别";
            this.btnMarkFaces.UseVisualStyleBackColor = false;
            this.btnMarkFaces.Click += new System.EventHandler(this.btnMarkFaces_Click);
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.BackColor = System.Drawing.Color.Transparent;
            this.lblToken.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblToken.Location = new System.Drawing.Point(318, 371);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(0, 27);
            this.lblToken.TabIndex = 11;
            this.lblToken.Visible = false;
            // 
            // picSample
            // 
            this.picSample.Location = new System.Drawing.Point(550, 0);
            this.picSample.Name = "picSample";
            this.picSample.Size = new System.Drawing.Size(150, 150);
            this.picSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSample.TabIndex = 1;
            this.picSample.TabStop = false;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAddress.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblAddress.Location = new System.Drawing.Point(438, 335);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(0, 27);
            this.lblAddress.TabIndex = 10;
            // 
            // lblIDCard
            // 
            this.lblIDCard.AutoSize = true;
            this.lblIDCard.BackColor = System.Drawing.Color.Transparent;
            this.lblIDCard.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblIDCard.Location = new System.Drawing.Point(438, 275);
            this.lblIDCard.Name = "lblIDCard";
            this.lblIDCard.Size = new System.Drawing.Size(0, 27);
            this.lblIDCard.TabIndex = 9;
            // 
            // lblUName
            // 
            this.lblUName.AutoSize = true;
            this.lblUName.BackColor = System.Drawing.Color.Transparent;
            this.lblUName.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUName.Location = new System.Drawing.Point(438, 215);
            this.lblUName.Name = "lblUName";
            this.lblUName.Size = new System.Drawing.Size(0, 27);
            this.lblUName.TabIndex = 8;
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.BackColor = System.Drawing.Color.Transparent;
            this.lblSex.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSex.Location = new System.Drawing.Point(438, 145);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(0, 27);
            this.lblSex.TabIndex = 7;
            // 
            // lblRealName
            // 
            this.lblRealName.AutoSize = true;
            this.lblRealName.BackColor = System.Drawing.Color.Transparent;
            this.lblRealName.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRealName.Location = new System.Drawing.Point(438, 85);
            this.lblRealName.Name = "lblRealName";
            this.lblRealName.Size = new System.Drawing.Size(0, 27);
            this.lblRealName.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(318, 335);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 27);
            this.label5.TabIndex = 5;
            this.label5.Text = "地       址：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(315, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 27);
            this.label4.TabIndex = 4;
            this.label4.Text = "身份证号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(318, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 27);
            this.label3.TabIndex = 3;
            this.label3.Text = "账       号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(318, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "性       别：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(318, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "姓       名：";
            // 
            // picShow
            // 
            this.picShow.Location = new System.Drawing.Point(25, 67);
            this.picShow.Name = "picShow";
            this.picShow.Size = new System.Drawing.Size(261, 345);
            this.picShow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picShow.TabIndex = 0;
            this.picShow.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::YunLib_Vertical.Properties.Resources.renlian_d;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.lblPwdErrMsg);
            this.panel2.Controls.Add(this.picFaceRegister);
            this.panel2.Controls.Add(this.pictLogin);
            this.panel2.Controls.Add(this.txtPwd);
            this.panel2.Controls.Add(this.txtUserName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(180, 637);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(700, 357);
            this.panel2.TabIndex = 1;
            this.panel2.Visible = false;
            // 
            // lblPwdErrMsg
            // 
            this.lblPwdErrMsg.AutoSize = true;
            this.lblPwdErrMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblPwdErrMsg.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblPwdErrMsg.ForeColor = System.Drawing.Color.Green;
            this.lblPwdErrMsg.Location = new System.Drawing.Point(134, 233);
            this.lblPwdErrMsg.Name = "lblPwdErrMsg";
            this.lblPwdErrMsg.Size = new System.Drawing.Size(0, 17);
            this.lblPwdErrMsg.TabIndex = 43;
            // 
            // picFaceRegister
            // 
            this.picFaceRegister.Enabled = false;
            this.picFaceRegister.Image = global::YunLib_Vertical.Properties.Resources.renlianbd_3;
            this.picFaceRegister.Location = new System.Drawing.Point(540, 103);
            this.picFaceRegister.Name = "picFaceRegister";
            this.picFaceRegister.Size = new System.Drawing.Size(107, 114);
            this.picFaceRegister.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picFaceRegister.TabIndex = 42;
            this.picFaceRegister.TabStop = false;
            this.picFaceRegister.Click += new System.EventHandler(this.picFaceRegister_Click);
            this.picFaceRegister.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picFaceRegister_MouseDown);
            this.picFaceRegister.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picFaceRegister_MouseUp);
            // 
            // pictLogin
            // 
            this.pictLogin.Image = global::YunLib_Vertical.Properties.Resources.login1;
            this.pictLogin.Location = new System.Drawing.Point(406, 103);
            this.pictLogin.Name = "pictLogin";
            this.pictLogin.Size = new System.Drawing.Size(107, 114);
            this.pictLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictLogin.TabIndex = 2;
            this.pictLogin.TabStop = false;
            this.pictLogin.Click += new System.EventHandler(this.pictLogin_Click);
            this.pictLogin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictLogin_MouseDown);
            this.pictLogin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictLogin_MouseUp);
            // 
            // txtPwd
            // 
            this.txtPwd.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPwd.Location = new System.Drawing.Point(137, 184);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(236, 34);
            this.txtPwd.TabIndex = 41;
            this.txtPwd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtPwd_MouseDown);
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtUserName.Location = new System.Drawing.Point(137, 116);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(236, 34);
            this.txtUserName.TabIndex = 40;
            this.txtUserName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtUserName_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(78, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 27);
            this.label7.TabIndex = 39;
            this.label7.Text = "密码：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(78, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 27);
            this.label6.TabIndex = 38;
            this.label6.Text = "账号：";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // picBack
            // 
            this.picBack.Image = global::YunLib_Vertical.Properties.Resources.hui_w;
            this.picBack.Location = new System.Drawing.Point(942, 1018);
            this.picBack.Name = "picBack";
            this.picBack.Size = new System.Drawing.Size(100, 100);
            this.picBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBack.TabIndex = 39;
            this.picBack.TabStop = false;
            this.picBack.Click += new System.EventHandler(this.picBack_Click);
            this.picBack.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseDown);
            this.picBack.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picBack_MouseUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.ForeColor = System.Drawing.Color.Magenta;
            this.label8.Location = new System.Drawing.Point(1047, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 28);
            this.label8.TabIndex = 41;
            this.label8.Text = "秒";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblCount.ForeColor = System.Drawing.Color.Magenta;
            this.lblCount.Location = new System.Drawing.Point(1005, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(48, 28);
            this.lblCount.TabIndex = 40;
            this.lblCount.Text = "120";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // FaceLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.di;
            this.ClientSize = new System.Drawing.Size(1080, 1129);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.picBack);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FaceLoginForm";
            this.Text = "FaceLoginForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FaceLoginForm_FormClosed);
            this.Load += new System.EventHandler(this.FaceLoginForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picShow)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFaceRegister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox picShow;
        private System.Windows.Forms.PictureBox picSample;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblIDCard;
        private System.Windows.Forms.Label lblUName;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label lblRealName;
        private System.Windows.Forms.Label lblToken;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnMarkFaces;
        private System.Windows.Forms.Button btnSure;
        private System.Windows.Forms.PictureBox pictLogin;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox picFaceRegister;
        private System.Windows.Forms.Label lblPwdErrMsg;
        private System.Windows.Forms.PictureBox picBack;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Timer timer2;
    }
}