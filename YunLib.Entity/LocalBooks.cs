﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Entity
{
    public class LocalBooks
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 代替Image字段
        /// </summary>
        public string StandardCode { get; set; }

        /// <summary>
        /// 条形码
        /// </summary>
        public string BarCode { get; set; }

        /// <summary>
        /// 书柜行号
        /// </summary>
        public int DoorID { get; set; }

        /// <summary>
        /// 书柜列号
        /// </summary>
        public int DoorCode { get; set; }

        /// <summary>
        /// 书名
        /// </summary>
        public string BName { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Writer { get; set; }

        /// <summary>
        /// 索引
        /// </summary>
        public string Classify { get; set; }

        /// <summary>
        /// 是否借出
        /// </summary>
        public string IsLend { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string Press { get; set;}

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublicationDate { get; set; }

        /// <summary>
        /// 书格号
        /// </summary>
        public int DoorNUM { get; set; }
    }
}
