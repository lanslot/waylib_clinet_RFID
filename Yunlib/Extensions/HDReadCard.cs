﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using YunLib.Common;

namespace Yunlib.Extensions
{
    public class HDReadCard
    {
        //打开端口 

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Open")]
        public static extern int ICC_Reader_Open(int port, int buod);



        //关闭端口
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Close")]
        public static extern int ICC_Reader_Close(int ReaderHandle);

        [DllImport("SSSE32.dll", EntryPoint = "ICC_PosBeep")]
        public static extern int ICC_PosBeep(int ReaderHandle, byte time);/*蜂鸣*/

        //注意：输入的是12位的密钥，例如12个f，但是password必须是6个字节的密钥，需要用StrToHex函数处理。
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Authentication_Pass")]
        public static extern int PICC_Reader_Authentication_Pass(int ReaderHandle,
                                                                    byte Mode,
                                                                    byte SecNr,
                                                                    byte[] PassWord);
        //读卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Read")]
        public static extern int PICC_Reader_Read(int ReaderHandle, byte Addr,
                                                        byte[] Data);
        //写卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Write")]
        public static extern int PICC_Reader_Write(int ReaderHandle, byte Addr,
                                                        byte[] Data);

        //将字符命令流转为16进制流
        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(StringBuilder strIn, int len, Byte[] HexOut);

        //将16进制流命令转为字符流
        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(Byte[] strIn, int inLen,
                                                StringBuilder strOut);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_PowerOnTypeA")]
        public static extern int PICC_Reader_PowerOnTypeA(int ReaderHandle, byte[] Response);//上电 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_pre_PowerOn")]
        public static extern int ICC_Reader_pre_PowerOn(int ReaderHandle, byte SLOT, byte[] Response);//上电 返回数据长度 失败小于0
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Application")]
        public static extern int ICC_Reader_Application(int ReaderHandle, byte SLOT, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Application")]
        public static extern int PICC_Reader_Application(int ReaderHandle, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        //HD100身份证
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ReadIDMsg")]
        public static extern int PICC_Reader_ReadIDMsg(int RHandle, StringBuilder pBmpFile, StringBuilder pName, StringBuilder pSex, StringBuilder pNation, StringBuilder pBirth, StringBuilder pAddress, StringBuilder pCertNo, StringBuilder pDepartment, StringBuilder pEffectData, StringBuilder pExpire, StringBuilder pErrMsg);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ID_ReadUID")]
        public static extern int PICC_Reader_ID_ReadUID(int fd, byte[] UID);//获取身份证UID, 8Byte

        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(byte[] strIn, int inLen, byte[] strOut);


        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(byte[] strIn, int inLen, byte[] strOut);

        
        public static List<string> ReadIDCardID(string port,int Baud)
        {
            //该函数封装了两种不同协议的身份证操作，根据情况选择
            //HD100读身份证
            StringBuilder pBmpFile = new StringBuilder(100);
            StringBuilder pFingerData = new StringBuilder(1025);
            StringBuilder pBmpData = new StringBuilder(77725);
            StringBuilder pBase64Data = new StringBuilder(6025);
            StringBuilder pName = new StringBuilder(100);
            StringBuilder pSex = new StringBuilder(100);
            StringBuilder pNation = new StringBuilder(100);
            StringBuilder pBirth = new StringBuilder(100);
            StringBuilder pAddress = new StringBuilder(400);
            StringBuilder pCertNo = new StringBuilder(100);
            StringBuilder pDepartment = new StringBuilder(100);
            StringBuilder pEffectData = new StringBuilder(100);
            StringBuilder pExpire = new StringBuilder(100);
            StringBuilder pData = new StringBuilder(100);
            StringBuilder pErrMsg = new StringBuilder(100);
            StringBuilder strUID = new StringBuilder(100);
            List<string> list = new List<string>();
            byte[] uid = new byte[20];

            string str = System.Environment.CurrentDirectory;
            pBmpFile.Append(str);
            pBmpFile.Append(@"\zp.bmp");


            int Rhandle;
            int ret;

            //  String a = textBox1.Text;
            int p = int.Parse(port.Substring(3));

            Rhandle = ICC_Reader_Open(p, Baud);
            if (Rhandle < 0)
            {
                // //list.Add("连接失败!");
                LogManager.ErrorRecord("身份证读卡连接失败,端口："+port);
                ICC_Reader_Close(Rhandle);
                return list;
            }
            else
            {
               // //list.Add("连接成功!:" + Rhandle);
            }
            //该函数获取身份证信息的同时保存照片到指定路径
            ret = PICC_Reader_ReadIDMsg(Rhandle, pBmpFile, pName, pSex, pNation, pBirth, pAddress, pCertNo, pDepartment, pEffectData, pExpire, pErrMsg);
            //  ret = PICC_Reader_ID_ReadUID(Rhandle, uid);
            ICC_Reader_Close(Rhandle);
            if (ret != 0)
            {
                //读卡失败
                ICC_Reader_Close(Rhandle);
                // //list.Add("读卡失败!");
                LogManager.ErrorRecord("身份证读卡失败");
                return list;
            }
            else
            {
                list.Add( pName.ToString());
                list.Add( pSex.ToString());
                list.Add( pCertNo.ToString());
                list.Add( pAddress.ToString());
                list.Add(pNation.ToString());
                list.Add(pBmpFile.ToString());

                return list;
            }
        }

        /// <summary>
        /// 正常读im卡
        /// 读第12扇区48区块
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        public string ReadCardIM(string port, int Baud)
        {
            try
            {
                //M1卡操作示例
                int Rhandle;
                int ret;
                int p = int.Parse(port.Substring(3));

                Rhandle = ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    //list.Add("M1连接失败!");

                    return "";
                }
                else
                {
                    //list.Add("M1连接成功!:" + Rhandle);
                }
               // ret = ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用

               
                
                byte mode = 0x60;//认证KeyA时为0x60，认证KeyB时为0x61，此处测试用了KeyA
                byte secNr = 0x0c;// 扇区号0~15, 此处测试认证的为1扇区
                byte[] password = new byte[7];//1扇区的秘钥，此处为默认12个 f
                password[0] = 0xff;
                password[1] = 0xff;
                password[2] = 0xff;
                password[3] = 0xff;
                password[4] = 0xff;
                password[5] = 0xff;
                //   password[6] = 0x00;
                ret = PICC_Reader_Authentication_Pass(Rhandle, mode, 12, password);
                //  ret = PICC_Reader_Authentication_Pass(Rhandle, mode, secNr, password);
                if (ret != 0)
                {
                    //秘钥认证失败
                    //list.Add("秘钥认证失败:" + ret);
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                byte[] hexData = new byte[17];
                //48h是块号（范围0~63），因为秘钥认证的是1扇区所以只能读取1扇区的块，4~7块
                ret = PICC_Reader_Read(Rhandle, 48, hexData);
                //   ret = PICC_Reader_Read(Rhandle, Addr, hexData);
                byte[] RecData = new byte[34];
                if (ret != 0)
                {
                    //读卡失败了
                    //list.Add("读卡失败了");
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                else
                {
                    //list.Add("读卡成功了lengh:" + hexData.Length);
                    
                    HexToStr(hexData, 17, RecData);
                    //list.Add("读卡成功了:" + Encoding.Default.GetString(RecData));
                }
                
                ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                ICC_Reader_Close(Rhandle);

                
                return Encoding.Default.GetString(RecData);
            }
            catch (Exception ex)
            {
                //list.Add(ex.Message);

                LogManager.ErrorRecord("{0}-读IM卡出现错误：{1}".FormatWith(DateTime.Now, ex.Message));
                return "";
            }
        }

        /// <summary>
        /// 成都武警学院读IM卡
        /// 读物理卡号
        /// </summary>
        /// <param name="port"></param>
        /// <param name="Baud"></param>
        /// <returns></returns>
        public string ReadCardIMForSCWJ(string port, int Baud)
        {
            try
            {
                //M1卡操作示例
                int Rhandle;
                int ret;
                int p = int.Parse(port.Substring(3));

                Rhandle = ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    //list.Add("M1连接失败!");

                    return "";
                }
                else
                {
                    //list.Add("M1连接成功!:" + Rhandle);
                }
                // ret = ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用



                byte mode = 0x60;//认证KeyA时为0x60，认证KeyB时为0x61，此处测试用了KeyA
                byte secNr = 0x0c;// 扇区号0~15, 此处测试认证的为1扇区
                byte[] password = new byte[7];//1扇区的秘钥，此处为默认12个 f
                password[0] = 0xff;
                password[1] = 0xff;
                password[2] = 0xff;
                password[3] = 0xff;
                password[4] = 0xff;
                password[5] = 0xff;
                //   password[6] = 0x00;
                ret = PICC_Reader_Authentication_Pass(Rhandle, mode, 0, password);
                //  ret = PICC_Reader_Authentication_Pass(Rhandle, mode, secNr, password);
                if (ret != 0)
                {
                    //秘钥认证失败
                    //list.Add("秘钥认证失败:" + ret);
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                byte[] hexData = new byte[17];
                //48h是块号（范围0~63），因为秘钥认证的是1扇区所以只能读取1扇区的块，4~7块
                ret = PICC_Reader_Read(Rhandle, 0, hexData);
                //   ret = PICC_Reader_Read(Rhandle, Addr, hexData);
                byte[] RecData = new byte[34];
                if (ret != 0)
                {
                    //读卡失败了
                    //list.Add("读卡失败了");
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                //else
                //{
                //    //list.Add("读卡成功了lengh:" + hexData.Length);

                //    HexToStr(hexData, 17, RecData);
                //    //list.Add("读卡成功了:" + Encoding.Default.GetString(RecData));
                //}

                ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                ICC_Reader_Close(Rhandle);

                var data = hexData.Take(4).ToArray();
                string str = BitConverter.ToString(data);

                var ss = str.Split('-');
                string str1 = "";
                for (int i = ss.Length - 1; i >= 0; i--)
                {
                    str1 += ss[i];
                }
                long num = long.Parse(str1, System.Globalization.NumberStyles.HexNumber);

                return num.ToString();
            }
            catch (Exception ex)
            {
                //list.Add(ex.Message);

                LogManager.ErrorRecord("{0}-读IM卡出现错误：{1}".FormatWith(DateTime.Now, ex.Message));
                return "";
            }
        }

        public string WriteCardIM(string port, int Baud,string msg)
        {
            try
            {
                //M1卡操作示例
                int Rhandle;
                int ret;
                //连接读卡器
                int p = int.Parse(port.Substring(3));

                Rhandle = ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    //list.Add("M1连接失败!");

                    return "";
                }

                ret = ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用

                if (ret != 0)
                {
                    //秘钥认证失败
                    //list.Add("蜂鸣认证失败:" + ret);
                    ICC_Reader_Close(Rhandle);
                    return "";
                }


                
                byte mode = 0x60;//认证KeyA时为0x60，认证KeyB时为0x61，此处测试用了KeyA
                byte secNr = 0x0c;// 扇区号0~15, 此处测试认证的为1扇区
                byte[] password = new byte[7];//1扇区的秘钥，此处为默认12个 f
                password[0] = 0xff;
                password[1] = 0xff;
                password[2] = 0xff;
                password[3] = 0xff;
                password[4] = 0xff;
                password[5] = 0xff;
                //   password[6] = 0x00;
                ret = PICC_Reader_Authentication_Pass(Rhandle, mode, secNr, password);
                //  ret = PICC_Reader_Authentication_Pass(Rhandle, mode, secNr, password);
                if (ret != 0)
                {
                    //秘钥认证失败
                    //list.Add("秘钥认证失败:" + ret);
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                byte Addr = 0x04; //块号（范围0~63），因为秘钥认证的是1扇区所以只能读取1扇区的块，4~7块
                byte[] hexData = new byte[17];

                var msg1 = msg + "0000000000000000000000000000000000";
                var msg2 = msg1.Substring(0, 34);
                
                ret = StrToHex(Encoding.Default.GetBytes(msg2), 17, hexData);

                ret = PICC_Reader_Write(Rhandle, 48, hexData);
                //   ret = PICC_Reader_Read(Rhandle, Addr, hexData);
                byte[] RecData = new byte[34];
                if (ret != 0)
                {
                    //写卡失败了
                    //list.Add("写卡失败了");
                    ICC_Reader_Close(Rhandle);
                    return "";
                }
                else
                {
                    //list.Add("写卡成功了lengh:" + hexData.Length);

                    HexToStr(hexData, 17, RecData);
                    //list.Add("写卡成功了:" + Encoding.Default.GetString(RecData));
                }
                
                ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                ICC_Reader_Close(Rhandle);
                return Encoding.Default.GetString(RecData).Substring(0, msg.Length);
            }
            catch (Exception ex)
            {
                //list.Add(ex.Message);

                LogManager.ErrorRecord("{0}-写IM卡出现错误：{1}".FormatWith(DateTime.Now, ex.Message));
                return "";
            }
        }

    }
}
