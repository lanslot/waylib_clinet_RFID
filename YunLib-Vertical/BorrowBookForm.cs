﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib.ReadCard;
using Yunlib_Vertical.Extensions;

namespace YunLib_Vertical
{
    public partial class BorrowBookForm : Form
    {

        public string BarCode { get; set; }

        public string DoorNum { get; set; }
        

        public string BorrowType { get; set; }


        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        //获取服务器url
        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string loginKey = ConfigManager.LoginKey;
        private int borrowType = ConfigManager.BorrowType;
        private string url = "";
        private IReadCard read = ObjectCreator.GetReadClass();
        private JPForm jp;
        public BorrowBookForm()
        {
            InitializeComponent();
            jp = new JPForm();
            ZooKeeperClient.wc.onReceiveMessage += onReceiveMessage;
        }

        private void onReceiveMessage(object source, string rDataStr)
        {
            //   string barCode = Encoding.ASCII.GetString(scanFrame);
            this.BeginInvoke((Action)(() =>
            {

                timer1.Stop();
                timer2.Stop();
                SuccessForm frm = new SuccessForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = Parent;
                frm.Title = "借 阅 成 功";
                frm.Message = "请到" + DoorNum + "号柜门取走图书，完成后请关闭柜门";
                ZooKeeperClient.wc.onReceiveMessage -= onReceiveMessage;
                this.Close();
                frm.Show();
              
            }));
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void BorrowBookForm_Load(object sender, EventArgs e)
        {
            try
            {
                //生成相应书籍的二维码
                LoadCreateQrCode();
                
               if(borrowType == 0)
                {
                    
                    panel2.Visible = false;
                    panel4.Visible = false;
                }else if(borrowType == 1)
                {
                    panel4.Visible = false;
                }

                txtAccount.Focus();
                PlayVoice.play(this.Name);
            }
            catch(Exception ex)
            {
                LogManager.WriteLogs(this.Name + "加载出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //验证TextBox非空
                if (string.IsNullOrEmpty(txtAccount.Text))
                {
                    lblERR.Text = "请输入读者证号/手机号";
                }
                else if (string.IsNullOrEmpty(txtPwd.Text))
                {
                    lblERR.Text = "请输入密码";
                }
                else
                {
                    var uName = txtAccount.Text.Trim();
                    var uPwd = txtPwd.Text.Trim();

                    var timeStamp = DateTime.Now.Ticks;

                    var pwd = "{0}{1}".FormatWith(uPwd, loginKey);
                    var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                    var signstr = "{0}{1}{2}{3}{4}{5}{6}{7}".FormatWith(BarCode, libraryId, signPwd, DoorNum, machineId, timeStamp, uName, commonKey);
                    var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                    url = "{0}/public/auth/machinelogin?uName={1}&timeStamp={2}&libraryPwd={3}&bookId={4}&machineId={5}&machLatNum={6}&sign={7}&libraryId={8}".FormatWith(servicePath, uName, timeStamp, signPwd, BarCode, machineId, DoorNum, sign, libraryId);

                    var loginType = ConfigurationManager.AppSettings["LoginType"].ToString();
                    if (loginType == "1")
                    {
                        url += "&sipPwd={0}".FormatWith(uPwd);
                    }
                    Func<string> longTask = new Func<string>(delegate ()
                    {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return lendBook(url);
                        //  返回任务结果：5
                    });
                    //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                    //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                    longTask.BeginInvoke(ar =>
                    {
                        //  使用EndInvoke获取到任务结果（5）
                        string result = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                        {
                            Invoke(new Action(() =>

                           EndLend(result)
                            // dataGridView1.DataSource = result

                            ));
                        }
                    }, null);
                }
               

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(this.Name + "登录借书请求出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void btnFaceLogin_Click(object sender, EventArgs e)
        {
            FaceLoginForm frm = new FaceLoginForm();
            frm.MdiParent = this.ParentForm;
            frm.Parent = this.Parent;
            frm.barCode = BarCode;
            this.Close();
            frm.Show();
        }

        #region 登录借书请求
        private string lendBook(string url)
        {
            try
            {
                var IsLoginSuccess = WebHelper.HttpWebRequest(url, "", true);

                return IsLoginSuccess;
            }catch(Exception ex)
            {
                LogManager.WriteLogs("lendBook出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void EndLend(string json)
        {
            LogManager.WriteLogs("登录借书操作返回", json, LogFile.Record);
            if (string.IsNullOrWhiteSpace(json))
            {

            }
            else
            {
                var result = json.ToJObject();
                if (result["code"].ToString() == "100")
                {
                    var list = accessBLL.GetBookByDoorNum(int.Parse(DoorNum.ToString()));

                    CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                    SuccessForm frm = new SuccessForm();
                    frm.MdiParent = this.ParentForm;
                    frm.Parent = Parent;
                    frm.Title = "借 阅 成 功";
                    frm.Message = "请到"+DoorNum+"号柜门取走图书，完成后请关闭柜门";
                    this.Close();
                    frm.Show();
                }
                else
                {
                    FailForm frm = new FailForm();
                    frm.MdiParent = this.ParentForm;
                    frm.Parent = Parent;
                    frm.Title = "借 阅 失 败";
                    frm.Message = result["msg"].ToString();
                    this.Close();
                    frm.Show();
                }
            }
        }

        #endregion

        #region 生成二维码
        /// <summary>
        /// 生成二维码
        /// </summary>
        private void LoadCreateQrCode()
        {
            string url = "";
            if (BorrowType == "2")
            {
                url = string.Format("{3}wxpay/redirectUrl?url={3}bookinfo.html?bookId={0}&machineId={1}&machLatNum={2}&libraryId={4}", BarCode, ConfigManager.MachineID, DoorNum, ConfigManager.ServicePath, ConfigManager.LibraryID);
            }
            else
            {
                url = string.Format("{3}wxpay/redirectUrl?url={3}lend-book.html?bookId={0}&machineId={1}&machLatNum={2}&libraryId={4}", BarCode, ConfigManager.MachineID, DoorNum, ConfigManager.ServicePath, ConfigManager.LibraryID);
            }

            Image image = CreateImage(url);
            if (image != null)
            {
                picEncode.Image = image;
            }
        }

        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }


        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            lblCount.Text = time.ToString();
            if (time <= 0)
            {
                MainForm frm = new MainForm();
                frm.MdiParent = ParentForm;
                frm.Parent = this.Parent;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }

       

        private void BorrowBookForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;
            timer2.Stop();
            timer2.Enabled = false;
            jp.Close();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                string strID = "";
                if (ConfigManager.ReadCardType == "1")
                {
                    // 读身份证
                    strID = read.ReadIDCard(ConfigManager.Pos, ConfigManager.IMPort);
                }
                else if (ConfigManager.ReadCardType == "2")
                {
                    // 武警学院读IM卡
                    // strID = hdr.ReadCardIMForSCWJ(ConfigManager.Pos, ConfigManager.IMPort);

                    strID = read.ReadIM(ConfigManager.Pos, ConfigManager.IMPort);
                    //  MessageBox.Show(strID);
                    //  读IM卡
                    // strID = hdr.ReadCardIM(ConfigManager.Pos, ConfigManager.IMPort);
                }
                else if (ConfigManager.ReadCardType == "3")
                {

                    strID = read.ReadIM(ConfigManager.Pos, ConfigManager.IMPort);
                    if (string.IsNullOrWhiteSpace(strID))
                    {
                        strID = read.ReadIDCard(ConfigManager.Pos, ConfigManager.IMPort);
                    }

                }
                if (!string.IsNullOrWhiteSpace(strID))
                {
                    txtAccount.Text = strID;
                    if (ConfigManager.IsNeedPassWord == 1)
                    {
                        txtPwd.Text = "111111";
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void txtAccount_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(155, 1400);
            jp.Show();
            txtAccount.Focus();

            this.Activate();
        }

        private void txtPwd_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(155, 1400);
            jp.Show();
            txtPwd.Focus();

            this.Activate();
        }
    }
}
