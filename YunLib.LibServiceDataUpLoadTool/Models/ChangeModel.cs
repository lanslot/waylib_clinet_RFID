﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.LibServiceDataUpLoadTool.Models
{
    public class ChangeModel
    {
        public int lend_status{get;set;}
        public string lib_dz{get;set;}
        public string op_user{get;set;}
        public int book_id{get;set;}
        public string barcode{get;set;}
        public string site_name{get;set;}
        public string tsg_site_code{get;set;}
        public long add_time{get;set;}
        public long must_time{get;set;}
        public long real_time { get; set; }

        //public string IsTeamard { get; set; }

        //public string pd1 { get; set; }
    }
}
