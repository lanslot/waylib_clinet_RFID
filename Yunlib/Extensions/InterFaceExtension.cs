﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Extensions
{
    public class InterFaceExtension
    {
        /// <summary>
        /// 后台接口Uri
        /// </summary>
        public string ServicePath
        {
            get
            {
                return ConfigurationManager.AppSettings["ServicePath"];
            }
        }

        /// <summary>
        /// 机器编号
        /// </summary>
        public string MachineID
        {
            get
            {
                return ConfigurationManager.AppSettings["MachineID"];
            }
        }

        /// <summary>
        /// 图书馆编号
        /// </summary>
        public string LibraryID
        {
            get
            {
                return ConfigurationManager.AppSettings["LibraryID"];
            }
        }

        /// <summary>
        /// 口令
        /// </summary>
        public string CommonKey
        {
            get
            {
                return ConfigurationManager.AppSettings["CommonKey"];
            }
        }

        /// <summary>
        /// 密码口令
        /// </summary>
        public string LoginKey
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginKey"];
            }
        }
    }
}
