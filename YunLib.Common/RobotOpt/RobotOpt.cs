﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShenZhen.SmartLife.Control;
using System.Threading;

namespace Yunlib.Common
{
    public static class RobotOpt
    {
        static SmartLifeBroker model = new SmartLifeBroker();

        /// <summary>
        /// 初始化控制台串口
        /// </summary>
        public static void InitConsole()
        {
            model.Init("COM3", 9600);
        }

        /// <summary>
        /// 初始化扫描串口
        /// </summary>
        public static void InitScan()
        {
            model.Init("COM6", 9600);
        }

        /// <summary>
        /// 初始化扫描设备
        /// </summary>
        public static void InitScanDevice()
        {
            model.InitScanDevice(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
        }

        /// <summary>
        /// 打开相应编号的门
        /// </summary>
        /// <param name="doorID">行数</param>
        /// <param name="doorCode">列数</param>
        public static void OpenDoor(int doorID, int doorCode)
        {
            model.OpenDoor(doorID, doorCode);
        }

        /// <summary>
        /// 打开一组灯
        /// </summary>
        /// <param name="doorID"></param>
        public static void OpenLight(int doorID)
        {
            model.OpenLight(doorID);
        }

        /// <summary>
        /// 关闭一组灯
        /// </summary>
        /// <param name="doorID"></param>
        public static void CloseLight(int doorID)
        {
            model.CloseLight(doorID);
        }

        /// <summary>
        /// 打开扫描器
        /// </summary>
        public static void StartScan()
        {
            model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
        }

        /// <summary>
        /// 关闭扫描器
        /// </summary>
        public static void StopScan()
        {
            model.StopScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
        }

        /// <summary>
        /// 循环打开所有的灯
        /// </summary>
        public static void WhileOpenLight()
        {
            for (int i = 1; i <= 12; i++)
            {
                OpenLight(i);
                Thread.Sleep(500);
            }
        }
    }
}
