﻿using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Entity;
using Yunlib.SellBook.Extensions;
using YunLib.BLL;
using YunLib.Common;

namespace Yunlib.SellBook.AdminManage
{
    public partial class BackMain : Form
    {

        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        public BackMain()
        {
            InitializeComponent();
        }

        private void btnAddBooks_Click(object sender, EventArgs e)
        {
            FillBookNew frm = new FillBookNew{ MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }

        private void btnDoorManage_Click(object sender, EventArgs e)
        {
            CardReadOrWrite frm = new CardReadOrWrite { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }

        private void btnDoorManage240_Click(object sender, EventArgs e)
        {
            InitForm frm = new InitForm { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }


        private void btnReturnMain_Click(object sender, EventArgs e)
        {
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Hide();

            this.Dispose();

            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LocalBookManager frm = new LocalBookManager { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }

        private void btnUnderCarriage_Click(object sender, EventArgs e)
        {
            UnderCarriage frm = new UnderCarriage { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //InitData();
            InitForm frm = new InitForm { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }

        private void InitData()
        {
            var servicePath = ConfigManager.ServicePath;
            var libraryId = ConfigManager.LibraryID;
            var commonKey = ConfigManager.CommonKey;

            var timeStamp = DateTime.Now.Ticks;

            var signStr = "{0}{1}{2}".FormatWith(libraryId, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            DataTable dt = BLL.GetMachineInitData();

            string jsonStr = JsonHelper.ToJson(dt).Replace("DoorNUM", "machLatNum").Replace("BarCode", "bookId ");

            JObject jo = new JObject();
            jo.Add("machineId", ConfigManager.MachineID);
            jo.Add("machineLattice", jsonStr);

            JObject joNew = new JObject();
            joNew.Add("json", jo.ToJson());

            JToken jtoken = JToken.Parse(joNew.ToJson());

            string jsonStr1 = "json=" + jtoken.ToString().Replace("\\\\\\\"", "\"").Replace("\\\"", "\"").Replace("\\", "").Replace("r", "").Replace("\"{", "{").Replace("\"[", "[").Replace("\"]", "]").Replace("]\"", "]").Replace("}\"", "}").Replace(" ", "");

            try
            {
                var url = "{0}/public/machine/putMachine?timeStamp={1}&libraryId={2}&sign={3}".FormatWith(servicePath, timeStamp, libraryId, sign);
                var back = WebHelper.HttpWebRequest(url, jsonStr1, Encoding.GetEncoding("utf-8"), true).ToJObject();

                if (back["code"].ToString() == "100")
                {
                    MessageBox.Show("数据同步成功");
                }
                else
                {
                    MessageBox.Show("数据同步化失败");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DoorManager frm = new DoorManager{ MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }
    }
}
