﻿using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.Entity;
using YunLib.BLL;
using YunLib.Common;
using ZooKeeperNet;

namespace Yunlib.Extensions
{
    public class Watcher : IWatcher
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        public event Action<object, string> onReceiveMessage;
        string msg = "";

        public void Process(WatchedEvent @event)
        {
            string alertMsg = string.Empty;
            try
            {
                LogManager.BorrowRecord(" 收到节点事件通知：{0}，内容： {1}".FormatWith(DateTime.Now, @event.State));
                if (@event.Type == EventType.NodeDataChanged)
                {
                    alertMsg += "@event.Type == EventType.NodeDataChanged|";

                    string rDataStr = ZookeeperCallBack(ConfigManager.ZookeeperPath, ConfigManager.MachineID);
                    alertMsg += "string rDataStr = ZookeeperCallBack(ConfigManager.ZookeeperPath, ConfigManager.MachineID)|";

                    LogManager.WriteLogs("zookeeper接收命令"," 节点数据修改时间：{0}，内容：{1}".FormatWith(DateTime.Now, rDataStr),LogFile.SQL);

                    string[] dataArr = rDataStr.Split('-');
                    
                  msg += "Zookeeper返回信息:" + dataArr[0];

                    switch (dataArr[0])
                    {
                        case "1":
                            try
                            {
                                //借书成功打开柜门
                                HttpBorrSuccess(rDataStr, dataArr);
                                alertMsg += "HttpBorrSuccess(rDataStr, dataArr);|";
                            }
                            catch (Exception ex)
                            {
                                LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                            }
                            break;
                        case "2":
                            try
                            {
                                //还书成功打开柜门
                                HttpReturnSuccess(dataArr);
                                alertMsg += "HttpReturnSuccess(dataArr);|";
                            }
                            catch (Exception ex)
                            {
                                LogManager.ErrorRecord("扫码借书出现错误，错误原因：{0}".FormatWith(ex.Message));
                            }
                            break;
                        case "3":
                            HttpOrderBorr(dataArr[1], dataArr[2]);
                            alertMsg += "HttpOrderBorr(dataArr[1], dataArr[2]);|";
                            break;
                        //预约还书
                        case "4":
                            HttpOrderReturn(dataArr[1], dataArr[2], dataArr[3]);
                            alertMsg += "HttpOrderReturn(dataArr[1], dataArr[2], dataArr[3]);|";
                            break;
                        //预约上架
                        case "5":
                            HttpOrderShelves(dataArr[1], dataArr);
                            alertMsg += "HttpOrderShelves(dataArr[1], dataArr);|";
                            break;
                        //取消预约借书
                        case "7":
                            HttpCancelOrderBorr(dataArr[1]);
                            alertMsg += "HttpCancelOrderBorr(dataArr[1]);|";
                            break;
                        //取消预约还书
                        case "8":
                            //还书失败 Zookeeper返回的状态
                            HttpCancelOrderReturn(dataArr[1]);
                            alertMsg += "HttpCancelOrderReturn(dataArr[1]);|";
                            break;
                        case "9":
                            //借书失败 Zookeeper返回的状态
                            LogManager.ErrorRecord("【借书】开门失败，错误信息：{0}".FormatWith(rDataStr));
                            break;
                        case "11":
                            //售书成功
                            HttpBorrSuccess(rDataStr, dataArr);
                            LogManager.ErrorRecord("【售书】售书成功开门：{0}".FormatWith(rDataStr));
                            break;
                        default:
                            break;
                    }
                }

                //重新建立连接
                if (@event.State == KeeperState.Expired || @event.State == KeeperState.Disconnected)
                {

                    LogManager.BorrowRecord("**** 重新建立连接时间：{0}".FormatWith(DateTime.Now));
                    ZooKeeperClient.createNode();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("Watcher出现错误：{1},错误原因：{0}".FormatWith(ex.Message, alertMsg));
                //  MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private string ZookeeperCallBack(string zookeeperPath, string machineID)
        {
            //节点路径+/+机器编号
            string path = zookeeperPath + "/" + machineID;

            //获取节点修改后的数据，绑定监听
            byte[] data = ZooKeeperClient.zk.GetData(path, ZooKeeperClient.wc, null);

            //获取Zookeeper返回的字符串
            return Encoding.Default.GetString(data);
        }

        private void HttpReturnSuccess(string[] sdata)
        {
            //获得空书柜集合
            IList<LocalBooks> currentModel = accessBLL.GetNullBookCase();

            var back = WebHelper.HttpWebRequest("{0}/public/book/bookdesc?bookId={1}".FormatWith(ConfigManager.ServicePath, sdata[1].ToString())).ToJObject();

            if (back["code"].ToString() == "100")
            {
                JToken datas = back["data"];
                LocalBooks model = new LocalBooks
                {
                    BarCode = sdata[1].ToString(),
                    BName = datas["title"].ToString() ?? "",
                    Image = datas["coverUrl"].ToString() ?? "",
                    Introduction = datas["abstract"].ToString() ?? "",
                    Press = datas["publisher"].ToString() ?? "",
                    PublicationDate = datas["pubdate"].ToString() ?? "",
                    Writer = datas["author"].ToString() ?? "",
                    DoorID = currentModel[0].DoorID,
                    DoorNUM = currentModel[0].DoorNUM
                };

                bool res = accessBLL.FillBookToAcess(model);

                if (res)
                {
                    //自动释放资源
                    using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                    {
                        //初始化控制台串口
                        consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                        //打开相应行号的柜门
                        consoleModel.OpenDoor(currentModel[0].DoorID, currentModel[0].DoorCode, 4);

                        LogManager.BorrowRecord("【还书】成功开门，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                    }
                }
                else
                {
                    //Zookeeper返回成功，但是本机执行录入数据
                    LogManager.ErrorRecord("【还书】开门失败，{0}-{1}*****{2}".FormatWith(currentModel[0].DoorID, currentModel[0].DoorCode, DateTime.Now));
                }
            }
            else
            {
                //code 返回的不是101
                LogManager.ErrorRecord("【还书】返回信息——code:{0}  msg:{1}  datetime:{2}".FormatWith(back["code"].ToString(), back["msg"].ToString(), DateTime.Now));
            }
        }

        private void HttpBorrSuccess(string rDataStr, string[] sdata)
        {
            //传的多本书，就有逗号隔开
            if (sdata[2].Contains(","))
            {
                List<string> doorNum = new List<string>();
                sdata[2].Split(',').Each(x => doorNum.Add(x.ToString()));
                doorNum.Each(y => BorrBookOpen(rDataStr, y));
            }
            else
            {
                string doorNum = sdata[2];
                BorrBookOpen(rDataStr, doorNum);
            }

            try
            {
                if (sdata[0] == "1")
                {
                    
                    Action<object, string> onReceiveMessage;
                    onReceiveMessage = this.onReceiveMessage;
                    onReceiveMessage(this, sdata[2]);

                }
                else if (sdata[0] == "11")
                {
                    SellBook.sb.Close();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【界面跳转失败】，{0}".FormatWith(ex.Message));
            }
        }

        private void HttpOrderBorr(string barCode, string timeStamp)
        {
            try
            {
                barCode = barCode.Replace(ConfigManager.LibraryID, "");
                string isLend = barCode + "_" + timeStamp;
                bool res = accessBLL.UpdateLocalBookbyCode(barCode, isLend);
                if (res)
                {
                    LogManager.BorrowRecord("预约图书成功，图书BarCode:{0},过期日期:{1}".FormatWith(barCode, timeStamp));
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("预约借书错误信息：{0}".FormatWith(ex.Message));
            }
        }

        private void HttpCancelOrderBorr(string barCode)
        {
            barCode = barCode.Replace(ConfigManager.LibraryID, "");
            bool res = accessBLL.UpdateLocalBookbyCode(barCode, "");
            if (res)
            {
                LogManager.BorrowRecord("取消预约图书成功，图书BarCode:{0},取消时间:{1}".FormatWith(barCode, DateTime.Now.ToString()));
            }
        }

        private void HttpOrderReturn(string doorNum, string timeStamp, string barCode)
        {
            string barTime = barCode + "_" + timeStamp;

            bool res = accessBLL.UpdateLocalBookbydoornum(barTime, Convert.ToInt32(doorNum));
            if (res)
            {
                LogManager.BorrowRecord("预约还书成功，预约格子编号:{0},过期日期:{1},书BarCode:{2}".FormatWith(doorNum, timeStamp, barCode));
            }
            else
            {
                LogManager.BorrowRecord("预约还书失败，预约格子编号:{0},过期日期:{1},书BarCode:{2}".FormatWith(doorNum, timeStamp, barCode));
            }
        }

        private void HttpCancelOrderReturn(string doorNUM)
        {
            bool res = accessBLL.UpdateLocalBookbydoornum("", Convert.ToInt32(doorNUM));
            if (res)
            {
                LogManager.BorrowRecord("取消预约还书成功，取消预约格子编号:{0},".FormatWith(doorNUM));
            }
            else
            {
                LogManager.BorrowRecord("取消预约还书失败，取消预约格子编号:{0},".FormatWith(doorNUM));
            }
        }

        private void HttpOrderShelves(string barCode, string[] dataArr)
        {
            try
            {
                FillBooks(barCode);
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【预约上架】预约上架失败，错误信息:{2} --- 预约上架书籍BarCode:{0}，返回信息{1}".FormatWith(barCode, dataArr, ex.Message));
            }
        }

        #region 扩展
        private void BorrBookOpen(string rDataStr, string doorNum)
        {
            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));


            msg += "打开的门行数：" + list[0].DoorID + "、打开的门编号：" + list[0].DoorCode;

            LocalBooks model = new LocalBooks
            {
                DoorID = list[0].DoorID,
                DoorCode = list[0].DoorCode,
                IsLend = ""
            };


            //自动释放资源
            using (SmartLifeBroker consoleModel = new SmartLifeBroker())
            {
                //初始化控制台串口
                consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);

                //打开相应行号的柜门
                consoleModel.OpenDoor(list[0].DoorID, list[0].DoorCode, 2);

                LogManager.BorrowRecord("【借书】成功开门，{0}{1}*****{2},返回的信息：{3}".FormatWith(list[0].DoorID, list[0].DoorCode, DateTime.Now, rDataStr));

            }

        }

        public void FillBooks(string barCode)
        {
            var list = accessBLL.GetNullBookCase();
            var localModel = accessBLL.GetBookByBarCode(barCode);
            if (string.IsNullOrEmpty(barCode))
            {
                LogManager.BorrowRecord("【预约上架】，BarCode为空！");
            }
            else
            {
                long timeStamp = DateTime.Now.Ticks;
                string signStr = GetSign(barCode, list[0].DoorNUM, timeStamp);
                string urlStr = GetUrl(timeStamp, signStr, barCode, list[0].DoorNUM);

                JObject backData = WebHelper.HttpWebRequest(urlStr, "", true).ToJObject();

                if (backData["code"].ToString() == "100")
                {
                    JToken datas = backData["data"];
                    LocalBooks model = new LocalBooks();
                    model.BarCode = barCode;
                    model.BName = datas["title"].ToString();
                    model.Image = datas["coverUrl"].ToString();
                    model.Introduction = datas["abstracts"].ToString();
                    model.Press = datas["publisher"].ToString();
                    model.PublicationDate = datas["pubdate"].ToString();
                    model.Writer = datas["author"].ToString();
                    model.DoorID = list[0].DoorID;
                    model.DoorCode = list[0].DoorCode;

                    accessBLL.FillBookToAcess(model);

                    CodeOptimize.OpenDoor(model.DoorID, model.DoorCode);

                    LogManager.BorrowRecord("【预约上架】，上架书籍成功,书籍名称：《{0}》".FormatWith(model.BName));
                }
                else
                {
                    LogManager.BorrowRecord("【预约上架】，上架书籍失败,书籍名称：《{0}》".FormatWith(backData["data"]["title"].ToString()));
                }
            }
        }

        private string GetSign(string barCode, int doorNum, long timeStamp)
        {
            StringBuilder signStr = new StringBuilder();
            signStr.Append("{0}".FormatWith(barCode));
            signStr.Append("{0}".FormatWith(ConfigManager.LibraryID));
            signStr.Append("{0}".FormatWith(doorNum));
            signStr.Append("{0}".FormatWith(ConfigManager.MachineID));
            signStr.Append("{0}".FormatWith(timeStamp));
            signStr.Append("{0}".FormatWith(ConfigManager.CommonKey));

            return EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr.ToString()).ToLower());
        }

        private string GetUrl(long timeStamp, string sign, string barCode, int doorNum)
        {
            StringBuilder urlStr = new StringBuilder();
            urlStr.Append("{0}/public/book/syn/shelvesservice".FormatWith(ConfigManager.ServicePath));
            urlStr.Append("?bookId={0}".FormatWith(barCode));
            urlStr.Append("&timeStamp={0}".FormatWith(timeStamp));
            urlStr.Append("&machineId={0}".FormatWith(ConfigManager.MachineID));
            urlStr.Append("&machLatNum={0}".FormatWith(doorNum));
            urlStr.Append("&sign={0}".FormatWith(sign));
            urlStr.Append("&libraryId={0}".FormatWith(ConfigManager.LibraryID));
            return urlStr.ToString();
        }
        #endregion




    }
}
