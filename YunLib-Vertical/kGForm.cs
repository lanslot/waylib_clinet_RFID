﻿using ShenZhen.SmartLife.Control;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib_Vertical
{
    public partial class kGForm : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        String machineId = ConfigManager.MachineID;
        String libraryId = ConfigManager.LibraryID;
        String commonKey = ConfigManager.CommonKey;
        String servicePath = ConfigManager.ServicePath;
        public static bool isAdmin = false;
        public string sType { get; set; }
        public kGForm()
        {
            InitializeComponent();
        }


        private void picClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            string strkey = p.Name.Substring(3);
           
            SendKeys.Send("{" + strkey + "}");
        }

        private void picDel_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void picSure_Click(object sender, EventArgs e)
        {
            string code = textBox1.Text;
            if (sType == "1")
            {
                if (string.IsNullOrWhiteSpace(code))
                {
                    textBox1.Text = "请输入正确的开柜码";
                }
                else
                {
                    Func<string> longTask = new Func<string>(delegate ()
                    {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return GetBooksInfo(code);
                        //  返回任务结果：5
                    });
                    //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                    //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                    longTask.BeginInvoke(ar =>
                    {
                        //  使用EndInvoke获取到任务结果（5）
                        string result = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                        {
                            Invoke(new Action(() =>

                           ReturnData(result)

                            ));
                        }
                    }, null);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(code))
                {
                    this.Close();
                }
                else if (code == "020912")
                {
                    isAdmin = true;
                    this.Close();
                }
                else if (code == "753951")
                {
                    process1.StartInfo.FileName = @"C:\WINDOWS\system32\taskmgr.exe";

                    process1.Start();
                }
                else
                {
                    Func<string> longTask = new Func<string>(delegate ()
                    {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return CheckLogin(code);
                        //  返回任务结果：5
                    });
                    //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                    //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                    longTask.BeginInvoke(ar =>
                    {
                        //  使用EndInvoke获取到任务结果（5）
                        string result = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                        {
                            Invoke(new Action(() =>

                           EndCheck(result)

                            ));
                        }
                    }, null);
                }
               
            }
        }

        #region HTTP请求
        private string GetBooksInfo(string borrCode)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}".FormatWith(borrCode, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/book/openCase_2?code={1}&timeStamp={2}&sign={3}".FormatWith(
                    servicePath, borrCode, timeStamp, sign);

                var callback = WebHelper.HttpWebRequest(url, "", true);

                return callback;
            }catch(Exception ex)
            {
                LogManager.WriteLogs("开柜码请求错误", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void ReturnData(string result)
        {
            lblMsg.ForeColor = Color.FromArgb(255, 0, 255);
            if (string.IsNullOrWhiteSpace(result))
            {
                lblMsg.Text = "开柜失败";
            }
            else
            {
                var data = result.ToJObject();
                if(data["code"].ToString() == "100")
                {
                    string doorNumStr = data["data"]["lendLat"].ToString();
                    string doorNum = doorNumStr.Replace(machineId, "");

                    //  LocalBooks currentModel = accessBLL.GetBookByBarCode(doorNum);
                    var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                    if (list.Count > 0)
                    {
                        using (SmartLifeBroker consoleModel = new SmartLifeBroker())
                        {
                            consoleModel.Init(ConfigManager.Control, ConfigManager.CmdPort);
                            consoleModel.OpenDoor(list[0].DoorID, list[0].DoorCode, 2);
                        }
                    }
                    lblMsg.Text = "开柜成功";
                    lblMsg.ForeColor = Color.FromArgb(255, 0, 255);
                    this.Close();
                }
                else
                {
                    lblMsg.Text = "请输入正确的开柜码";
                }
            }
        }


        public string CheckLogin(string pwd)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}".FormatWith(machineId, pwd, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}public/pubmachine/validate/managerLogin?machId={1}&password={2}&timeStamp={3}&sign={4}".FormatWith(
                    servicePath, machineId, pwd, timeStamp, sign);

                var callback = WebHelper.HttpWebRequest(url, "", true);
                return callback;

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("请求登陆后台出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        public void EndCheck(string result)
        {
            if (!string.IsNullOrWhiteSpace(result))
            {
                
                var data = result.ToJObject();
                if (data["code"].ToString() == "100")
                {
                    isAdmin = true;
                }
                
            }
            this.Close();
        }
        #endregion

        #region
        private void picBackspace_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_101;
        }

        private void picBackspace_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_101;
        }

        private void pic1_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_114;
        }

        private void pic1_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_114;
        }

        private void pic2_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_116;
        }

        private void pic2_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_116;
        }

        private void pic3_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_117;
        }

        private void pic3_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_117;
        }

        private void pic4_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_121;
        }

        private void pic4_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_121;
        }

        private void pic5_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_122;
        }

        private void pic5_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_122;
        }

        private void pic6_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_123;
        }

        private void pic6_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_123;
        }

        private void pic7_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_127;
        }

        private void pic7_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_127;
        }

        private void pic8_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_128;
        }

        private void pic8_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_128;
        }

        private void pic9_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_129;
        }

        private void pic9_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_129;
        }

        private void picDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_133;
        }
        private void picDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_133;
        }
        private void pic0_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_134;
        }

        private void pic0_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_134;
        }



        private void picSure_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_135;
        }

        private void picSure_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_135;
        }

        #endregion


        private void kGForm_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
        }
        int time = 120;
        private void timer1_Tick(object sender, EventArgs e)
        {
            time--;
            if(time <= 0)
            {
                this.Close();
            }
        }

        private void kGForm_FontChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Enabled = false;
        }
    }
}
