﻿using MT3;
using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ShenZhen.SmartLife.Control;
using Yunlib.Extensions;
using YunLib.Common;
using Yunlib.Common;
using ThoughtWorks.QRCode.Codec;
using System.Drawing;
using Newtonsoft.Json.Linq;
using YunLib.BLL;
using Yunlib.Entity;
using System.Configuration;
using Yunlib.AdminManage;
using System.Data;
using System.Collections.Generic;
using Yunlib.Properties;

namespace Yunlib
{
    public partial class Main : Form
    {
        private string IsStop = ConfigurationManager.AppSettings["IsStop"];
        public Main()
        {
            InitializeComponent();
           
        }

        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        String machineId = ConfigManager.MachineID;
        String libraryId = ConfigManager.LibraryID;
        String commonKey = ConfigManager.CommonKey;
        String servicePath = ConfigManager.ServicePath;

        //public static int IsInitOpen = 0;
        

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;
                //Main frm default loading is Max screen
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm:ss");

                if(ConfigManager.CanRegister == 1)
                {
                    picRegister.Visible = true;
                }
                if (IsStop == "1")
                {
                    label4.Text = "系统维护中，请稍后使用";
                }
                //  LoadCreateQrCode();

                //solve shadow problem
                // CodeOptimize.SolveLightScreen(this);

                //picOpenCode.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/kaiguima1.png");
                //pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/借666.png");
                //pBtnBorrowOnceOrReturn.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/还666.png");

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseReason s = e.CloseReason;

          
           
           
        }

        #region 窗体扩展事件
        //private void LoadCreateQrCode()
        //{
        //    Image image = CreateImage(string.Format("{0}/wx-manager-putbook.html?libraryId={1}&machineId={2}", ConfigManager.ServicePath, ConfigManager.LibraryID, ConfigManager.MachineID));
        //    if (image != null)
        //    {
        //        picEncode.Image = image;
        //    }
        //}

        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }

      

      

       
        #endregion

        #region 控件事件函数
       

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            //this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm:ss");
            string[] s = new string[1];
            s[0] = ConfigManager.PingServicePath;
            bool flag = NetTest.CheckServeStatus(s);
            if (flag)
            {
                if (!string.IsNullOrWhiteSpace(lblNetTest.Text))
                {
                    lblNetTest.Text = "";
                }
                if (!pBtnBorrow.Enabled)
                {
                    pBtnBorrow.Enabled = true;
                    pBtnBorrowOnceOrReturn.Enabled = true;
                    picRegister.Enabled = true;
                }

            }
            else
            {
                if (string.IsNullOrWhiteSpace(lblNetTest.Text))
                {
                    lblNetTest.Text = "网络繁忙，请稍后操作......";
                    
                }
                if (pBtnBorrow.Enabled)
                {
                    pBtnBorrow.Enabled = false;
                    pBtnBorrowOnceOrReturn.Enabled = false;
                    picRegister.Enabled = false;
                }
            }
            
        }

        //续借或者还书
        private void pBtnBorrowOnceOrReturn_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsStop == "0")
                {
                    pBtnBorrowOnceOrReturn.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/还666.png");

                    //跳转页面前先关闭主页的计时器
                    timerLogin.Enabled = false;
                    CreateQRCode.Enabled = false;

                    //先断开当前窗体的连接


                    ReturnOrBr returnOrBrFrm = new ReturnOrBr { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Hide();

                    returnOrBrFrm.Show();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        //借书
        private void pBtnBorrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsStop == "0")
                {
                    // pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/借666.png");
                    //  pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/lvao.png");

                    //跳转页面前先关闭主页的计时器
                    timerLogin.Enabled = false;
                    CreateQRCode.Enabled = false;

                    //先断开当前窗体的连接


                    // BorrowBook borrowBookFrm = Singleton<BorrowBook>.CreateInstance();
                    BorrowBook borrowBookFrm = new BorrowBook { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Hide();

                    borrowBookFrm.Show();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void CreateQRCode_Tick(object sender, EventArgs e)
        {
            ////每10min生成一次二维码
            //Action atc = new Action(LoadCreateQrCode);

            //IAsyncResult res = atc.BeginInvoke(null, null);

            //atc.EndInvoke(res);
        }
        #endregion

        private void picBtnSure_Click(object sender, EventArgs e)
        {
            try
            {
                string borrCode = openCode;

                if (string.IsNullOrWhiteSpace(borrCode) || borrCode == "请输入8位数开柜码")
                {
                    AlertMsg alert = new AlertMsg("开柜码不能为空！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                    alert.ShowDialog();
                }
                else
                {
                    JObject callback = GetBooksInfo(borrCode);

                    if (callback["code"].ToString() != "100")
                    {
                        AlertMsg alert = new AlertMsg("请输入正确的开柜码！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                        alert.ShowDialog();
                    }
                    else
                    {
                        string doorNumStr = callback["data"]["lendLat"].ToString();
                        string doorNum = doorNumStr.Replace(machineId, "");

                      //  LocalBooks currentModel = accessBLL.GetBookByBarCode(doorNum);
                        var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                        if (list.Count>0)
                        {

                            CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                            LogManager.ErrorRecord("开柜码借书成功！书籍barCode：{0},书籍名称：{1}".FormatWith(list[0].DoorID, list[0].DoorCode));
                        }
                        else
                        {
                            AlertMsg alert = new AlertMsg("请勿重复输入！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                            alert.ShowDialog();

                            Thread.Sleep(2000);

                            alert.label1_Click(null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("开柜码错误信息：{0}".FormatWith(ex.Message));
                MessageBox.Show("网络繁忙，请稍后再试！", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private JObject GetBooksInfo(string borrCode)
        {
            var timeStamp = DateTime.Now.Ticks.ToString();

            var signStr = "{0}{1}{2}".FormatWith(borrCode, timeStamp, commonKey);
            var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

            var url = "{0}/public/book/openCase_2?code={1}&timeStamp={2}&sign={3}".FormatWith(
                servicePath, borrCode, timeStamp, sign);

            var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();

            return callback;
        }

        private void picEncode_Click(object sender, EventArgs e)
        {
            CodeOptimize.KillProcess("YUNLIB");
        }

        private void tmLastOp_Tick(object sender, EventArgs e)
        {
            //lblLastOp.Text = string.Format("用户已经{0}秒没有路过了", LastOp.GetLastInputTime() / 1000);
            var time = DateTime.Now;
            this.lblDataTimeNow.Text = time.ToString("yyyy/MM/dd  HH:mm:ss");

            


        }

        

        private void pBtnBorrow_MouseDown(object sender, MouseEventArgs e)
        {
            pBtnBorrow.Image = Resources.lvao;
        }

        private void pBtnBorrowOnceOrReturn_MouseDown(object sender, MouseEventArgs e)
        {
            pBtnBorrowOnceOrReturn.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/lanao.png");
        }

        private void picOpenCode_MouseDown(object sender, MouseEventArgs e)
        {
            picOpenCode.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/kaiguima2.png");
        }

        string openCode = string.Empty;
        private void picOpenCode_Click(object sender, EventArgs e)
        {
            if (IsStop == "0")
            {
                picOpenCode.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/kaiguima1.png");

                DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

                frm.ShowDialog();

                if (frm.DialogResult == DialogResult.Cancel)
                {
                    //关闭虚拟键盘时,把值传到登录页面的文本框中
                    openCode = DymicNewJP.inputCount;

                    if (!string.IsNullOrEmpty(openCode))
                    {
                        if (openCode.Length == 6)
                        {
                            if (openCode == "020912")
                            {
                                AdminManage.BackMain bm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

                                timerLogin.Enabled = false;
                                CreateQRCode.Enabled = false;

                                this.Hide();

                                bm.Show();
                            }
                            else
                            {
                                bool flag = CheckLogin(openCode);

                                if (!flag)
                                {
                                    AlertMsg alert = new AlertMsg("请输入正确的开柜码！") { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
                                    alert.ShowDialog();
                                }
                                else
                                {
                                    AdminManage.BackMain bm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

                                    timerLogin.Enabled = false;
                                    CreateQRCode.Enabled = false;

                                    this.Hide();

                                    bm.Show();
                                }
                            }
                        }
                        else
                        {
                            picBtnSure_Click(null, null);
                        }
                    }
                }
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        public bool CheckLogin(string pwd)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}".FormatWith(machineId, pwd, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}public/pubmachine/validate/managerLogin?machId={1}&password={2}&timeStamp={3}&sign={4}".FormatWith(
                    servicePath, machineId, pwd, timeStamp, sign);

                var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                if (callback["code"].ToString() == "100")
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsStop == "0")
                {
                    // pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/借666.png");
                    //  pBtnBorrow.BackgroundImage = CodeOptimize.InitPicture(Application.StartupPath + "/Image/lvao.png");

                    //跳转页面前先关闭主页的计时器
                    timerLogin.Enabled = false;
                    CreateQRCode.Enabled = false;

                    RegisterForm frm = new RegisterForm { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Close();

                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsStop == "0")
                {
                    //跳转页面前先关闭主页的计时器
                    timerLogin.Enabled = false;

                    //先断开当前窗体的连接


                    FaceRegister frm = new FaceRegister { MdiParent = ParentForm, Dock = DockStyle.Fill };

                    this.Hide();

                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
