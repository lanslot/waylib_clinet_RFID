﻿using Yunlib.Common;
using Yunlib.DAL;

namespace YunLib.DAL
{
    public class UserLoanRecordForAcess
    {
        /// <summary>
        /// 插入借书记录
        /// </summary>
        /// <returns></returns>
        public bool AddRecord(string userId, string bookId)
        {
            string sqlText = "INSERT INTO LoanRecord (UserID,BookID) VALUES ('{0}','{1}')".FormatWith(userId, bookId);

            bool flag = AcessHelper.ExecuteNonQuery(sqlText);

            return flag;
        }

        /// <summary>
        /// 获取用户当天借阅次数
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetUserRecordCount(string userId)
        {
            string sqlText = "SELECT * FROM LoanRecord WHERE UserID ='{0}'".FormatWith(userId);

            var ds = AcessHelper.ExecuteDataSet(sqlText);
            var dt = ds.Tables[0];
            return dt.Rows.Count;
        }

        public bool DeleteAll()
        {
            string sqlText = "DELETE FROM LoanRecord";

            bool flag = AcessHelper.ExecuteNonQuery(sqlText);
            return flag;
        }

    }
}
