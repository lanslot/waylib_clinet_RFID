﻿//#######################
//#                     #
//#       流通库        #
//#                     #
//#######################
//Author:纪钟磊
//CreateTime:2017/6/23
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Entity
{
    public class ChangeKuInfo
    {
        /// <summary>
        /// 条形码
        /// </summary>
        public string BarCode { get; set; }

        /// <summary>
        /// 登录号
        /// </summary>
        public string LoginAccount { get; set; }

        /// <summary>
        /// 读者条码
        /// </summary>
        public string ReaderCode { get; set; }

        /// <summary>
        /// 外借时间
        /// </summary>
        public DateTime BorrowTime { get; set; }

        /// <summary>
        /// 应归还时间
        /// </summary>
        public DateTime ShouldReturnTime { get; set; }

        /// <summary>
        /// 主键码
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 虚拟库室
        /// </summary>
        public int DummyKu { get; set; }

        /// <summary>
        /// 续借次数
        /// </summary>
        public int OnceBorrowCount { get; set; }

    }
}
