﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using Yunlib.Common;
using Yunlib.SellBook.Extensions;
using YunLib.BLL;
using YunLib.Common;

namespace Yunlib.SellBook
{
    public partial class SellBook : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        public static SellBook sb;
        public delegate void GetDescHandel(string machLatNum);
        GetDescHandel md = null;
        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;
        private string order = "";
        private bool shouldStop = false;

        public int DoorNum { get; set; }
        
        public SellBook()
        {
            InitializeComponent();
            this.md = new GetDescHandel(GetBookDesc);

        }

       

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        private void SellBook_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            sb = this;
            
            Invoke(md, DoorNum + "");

            Thread st = new Thread(GetMessage);
            st.IsBackground = true;
            st.Start();
        }

        private void LoadCreateQrCode(string url)
        {
           
             //string url = string.Format("{3}wxpay/redirectUrl?url={3}wx-bookInfo.html?bookId={0}&machineId={1}&machLatNum={2}&libraryId={4}", BarCode, ConfigManager.MachineID, DoorNum, ConfigManager.ServicePath, ConfigManager.LibraryID);
           
            Image image = CreateImage(url);
            if (image != null)
            {
                picEncode.Image = image;
            }
        }

        // 生成二维码
        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }

        private void GetBookDesc(string machLatNum)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}{4}".FormatWith(libraryId, machineId, machLatNum, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/query/desc?machId={1}&machLatNum={2}&libraryId={3}&timeStamp={4}&sign={5}".FormatWith(
                    servicePath, machineId, machLatNum, libraryId, timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                if (res["code"].ToString() == "100")
                {
                    var data = res["data"];
                    lblAuthor.Text = data["author"].ToString();
                    lblTitle.Text = data["title"].ToString();
                    lblPublishDate.Text = data["pubdate"].ToString();
                    lblPublisher.Text = data["publisher"].ToString();
                    lblAbstracts.Text = data["abstracts"].ToString();
                    lblDoorNum.Text = DoorNum + "";
                    picBook.ImageLocation = data["coverUrl"].ToString();
                    lblPrice.Text = "￥" + data["realPrice"].ToString();
                    order = data["outTradeNo"].ToString();
                    string payUrl = data["payUrl"].ToString();
                    if (string.IsNullOrWhiteSpace(payUrl))
                    {
                        MessageBox.Show("订单发送失败，请稍后再试");
                        LogManager.WriteLogs("售书订单请求失败", "售书的二维码URL为空", LogFile.Error);

                    }
                    else
                    {

                        LoadCreateQrCode(payUrl);
                    }
                }
                else
                {
                    LogManager.BorrowRecord("{0}--BookDesc请求错误返回：{1}".FormatWith(DateTime.Now, res.ToString()));
                }
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("{0}--BookDesc请求报错：{1}".FormatWith(DateTime.Now, ex.Message));
            }
        }
        
        private void GetMessage()
        {
            while (!shouldStop)
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}".FormatWith(machineId, order, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/sell/wechat/order/isSuccess?machId={1}&outTradeNo={2}&timeStamp={3}&sign={4}".FormatWith(
                    servicePath, machineId, order, timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();

                if (res["code"].ToString() == "100")
                {
                    shouldStop = true;
                    var list = accessBLL.GetBookByDoorNum(DoorNum);
                    CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                    this.BeginInvoke((Action)(() =>
                    {
                        //perform on the UI thread
                        SellBookSuccess frm = new SellBookSuccess(DoorNum.ToString()) { MdiParent = ParentForm, Dock = DockStyle.Fill };
                        timer1.Stop();
                        this.Close();

                        frm.Show();
                    }));
                }
                Thread.Sleep(1000);
            }
        }

      

        private void button1_Click_1(object sender, EventArgs e)
        {
            BorrowBook frm = new BorrowBook();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;
            timer1.Enabled = false;
            timer1.Stop();
            this.Hide();

            frm.Show();
        }

        int i = 90;
        private void timer1_Tick_1(object sender, EventArgs e)
        {
            lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");
            lblDaoJiShi.Text = i.ToString();
            i--;
            if (i <= 0)
            {
                Main frm = new Main();

                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();
                timer1.Enabled = false;
                timer1.Stop();
                frm.Show();
            }
            else
            {
                
            }
           

        }



      

        private void lblTotalPrice_Paint_1(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Red, 2);

            g.DrawLine(p, new Point(e.ClipRectangle.X, e.ClipRectangle.Top+e.ClipRectangle.Height/2), new Point(e.ClipRectangle.Right, e.ClipRectangle.Top + e.ClipRectangle.Height / 2));
        }

        private void SellBook_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void SellBook_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                LogManager.ErrorRecord("进入了SellBook的FormClosed事件");

                //SellBookSuccess frm = new SellBookSuccess(DoorNum.ToString()) { MdiParent = ParentForm, Dock = DockStyle.Fill };

                //frm.Show();
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("进入了SellBook的FormClosed事件的catch中，{0}".FormatWith(ex.Message));
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
