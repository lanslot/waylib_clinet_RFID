﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Entity;
using YunLib.BLL;

namespace Yunlib.AdminManage
{
    public partial class LocalBookManager : Form
    {
        private BorrowForAcessBLL acessBll = new BorrowForAcessBLL();

        private int PageIndex { get; set; }

        private int TotalCount { get; set; }

        private int PageCount
        {
            get {
                if (TotalCount % PageSize == 0)
                {
                    return TotalCount / PageSize;
                }
                else
                {
                    return TotalCount / PageSize + 1;
                }
            }
        }
        private int PageSize
        {
            get { return 80; }
        }

        public LocalBookManager()
        {
            InitializeComponent();
        }

        private void LocalBookManager_Load(object sender, EventArgs e)
        {
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            dataGridView1.RowTemplate.Height = dataGridView1.Height/(11);
            
            PageIndex = 1;
            label2.Text = "/";
           
            BindDateToGridView(PageIndex, PageSize);
        }


        public void BindDateToGridView(int pageIndex, int pageSize) {
            var data = acessBll.GetBookToDataTableByPaging(pageIndex, pageSize);
            TotalCount = acessBll.GetTotalCount();
            dataGridView1.DataSource = data;

            lblThisPage.Text = pageIndex + "";
            lblTotalPage.Text = PageCount + "";

        }

        private void picBtnNextPage_Click(object sender, EventArgs e)
        {
            if (PageIndex < PageCount)
            {
                PageIndex += 1;
                BindDateToGridView(PageIndex, PageSize);
            }
        }

        private void picBtnLastPage_Click(object sender, EventArgs e)
        {
            if (PageIndex > 1)
            {
                PageIndex -= 1;
                BindDateToGridView(PageIndex, PageSize);
            }
        }
        private void SetAllRowChecked()
        {
            // DataGridCell cel=(sender as DataGridCell).
            int count = Convert.ToInt16(this.dataGridView1.Rows.Count.ToString());
            for (int i = 0; i < count; i++)
            {
                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)dataGridView1.Rows[i].Cells["cb_check"];
                Boolean flag = Convert.ToBoolean(checkCell.Value);
                if (flag == false) //查找被选择的数据行
                {
                    checkCell.Value = true;
                }
                continue;
            }
        }

        /// <summary>
        /// 表格单元点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex != -1)
            {
                //获取控件的值

                DataGridViewCheckBoxCell checkCell = (DataGridViewCheckBoxCell)this.dataGridView1.Rows[e.RowIndex].Cells["IsChecked"];
                Boolean flag = Convert.ToBoolean(checkCell.Value);
                if (flag == true)
                {
                    checkCell.Value = false;
                }
                else
                {
                    checkCell.Value = true;
                }
            }
        }

        /// <summary>
        /// 删除本地书籍
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBtnDelete_Click(object sender, EventArgs e)
        {
            var rows = this.dataGridView1.Rows;

            foreach (DataGridViewRow item in rows) {
                var value = item.Cells["IsChecked"].Value;
                if (Convert.ToBoolean(value)) {
                    string ID = item.Cells["ID"].Value.ToString();

                    LocalBooks model = new LocalBooks() {
                        ID = int.Parse(ID)
                    };
                    var flag = acessBll.DeleteBookByID(model);

                    if (flag)
                    {
                        dataGridView1.Rows.Remove(item);
                        //PageIndex = 1;
                        //BindDateToGridView(PageIndex, PageSize);
                    }
                }
            }
        }

        private void picBtnReturn_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Hide();

            frm.Show();
        }
    }
}
