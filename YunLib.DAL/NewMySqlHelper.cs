﻿using System;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;

namespace YunLib.DAL
{
    public class NewMySqlHelper
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["MySqlDB"].ConnectionString.ToString();

        public static int ExecuteNonQuery(string sqlText)
        {
            return MySqlHelper.ExecuteNonQuery(ConnectionString,sqlText,null);
            
        }
        public static MySqlDataReader ExecuteReader(string sqlText)
        {
            return MySqlHelper.ExecuteReader(ConnectionString, sqlText);
        }

        public static DataSet ExecuteDataSet(string sqlText)
        {
            return MySqlHelper.ExecuteDataset(ConnectionString,sqlText);
        }

        public static DataTable ExecuteDataTable(String sqlText)
        {
            return ExecuteDataSet(sqlText).Tables[0];
        }

        public static int getSingleInt(String sqlText)
        {
            return ExecuteReader(sqlText).GetInt32(0);
        }
    }
}
