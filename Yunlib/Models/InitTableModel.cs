﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Models
{
    public class InitTableModel
    {
        public int machLatNum { get; set; }

        public int DoorID { get; set; }

        public int DoorCode { get; set; }
    }
}
