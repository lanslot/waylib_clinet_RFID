﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Yunlib.AdminManage;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Process[] proc = Process.GetProcessesByName("Yunlib");
            if (proc.Length > 1)
            {
                for(int i = 0; i < proc.Length-1; i++)
                {
                    proc[i].Kill();
                }
            }

            Application.Run(new ParentForm());
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogManager.ErrorRecord("捕获到全局报错信息,报错对象：{0},报错类型名:{1}".FormatWith(e.ExceptionObject.ToString(), e.ExceptionObject.GetType().Name));
           
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            LogManager.ErrorRecord("捕获到全局报错信息,报错信息：{0}".FormatWith(e.Exception.Message));
            LogManager.WriteLogs(e.Exception.Message, e.Exception.StackTrace, LogFile.Error);
        }
    }
}
