﻿namespace Yunlib
{
    partial class BooksOutTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BooksOutTime));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pic_btnTurnBack = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblGun = new System.Windows.Forms.Label();
            this.lblMiao = new System.Windows.Forms.Label();
            this.lblDaoJiShi = new System.Windows.Forms.Label();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.lblOverTimeInfo = new System.Windows.Forms.Label();
            this.tmTime = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(195, 130);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(618, 471);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pic_btnTurnBack
            // 
            this.pic_btnTurnBack.BackColor = System.Drawing.Color.Transparent;
            this.pic_btnTurnBack.Image = global::Yunlib.Properties.Resources.btnX;
            this.pic_btnTurnBack.Location = new System.Drawing.Point(471, 627);
            this.pic_btnTurnBack.Name = "pic_btnTurnBack";
            this.pic_btnTurnBack.Size = new System.Drawing.Size(62, 63);
            this.pic_btnTurnBack.TabIndex = 11;
            this.pic_btnTurnBack.TabStop = false;
            this.pic_btnTurnBack.Click += new System.EventHandler(this.pic_btnTurnBack_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Location = new System.Drawing.Point(599, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(26, 31);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblGun);
            this.panel1.Controls.Add(this.lblMiao);
            this.panel1.Controls.Add(this.lblDaoJiShi);
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(631, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 57);
            this.panel1.TabIndex = 14;
            // 
            // lblGun
            // 
            this.lblGun.BackColor = System.Drawing.Color.Transparent;
            this.lblGun.Font = new System.Drawing.Font("微软雅黑", 17F);
            this.lblGun.ForeColor = System.Drawing.Color.White;
            this.lblGun.Location = new System.Drawing.Point(221, 13);
            this.lblGun.Name = "lblGun";
            this.lblGun.Size = new System.Drawing.Size(27, 31);
            this.lblGun.TabIndex = 4;
            this.lblGun.Text = "丨";
            // 
            // lblMiao
            // 
            this.lblMiao.BackColor = System.Drawing.Color.Transparent;
            this.lblMiao.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblMiao.ForeColor = System.Drawing.Color.Orange;
            this.lblMiao.Location = new System.Drawing.Point(297, 11);
            this.lblMiao.Name = "lblMiao";
            this.lblMiao.Size = new System.Drawing.Size(36, 31);
            this.lblMiao.TabIndex = 3;
            this.lblMiao.Text = "秒";
            // 
            // lblDaoJiShi
            // 
            this.lblDaoJiShi.BackColor = System.Drawing.Color.Transparent;
            this.lblDaoJiShi.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDaoJiShi.ForeColor = System.Drawing.Color.Orange;
            this.lblDaoJiShi.Location = new System.Drawing.Point(227, 12);
            this.lblDaoJiShi.Name = "lblDaoJiShi";
            this.lblDaoJiShi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDaoJiShi.Size = new System.Drawing.Size(70, 31);
            this.lblDaoJiShi.TabIndex = 3;
            this.lblDaoJiShi.Text = "60";
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // lblOverTimeInfo
            // 
            this.lblOverTimeInfo.BackColor = System.Drawing.Color.White;
            this.lblOverTimeInfo.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblOverTimeInfo.ForeColor = System.Drawing.Color.Red;
            this.lblOverTimeInfo.Location = new System.Drawing.Point(260, 392);
            this.lblOverTimeInfo.Name = "lblOverTimeInfo";
            this.lblOverTimeInfo.Size = new System.Drawing.Size(468, 23);
            this.lblOverTimeInfo.TabIndex = 34;
            this.lblOverTimeInfo.Text = "您的书已逾期，请到图书馆归还";
            this.lblOverTimeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmTime
            // 
            this.tmTime.Enabled = true;
            this.tmTime.Interval = 1000;
            this.tmTime.Tick += new System.EventHandler(this.tmTime_Tick);
            // 
            // BooksOutTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.Properties.Resources.BackGround_new1;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblOverTimeInfo);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pic_btnTurnBack);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BooksOutTime";
            this.Text = "BooksOutTime";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.BooksOutTime_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_btnTurnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pic_btnTurnBack;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblGun;
        private System.Windows.Forms.Label lblMiao;
        private System.Windows.Forms.Label lblDaoJiShi;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Label lblOverTimeInfo;
        private System.Windows.Forms.Timer tmTime;
    }
}