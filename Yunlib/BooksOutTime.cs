﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using ThoughtWorks.QRCode.Codec;
using YunLib.Common;
using Yunlib.Common;
using Yunlib.Extensions;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Yunlib
{
    public partial class BooksOutTime : Form
    {
        public BooksOutTime()
        {
            InitializeComponent();
        }

        public string BarCode { get; set; }

        private void BooksOutTime_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

                LoadCreateQrCode();
                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("BookOutTimes_Load 运行时间{0}".FormatWith(ts.TotalSeconds));

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }

        }

        private void tmTime_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

            int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;

            this.lblDaoJiShi.Text = second.ToString();

            if (second <= 0)
            {
                tmTime.Enabled = false;

                Main frm = new Main();

                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Hide();

                frm.Show();
            }
        }

        private void pic_btnTurnBack_Click(object sender, EventArgs e)
        {
            tmTime.Enabled = false;

            // Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Hide();

            frm.Show();
        }

        #region 窗体扩展事件
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        public static Image CombinImage(Image imgBack, Image img)
        {
            if (img.Height != 50 || img.Width != 50)
            {
                img = ResizeImage(img, 50, 50, 0);
            }
            Graphics g = Graphics.FromImage(imgBack);

            g.DrawImage(imgBack, 0, 0, imgBack.Width, imgBack.Height); //g.DrawImage(imgBack, 0, 0, 相框宽, 相框高);   

            g.DrawImage(img, imgBack.Width / 2 - img.Width / 2, imgBack.Width / 2 - img.Width / 2, img.Width, img.Height);
            GC.Collect();
            return imgBack;
        }

        public static Image ResizeImage(Image bmp, int newW, int newH, int mode)
        {
            try
            {
                Image b = new Bitmap(newW, newH);
                Graphics g = Graphics.FromImage(b);

                // 插值算法的质量  
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(bmp,
                            new Rectangle(0, 0, newW, newH),
                            new Rectangle(0, 0, bmp.Width, bmp.Height),
                            GraphicsUnit.Pixel);
                g.Dispose();
                return b;
            }
            catch
            {
                return null;
            }
        }
        
        private void LoadCreateQrCode()
        {
            long timeStamp = DateTime.Now.Ticks;
            string signStr = GetSign(timeStamp);
            string urlStr = GetUrl(timeStamp, signStr);
            JObject backData = WebHelper.HttpWebRequest(urlStr, "", true).ToJObject();
            if (backData["code"].ToString() == "100")
            {
                JToken data = backData["data"];
                string wxStr = data["code_url"].ToString();
                Image image = CreateImage(wxStr);
                
            }
        }

        private string GetUrl(long timeStamp, string sign)
        {
            StringBuilder urlStr = new StringBuilder();
            urlStr.Append("{0}public/users/pay".FormatWith(ConfigManager.ServicePath));
            urlStr.Append("?bookId={0}".FormatWith(BarCode));
            urlStr.Append("&libraryId={0}".FormatWith(ConfigManager.LibraryID));
            urlStr.Append("&machineId={0}".FormatWith(ConfigManager.MachineID));
            urlStr.Append("&timeStamp={0}".FormatWith(timeStamp));
            urlStr.Append("&sign={0}".FormatWith(sign));
            return urlStr.ToString();
        }

        private string GetSign(long timeStamp)
        {
            StringBuilder signStr = new StringBuilder();
            signStr.Append("{0}".FormatWith(BarCode));
            signStr.Append("{0}".FormatWith(ConfigManager.LibraryID));
            signStr.Append("{0}".FormatWith(ConfigManager.MachineID));
            signStr.Append("{0}".FormatWith(timeStamp));
            signStr.Append("{0}".FormatWith(ConfigManager.CommonKey));
            return EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr.ToString()).ToLower());
        }
        
        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }
        #endregion
    }
}
