﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib.SellBook
{
    public partial class Form2 : Form
    {
        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATE = 0x0003;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_NOACTIVATE;
                return cp;
            }

        }

        protected override void WndProc(ref Message m)
        {
            //If we're being activated because the mouse clicked on us...
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                //Then refuse to be activated, but allow the click event to pass through (don't use MA_NOACTIVATEEAT)
                m.Result = (IntPtr)MA_NOACTIVATE;
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            lbl_0.Click += new EventHandler(lbl_Click);
            lbl_1.Click += new EventHandler(lbl_Click);
            lbl_2.Click += new EventHandler(lbl_Click);
            lbl_3.Click += new EventHandler(lbl_Click);
            lbl_4.Click += new EventHandler(lbl_Click);
            lbl_5.Click += new EventHandler(lbl_Click);
            lbl_6.Click += new EventHandler(lbl_Click);
            lbl_7.Click += new EventHandler(lbl_Click);
            lbl_8.Click += new EventHandler(lbl_Click);
            lbl_9.Click += new EventHandler(lbl_Click);
            lbl_Q.Click += new EventHandler(lbl_Click);
            lbl_W.Click += new EventHandler(lbl_Click);
            lbl_R.Click += new EventHandler(lbl_Click);
            lbl_E.Click += new EventHandler(lbl_Click);
            lbl_T.Click += new EventHandler(lbl_Click);
            lbl_Y.Click += new EventHandler(lbl_Click);
            lbl_U.Click += new EventHandler(lbl_Click);
            lbl_I.Click += new EventHandler(lbl_Click);
            lbl_O.Click += new EventHandler(lbl_Click);
            lbl_P.Click += new EventHandler(lbl_Click);
            lbl_A.Click += new EventHandler(lbl_Click);
            lbl_S.Click += new EventHandler(lbl_Click);
            lbl_D.Click += new EventHandler(lbl_Click);
            lbl_F.Click += new EventHandler(lbl_Click);
            lbl_G.Click += new EventHandler(lbl_Click);
            lbl_H.Click += new EventHandler(lbl_Click);
            lbl_J.Click += new EventHandler(lbl_Click);
            lbl_K.Click += new EventHandler(lbl_Click);
            lbl_L.Click += new EventHandler(lbl_Click);
            lbl_Z.Click += new EventHandler(lbl_Click);
            lbl_X.Click += new EventHandler(lbl_Click);
            lbl_C.Click += new EventHandler(lbl_Click);
            lbl_V.Click += new EventHandler(lbl_Click);
            lbl_B.Click += new EventHandler(lbl_Click);
            lbl_N.Click += new EventHandler(lbl_Click);
            lbl_M.Click += new EventHandler(lbl_Click);
            lbl_BACKSPACE.Click += new EventHandler(lbl_Click);
        }



       private void lbl_Click(object sender, EventArgs e)
        {
            Label l = (Label)sender;
            string strkey = l.Name.Substring(4);
            if (label30.Text == "大写")
            {
                strkey = strkey.ToUpper();
            }
            else
            {
                strkey = strkey.ToLower();
            }
            SendKeys.Send("{" + strkey + "}");
        }

       

       

        private void label39_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label30_Click(object sender, EventArgs e)
        {
            if (label30.Text == "大写")
            {
                label30.Text = "小写";
            }
            else
            {
                label30.Text = "大写";
            }
        }

        private void lbl_1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbl_2_Click(object sender, EventArgs e)
        {

        }
    }
}
