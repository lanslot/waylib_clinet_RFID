﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YunLib.DAL;

namespace YunLib.BLL
{
    public class LibMessageForMySqlBLL
    {
        private LibMessageForMySql dal = new LibMessageForMySql();

        public DataTable Get_FACT_LOAN(DateTime start, DateTime end)
        {
            return dal.Get_FACT_LOAN(start, end);
        }

        public DataTable Get_HOLDING(DateTime start, DateTime end)
        {
            return dal.Get_HOLDING(start, end);
        }

        public DataTable Get_READER(DateTime start, DateTime end)
        {
            return dal.Get_READER(start, end);
        }

        public DataTable Get_BIBLIOS(DateTime start, DateTime end)
        {
            return dal.Get_BIBLIOS(start, end);
        }

        public DataTable Get_P_LIBCODE()
        {
            return dal.Get_P_LIBCODE();
        }

        public DataTable Get_P_LOCAL()
        {
            return dal.Get_P_LOCAL();
        }
    }
}
