﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YunLib_Vertical
{
    public partial class JPForm : Form
    {
        private bool IsLower = true;

        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATE = 0x0003;
        //定义无边框窗体Form
        [DllImport("user32.dll")]//*********************拖动无窗体的控件
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;

        private void gPanelTitleBack_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0);//*********************调用移动无窗体控件函数
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_NOACTIVATE;
                return cp;
            }

        }

        protected override void WndProc(ref Message m)
        {
            //If we're being activated because the mouse clicked on us...
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                //Then refuse to be activated, but allow the click event to pass through (don't use MA_NOACTIVATEEAT)
                m.Result = (IntPtr)MA_NOACTIVATE;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
        public JPForm()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox p = (PictureBox)sender;
            string strkey = p.Name.Substring(3);
            if (IsLower)
            {
                strkey = strkey.ToLower();
            }
            else
            {
                
                strkey = strkey.ToUpper();
            }
            SendKeys.Send("{" + strkey + "}");
            
        }

        private void picCapslock_Click(object sender, EventArgs e)
        {
            if (IsLower)
            {
                IsLower = false;
            }
            else
            {
                IsLower = true;
            }
        }

        #region 按钮样式
        private void pic1_MouseUp(object sender, MouseEventArgs e)
        {
            pic1.Image = Properties.Resources.keyboard_d_01;
        }

        private void pic1_MouseDown(object sender, MouseEventArgs e)
        {
            pic1.Image = Properties.Resources.keyboard_u_01;
        }

        private void pic2_MouseDown(object sender, MouseEventArgs e)
        {
            pic2.Image = Properties.Resources.keyboard_u_02;
        }

        private void pic2_MouseUp(object sender, MouseEventArgs e)
        {
            pic2.Image = Properties.Resources.keyboard_d_02;
        }

        private void pic3_MouseDown(object sender, MouseEventArgs e)
        {
            pic3.Image = Properties.Resources.keyboard_u_03;
        }

        private void pic3_MouseUp(object sender, MouseEventArgs e)
        {
            pic3.Image = Properties.Resources.keyboard_d_03;
        }


        private void pic4_MouseDown(object sender, MouseEventArgs e)
        {
            pic4.Image = Properties.Resources.keyboard_u_04;
        }

        private void pic4_MouseUp(object sender, MouseEventArgs e)
        {
            pic4.Image = Properties.Resources.keyboard_d_04;
        }

        private void pic5_MouseDown(object sender, MouseEventArgs e)
        {
            pic5.Image = Properties.Resources.keyboard_u_05;
        }

        private void pic5_MouseUp(object sender, MouseEventArgs e)
        {
            pic5.Image = Properties.Resources.keyboard_d_05;
        }

        private void pic6_MouseDown(object sender, MouseEventArgs e)
        {
            pic6.Image = Properties.Resources.keyboard_u_06;
        }

        private void pic6_MouseUp(object sender, MouseEventArgs e)
        {
            pic6.Image = Properties.Resources.keyboard_d_06;
        }

        private void pic7_MouseDown(object sender, MouseEventArgs e)
        {
            pic7.Image = Properties.Resources.keyboard_u_07;
        }

        private void pic7_MouseUp(object sender, MouseEventArgs e)
        {
            pic7.Image = Properties.Resources.keyboard_d_07;
        }

        private void pic8_MouseDown(object sender, MouseEventArgs e)
        {
            pic8.Image = Properties.Resources.keyboard_u_08;
        }

        private void pic8_MouseUp(object sender, MouseEventArgs e)
        {
            pic8.Image = Properties.Resources.keyboard_d_08;
        }

        private void pic9_MouseDown(object sender, MouseEventArgs e)
        {
            pic9.Image = Properties.Resources.keyboard_u_09;
        }

        private void pic9_MouseUp(object sender, MouseEventArgs e)
        {
            pic9.Image = Properties.Resources.keyboard_d_09;
        }

        private void pic0_MouseDown(object sender, MouseEventArgs e)
        {
            pic0.Image = Properties.Resources.keyboard_u_10;
        }

        private void pic0_MouseUp(object sender, MouseEventArgs e)
        {
            pic0.Image = Properties.Resources.keyboard_d_10;
        }


        private void picQ_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_37;
        }

        private void picQ_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_37;
        }

        private void picW_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_38;
        }

        private void picW_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_38;
        }

        private void picE_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_39;
        }
        private void picE_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_39;
        }

        private void picR_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_40;
        }

        private void picR_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_40;
        }

        private void picT_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_41;
        }

        private void picT_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_41;
        }

        private void picY_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_42;
        }

        private void picY_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_42;
        }

        private void picU_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_43;
        }

        private void picU_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_43;
        }

        private void picI_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_44;
        }

        private void picI_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_44;
        }

        private void picO_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_45;
        }

        private void picO_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_45;
        }

        private void picP_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_46;
        }

        private void picP_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_46;
        }

        private void picA_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_58;
        }

        private void picA_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_58;
        }

        private void picS_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_60;
        }

        private void picS_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_60;
        }

        private void picD_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_62;
        }

        private void picD_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_62;
        }

        private void picF_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_64;
        }

        private void picF_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_64;
        }

        private void picG_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_66;
        }

        private void picG_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_66;
        }

        private void picH_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_68;
        }

        private void picH_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_68;
        }

        private void picJ_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_70;
        }

        private void picJ_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_70;
        }

        private void picK_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_72;
        }

        private void picK_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_72;
        }

        private void picL_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_74;
        }

        private void picL_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_74;
        }

        private void picCapslock_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_88;
        }

        private void picCapslock_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_88;
        }

        private void picZ_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_89;
        }

        private void picZ_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_89;
        }

        private void picX_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_90;
        }

        private void picX_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_90;
        }

        private void picC_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_91;
        }

        private void picC_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_91;
        }

        private void picV_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_92;
        }

        private void picV_MouseUp(object sender, EventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_92;
        }

        private void pictB_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_93;
        }

        private void pictB_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_93;
        }

        private void picN_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_94;
        }

        private void picN_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_94;
        }

        private void picM_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_95;
        }

        private void picM_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_95;
        }

        private void picDel_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_u_96;
        }

        private void picDel_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox p = sender as PictureBox;
            p.Image = Properties.Resources.keyboard_d_96;
        }


        #endregion

        private void picClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void JPForm_MouseMove(object sender, MouseEventArgs e)
        {

        }

       

       

    }
}
