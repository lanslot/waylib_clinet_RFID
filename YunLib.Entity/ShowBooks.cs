﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Entity
{
    public class ShowBooks
    {
        public string BName { get; set; }

        public int DoorID { get; set; }

        public int DoorCode { get; set; }

        public int ID { get; set; }

        public int DoorNUM { get; set; }

        public string IsLend { get; set; }
    }
}
