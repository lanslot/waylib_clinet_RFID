﻿namespace Yunlib
{
    partial class ReturnOrBrDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReturnOrBrDetail));
            this.lblBooksName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblBorrowDate = new System.Windows.Forms.Label();
            this.lblBorrowDays = new System.Windows.Forms.Label();
            this.lblOverplusDays = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pBtnOnceBorrowBook = new System.Windows.Forms.PictureBox();
            this.pBtnReturnBook = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnOnceBorrowBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnReturnBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBooksName
            // 
            this.lblBooksName.AutoSize = true;
            this.lblBooksName.BackColor = System.Drawing.Color.Transparent;
            this.lblBooksName.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblBooksName.ForeColor = System.Drawing.Color.White;
            this.lblBooksName.Location = new System.Drawing.Point(543, 215);
            this.lblBooksName.Name = "lblBooksName";
            this.lblBooksName.Size = new System.Drawing.Size(0, 30);
            this.lblBooksName.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(463, 322);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 30);
            this.label1.TabIndex = 25;
            this.label1.Text = "借阅时间:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(463, 376);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 30);
            this.label3.TabIndex = 25;
            this.label3.Text = "已借天数:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(463, 429);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 30);
            this.label4.TabIndex = 25;
            this.label4.Text = "剩余天数:";
            // 
            // lblBorrowDate
            // 
            this.lblBorrowDate.AutoSize = true;
            this.lblBorrowDate.BackColor = System.Drawing.Color.Transparent;
            this.lblBorrowDate.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblBorrowDate.ForeColor = System.Drawing.Color.White;
            this.lblBorrowDate.Location = new System.Drawing.Point(575, 322);
            this.lblBorrowDate.Name = "lblBorrowDate";
            this.lblBorrowDate.Size = new System.Drawing.Size(0, 30);
            this.lblBorrowDate.TabIndex = 25;
            // 
            // lblBorrowDays
            // 
            this.lblBorrowDays.AutoSize = true;
            this.lblBorrowDays.BackColor = System.Drawing.Color.Transparent;
            this.lblBorrowDays.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblBorrowDays.ForeColor = System.Drawing.Color.White;
            this.lblBorrowDays.Location = new System.Drawing.Point(575, 376);
            this.lblBorrowDays.Name = "lblBorrowDays";
            this.lblBorrowDays.Size = new System.Drawing.Size(0, 30);
            this.lblBorrowDays.TabIndex = 25;
            // 
            // lblOverplusDays
            // 
            this.lblOverplusDays.AutoSize = true;
            this.lblOverplusDays.BackColor = System.Drawing.Color.Transparent;
            this.lblOverplusDays.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblOverplusDays.ForeColor = System.Drawing.Color.White;
            this.lblOverplusDays.Location = new System.Drawing.Point(575, 429);
            this.lblOverplusDays.Name = "lblOverplusDays";
            this.lblOverplusDays.Size = new System.Drawing.Size(0, 30);
            this.lblOverplusDays.TabIndex = 25;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(677, 426);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(305, 290);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // pBtnOnceBorrowBook
            // 
            this.pBtnOnceBorrowBook.BackColor = System.Drawing.Color.White;
            this.pBtnOnceBorrowBook.Image = ((System.Drawing.Image)(resources.GetObject("pBtnOnceBorrowBook.Image")));
            this.pBtnOnceBorrowBook.Location = new System.Drawing.Point(707, 577);
            this.pBtnOnceBorrowBook.Name = "pBtnOnceBorrowBook";
            this.pBtnOnceBorrowBook.Size = new System.Drawing.Size(107, 99);
            this.pBtnOnceBorrowBook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBtnOnceBorrowBook.TabIndex = 28;
            this.pBtnOnceBorrowBook.TabStop = false;
            this.pBtnOnceBorrowBook.Click += new System.EventHandler(this.pBtnOnceBorrowBook_Click);
            this.pBtnOnceBorrowBook.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnOnceBorrowBook_MouseDown);
            // 
            // pBtnReturnBook
            // 
            this.pBtnReturnBook.BackColor = System.Drawing.Color.White;
            this.pBtnReturnBook.Image = ((System.Drawing.Image)(resources.GetObject("pBtnReturnBook.Image")));
            this.pBtnReturnBook.Location = new System.Drawing.Point(845, 453);
            this.pBtnReturnBook.Name = "pBtnReturnBook";
            this.pBtnReturnBook.Size = new System.Drawing.Size(107, 99);
            this.pBtnReturnBook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBtnReturnBook.TabIndex = 28;
            this.pBtnReturnBook.TabStop = false;
            this.pBtnReturnBook.Click += new System.EventHandler(this.pBtnReturnBook_Click);
            this.pBtnReturnBook.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pBtnReturnBook_MouseDown);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(888, 635);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 81);
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(463, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 30);
            this.label5.TabIndex = 25;
            this.label5.Text = "书名:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(463, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 30);
            this.label2.TabIndex = 31;
            this.label2.Text = "作者:";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.BackColor = System.Drawing.Color.Transparent;
            this.lblAuthor.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblAuthor.ForeColor = System.Drawing.Color.White;
            this.lblAuthor.Location = new System.Drawing.Point(543, 268);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(0, 30);
            this.lblAuthor.TabIndex = 25;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.Location = new System.Drawing.Point(73, 121);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(195, 200);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 35;
            this.pictureBox5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Location = new System.Drawing.Point(69, 215);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(345, 439);
            this.panel3.TabIndex = 54;
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(687, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 57);
            this.panel1.TabIndex = 8;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Location = new System.Drawing.Point(657, 40);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(26, 31);
            this.pictureBox4.TabIndex = 30;
            this.pictureBox4.TabStop = false;
            // 
            // ReturnOrBrDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pBtnReturnBook);
            this.Controls.Add(this.pBtnOnceBorrowBook);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblOverplusDays);
            this.Controls.Add(this.lblBorrowDays);
            this.Controls.Add(this.lblBorrowDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.lblBooksName);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "ReturnOrBrDetail";
            this.Text = "ReturnOrBrDetail";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ReturnOrBrDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnOnceBorrowBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnReturnBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblBooksName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBorrowDate;
        private System.Windows.Forms.Label lblBorrowDays;
        private System.Windows.Forms.Label lblOverplusDays;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pBtnOnceBorrowBook;
        private System.Windows.Forms.PictureBox pBtnReturnBook;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}