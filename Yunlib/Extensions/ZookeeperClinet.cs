﻿using System;
using Yunlib.Common;
using YunLib.Common;
using ZooKeeperNet;
using Yunlib.Extensions;
using System.Text;
using Org.Apache.Zookeeper.Data;

namespace Yunlib
{
    public class ZooKeeperClient
    {

        public static ZooKeeper zk;
        public static Watcher wc = new Watcher();

        /// <summary>
        /// 创建节点
        /// </summary>
        public static bool createNode()
        {
            try
            {
                zk = new ZooKeeper(ConfigManager.ZooKeeperIP, new TimeSpan(0, 24, 0, 0), null);

                string path = ConfigManager.ZookeeperPath + "/" + ConfigManager.MachineID;

                Stat stat = zk.Exists(path, true);
                if (stat != null)
                {
                    // DelNode();
                    return false;
                }
                else
                {
                    //创建节点
                    zk.Create(path, "childone".GetBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.Ephemeral);

                    //绑定监听
                    zk.Exists(path, wc);

                    LogManager.BorrowRecord("**** 创建节点：{0}".FormatWith(DateTime.Now));
                    return true;
                }
                
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("ZooKeeper节点存在问题:{0}".FormatWith(ex.Message));
                return false;
            }
        }

        /// <summary>
        /// 建立连接
        /// </summary>
        public static void content()
        {
            zk = new ZooKeeper(ConfigManager.ZooKeeperIP, new TimeSpan(0, 0, 0, 1000), null);
        }

        public static void DelNode()
        {
            string path = ConfigManager.ZookeeperPath + "/" + ConfigManager.MachineID;

            zk.Delete(path, -1);
        }
    }
}
