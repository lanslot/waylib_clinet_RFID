﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Extensions;
using YunLib.Common;

namespace YunLib_Vertical.AdminManager
{
    public partial class InitForm : Form
    {
        public InitForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void InitForm_Load(object sender, EventArgs e)
        {
            try
            {
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                cmbControl.Text = ConfigManager.Control;
                cmbScan.Text = ConfigManager.Scan;
                cmbPos.Text = ConfigManager.Pos;
                cmbUHF.Text = ConfigManager.UHF;
                txtLibaryID.Text = ConfigManager.LibraryID;
                txtMachineID.Text = ConfigManager.MachineID;
               
                cmbBType.SelectedIndex = ConfigManager.BorrowType;
                cmbReadCardType.SelectedIndex = int.Parse(ConfigManager.ReadCardType);
                cmbReadBookType.SelectedIndex = ConfigManager.ReadBookType;
                cmbScanType.SelectedIndex = ConfigManager.ScanType;
                cmbCanRegister.SelectedIndex = ConfigManager.CanRegister;

               
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("{0}---InitForm_Load出错:{1}".FormatWith(DateTime.Now, ex.Message));
            }

        }

        public static string servicePath = ConfigManager.ServicePath;
        private void btnSure_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Control", cmbControl.Text))
                {
                    MessageBox.Show("Control同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Scan", cmbScan.Text))
                {
                    MessageBox.Show("Scan同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Pos", cmbPos.Text))
                {
                    MessageBox.Show("Pos同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "UHF", cmbUHF.Text))
                {
                    MessageBox.Show("UHF同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ReadCardType", cmbReadCardType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ReadCardType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ReadBookType", cmbReadBookType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ReadBookType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ScanType", cmbScanType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ScanType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "BorrowType", cmbBType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("BorrowType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "CanRegister", cmbCanRegister.SelectedIndex.ToString()))
                {
                    MessageBox.Show("CanRegister同步失败");
                    return;
                }

                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "LibraryID", txtLibaryID.Text))
                {
                    MessageBox.Show("LibraryID同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "MachineID", txtMachineID.Text))
                {
                    MessageBox.Show("MachineID同步失败");
                    return;
                }
                if (radioButton1.Checked)
                {
                    if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ServicePath", "http://ceshi.yunlib.cn/"))
                    {
                        MessageBox.Show("ServicePath同步失败");
                        return;
                    }
                    else
                    {
                        servicePath = "http://ceshi.yunlib.cn/";
                    }
                }
                else
                {
                    if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ServicePath", "http://service.yunlib.cn/"))
                    {
                        MessageBox.Show("ServicePath同步失败");
                        return;
                    }
                    else
                    {
                        servicePath = "http://service.yunlib.cn/";
                    }
                }

                //  Application.ExitThread();
                //Application.Exit();
                Application.Restart();
                //Process.GetCurrentProcess().Kill();

            }
            catch (Exception ex)
            {
                MessageBox.Show("保存配置失败");
                LogManager.BorrowRecord("{0}---初始化数据出错:{1}".FormatWith(DateTime.Now, ex.Message));
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Properties.Resources.hui_w;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Properties.Resources.hui_x;
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
