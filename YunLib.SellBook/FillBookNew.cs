﻿using System;
using YunLib.BLL;
using System.Text;
using Yunlib.Common;
using YunLib.Common;
using System.Drawing;
using Yunlib.Entity;
using Yunlib.SellBook.Extensions;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using ShenZhen.SmartLife.Control;
using System.IO.Ports;
using YunLib.ReadCard;

namespace Yunlib.SellBook
{
    public partial class FillBookNew : Form
    {
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();
        private int cmdPort = ConfigManager.CmdPort;
        private string control = ConfigManager.Control;
        private bool IsNeedReadCard = true;
        //定义全局的控制变量

        private IReadCard read;
        //定义全局的控制变量
        private SmartLifeBroker model;

        public FillBookNew()
        {
            InitializeComponent();
            if (ConfigManager.ReadBookType == 1)
            {
                model = new SmartLifeBroker();
                model.OnScanedDataEvent += OnScanedDataEvent;
                pictureBox5.Visible = true;
               
            }
            else
            {
                read = ObjectCreator.GetReadClass();
               
            }
        }

        #region 窗体扩展事件
        private void OnScanedDataEvent(object source, byte[] scanFrame)
        {
            string barCode = Encoding.ASCII.GetString(scanFrame);

            txtBarCode.Text = barCode;
            IsNeedReadCard = false;
        }


        #endregion


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        private void FillBookNew_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Now;
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

              //  CodeOptimize.SolveLightScreen(this);

                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

                pictureBox1.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Scan1.png");
                pictureBox2.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Write_in111.png");
                pictureBox3.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/Split11.png");
                pic_BtnTurnBack.Image = CodeOptimize.InitPicture(Application.StartupPath + "/Image/pic_btn_turnBack.png");

                
                if ( ConfigManager.ReadBookType == 1)
                {
                   
                        model.Init(ConfigManager.Scan, cmdPort);
                    if (ConfigManager.ScanType == 1)
                    {
                        //打开扫描器
                        model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
                    }
                    //设置扫描图片
                    pictureBox1.Image = Properties.Resources.Scan22;
                }
                

                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.TimeRecord("FillBookNew_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        #region 窗体扩展事件
        private string GetSign(string barCode,  long timeStamp)
        {
            StringBuilder signStr = new StringBuilder();
            signStr.Append("{0}".FormatWith(barCode));
            signStr.Append("{0}".FormatWith(ConfigManager.LibraryID));
            signStr.Append("{0}".FormatWith(ConfigManager.MachineID));
            signStr.Append("{0}".FormatWith(timeStamp));
            signStr.Append("{0}".FormatWith(ConfigManager.CommonKey));

            return EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr.ToString()).ToLower());
        }

        private string GetUrl(long timeStamp, string sign, string barCode)
        {
            StringBuilder urlStr = new StringBuilder();
            urlStr.Append("{0}/public/book/syn/shelvesserviceForSell".FormatWith(ConfigManager.ServicePath));
            urlStr.Append("?isbn={0}".FormatWith(barCode));
            urlStr.Append("&timeStamp={0}".FormatWith(timeStamp));
            urlStr.Append("&machineId={0}".FormatWith(ConfigManager.MachineID));
            urlStr.Append("&sign={0}".FormatWith(sign));
            urlStr.Append("&libraryId={0}".FormatWith(ConfigManager.LibraryID));
            return urlStr.ToString();
        }

        /// <summary>
        /// 上书操作
        /// </summary>
        public void FillBooks()
        {
            try
            {
                string barCode = txtBarCode.Text.Trim();
                if (string.IsNullOrEmpty(barCode))
                {
                    lblErrMsg.Text = "请输入条码号";
                }
                else
                {
                    long timeStamp = DateTime.Now.Ticks;

                    string signStr = GetSign(barCode, timeStamp);
                    string urlStr = GetUrl(timeStamp, signStr, barCode);

                    string[] s = new string[1];
                    s[0] = ConfigManager.PingServicePath;
                    bool flag = NetTest.CheckServeStatus(s);
                    //验证网络连接状态，true就发起请求，false不请求
                    if (flag)
                    {
                        JObject backData = WebHelper.HttpWebRequest(urlStr, "", true).ToJObject();


                        if (backData["code"].ToString() == "100")
                        {
                            string doorNum = backData["data"]["machLatNum"].ToString();
                            var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                            CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                            LogManager.BorrowRecord("机器上书开柜，doorid={0},doorcode={1},doorNum={2}".FormatWith(list[0].DoorID,list[0].DoorCode,list[0].DoorNUM));
                            lblErrMsg.Text = "读取成功，请将书放入储物格!".FormatWith(list[0].DoorID, list[0].DoorCode, list[0].DoorNUM);
                            lblErrMsg.ForeColor = Color.Green;

                        }
                        else
                        {
                            //lblErrMsg.Text = "加入失败，请重新扫描!";
                            lblErrMsg.Text = backData["msg"].ToString();
                            lblErrMsg.ForeColor = Color.Red;
                        }

                        txtBarCode.Text = string.Empty;


                        if (ConfigManager.ScanType == 1)
                        {
                                //打开扫描器
                                model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
                            
                        }


                    }
                    else
                    {
                        lblErrMsg.Text = "网络繁忙，请稍后再试···";
                    }

                }
            }catch(Exception ex)
            {
                LogManager.ErrorRecord("{0}--书籍上架错误：{1}".FormatWith(DateTime.Now, ex.Message));
            }
        }

       
        #endregion

        #region 控件事件函数
        //点击上架的操作
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            try
            {
                objDelegate objDel = new objDelegate(ShowLblDao);
                //点击打开扫描器,就重置一下自动跳转时间
                IAsyncResult result = objDel.BeginInvoke(null, null);

                lblDaoJiShi.Text = objDel.EndInvoke(result);
                FillBooks();
                IsNeedReadCard = true;
                i = 0;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        public delegate string objDelegate();

        private string ShowLblDao()
        {
            return "120";
        }

        

        //当鼠标在TextBarCode上按下鼠标事件
        private void txtBarCode_MouseDown(object sender, MouseEventArgs e)
        {
            //    DymicNewJP frm = new DymicNewJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

            //    frm.ShowDialog();

            //    if (frm.DialogResult == DialogResult.Cancel)
            //    {
            //        //关闭虚拟键盘时,把值传到登录页面的文本框中
            //        txtBarCode.Text = DymicNewJP.inputCount;
            //    }

            //DymicJP frm = new DymicJP { TopMost = true, StartPosition = FormStartPosition.Manual };

            //frm.getTextHandler = getValue;//将方法赋给委托对象
            //frm.ShowDialog();
            CodeOptimize.form.Show();

            txtBarCode.Focus();

            this.Activate();
        }

        public void getValue(string strV)
        {
            if (strV == "Backspace")
            {
                string str = txtBarCode.Text;
                txtBarCode.Text = str.Substring(0, str.Length - 1);
            }
            else if (strV == "Clear")
            {
                this.txtBarCode.Text = "";
            }
            else
            {
                this.txtBarCode.Text += strV;
            }
        }

        //返回按钮事件
        private void pic_BtnTurnBack_Click(object sender, EventArgs e)
        {
            try
            {

                CloseTimer();

                AdminManage.BackMain frm = new AdminManage.BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

                this.Close();

                frm.Show();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }
        int i = 0;
        string barCode = "";
        private void timer2_Tick(object sender, EventArgs e)
        {
           
            if (i % 5 == 0)
            {
                this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");
                int s = Convert.ToInt32(lblDaoJiShi.Text);
                s -= 1;
                this.lblDaoJiShi.Text = s.ToString();

                if (s == 0)
                {
                    foreach (Form item in Application.OpenForms)
                    {
                        if (item is DymicNewJP)
                        {
                            (item as DymicNewJP).Close();
                        }
                    }

                    //停止计时器
                    timer2.Enabled = false;
                    timer2.Stop();

                    //调回上一窗体
                    //Main frm = Singleton<Main>.CreateInstance();
                    Main frm = new Main();
                    frm.MdiParent = ParentForm;
                    frm.Dock = DockStyle.Fill;

                    this.Close();

                    frm.Show();

                }
            }

          

            if (IsNeedReadCard)
            {
                string str = "";
                if (ConfigManager.ReadBookType == 2)
                {
                    barCode = read.ReadRFID(ConfigManager.Pos, ConfigManager.IMPort);
                   
                    str = barCode;
                }
                else if (ConfigManager.ReadBookType == 3)
                {
                    barCode = read.ReadUHFRFID(ConfigManager.UHF, ConfigManager.IMPort);
                  
                    str = barCode;

                }

                if (!string.IsNullOrWhiteSpace(str))
                {
                    txtBarCode.Text = str;
                    IsNeedReadCard = false;
                }
                
                
            }
            i++;
        }
        #endregion

        #region 按钮的样式控制代码
       


        private void pictureBox6_MouseDown(object sender, MouseEventArgs e)
        {
            this.pictureBox6.Image = Image.FromFile(Application.StartupPath + "/Image/pic_btn_ShangBooksEd.png");
        }

        private void pictureBox6_MouseUp(object sender, MouseEventArgs e)
        {
            this.pictureBox6.Image = Image.FromFile(Application.StartupPath + "/Image/pic_btn_ShangBooks.png");
        }

        private void pictureBox6_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        #endregion

        

        #region 操作函数之外的函数

        public int icdev;   // 通讯设备标识符
        public short st;    //函数返回值

        //关闭计时器
        private void CloseTimer()
        {
            timer2.Enabled = false;
            timer2.Stop();
        }
        #endregion

        private void FillBookNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ConfigManager.ReadBookType == 1)
            {
                model.Dispose();
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            try
            {
                objDelegate objDel = new objDelegate(ShowLblDao);
                //点击打开扫描器,就重置一下自动跳转时间
                IAsyncResult result = objDel.BeginInvoke(null, null);

                lblDaoJiShi.Text = objDel.EndInvoke(result);
                
                //打开扫描器
                model.StartScan(ShenZhen.SmartLife.Control.ScanDevice.DeviceType.Winson);
            }
            catch (Exception ex)
            {
                string st = ex.StackTrace;
                LogManager.WriteLogs(ex.Message, st, LogFile.Error);
                MessageBox.Show(st, "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }
    }
}
