﻿namespace YunLib_Vertical
{
    partial class NumJPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic7 = new System.Windows.Forms.PictureBox();
            this.pic8 = new System.Windows.Forms.PictureBox();
            this.pic9 = new System.Windows.Forms.PictureBox();
            this.picBackspace = new System.Windows.Forms.PictureBox();
            this.pic0 = new System.Windows.Forms.PictureBox();
            this.picSure = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackspace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSure)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pic1
            // 
            this.pic1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic1.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_114;
            this.pic1.Location = new System.Drawing.Point(3, 3);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(71, 63);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic1.TabIndex = 4;
            this.pic1.TabStop = false;
            this.pic1.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic1_MouseDown);
            this.pic1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic1_MouseUp);
            // 
            // pic2
            // 
            this.pic2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic2.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_116;
            this.pic2.Location = new System.Drawing.Point(80, 3);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(71, 63);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic2.TabIndex = 5;
            this.pic2.TabStop = false;
            this.pic2.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic2_MouseDown);
            this.pic2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic2_MouseUp);
            // 
            // pic3
            // 
            this.pic3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic3.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_117;
            this.pic3.Location = new System.Drawing.Point(157, 3);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(72, 63);
            this.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic3.TabIndex = 6;
            this.pic3.TabStop = false;
            this.pic3.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic3_MouseDown);
            this.pic3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic3_MouseUp);
            // 
            // pic4
            // 
            this.pic4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic4.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_121;
            this.pic4.Location = new System.Drawing.Point(3, 72);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(71, 63);
            this.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic4.TabIndex = 7;
            this.pic4.TabStop = false;
            this.pic4.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic4_MouseDown);
            this.pic4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic4_MouseUp);
            // 
            // pic5
            // 
            this.pic5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic5.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_122;
            this.pic5.Location = new System.Drawing.Point(80, 72);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(71, 63);
            this.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic5.TabIndex = 8;
            this.pic5.TabStop = false;
            this.pic5.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic5_MouseDown);
            this.pic5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic5_MouseUp);
            // 
            // pic6
            // 
            this.pic6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic6.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_123;
            this.pic6.Location = new System.Drawing.Point(157, 72);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(72, 63);
            this.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic6.TabIndex = 9;
            this.pic6.TabStop = false;
            this.pic6.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic6_MouseDown);
            this.pic6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic6_MouseUp);
            // 
            // pic7
            // 
            this.pic7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic7.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_127;
            this.pic7.Location = new System.Drawing.Point(3, 141);
            this.pic7.Name = "pic7";
            this.pic7.Size = new System.Drawing.Size(71, 63);
            this.pic7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic7.TabIndex = 10;
            this.pic7.TabStop = false;
            this.pic7.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic7_MouseDown);
            this.pic7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic7_MouseUp);
            // 
            // pic8
            // 
            this.pic8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic8.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_128;
            this.pic8.Location = new System.Drawing.Point(80, 141);
            this.pic8.Name = "pic8";
            this.pic8.Size = new System.Drawing.Size(71, 63);
            this.pic8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic8.TabIndex = 11;
            this.pic8.TabStop = false;
            this.pic8.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic8_MouseDown);
            this.pic8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic8_MouseUp);
            // 
            // pic9
            // 
            this.pic9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic9.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_129;
            this.pic9.Location = new System.Drawing.Point(157, 141);
            this.pic9.Name = "pic9";
            this.pic9.Size = new System.Drawing.Size(72, 63);
            this.pic9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic9.TabIndex = 12;
            this.pic9.TabStop = false;
            this.pic9.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic9_MouseDown);
            this.pic9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic9_MouseUp);
            // 
            // picBackspace
            // 
            this.picBackspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBackspace.Image = global::YunLib_Vertical.Properties.Resources.keyboard_del_1;
            this.picBackspace.Location = new System.Drawing.Point(3, 210);
            this.picBackspace.Name = "picBackspace";
            this.picBackspace.Size = new System.Drawing.Size(71, 63);
            this.picBackspace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBackspace.TabIndex = 13;
            this.picBackspace.TabStop = false;
            this.picBackspace.Click += new System.EventHandler(this.pictureBox_Click);
            this.picBackspace.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picDel_MouseDown);
            this.picBackspace.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picDel_MouseUp);
            // 
            // pic0
            // 
            this.pic0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic0.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_134;
            this.pic0.Location = new System.Drawing.Point(80, 210);
            this.pic0.Name = "pic0";
            this.pic0.Size = new System.Drawing.Size(71, 63);
            this.pic0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic0.TabIndex = 14;
            this.pic0.TabStop = false;
            this.pic0.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic0_MouseDown);
            this.pic0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic0_MouseUp);
            // 
            // picSure
            // 
            this.picSure.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_135;
            this.picSure.Location = new System.Drawing.Point(157, 210);
            this.picSure.Name = "picSure";
            this.picSure.Size = new System.Drawing.Size(72, 63);
            this.picSure.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSure.TabIndex = 15;
            this.picSure.TabStop = false;
            this.picSure.Click += new System.EventHandler(this.picSure_Click);
            this.picSure.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picSure_MouseDown);
            this.picSure.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picSure_MouseUp);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.picSure, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.pic0, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.picBackspace, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.pic9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.pic8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.pic7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pic6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.pic5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pic4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pic3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pic2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pic1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 45);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(232, 276);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // picClose
            // 
            this.picClose.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_00;
            this.picClose.Location = new System.Drawing.Point(213, 10);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(29, 29);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picClose.TabIndex = 6;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picSure_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // NumJPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.keyboard_bj_03;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(257, 333);
            this.Controls.Add(this.picClose);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NumJPForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "NumJPForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.NumJPForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackspace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSure)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic4;
        private System.Windows.Forms.PictureBox pic5;
        private System.Windows.Forms.PictureBox pic6;
        private System.Windows.Forms.PictureBox pic7;
        private System.Windows.Forms.PictureBox pic8;
        private System.Windows.Forms.PictureBox pic9;
        private System.Windows.Forms.PictureBox picBackspace;
        private System.Windows.Forms.PictureBox pic0;
        private System.Windows.Forms.PictureBox picSure;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.Timer timer1;
    }
}