﻿namespace Yunlib.SellBook.AdminManage
{
    partial class DoorManage210
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenDoorAll = new System.Windows.Forms.Button();
            this.btnReturnMain = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenDoorAll
            // 
            this.btnOpenDoorAll.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.btnOpenDoorAll.Location = new System.Drawing.Point(12, 12);
            this.btnOpenDoorAll.Name = "btnOpenDoorAll";
            this.btnOpenDoorAll.Size = new System.Drawing.Size(122, 29);
            this.btnOpenDoorAll.TabIndex = 0;
            this.btnOpenDoorAll.Text = "打开所有柜门";
            this.btnOpenDoorAll.UseVisualStyleBackColor = true;
            this.btnOpenDoorAll.Click += new System.EventHandler(this.btnOpenDoorAll_Click);
            // 
            // btnReturnMain
            // 
            this.btnReturnMain.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.btnReturnMain.Location = new System.Drawing.Point(910, 12);
            this.btnReturnMain.Name = "btnReturnMain";
            this.btnReturnMain.Size = new System.Drawing.Size(86, 29);
            this.btnReturnMain.TabIndex = 0;
            this.btnReturnMain.Text = "返回首页";
            this.btnReturnMain.UseVisualStyleBackColor = true;
            this.btnReturnMain.Click += new System.EventHandler(this.btnReturnMain_Click);
            // 
            // DoorManage210
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.btnReturnMain);
            this.Controls.Add(this.btnOpenDoorAll);
            this.Name = "DoorManage210";
            this.Text = "DoorManage";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DoorManage210_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOpenDoorAll;
        private System.Windows.Forms.Button btnReturnMain;
    }
}