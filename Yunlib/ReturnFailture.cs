﻿using System;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class ReturnFailture : Form
    {
        public string errorMessage { get; set; }
        public ReturnFailture()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }

        private void ReturnFailture_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            CodeOptimize.SolveLightScreen(this);
            label1.Text = errorMessage;
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.TimeRecord("ReturnFailture_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void pic_btnTurnBack_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";

            if (int.Parse(lblSecond.Text) < 0)
            {
                timer1.Enabled = false;

                // Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }
    }
}
