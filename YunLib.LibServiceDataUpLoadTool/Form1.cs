﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.BLL;
using YunLib.LibServiceDataUpLoadTool.Common;
using YunLib.LibServiceDataUpLoadTool.Models;

namespace YunLib.LibServiceDataUpLoadTool
{
    public partial class Form1 : Form
    {
        LibMessageForMySqlBLL bll = new LibMessageForMySqlBLL();
        LibMessageForSqlServerBLL bl = new LibMessageForSqlServerBLL();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            this.txtFilePath.Text = path.SelectedPath;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.FACT_LOAN);
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.HOLDING);
        }

        public void WriteFile(FileTypeEnum fileType)
        {
            try
            {
                string type = ConfigurationManager.AppSettings["DataType"].ToString();
                if (string.IsNullOrWhiteSpace(txtFilePath.Text))
                {
                    listBox1.Items.Add("文件保存路径不能为空");

                    return;
                }

                var count = (dtpEnd.Value.Year - dtpStart.Value.Year) * 12 + (dtpEnd.Value.Month - dtpStart.Value.Month);

                DataTable dt = new DataTable();
                listBox1.Items.Add("数据查询中......");
                for (int i = 0; i < count; i++)
                {

                    var end = dtpStart.Value.AddMonths(1);
                    if (end > dtpEnd.Value)
                    {
                        end = dtpEnd.Value;
                    }
                    DataTable table = new DataTable();
                   
                    if (type == "1")
                    {
                        if (fileType == FileTypeEnum.FACT_LOAN)
                        {
                            table = bll.Get_FACT_LOAN(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.HOLDING)
                        {
                            table = bll.Get_HOLDING(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.BIBLIOS)
                        {
                            table = bll.Get_BIBLIOS(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.READER)
                        {
                            table = bll.Get_READER(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.P_LIBCODE)
                        {
                            table = bll.Get_P_LIBCODE();
                        }
                        else if (fileType == FileTypeEnum.P_LOCAL)
                        {
                            table = bll.Get_P_LOCAL();
                        }
                    }
                    else
                    {
                        if (fileType == FileTypeEnum.FACT_LOAN)
                        {
                            table = bl.Get_FACT_LOAN(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.HOLDING)
                        {
                            table = bl.Get_HOLDING(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.BIBLIOS)
                        {
                            table = bl.Get_BIBLIOS(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.READER)
                        {
                            table = bl.Get_READER(dtpStart.Value, end);
                        }
                        else if (fileType == FileTypeEnum.P_LIBCODE)
                        {
                            table = bl.Get_P_LIBCODE();
                        }
                        else if (fileType == FileTypeEnum.P_LOCAL)
                        {
                            table = bl.Get_P_LOCAL();
                        }
                    }

                    if (i == 0)
                    {
                        dt = table;
                    }
                    else
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            dt.Rows.Add(row.ItemArray);
                        }
                    }
                }
                //List<ChangeModel> list = dt.ToList<ChangeModel>();
                listBox1.Items.Add("数据查询成功......");

                bool flag = false;

                if(type == "1")
                {
                    flag = WriteFiles.WriteDataToFile(txtFilePath.Text, dt, fileType);
                }
                else
                {
                    flag = WriteFiles.WriteDataToFileForSqlServer(txtFilePath.Text, dt, fileType);
                }

                if (flag)
                {
                    listBox1.Items.Add("文件保存成功");
                }
                else
                {
                    listBox1.Items.Add("文件保存失败");
                }
            }
            catch (Exception ex)
            {
                listBox1.Items.Add(ex.StackTrace);
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.BIBLIOS);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.P_LOCAL);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.P_LIBCODE);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WriteFile(FileTypeEnum.READER);
        }
    }
}
