﻿using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;

namespace Yunlib
{
    public partial class FaceLogin : Form
    {
        Mat matImg;//摄像头图像
        Capture capture;//摄像头对象

        Util.KingFaceDetect kfd;

        int currentFaceFlag = 0;
        Util.KingFaceDetect.faceDetectedObj currentfdo;//点击鼠标时的人脸检测对象

        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string loginKey = ConfigManager.LoginKey;
        private string path = "";
        private BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        public string barCode { get; set; }

        public FaceLogin()
        {
            InitializeComponent();
            CvInvoke.UseOpenCL = false;
            kfd = new Util.KingFaceDetect();
            try
            {
                capture = new Capture();
                capture.Start();//摄像头开始工作
                capture.ImageGrabbed += frameProcess;//实时获取图像
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }


        private void FaceLogin_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

            int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;
            this.lblDaoJiShi.Text = second.ToString();

            if (second <= 0)
            {
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            markFaces();
        }

        #region 人脸识别
        private void markFaces()
        {
            try
            {
                // Image image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap;
                picShow.Image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap;
                //if (image != null)
                //{
                currentfdo = kfd.GetFaceRectangle(capture.QueryFrame());
                currentFaceFlag = 0;
                getCurrentFaceSample(0);
                capture.Stop();
                //}
            }
            catch
            {
            }
        }

        public static string UrlEncode(string str)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }

            return (sb.ToString());
        }


        private void getCurrentFaceSample(int i)
        {
            try
            {
               
                if (currentfdo.facesRectangle.Count > 0)
                {

                    Image<Rgba, byte> result = currentfdo.originalImg.ToImage<Rgba, byte>().Resize(200, 200, Emgu.CV.CvEnum.Inter.Cubic);
                    //  result._EqualizeHist();//灰度直方图均衡化
                    //  sampleBox.Image = result.Bitmap;
                    picSample.Image = result.Bitmap;
                    path = AppDomain.CurrentDomain.BaseDirectory;
                    if (!string.IsNullOrEmpty(path))
                    {
                        path = AppDomain.CurrentDomain.BaseDirectory + "face";
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                    }
                    path = path + "\\123.jpg";
                    picSample.Image.Save(path);

                    byte[] b = File.ReadAllBytes(path);

                    string strbaser64 = Convert.ToBase64String(b);
                    string s1 = UrlEncode(strbaser64);

                   

                    Func<string> longTask = new Func<string>(delegate ()
                    {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return getData(s1);
                        //  返回任务结果：5
                    });
                    //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                    //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                    longTask.BeginInvoke(ar =>
                    {
                        //  使用EndInvoke获取到任务结果（5）
                        string result1 = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                        {
                            Invoke(new Action(() =>

                           changeData(result1)
                            // dataGridView1.DataSource = result

                            ));
                        }
                    }, null);
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("没有检测到人脸");
            }
        }

        public void changeData(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {

                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        var data = result["data"];
                        if (data == null)
                        {
                           // lblPwdErrMsg.Text = "返回用户数据错误";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(data["uName"].ToString()))
                            {
                                lblUName.Text = data["uName"].ToString();
                            }
                            else
                            {
                                lblUName.Text = data["uMobile"].ToString();
                            }
                            if (!string.IsNullOrWhiteSpace(data["userRealName"].ToString()))
                            {
                                lblRealName.Text = data["userRealName"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["uIdcard"].ToString()))
                            {
                                lblIDCard.Text = data["uIdcard"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["gender"].ToString()))
                            {
                                lblSex.Text = data["gender"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["address"].ToString()))
                            {
                                lblAddress.Text = data["address"].ToString();
                            }

                            lblToken.Text = data["token"].ToString();
                            lblError.Text = result["msg"].ToString();
                            //button2.BackColor = Color.DodgerBlue;
                            //button2.Enabled = true;
                            capture.Stop();
                            timer1.Stop();


                        }
                    }
                    else
                    {
                        lblError.Text = result["msg"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("解析用户绑定登陆返回数据出错", ex.StackTrace, LogFile.Error);
               // lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }

        public string getData(string strbaser64)
        {
            string dt = "";
            try
            {
                var time = DateTime.Now.Ticks;
                var signstr = "{0}{1}{2}".FormatWith( libraryId, time, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/faceLogin".FormatWith(servicePath);
                string parameters = "image={0}&libraryId={1}&timeStamp={2}&sign={3}".FormatWith(strbaser64, libraryId, time, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

                //if (res["code"].ToString() == "100")
                //{
                //    dt = res["data"].ToJson().ToString();



                //}
                return res.ToString(); 
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs(ex.Message, ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        /// <summary>
        /// 实时获取图像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arg"></param>
        private void frameProcess(object sender, EventArgs arg)
        {
            matImg = new Mat();
            capture.Retrieve(matImg, 0);
            picShow.Image = matImg.Bitmap;
        }
        #endregion

        private void btnMarkFaces_Click(object sender, EventArgs e)
        {
            capture.Start();
            timer2.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return BorrowBook(lblToken.Text);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result1 = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result1 != null)
                {
                    Invoke(new Action(() =>

                   EndBorrow(result1)
                    // dataGridView1.DataSource = result

                    ));
                }
            }, null);
        }

        public string BorrowBook(string toc)
        {
            string dt = "";
            try
            {
                string bc = barCode.Replace(libraryId, "");
                var time = DateTime.Now.Ticks;
                var signstr = "{0}{1}{2}{3}{4}{5}".FormatWith(bc,libraryId,machineId, time,toc, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/faceLendBook".FormatWith(servicePath);
                string parameters = "bookId={0}&libraryId={1}&machineId={2}&timeStamp={3}&token={4}&sign={5}".FormatWith(bc, libraryId, machineId, time,toc,sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

                //if (res["code"].ToString() == "100")
                //{
                //    dt = res["data"].ToJson().ToString();



                //}
                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("刷脸登录借书出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        public void EndBorrow(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {

                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        string DoorNum = result["data"]["machineLatNum"].ToString();
                        BorrSuccess frm = new BorrSuccess(DoorNum) { MdiParent = ParentForm, Dock = DockStyle.Fill };

                        this.Close();

                        frm.Show();

                        var list = accessBLL.GetBookByDoorNum(int.Parse(DoorNum.ToString()));

                        CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                        capture.Stop();
                            timer2.Stop();

                        
                    }
                    else
                    {
                        lblError.Text = result["msg"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("刷脸登陆返回数据出错", ex.StackTrace, LogFile.Error);
                // lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }
    }
}
