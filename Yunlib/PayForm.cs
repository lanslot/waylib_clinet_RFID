﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;
using Yunlib.Common;
using YunLib.Common;

namespace Yunlib
{
    public partial class PayForm : Form
    {
        public string url { get; set; }
        public string outTradeNo { get; set; }

        public string money { get; set; }

        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;
        public PayForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void PayForm_Load(object sender, EventArgs e)
        {
            LoadCreateQrCode(url);
            label1.Text = money;
            
        }

        private void LoadCreateQrCode(string url)
        {
            Image image = CreateImage(url);
            if (image != null)
            {
                pictureBox1.Image = image;
            }
        }

        // 生成二维码
        private Image CreateImage(string data)
        {
            var encoder = new QRCodeEncoder();
            encoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            try
            {
                encoder.QRCodeScale = 4;
            }
            catch (Exception ex)
            {
                MessageBox.Show("大小参数错误!");
                return null;
            }
            try
            {
                encoder.QRCodeVersion = 9;
            }
            catch (Exception ex)
            {
                MessageBox.Show("版本参数错误 !");
                return null;
            }

            encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            Image image = encoder.Encode(data);
            return image;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblDataTimeNow.Text = DateTime.Now.ToString("yyyy/MM/dd  HH:mm");

            int second = Convert.ToInt32(lblDaoJiShi.Text) - 1;
            this.lblDaoJiShi.Text = second.ToString();

            if (second <= 0)
            {
                //断开连接


                // Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();

                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }

            var timeStamp1 = DateTime.Now.Ticks.ToString();
            var signStr1 = "{0}{1}{2}".FormatWith(outTradeNo, timeStamp1, commonKey);
            var sign1 = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr1).ToLower());

            var url1 = "{0}/public/idCard/order/isSuccess?outTradeNo={1}&timeStamp={2}&sign={3}".FormatWith(servicePath, outTradeNo, timeStamp1, sign1);

            var res2 = WebHelper.HttpWebRequest(url1, "", false).ToJObject();

            if (res2["code"].ToString() == "100")
            {
                RegisterSuccessForm frm = new RegisterSuccessForm { MdiParent = ParentForm ,Dock= DockStyle.Fill};
                
                this.Close();
                frm.Show();
            }
        }

        private void PayForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();
        }

        private void pic_BtnTurnBack_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();

            //返回主界面
            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();

            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }
    }
}
