﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YunLib.Common;
using YunLib.BLL;
using Emgu.CV.Structure;
using System.IO;
using Yunlib.Common;
using Yunlib.DAL;
using Newtonsoft.Json.Linq;
using Yunlib.Extensions;
using Emgu.CV.CvEnum;

namespace YunLib_Vertical
{
    public partial class FaceLoginForm : Form
    {
        Mat matImg;//摄像头图像
        Capture capture;//摄像头对象

        Util.KingFaceDetect kfd;

        int currentFaceFlag = 0;
        Util.KingFaceDetect.faceDetectedObj currentfdo;//点击鼠标时的人脸检测对象

        private string servicePath = ConfigManager.ServicePath;
        private string machineId = ConfigManager.MachineID;
        private string libraryId = ConfigManager.LibraryID;
        private string commonKey = ConfigManager.CommonKey;
        private string loginKey = ConfigManager.LoginKey;
        private string path = "";
        private BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        private JPForm jp;

        public string barCode { get; set; }
        public FaceLoginForm()
        {
            InitializeComponent();
            jp = new JPForm();
            try
            {
                CvInvoke.UseOpenCL = false;
                kfd = new Util.KingFaceDetect();
                capture = new Capture();
                //  capture.FlipType = FlipType.Vertical;
                capture.Start();//摄像头开始工作
                capture.ImageGrabbed += frameProcess;//实时获取图像

            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("FaceLoginForm_Load出错", ex.StackTrace, LogFile.Error);
            }
        }

        private void FaceLoginForm_Load(object sender, EventArgs e)
        {


        }

        #region 人脸识别
        private void markFaces()
        {
            try
            {
                // Bitmap image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap;
                picShow.Image = kfd.faceRecognize(capture.QueryFrame()).originalImg.Bitmap; ;
                //if (image != null)
                //{
                currentfdo = kfd.GetFaceRectangle(capture.QueryFrame());
                currentFaceFlag = 0;
                getCurrentFaceSample(0);
                //   capture.Stop();
                //}
            }
            catch
            {
            }
        }

        public Image<Bgra, byte> ImageRotates(Bitmap Map, double Dou, bool Bol = false)
        {
            Image<Bgra, byte> Imga1 = new Image<Bgra, byte>(Map);
            //Image<Bgra, byte> Imga2 = Imga1.Rotate(Dou, new Bgra(0, 0, 0, 0), Bol);
            // Image<Bgra, byte> Imga2 = Imga1.Rotate(Dou, new Bgra(0, 0, 0, 0), Bol) 等同于
            Image<Bgra, byte> Imga2 = Imga1.Rotate(Dou, new PointF(Imga1.Width / 2, Imga1.Height / 2), Inter.Cubic, new Bgra(0, 0, 0, 0), Bol);
            return Imga2;

        }

        public static string UrlEncode(string str)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }

            return (sb.ToString());
        }


        private void getCurrentFaceSample(int i)
        {
            try
            {


                if (currentfdo.facesRectangle.Count > 0)
                {

                    Image<Rgba, byte> result = currentfdo.originalImg.ToImage<Rgba, byte>().Resize(200, 200, Emgu.CV.CvEnum.Inter.Cubic);
                    //  result._EqualizeHist();//灰度直方图均衡化
                    //  sampleBox.Image = result.Bitmap;
                    picSample.Image = ImageRotates(result.Bitmap, 90, false).Bitmap;
                    path = AppDomain.CurrentDomain.BaseDirectory;
                    if (!string.IsNullOrEmpty(path))
                    {
                        path = AppDomain.CurrentDomain.BaseDirectory + "face";
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                    }
                    path = path + "\\123.jpg";
                    picSample.Image.Save(path);

                    byte[] b = File.ReadAllBytes(path);

                    string strbaser64 = Convert.ToBase64String(b);
                    string s1 = UrlEncode(strbaser64);



                    Func<string> longTask = new Func<string>(delegate ()
                    {
                        //  模拟长时间任务
                        ///  Thread.Sleep(2000);
                        return getData(s1);
                        //  返回任务结果：5
                    });
                    //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                    //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                    longTask.BeginInvoke(ar =>
                    {
                        //  使用EndInvoke获取到任务结果（5）
                        string result1 = longTask.EndInvoke(ar);

                        //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                        //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                        if (result != null)
                        {
                            Invoke(new Action(() =>

                           changeData(result1)

                            ));
                        }
                    }, null);

                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("没有检测到人脸");
            }
        }
        int faceCount = 0;
        public void changeData(string res)
        {
            try
            {
                faceCount++;
                if (string.IsNullOrWhiteSpace(res))
                {

                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        var data = result["data"];
                        if (data == null)
                        {
                            // lblPwdErrMsg.Text = "返回用户数据错误";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(data["uName"].ToString()))
                            {
                                lblUName.Text = data["uName"].ToString();
                            }
                            else
                            {
                                lblUName.Text = data["uMobile"].ToString();
                            }
                            if (!string.IsNullOrWhiteSpace(data["userRealName"].ToString()))
                            {
                                lblRealName.Text = data["userRealName"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["uIdcard"].ToString()))
                            {
                                lblIDCard.Text = data["uIdcard"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["gender"].ToString()))
                            {
                                lblSex.Text = data["gender"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["address"].ToString()))
                            {
                                lblAddress.Text = data["address"].ToString();
                            }

                            lblToken.Text = data["token"].ToString();
                            //lblError.Text = result["msg"].ToString();
                            //button2.BackColor = Color.DodgerBlue;
                            //button2.Enabled = true;
                            btnSure.Enabled = true;
                            btnSure.Text = "确    定";
                            btnSure.BackColor = Color.FromArgb(65, 184, 249);
                            capture.Stop();
                            timer1.Stop();
                        }
                    }
                    else
                    {
                        // lblError.Text = result["msg"].ToString();
                        btnSure.Text = "账 号 未 绑 定";
                        btnSure.Enabled = false;
                        btnSure.BackColor = Color.LightGray;
                        btnSure.ForeColor = Color.White;
                        panel2.Visible = true;
                    }
                    if (faceCount == 5)
                    {
                        capture.Stop();
                        timer1.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("解析用户绑定登陆返回数据出错", ex.StackTrace, LogFile.Error);
                // lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }

        public string getData(string strbaser64)
        {
            string dt = "";
            try
            {
                var time = DateTime.Now.Ticks;
                var signstr = "{0}{1}{2}".FormatWith(libraryId, time, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/faceLogin".FormatWith(servicePath);
                string parameters = "image={0}&libraryId={1}&timeStamp={2}&sign={3}".FormatWith(strbaser64, libraryId, time, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

                //if (res["code"].ToString() == "100")
                //{
                //    dt = res["data"].ToJson().ToString();



                //}
                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("人脸登录请求出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        /// <summary>
        /// 实时获取图像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arg"></param>
        private void frameProcess(object sender, EventArgs arg)
        {
            matImg = new Mat();
            capture.Retrieve(matImg, 0);
            picShow.Image = ImageRotates(matImg.Bitmap, 90, false).Bitmap;
        }
        #endregion

        #region 用户登陆
        private void pictLogin_Click(object sender, EventArgs e)
        {
            string uName = txtUserName.Text;
            string uPwd = txtPwd.Text;
            if (string.IsNullOrEmpty(uName))
            {
                lblPwdErrMsg.Text = "请输入读者证号/手机号";
            }
            else if (string.IsNullOrEmpty(uPwd))
            {
                lblPwdErrMsg.Text = "请输入密码";
            }
            else
            {
                Func<string> longTask = new Func<string>(delegate ()
                {
                    //  模拟长时间任务
                    ///  Thread.Sleep(2000);
                    return userLogin(uName, uPwd);
                    //  返回任务结果：5
                });
                //  发起一次异步调用，实际上就是在.net线程池中执行longTask
                //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
                longTask.BeginInvoke(ar =>
                {
                    //  使用EndInvoke获取到任务结果（5）
                    string result = longTask.EndInvoke(ar);

                    //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                    //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                    if (result != null)
                    {
                        Invoke(new Action(() =>

                       endLogin(result)
                        // dataGridView1.DataSource = result

                        ));
                    }
                }, null);
            }
        }

        public string userLogin(string uName, string uPwd)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks;

                var pwd = "{0}{1}".FormatWith(uPwd, loginKey);
                var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                var signstr = "{0}{1}{2}{3}{4}{5}".FormatWith(libraryId, signPwd, machineId, timeStamp, uName, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}/public/face/login?uName={1}&sipPwd={2}&libraryPwd={3}&libraryId={4}&machineId={5}&timeStamp={6}&sign={7}".FormatWith(servicePath, uName, uPwd, signPwd, libraryId, machineId, timeStamp, sign);

                string res = WebHelper.HttpWebRequest(url, "", true);
                return res;
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("用户绑定登陆出错", ex.StackTrace, LogFile.Error);
                return "";
            }

        }


        public void endLogin(string res)
        {
            try
            {

                LogManager.WriteLogs("FACELOGIN登陆返回",res, LogFile.Record);
                if (string.IsNullOrWhiteSpace(res))
                {
                    lblPwdErrMsg.Text = "用户绑定登陆出错";
                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        var data = result["data"];
                        if (data == null)
                        {
                            lblPwdErrMsg.Text = "返回用户数据错误";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(data["uName"].ToString()))
                            {
                                lblUName.Text = data["uName"].ToString();
                            }
                            else
                            {
                                lblUName.Text = data["uMobile"].ToString();
                            }
                            if (!string.IsNullOrWhiteSpace(data["userRealName"].ToString()))
                            {
                                lblRealName.Text = data["userRealName"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["uIdcard"].ToString()))
                            {
                                lblIDCard.Text = data["uIdcard"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["gender"].ToString()))
                            {
                                lblSex.Text = data["gender"].ToString();
                            }

                            if (!string.IsNullOrWhiteSpace(data["address"].ToString()))
                            {
                                lblAddress.Text = data["address"].ToString();
                            }

                            lblToken.Text = data["token"].ToString();
                            string faceUid = data["faceUid"].ToString();
                            if (!string.IsNullOrWhiteSpace(faceUid))
                            {
                                lblPwdErrMsg.Text = "该账号已绑定人脸";
                            }
                            else
                            {
                                picFaceRegister.Enabled = true;
                                picFaceRegister.Image = Properties.Resources.renlianbd_1;
                                lblPwdErrMsg.Text = "账号登录成功";
                                pictLogin.Enabled = false;
                                pictLogin.Image = Properties.Resources.login3;
                            }
                        }
                    }
                    else
                    {
                        lblPwdErrMsg.Text = result["msg"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("解析用户绑定登陆返回数据出错", ex.StackTrace, LogFile.Error);
                lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }
        #endregion

        #region 人脸绑定
        private void picFaceRegister_Click(object sender, EventArgs e)
        {
            byte[] b = File.ReadAllBytes(path);

            string strbaser64 = Convert.ToBase64String(b);
            string s1 = UrlEncode(strbaser64);
            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return beginRegister(s1);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result != null)
                {
                    Invoke(new Action(() =>

                   endRegister(result)
                    // dataGridView1.DataSource = result

                    ));
                }
            }, null);
        }

        private string beginRegister(string fs)
        {
            string dt = "";

            try
            {
                var time = DateTime.Now.Ticks;
                string token = lblToken.Text;
                if (string.IsNullOrWhiteSpace(token))
                {
                    lblPwdErrMsg.Text = "请先用账号+密码登录";
                    return "";
                }
                var signstr = "{0}{1}{2}".FormatWith(time, token, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/addFace".FormatWith(servicePath);
                string parameters = "image={0}&token={1}&timeStamp={2}&sign={3}".FormatWith(fs, token, time, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();


                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("人脸绑定出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        private void endRegister(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {
                    lblPwdErrMsg.Text = "人脸绑定出错";
                }
                else
                {
                    var result = res.ToJObject();
                    if (result["code"].ToString() == "100")
                    {
                        lblPwdErrMsg.Text = "人脸绑定成功，您可以开始借书了！";
                        btnSure.Enabled = true;
                        btnSure.BackColor = Color.FromArgb(65, 184, 249);
                        btnSure.Text = "确    定";
                    }
                    else
                    {
                        lblPwdErrMsg.Text = "人脸绑定失败";
                        LogManager.WriteLogs("人脸绑定失败", result["msg"].ToString(), LogFile.Record);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("解析人脸绑定返回数据出错", ex.StackTrace, LogFile.Error);
                lblPwdErrMsg.Text = "解析人脸绑定返回数据出错";
            }
        }
        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            markFaces();
        }

        private void btnMarkFaces_Click(object sender, EventArgs e)
        {
            timer1.Start();
            capture.Start();
            faceCount = 0;
        }



        #region 按钮样式
        private void pictLogin_MouseUp(object sender, MouseEventArgs e)
        {
            pictLogin.Image = Properties.Resources.login1;
        }

        private void pictLogin_MouseDown(object sender, MouseEventArgs e)
        {
            pictLogin.Image = Properties.Resources.login2;
        }

        private void picFaceRegister_MouseUp(object sender, MouseEventArgs e)
        {
            picFaceRegister.Image = Properties.Resources.renlianbd_1;
        }

        private void picFaceRegister_MouseDown(object sender, MouseEventArgs e)
        {
            picFaceRegister.Image = Properties.Resources.renlianbd_2;
        }

        private void picBack_MouseDown(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_x;
        }

        private void picBack_MouseUp(object sender, MouseEventArgs e)
        {
            picBack.Image = Properties.Resources.hui_w;
        }


        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            lblCount.Text = time.ToString();
            if (time <= 0)
            {
                MainForm frm = new MainForm();
                frm.MdiParent = this.ParentForm;
                frm.Parent = this.Parent;
                this.Close();
                frm.Show();
            }
        }


        private void picBack_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void txtUserName_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(155, 1180);
            jp.Show();
            txtUserName.Focus();

            this.Activate();
        }

        private void txtPwd_MouseDown(object sender, MouseEventArgs e)
        {
            jp.Location = new Point(155, 1180);
            jp.Show();
            txtPwd.Focus();

            this.Activate();
        }

        private void FaceLoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            capture.ImageGrabbed -= frameProcess;//实时获取图像
            capture.Dispose();
            timer1.Stop();
            timer1.Enabled = false;
            jp.Close();
        }

        private void btnSure_Click(object sender, EventArgs e)
        {
            Func<string> longTask = new Func<string>(delegate ()
            {
                //  模拟长时间任务
                ///  Thread.Sleep(2000);
                return BorrowBook(lblToken.Text);
                //  返回任务结果：5
            });
            //  发起一次异步调用，实际上就是在.net线程池中执行longTask
            //  这时由于是其它线程在工作，UI线程未被阻塞，所以窗体不会假死
            longTask.BeginInvoke(ar =>
            {
                //  使用EndInvoke获取到任务结果（5）
                string result1 = longTask.EndInvoke(ar);

                //  使用Control.Invoke方法将5显示到一个label上，如果没有Invoke，
                //  直接写lblStatus.Text="5"，将会抛出跨线程访问UI控件的异常
                if (result1 != null)
                {
                    Invoke(new Action(() =>

                   EndBorrow(result1)
                    // dataGridView1.DataSource = result

                    ));
                }
            }, null);
        }

        public string BorrowBook(string toc)
        {
            string dt = "";
            try
            {
                string bc = barCode.Replace(libraryId, "");
                var time = DateTime.Now.Ticks;
                var signstr = "{0}{1}{2}{3}{4}{5}".FormatWith(bc, libraryId, machineId, time, toc, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                string url = "{0}public/face/faceLendBook".FormatWith(servicePath);
                string parameters = "bookId={0}&libraryId={1}&machineId={2}&timeStamp={3}&token={4}&sign={5}".FormatWith(bc, libraryId, machineId, time, toc, sign);
                JObject res = WebHelper.HttpWebRequest(url, parameters, true).ToJObject();

                //if (res["code"].ToString() == "100")
                //{
                //    dt = res["data"].ToJson().ToString();



                //}
                return res.ToString();
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("刷脸登录借书出错", ex.StackTrace, LogFile.Error);
                return "";
            }
        }

        public void EndBorrow(string res)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(res))
                {

                }
                else
                {
                    var result = res.ToJObject();


                    if (result["code"].ToString() == "100")
                    {
                        string DoorNum = result["data"]["machineLatNum"].ToString();
                        var list = accessBLL.GetBookByDoorNum(int.Parse(DoorNum));

                        CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);
                        SuccessForm frm = new SuccessForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = Parent;
                        frm.Title = "借 阅 成 功";
                        frm.Message = "请到" + DoorNum + "号柜门取走图书，完成后请关闭柜门";
                        this.Close();
                        frm.Show();
                    }
                    else
                    {
                        FailForm frm = new FailForm();
                        frm.MdiParent = this.ParentForm;
                        frm.Parent = Parent;
                        frm.Title = "借 阅 失 败";
                        frm.Message = result["msg"].ToString();
                        this.Close();
                        frm.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("刷脸登陆返回数据出错", ex.StackTrace, LogFile.Error);
                // lblPwdErrMsg.Text = "解析用户绑定登陆返回数据出错";
            }
        }
    }
}
