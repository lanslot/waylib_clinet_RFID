﻿namespace Yunlib.SellBook
{
    partial class FillBookNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pic_BtnTurnBack = new System.Windows.Forms.PictureBox();
            this.lblGun = new System.Windows.Forms.Label();
            this.lblMiao = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDaoJiShi = new System.Windows.Forms.Label();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.lblErrMsg = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_BtnTurnBack)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Location = new System.Drawing.Point(460, 194);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(75, 435);
            this.pictureBox3.TabIndex = 36;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Yunlib.SellBook.Properties.Resources.Write_in111;
            this.pictureBox2.Location = new System.Drawing.Point(549, 174);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(341, 470);
            this.pictureBox2.TabIndex = 35;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Yunlib.SellBook.Properties.Resources.Scan22;
            this.pictureBox1.Location = new System.Drawing.Point(101, 176);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(341, 468);
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // pic_BtnTurnBack
            // 
            this.pic_BtnTurnBack.BackColor = System.Drawing.Color.Transparent;
            this.pic_BtnTurnBack.Location = new System.Drawing.Point(908, 645);
            this.pic_BtnTurnBack.Name = "pic_BtnTurnBack";
            this.pic_BtnTurnBack.Size = new System.Drawing.Size(100, 81);
            this.pic_BtnTurnBack.TabIndex = 37;
            this.pic_BtnTurnBack.TabStop = false;
            this.pic_BtnTurnBack.Click += new System.EventHandler(this.pic_BtnTurnBack_Click);
            // 
            // lblGun
            // 
            this.lblGun.BackColor = System.Drawing.Color.Transparent;
            this.lblGun.Font = new System.Drawing.Font("微软雅黑", 17F);
            this.lblGun.ForeColor = System.Drawing.Color.White;
            this.lblGun.Location = new System.Drawing.Point(221, 13);
            this.lblGun.Name = "lblGun";
            this.lblGun.Size = new System.Drawing.Size(27, 31);
            this.lblGun.TabIndex = 4;
            this.lblGun.Text = "丨";
            // 
            // lblMiao
            // 
            this.lblMiao.BackColor = System.Drawing.Color.Transparent;
            this.lblMiao.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblMiao.ForeColor = System.Drawing.Color.Orange;
            this.lblMiao.Location = new System.Drawing.Point(296, 11);
            this.lblMiao.Name = "lblMiao";
            this.lblMiao.Size = new System.Drawing.Size(36, 31);
            this.lblMiao.TabIndex = 3;
            this.lblMiao.Text = "秒";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblGun);
            this.panel1.Controls.Add(this.lblMiao);
            this.panel1.Controls.Add(this.lblDaoJiShi);
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(630, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 57);
            this.panel1.TabIndex = 38;
            // 
            // lblDaoJiShi
            // 
            this.lblDaoJiShi.BackColor = System.Drawing.Color.Transparent;
            this.lblDaoJiShi.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDaoJiShi.ForeColor = System.Drawing.Color.Orange;
            this.lblDaoJiShi.Location = new System.Drawing.Point(242, 12);
            this.lblDaoJiShi.Name = "lblDaoJiShi";
            this.lblDaoJiShi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDaoJiShi.Size = new System.Drawing.Size(62, 31);
            this.lblDaoJiShi.TabIndex = 3;
            this.lblDaoJiShi.Text = "120";
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::Yunlib.SellBook.Properties.Resources.时间图标;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Location = new System.Drawing.Point(598, 44);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(26, 26);
            this.pictureBox4.TabIndex = 39;
            this.pictureBox4.TabStop = false;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 200;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(261, 676);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(472, 27);
            this.label4.TabIndex = 40;
            this.label4.Text = "欢迎使用智能微图借阅自助借还机，请选择操作方式";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Yunlib.SellBook.Properties.Resources.pic_btn_ShangBooks;
            this.pictureBox6.Location = new System.Drawing.Point(741, 484);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(105, 100);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 41;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            this.pictureBox6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox6_MouseDown);
            this.pictureBox6.MouseEnter += new System.EventHandler(this.pictureBox6_MouseEnter);
            this.pictureBox6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox6_MouseUp);
            // 
            // txtBarCode
            // 
            this.txtBarCode.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.txtBarCode.Location = new System.Drawing.Point(571, 299);
            this.txtBarCode.MaxLength = 20;
            this.txtBarCode.Multiline = true;
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(300, 45);
            this.txtBarCode.TabIndex = 42;
            this.txtBarCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtBarCode_MouseDown);
            // 
            // lblErrMsg
            // 
            this.lblErrMsg.AutoSize = true;
            this.lblErrMsg.BackColor = System.Drawing.Color.White;
            this.lblErrMsg.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblErrMsg.ForeColor = System.Drawing.Color.Red;
            this.lblErrMsg.Location = new System.Drawing.Point(572, 357);
            this.lblErrMsg.Name = "lblErrMsg";
            this.lblErrMsg.Size = new System.Drawing.Size(0, 17);
            this.lblErrMsg.TabIndex = 43;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Yunlib.SellBook.Properties.Resources.pic_btn_openScan;
            this.pictureBox5.Location = new System.Drawing.Point(585, 484);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(105, 100);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 44;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // FillBookNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Yunlib.SellBook.Properties.Resources.BackGround_new1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.lblErrMsg);
            this.Controls.Add(this.txtBarCode);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pic_BtnTurnBack);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FillBookNew";
            this.Text = "FillBookNew";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FillBookNew_FormClosing);
            this.Load += new System.EventHandler(this.FillBookNew_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_BtnTurnBack)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pic_BtnTurnBack;
        private System.Windows.Forms.Label lblGun;
        private System.Windows.Forms.Label lblMiao;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDaoJiShi;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.Label lblErrMsg;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}