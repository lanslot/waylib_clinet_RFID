﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.SellBook.Extensions;
using YunLib.Common;

namespace Yunlib.SellBook
{
    public partial class Form1 : Form
    {
        private string loginKey = ConfigManager.LoginKey;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {

                // var pwd = "{0}{1}{2}{3}{4}".FormatWith(textBox7, textBox8, textBox9, textBox10, loginKey);
                //var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                var signstr = "{0}{1}{2}{3}{4}".FormatWith(textBox7.Text.Trim(), textBox8.Text.Trim(), textBox9.Text.Trim(), textBox10.Text.Trim(), loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                StringBuilder url = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(textBox7.Text.Trim()))
                {
                    url.AppendLine("{0}?{1}={2}&".FormatWith(textBox1.Text,textBox2.Text.Trim(), textBox7.Text.Trim()));
                }
                if (!string.IsNullOrWhiteSpace(textBox8.Text.Trim()))
                {
                    url.AppendLine("{0}={1}&".FormatWith(textBox6.Text.Trim(), textBox8.Text.Trim()));
                }
                if (!string.IsNullOrWhiteSpace(textBox9.Text.Trim()))
                {
                    url.AppendLine("{0}={1}&".FormatWith(textBox5.Text.Trim(), textBox9.Text.Trim()));
                }
                if (!string.IsNullOrWhiteSpace(textBox10.Text.Trim()))
                {
                    url.AppendLine("{0}={1}&".FormatWith(textBox4.Text.Trim(), textBox10.Text.Trim()));
                }
                url.AppendLine("sign={0}".FormatWith(sign));
                string ss = "{0}={1}&{2}={3}&{4}={5}&{6}={7}&sign={8}".FormatWith(textBox2.Text.Trim(), textBox7.Text.Trim(), textBox3.Text.Trim(), textBox8.Text.Trim(), textBox4.Text.Trim(), textBox9.Text.Trim(), textBox5.Text.Trim(), textBox10.Text.Trim(), sign);
                string str = url.ToString();
                var IsLoginSuccess = WebHelper.HttpWebRequest(textBox1.Text, ss, true).ToJObject();
                textBox12.Text = IsLoginSuccess.ToString();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void getValue(string strV)
        {
            if (strV == "Backspace")
            {
                string str = textBox12.Text;
                textBox12.Text = str.Substring(0, str.Length - 1);
            }
            else if (strV == "Clear")
            {
                this.textBox12.Text = "";
            }
            else
            {
                this.textBox12.Text += strV;
            }
        }

        private void textBox12_MouseDown(object sender, MouseEventArgs e)
        {
            DymicJP frm = new DymicJP { TopMost = true, StartPosition = FormStartPosition.CenterScreen };
     
            //frm.fatherform = this;//将当前窗体赋给fatherform
            //frm.getTextHandler += new Form2.GetTextHandler(getValue);//给事件赋值（注意：GetText方法的参数必须与GetTextHandler委托的参数一样，方可委托）
            frm.getTextHandler = getValue;//将方法赋给委托对象
                       frm.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DAMControl dam = new DAMControl();
            dam.OpenDO(1);
           
        }
    }
}

