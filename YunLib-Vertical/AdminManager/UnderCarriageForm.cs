﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.DAL;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.BLL;
using YunLib.Common;
using YunLib_Vertical.ViewModels;

namespace YunLib_Vertical.AdminManager
{
    public partial class UnderCarriageForm : Form
    {
        public static BooKListForm borrowBook;
        BorrowForAcessBLL accessBLL = new BorrowForAcessBLL();

        public int icdev;   // 通讯设备标识符
        public short st;    //函数返回值
        public int Flag { get; set; }
        /// <summary>
        /// 数据总条数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 数据总页数
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// 当前页数
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int PageSize { get; set; }

        string servicePath = ConfigManager.ServicePath;
        string machineId = ConfigManager.MachineID;
        string libraryId = ConfigManager.LibraryID;
        string commonKey = ConfigManager.CommonKey;

        public delegate void MyDelegate(int pageSize, int currentPage, string machId, string libraryId, string keyword = null);
        MyDelegate md = null;
        private NumJPForm jp;

        public UnderCarriageForm()
        {
            InitializeComponent();
            this.md = new MyDelegate(GetData);
            jp = new NumJPForm();
        }

        private void UnderCarriageForm_Load(object sender, EventArgs e)
        {
            try
            {

                //不要默认选中TextBox
                txtBooksCode.SelectionStart = 0;
                txtBooksCode.SelectionLength = 0;

                //CodeOptimize.SolveLightScreen(this);

                DrawListView();

                PageIndex = 1;
                PageSize = 36;
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);



               PlayVoice.play(this.Name);
            }
            catch (Exception ex)
            {
                LogManager.WriteLogs("加载BookList出错", ex.StackTrace, LogFile.Record);
            }
        }

        private void GetData(int pageSize, int currentPage, string machId, string libraryId, string keyword = null)
        {
            try
            {
                var timeStamp = DateTime.Now.Ticks.ToString();

                var signStr = "{0}{1}{2}{3}{4}{5}{6}".FormatWith(currentPage, keyword, libraryId, machId, pageSize, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                var url = "{0}/public/query/page?pageSize={1}&currentPage={2}&machId={3}&libraryId={4}&keyword={5}&timeStamp={6}&sign={7}".FormatWith(
                    servicePath, pageSize, currentPage, machId, libraryId, keyword, timeStamp, sign);

                var res = WebHelper.HttpWebRequest(url, "", true).ToJObject();
                listView1.Clear();
                if (res["code"].ToString() == "100")
                {
                    string str = res["data"]["totalPage"].ToString();
                    List<ListViewModel> list = res["data"]["data"].ToJson().ToList<ListViewModel>();
                    lblTotalPage.Text = str;

                    if (list.Count > 0 && list != null)
                    {
                        List<ShowBooks> newList = new List<ShowBooks>();

                        TotalPage = int.Parse(str);

                        btnLastPage.Enabled = currentPage <= 1 ? false : true;

                        btnNextPage.Enabled = currentPage >= TotalPage ? false : true;

                        lblThisPage.Text = currentPage.ToString();
                        lblTotalPage.Text = TotalPage.ToString();




                        for (int i = 0; i < list.Count; i++)
                        {
                            //if (i >= (currentPage - 1) * 25 && i < (currentPage * 25))
                            //{
                            listView1.Items.Add(new ListViewItem(new string[] { list[i].machLatNum.ToString(), list[i].title, list[i].status, list[i].bookId }, 0));
                            //}
                        }

                        Application.DoEvents();

                        if (PageIndex * 36 > list.Count)
                        {
                            for (int i = 0; i < currentPage * 36 - list.Count; i++)
                            {
                                listView1.Items.Add(new ListViewItem());
                            }
                        }
                    }
                }
                else
                {
                    LogManager.BorrowRecord("{0}--BorrowBook请求错误返回：{1}".FormatWith(DateTime.Now, res.ToString()));
                }
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("{0}--BorrowBook请求报错：{1}".FormatWith(DateTime.Now, ex.Message));
            }
        }

        /// <summary>
        /// 绘制ListView
        /// </summary>
        private void DrawListView()
        {
            listView1.View = View.Tile;

            int w = listView1.Width / 4 - 1;
            int h = listView1.Height / 9 + 1;
            listView1.TileSize = new Size(w, h);
            listView1.OwnerDraw = true;
            listView1.Columns.AddRange(new ColumnHeader[]
            {
                    new ColumnHeader(), new ColumnHeader(), new ColumnHeader()
            });
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in this.listView1.SelectedItems)
                    {
                        if (item.SubItems[2].Text == "1")
                        {
                            //AlertFrm frm = new AlertFrm { TopMost = true, StartPosition = FormStartPosition.CenterScreen };

                            //frm.ShowDialog();
                            MessageBox.Show("该书已预约");
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Text)) //判断柜号是否为空
                            {
                                string doorNum = item.Text;


                                long timeStamp = DateTime.Now.Ticks;

                                string signstr = "{0}{1}{2}{3}".FormatWith(item.SubItems[3].Text, libraryId, timeStamp, commonKey);
                                string sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());

                                string url = "{0}/public/book/undercarriage?bookId={1}&libraryId={2}&timeStamp={3}&sign={4}".FormatWith(servicePath, item.SubItems[3].Text, libraryId, timeStamp, sign);

                                string[] s = new string[1];
                                s[0] = ConfigManager.PingServicePath;

                                
                                    JObject IsLoginSuccess = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                                    if (IsLoginSuccess["code"].ToString() == "100")
                                    {

                                        var list = accessBLL.GetBookByDoorNum(int.Parse(doorNum));
                                        CodeOptimize.OpenDoor(list[0].DoorID, list[0].DoorCode);

                                        //   MessageBox.Show("下架书籍成功,书籍名称：{0}".FormatWith(book.BName), "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                                        if (string.IsNullOrWhiteSpace(lblQuery.Text))
                                        {
                                            Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
                                        }
                                        else
                                        {
                                            Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
                                        }


                                    }
                                    else
                                    {
                                        MessageBox.Show("下架失败");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("下架失败");
                                }
                            }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("请稍后再试，系统繁忙", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// 绘制ListView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            var subitem = e.Item.SubItems;

            //获得行号
            var y = e.ItemIndex / 4;

            using (Graphics g = e.Graphics)
            {
                Rectangle rect = Rectangle.Empty;

                rect = new Rectangle(e.Bounds.Left - 1, e.Bounds.Top - 3, e.Item.Bounds.Width + 4, e.Bounds.Height + 2);

                //绘制背景色
                if ((e.ItemIndex + 1) % 2 == 0)
                {
                    using (LinearGradientBrush brush = new LinearGradientBrush(rect,
                                                                               Color.FromArgb(255, 209, 217, 248),
                                                                               Color.FromArgb(255, 209, 217, 248),
                                                                               LinearGradientMode.Horizontal))
                    {
                        e.Graphics.FillRectangle(brush, rect);
                    }
                }
                else
                {
                    using (LinearGradientBrush brush = new LinearGradientBrush(rect,
                                                                              Color.FromArgb(255, 221, 227, 247),
                                                                              Color.FromArgb(255, 221, 227, 247),
                                                                              LinearGradientMode.Horizontal))
                    {
                        e.Graphics.FillRectangle(brush, rect);
                    }
                }

                //绘制边框
                g.DrawLine(new Pen(Color.FromArgb(255, 235, 235, 235), 1), e.Item.Bounds.Right + 4, e.Bounds.Top - 3, e.Item.Bounds.Right + 4, e.Bounds.Bottom);

                g.DrawLine(new Pen(Color.FromArgb(255, 255, 255, 255), 1), e.Item.Bounds.Left, e.Bounds.Bottom, e.Item.Bounds.Right + 4, e.Bounds.Bottom);



                Graphics graphics = CreateGraphics();

                //判断item是否有值
                if (subitem.Count > 1)
                {
                    SizeF sizeH = graphics.MeasureString(subitem[0].Text, new Font("微软雅黑", 14));

                    float w = (e.Bounds.Width - sizeH.Width) / 2;

                    graphics.Dispose();

                    if (subitem[2].Text == "1")
                    {
                        g.DrawString(subitem[0].Text, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DeepSkyBlue, e.Bounds.Left + w, e.Bounds.Height - 70 + e.Bounds.Height * y);
                        g.DrawString("预", new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DeepSkyBlue, e.Bounds.Left + 10, e.Bounds.Height - 70 + e.Bounds.Height * y);
                    }
                    else
                    {
                        g.DrawString(subitem[0].Text, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DarkSeaGreen, e.Bounds.Left + w, e.Bounds.Height - 70 + e.Bounds.Height * y);
                        //绘制书籍状态
                        if (subitem[3].Text == "2")
                        {
                            g.DrawString("售", new Font("微软雅黑", 14, FontStyle.Regular), Brushes.Red, e.Bounds.Left + 10, e.Bounds.Height - 70 + e.Bounds.Height * y);
                        }
                        else
                        {
                            g.DrawString("借", new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DarkSeaGreen, e.Bounds.Left + 10, e.Bounds.Height - 70 + e.Bounds.Height * y);
                        }
                    }


                    string subText = subitem[1].Text;

                    Graphics graphics1 = CreateGraphics();

                    SizeF sizeF = graphics1.MeasureString(subText, new Font("微软雅黑", 14));

                    float m = (e.Bounds.Width - sizeF.Width) / 2;

                    graphics1.Dispose();

                    //获得行号
                    var n = e.ItemIndex / 5;

                    if (subitem[2].Text == "1")
                    {
                        if (subText.Length > 8)
                        {
                            subText = subText.Substring(0, 7) + "...";
                        }

                        g.DrawString(subText, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.DeepSkyBlue, e.Bounds.Left + 30, e.Bounds.Height - 36 + e.Bounds.Height * y);
                    }
                    else
                    {
                        if (m >= 30)
                        {
                            //绘制书名
                            g.DrawString(subText, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.Black, e.Bounds.Left + m, e.Bounds.Height - 36 + e.Bounds.Height * y);
                        }
                        else
                        {
                            if (subText[0] < 127 && subText[0] > 0)
                                subText = "丨" + subText.Replace("?", "");

                            byte[] strToBytes = Encoding.Default.GetBytes(subText);

                            string str = Encoding.Default.GetString(strToBytes, 0, 14);

                            str = str.Replace("?", "") + "...";

                            g.DrawString(str, new Font("微软雅黑", 14, FontStyle.Regular), Brushes.Black, e.Bounds.Left + 30, e.Bounds.Height - 36 + e.Bounds.Height * y);
                        }
                    }


                }
            }
        }

        #region 返回按钮事件

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Properties.Resources.hui_w;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox1.Image = Properties.Resources.hui_x;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            BackMain frm = new BackMain();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        #endregion

        private void pBtnLastPage_Click(object sender, EventArgs e)
        {

            PageIndex -= 1;
            lblThisPage.Text = PageIndex + "";

            this.listView1.Clear();
            if (string.IsNullOrWhiteSpace(lblQuery.Text))
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
            }
            else
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
            }

            // Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void pBtnNextPage_Click(object sender, EventArgs e)
        {


            PageIndex += 1;

            lblThisPage.Text = PageIndex + "";

            this.listView1.Clear();
            if (string.IsNullOrWhiteSpace(lblQuery.Text))
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
            }
            else
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
            }


            //Invoke(md, PageSize, PageIndex, machineId, libraryId, lblQuery.Text);
        }

        private void pBtn_Search_Click(object sender, EventArgs e)
        {
            try
            {
                string query = txtBooksCode.Text;

                if (string.IsNullOrWhiteSpace(query))
                {
                    MessageBox.Show("请输入柜门编号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (query.Equals("请输入书柜号"))
                {
                    MessageBox.Show("请输入柜门编号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (query.Length >= 4)
                    {
                        MessageBox.Show("你输入正确的柜门号", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        string[] s = new string[1];
                        //验证网络连接状态，true就发起请求，false不请求

                        var timeStamp = DateTime.Now.Ticks.ToString();

                        var signStr = "{0}{1}{2}{3}".FormatWith(machineId, query, timeStamp, commonKey);
                        var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                        var url = "{0}/public/book/bookInfo?machId={1}&machLatNum={2}&timeStamp={3}&sign={4}".FormatWith(
                            servicePath, machineId, query, timeStamp, sign);

                        var callback = WebHelper.HttpWebRequest(url, "", true).ToJObject();


                        if (callback["code"].ToString() == "100")
                        {
                            listView1.Clear();

                            List<ShowBooks> newList = new List<ShowBooks>();

                            TotalPage = 1;

                            btnLastPage.Enabled = false;

                            btnNextPage.Enabled = false;

                            lblThisPage.Text = "1";
                            lblTotalPage.Text = "1";
                          




                            listView1.Items.Add(new ListViewItem(new string[] { query, callback["data"]["title"].ToString(), callback["data"]["status"].ToString(), callback["data"]["bookId"].ToString() }, 0));


                        }
                        else
                        {
                            MessageBox.Show(callback["msg"].ToString());
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("柜号查询报错：" + ex.Message);
            }

        }

        private void toolStripButton29_Click(object sender, EventArgs e)
        {
            PageIndex = 1;
            ToolStripButton b = sender as ToolStripButton;
            this.BackColor = Color.SteelBlue;
            string strText = "";
            if (b.Text != "全部")
            {
                lblQuery.Text = b.Text;
                Invoke(md, PageSize, PageIndex, machineId, libraryId, b.Text);
            }
            else
            {
                Invoke(md, PageSize, PageIndex, machineId, libraryId, null);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //    int time = int.Parse(lblCount.Text);

            //    time--;
            //    lblCount.Text = time.ToString();
            //    if (time <= 0)
            //    {
            //        MainForm frm = new MainForm();
            //        frm.MdiParent = ParentForm;
            //        frm.Parent = this.Parent;
            //        frm.Dock = DockStyle.Fill;

            //        this.Close();

            //        frm.Show();
            //    }
        }

        private void BooKListForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Enabled = false;
            timer1.Stop();
            jp.Close();
        }

        private void txtBooksCode_MouseDown(object sender, MouseEventArgs e)
        {
            txtBooksCode.Text = "";
            jp.Location = new Point(txtBooksCode.Location.X, txtBooksCode.Location.Y + txtBooksCode.Height + 791);
            jp.Show();
            txtBooksCode.Focus();

            this.Activate();

        }
    }
}
