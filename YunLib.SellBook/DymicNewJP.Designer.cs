﻿namespace Yunlib.SellBook
{
    partial class DymicNewJP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtContent = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Label();
            this.btn4 = new System.Windows.Forms.Label();
            this.btn7 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Label();
            this.btn8 = new System.Windows.Forms.Label();
            this.btn5 = new System.Windows.Forms.Label();
            this.btn00 = new System.Windows.Forms.Label();
            this.btn3 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Label();
            this.btn9 = new System.Windows.Forms.Label();
            this.btn6 = new System.Windows.Forms.Label();
            this.btnSure = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picBtnClose = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // txtContent
            // 
            this.txtContent.Enabled = false;
            this.txtContent.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.txtContent.Location = new System.Drawing.Point(22, 34);
            this.txtContent.MaxLength = 20;
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.PasswordChar = '*';
            this.txtContent.Size = new System.Drawing.Size(195, 34);
            this.txtContent.TabIndex = 1;
            this.txtContent.TabStop = false;
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn1.ForeColor = System.Drawing.Color.Black;
            this.btn1.Location = new System.Drawing.Point(25, 93);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(82, 57);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            this.btn1.DoubleClick += new System.EventHandler(this.btn1_DoubleClick);
            this.btn1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn4.ForeColor = System.Drawing.Color.Black;
            this.btn4.Location = new System.Drawing.Point(25, 150);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(82, 57);
            this.btn4.TabIndex = 1;
            this.btn4.Text = "4";
            this.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            this.btn4.DoubleClick += new System.EventHandler(this.btn4_DoubleClick);
            this.btn4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn7.ForeColor = System.Drawing.Color.Black;
            this.btn7.Location = new System.Drawing.Point(25, 207);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(82, 57);
            this.btn7.TabIndex = 1;
            this.btn7.Text = "7";
            this.btn7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            this.btn7.DoubleClick += new System.EventHandler(this.btn7_DoubleClick);
            this.btn7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.btnClear.ForeColor = System.Drawing.Color.Orange;
            this.btnClear.Location = new System.Drawing.Point(25, 264);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(82, 57);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "清空";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnClear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btnClear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn2.ForeColor = System.Drawing.Color.Black;
            this.btn2.Location = new System.Drawing.Point(107, 93);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(82, 57);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            this.btn2.DoubleClick += new System.EventHandler(this.btn2_DoubleClick);
            this.btn2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn8.ForeColor = System.Drawing.Color.Black;
            this.btn8.Location = new System.Drawing.Point(107, 207);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(82, 57);
            this.btn8.TabIndex = 1;
            this.btn8.Text = "8";
            this.btn8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            this.btn8.DoubleClick += new System.EventHandler(this.btn8_DoubleClick);
            this.btn8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn5.ForeColor = System.Drawing.Color.Black;
            this.btn5.Location = new System.Drawing.Point(107, 150);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(82, 57);
            this.btn5.TabIndex = 1;
            this.btn5.Text = "5";
            this.btn5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            this.btn5.DoubleClick += new System.EventHandler(this.btn5_DoubleClick);
            this.btn5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn00
            // 
            this.btn00.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn00.ForeColor = System.Drawing.Color.Black;
            this.btn00.Location = new System.Drawing.Point(107, 264);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(82, 57);
            this.btn00.TabIndex = 1;
            this.btn00.Text = "0";
            this.btn00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn00.Click += new System.EventHandler(this.btn00_Click);
            this.btn00.DoubleClick += new System.EventHandler(this.btn00_DoubleClick);
            this.btn00.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn3.ForeColor = System.Drawing.Color.Black;
            this.btn3.Location = new System.Drawing.Point(189, 93);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(82, 57);
            this.btn3.TabIndex = 1;
            this.btn3.Text = "3";
            this.btn3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            this.btn3.DoubleClick += new System.EventHandler(this.btn3_DoubleClick);
            this.btn3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.Gray;
            this.btnCancel.Location = new System.Drawing.Point(222, 24);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 54);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "←退格";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.DoubleClick += new System.EventHandler(this.btnCancel_DoubleClick);
            this.btnCancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btnCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn9.ForeColor = System.Drawing.Color.Black;
            this.btn9.Location = new System.Drawing.Point(189, 207);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(82, 57);
            this.btn9.TabIndex = 1;
            this.btn9.Text = "9";
            this.btn9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            this.btn9.DoubleClick += new System.EventHandler(this.btn9_DoubleClick);
            this.btn9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn6.ForeColor = System.Drawing.Color.Black;
            this.btn6.Location = new System.Drawing.Point(189, 150);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(82, 57);
            this.btn6.TabIndex = 1;
            this.btn6.Text = "6";
            this.btn6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            this.btn6.DoubleClick += new System.EventHandler(this.btn6_DoubleClick);
            this.btn6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btn6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // btnSure
            // 
            this.btnSure.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.btnSure.ForeColor = System.Drawing.Color.Green;
            this.btnSure.Location = new System.Drawing.Point(189, 264);
            this.btnSure.Name = "btnSure";
            this.btnSure.Size = new System.Drawing.Size(82, 57);
            this.btnSure.TabIndex = 1;
            this.btnSure.Text = "确认";
            this.btnSure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSure.Click += new System.EventHandler(this.btnSure_Click);
            this.btnSure.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_MouseDown);
            this.btnSure.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn6_MouseUp);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnSure);
            this.panel1.Controls.Add(this.btn6);
            this.panel1.Controls.Add(this.btn9);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btn3);
            this.panel1.Controls.Add(this.btn00);
            this.panel1.Controls.Add(this.btn5);
            this.panel1.Controls.Add(this.btn8);
            this.panel1.Controls.Add(this.btn2);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btn7);
            this.panel1.Controls.Add(this.btn4);
            this.panel1.Controls.Add(this.btn1);
            this.panel1.Controls.Add(this.txtContent);
            this.panel1.Location = new System.Drawing.Point(12, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(305, 335);
            this.panel1.TabIndex = 0;
            // 
            // picBtnClose
            // 
            this.picBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.picBtnClose.Image = global::Yunlib.SellBook.Properties.Resources.btnX;
            this.picBtnClose.Location = new System.Drawing.Point(286, 3);
            this.picBtnClose.Name = "picBtnClose";
            this.picBtnClose.Size = new System.Drawing.Size(27, 27);
            this.picBtnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBtnClose.TabIndex = 1;
            this.picBtnClose.TabStop = false;
            this.picBtnClose.Click += new System.EventHandler(this.picBtnClose_Click);
            // 
            // DymicNewJP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(37)))), ((int)(((byte)(91)))));
            this.ClientSize = new System.Drawing.Size(329, 386);
            this.Controls.Add(this.picBtnClose);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DymicNewJP";
            this.Text = "DymicNewJP";
            this.Load += new System.EventHandler(this.DymicNewJP_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBtnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Label btn1;
        private System.Windows.Forms.Label btn4;
        private System.Windows.Forms.Label btn7;
        private System.Windows.Forms.Label btnClear;
        private System.Windows.Forms.Label btn2;
        private System.Windows.Forms.Label btn8;
        private System.Windows.Forms.Label btn5;
        private System.Windows.Forms.Label btn00;
        private System.Windows.Forms.Label btn3;
        private System.Windows.Forms.Label btnCancel;
        private System.Windows.Forms.Label btn9;
        private System.Windows.Forms.Label btn6;
        private System.Windows.Forms.Label btnSure;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picBtnClose;
    }
}