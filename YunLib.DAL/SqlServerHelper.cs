﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;
using System.Data;

namespace Yunlib.DAL
{
    public class SqlServerHelper
    {
        //public static readonly string ConnectionString = @"Data Source=192.168.0.128;Initial Catalog=YunLibDB5;Persist Security Info=True;User ID=sa;Password=jj916666";
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["WayLibDB"].ConnectionString.ToString();


        public static int ExecuteNonQuery(String sqlText)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sqlText;
                int value = cmd.ExecuteNonQuery();
                return value;
            }
        }
        public static SqlDataReader ExecuteReader(String sqlText, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sqlText;
                SqlDataReader reader = cmd.ExecuteReader();
                return reader;
            }
            catch (Exception e)
            {

                conn.Close();
                throw e;
            }
        }

        public static DataSet ExecuteDataSet(String sqlText)
        {
            SqlCommand cmd = new SqlCommand();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sqlText;
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                    return ds;
                }
            }
        }

        public static DataTable ExecuteDataTable(String sqlText)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sqlText;
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.SelectCommand = cmd;
                    da.Fill(dt);
                    return dt;
                }
            }
        }

        public static int getSingleInt(String sqlText)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(ConnectionString);
            try
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = sqlText;
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                if (reader.IsDBNull(0))
                    return 0;
                else
                    return reader.GetInt32(0);
            }
            catch (Exception e)
            {
                conn.Close();
                return -1;
            }
            finally { conn.Close(); }
        }
    }
}