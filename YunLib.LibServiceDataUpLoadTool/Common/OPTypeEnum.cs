﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.LibServiceDataUpLoadTool.Common
{
    public enum OPTypeEnum
    {
        /// <summary>
        ///文献外借
        /// </summary>
        [Description("30001")]
        LendBook = 1,
        /// <summary>
        /// 文献归还
        /// </summary>
        [Description("30002")]
        ReturnBook = 2,
        /// <summary>
        /// 文献续借
        /// </summary>
        [Description("30003")]
        ReLend = 3,
        /// <summary>
        /// 无证外借
        /// </summary>
        [Description("30001")]
        NoUserLend = 4,
        /// <summary>
        /// 读者办证
        /// </summary>
        [Description("30001")]
        AddUser = 5,
        /// <summary>
        /// 读者退证
        /// </summary>
        [Description("30002")]
        CalCard = 6,
        /// <summary>
        /// 编辑读者
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateUser = 7,
        /// <summary>
        /// 删除读者
        /// </summary>
        [Description("30113")]
        DelUser = 8,
        /// <summary>
        /// 修改读者单位
        /// </summary>
        [Description("30001")]
        UpdateUserUnit = 9,
        /// <summary>
        /// 修改读者状态
        /// </summary>
        [Description("30002")]
        UpdateUserStatus = 10,
        /// <summary>
        /// 修改读者密码
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateUserPWD = 11,
        /// <summary>
        /// 修改读者截至日期
        /// </summary>
        [Description("30113")]
        UpdateUserEndTime = 12,
        /// <summary>
        /// 修改读者类型
        /// </summary>
        [Description("30001")]
        ChangeUserType = 13,
        /// <summary>
        /// 读者换证
        /// </summary>
        [Description("30002")]
        UserChangeCard = 14,
        /// <summary>
        /// 新增读者单位
        /// </summary>
        [Description("FACT_LOAN")]
        AddUnit = 15,
        /// <summary>
        /// 修改读者单位
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateUnit = 16,
        /// <summary>
        /// 删除读者单位
        /// </summary>
        [Description("FACT_LOAN")]
        DelUnit = 17,
        /// <summary>
        /// 导入读者
        /// </summary>
        [Description("FACT_LOAN")]
        ImportUser = 18,

        /// <summary>
        /// 新增读者类型定义
        /// </summary>
        [Description("FACT_LOAN")]
        AddUserType = 19,
        /// <summary>
        /// 修改读者类型定义
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateUserType = 20,

        /// <summary>
        /// 文献流通类型-新增
        /// </summary>
        [Description("FACT_LOAN")]
        AddWXLTType = 21,

        /// <summary>
        /// 文献流通类型-编辑
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateWXLTType = 22,
        /// <summary>
        /// 文献流通类型-删除
        /// </summary>
        [Description("FACT_LOAN")]
        DelWXLTType = 23,

        /// <summary>
        /// 流通规则-新增
        /// </summary>
        [Description("FACT_LOAN")]
        AddLTGZ = 24,

        /// <summary>
        /// 流通规则-编辑
        /// </summary>
        [Description("FACT_LOAN")]
        UpdateLTGZ = 25,
        /// <summary>
        /// 流通规则-删除
        /// </summary>
        [Description("FACT_LOAN")]
        DelLTGZ = 26,
        /// <summary>
        /// 收费
        /// </summary>
        [Description("FACT_LOAN")]
        Charge = 32,
        /// <summary>
        /// 退费
        /// </summary>
        [Description("FACT_LOAN")]
        Refund = 33,

    }
}
