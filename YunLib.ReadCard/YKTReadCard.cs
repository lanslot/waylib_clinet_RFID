﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.DAL;
using YunLib.Common;
using YunLib.ReadCard.HF;
using YunLib.ReadCard.UHF;

namespace YunLib.ReadCard
{
    /// <summary>
    /// 一卡通读卡
    /// </summary>
    public class YKTReadCard : IReadCard
    {
        //打开端口 

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Open")]
        public static extern int ICC_Reader_Open(int port, int buod);



        //关闭端口
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Close")]
        public static extern int ICC_Reader_Close(int ReaderHandle);

        [DllImport("SSSE32.dll", EntryPoint = "ICC_PosBeep")]
        public static extern int ICC_PosBeep(int ReaderHandle, byte time);/*蜂鸣*/

        //注意：输入的是12位的密钥，例如12个f，但是password必须是6个字节的密钥，需要用StrToHex函数处理。
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Authentication_Pass")]
        public static extern int PICC_Reader_Authentication_Pass(int ReaderHandle,
                                                                    byte Mode,
                                                                    byte SecNr,
                                                                    byte[] PassWord);
        //读卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Read")]
        public static extern int PICC_Reader_Read(int ReaderHandle, byte Addr,
                                                        byte[] Data);
        //写卡
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Write")]
        public static extern int PICC_Reader_Write(int ReaderHandle, byte Addr,
                                                        byte[] Data);
        /// <summary>
        /// 读物理卡号
        /// </summary>
        /// <param name="ReaderHandle"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_GetUID")]
        public static extern int PICC_Reader_GetUID(int ReaderHandle, byte[] uid);

        //将字符命令流转为16进制流
        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(StringBuilder strIn, int len, Byte[] HexOut);

        //将16进制流命令转为字符流
        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(Byte[] strIn, int inLen,
                                                StringBuilder strOut);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_PowerOnTypeA")]
        public static extern int PICC_Reader_PowerOnTypeA(int ReaderHandle, byte[] Response);//上电 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_pre_PowerOn")]
        public static extern int ICC_Reader_pre_PowerOn(int ReaderHandle, byte SLOT, byte[] Response);//上电 返回数据长度 失败小于0
        [DllImport("SSSE32.dll", EntryPoint = "ICC_Reader_Application")]
        public static extern int ICC_Reader_Application(int ReaderHandle, byte SLOT, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_Application")]
        public static extern int PICC_Reader_Application(int ReaderHandle, int Lenth_of_Command_APDU, byte[] Command_APDU, byte[] Response_APDU);  //type a/b执行apdu命令 返回数据长度 失败小于0

        //HD100身份证
        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ReadIDMsg")]
        public static extern int PICC_Reader_ReadIDMsg(int RHandle, StringBuilder pBmpFile, StringBuilder pName, StringBuilder pSex, StringBuilder pNation, StringBuilder pBirth, StringBuilder pAddress, StringBuilder pCertNo, StringBuilder pDepartment, StringBuilder pEffectData, StringBuilder pExpire, StringBuilder pErrMsg);

        [DllImport("SSSE32.dll", EntryPoint = "PICC_Reader_ID_ReadUID")]
        public static extern int PICC_Reader_ID_ReadUID(int fd, byte[] UID);//获取身份证UID, 8Byte

        [DllImport("SSSE32.dll", EntryPoint = "HexToStr")]
        public static extern int HexToStr(byte[] strIn, int inLen, byte[] strOut);


        [DllImport("SSSE32.dll", EntryPoint = "StrToHex")]
        public static extern int StrToHex(byte[] strIn, int inLen, byte[] strOut);

        private byte fComAdr = 0xff; //当前操作的ComAdr
        private int ferrorcode;
        private byte fBaud;
        private int fCmdRet = 30; //所有执行指令的返回值
        private byte[] fOperEPC = new byte[100];
        private byte[] fPassWord = new byte[4];
        private byte[] fOperID_6B = new byte[10];
        private int frmcomportindex;
        private byte Target = 0;
        private byte InAnt = 0;
        private byte Scantime = 0;
        private byte FastFlag = 0;
        private byte Qvalue = 0;
        private byte Session = 0;
        private byte TIDFlag = 0;
        public static byte antinfo = 0;
        private int AA_times = 0;
        private int FrmPortIndex = 0;

        public string ReadIDCard(string port, int Baud)
        {
            //该函数封装了两种不同协议的身份证操作，根据情况选择
            //HD100读身份证
            StringBuilder pBmpFile = new StringBuilder(100);
            StringBuilder pFingerData = new StringBuilder(1025);
            StringBuilder pBmpData = new StringBuilder(77725);
            StringBuilder pBase64Data = new StringBuilder(6025);
            StringBuilder pName = new StringBuilder(100);
            StringBuilder pSex = new StringBuilder(100);
            StringBuilder pNation = new StringBuilder(100);
            StringBuilder pBirth = new StringBuilder(100);
            StringBuilder pAddress = new StringBuilder(400);
            StringBuilder pCertNo = new StringBuilder(100);
            StringBuilder pDepartment = new StringBuilder(100);
            StringBuilder pEffectData = new StringBuilder(100);
            StringBuilder pExpire = new StringBuilder(100);
            StringBuilder pData = new StringBuilder(100);
            StringBuilder pErrMsg = new StringBuilder(100);
            StringBuilder strUID = new StringBuilder(100);
            byte[] uid = new byte[20];

            string str = System.Environment.CurrentDirectory;
            pBmpFile.Append(str);
            pBmpFile.Append(@"\zp.bmp");


            int Rhandle;
            int ret;

            //  String a = textBox1.Text;
            int p = int.Parse(port.Substring(3));

            Rhandle = ICC_Reader_Open(p, Baud);
            if (Rhandle < 0)
            {
                // //listInfo.Items.Add("连接失败!");
                LogManager.ErrorRecord("身份证读卡连接失败,端口：" + port);
                ICC_Reader_Close(Rhandle);
                return "";
            }
            else
            {
                // //listInfo.Items.Add("连接成功!:" + Rhandle);
            }
            //该函数获取身份证信息的同时保存照片到指定路径
            ret = PICC_Reader_ReadIDMsg(Rhandle, pBmpFile, pName, pSex, pNation, pBirth, pAddress, pCertNo, pDepartment, pEffectData, pExpire, pErrMsg);
            //  ret = PICC_Reader_ID_ReadUID(Rhandle, uid);
            ICC_Reader_Close(Rhandle);
            if (ret != 0)
            {
                //读卡失败
               // ICC_Reader_Close(Rhandle);
                // //listInfo.Items.Add("读卡失败!");
                LogManager.ErrorRecord("身份证读卡失败");
                return "";
            }
            else
            {
                ////listInfo.Items.Add("姓名：" + pName);
                ////listInfo.Items.Add("性别：" + pSex);
                ////listInfo.Items.Add("民族：" + pNation);
                ////listInfo.Items.Add("身份证号：" + pCertNo);
                ////listInfo.Items.Add("地址：" + pAddress);

                return pCertNo.ToString();
            }
            //  HexToStr(uid, 8, strUID);
            // ICC_Reader_Close(Rhandle);
        }


        public string ReadIM(string port, int Baud)
        {
            try
            {
                LogManager.WriteLogs("M1", "开始", LogFile.Record);
                //M1卡操作示例
                int Rhandle;
                int ret;
                int p = int.Parse(port.Substring(3));

                Rhandle = ICC_Reader_Open(p, Baud);
                if (Rhandle < 0)
                {
                    //listInfo.Items.Add("M1连接失败!");
                    LogManager.WriteLogs("Rhandle", "M1连接失败", LogFile.Record);
                    return "";
                }

                byte[] UID = new byte[4];

                ret = PICC_Reader_GetUID(Rhandle, UID);
                if (ret <= 0)
                {
                    ICC_Reader_Close(Rhandle);
                    LogManager.WriteLogs("UID", "获取uid失败，返回值："+ret, LogFile.Record);
                    string str2 = BitConverter.ToString(UID);
                    LogManager.WriteLogs("UID", str2, LogFile.Record);
                    return "";
                }
                ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
                ICC_Reader_Close(Rhandle);

                string str = BitConverter.ToString(UID);
                LogManager.WriteLogs("UID", str, LogFile.Record);
                var ss = str.Split('-');
                string str1 = "";
                for (int i = ss.Length - 1; i >= 0; i--)
                {
                    str1 += ss[i];
                }
                long num = long.Parse(str1, System.Globalization.NumberStyles.HexNumber);

                string resStr = num.ToString();
                string newStr = "";
                if (resStr.Length < 10)
                {
                    for (int i = 0; i < 10 - resStr.Length; i++)
                    {
                        newStr += "0";
                    }
                }
                newStr += resStr;

                return newStr;
            }
            catch (Exception ex)
            {
                //listInfo.Items.Add(ex.Message);

                LogManager.ErrorRecord("{0}-读IM卡出现错误：{1}".FormatWith(DateTime.Now, ex.Message));
                return "";
            }
        }


        public string ReadRFID(string port, int Baud)
        {
            //15693卡操作示例
            int Rhandle;
            int ret;

            //连接读卡器
            int p = int.Parse(port.Substring(3));

            Rhandle = SSSE32.ICC_Reader_Open(p, Baud);
            if (Rhandle < 0)
            {
                // listInfo.Items.Add("连接失败!");

                return "";
            }
            //else
            //{
            //    //listInfo.Items.Add("连接成功!:" + Rhandle);
            //}
            byte[] cardId = new byte[8];
            ret = SSSE32.PICC_Reader_Inventory(Rhandle, cardId);

            if (ret <= 0)
            {
                // listInfo.Items.Add("寻卡失败!ret=" + ret);
                SSSE32.ICC_Reader_Close(Rhandle);
                return "";
            }
            //else
            //{
            //    listInfo.Items.Add("寻卡成功!ret=" + ret + "**卡号：" + BitConverter.ToString(cardId));
            //}
            byte[] hexData = new byte[4];
            byte[] rec = new byte[20];
            int len = 0;
            for (int i = 10; i <= 14; i++)
            {
                ret = SSSE32.PICC_Reader_15693_Read(Rhandle, (byte)i, hexData);
                if (ret <= 0)
                {
                    //  listInfo.Items.Add("读卡" + i + "块失败!ret=" + ret);
                    SSSE32.ICC_Reader_Close(Rhandle);
                    return "";
                }
                else
                {
                    // listInfo.Items.Add("读卡" + i + "块成功!ret:" + ret + "***数据：" + BitConverter.ToString(hexData));
                    //读标签成功处理数据
                    hexData.CopyTo(rec, len);
                    len += 4;
                }
            }

            SSSE32.ICC_PosBeep(Rhandle, 0x0a);//蜂鸣，可用可不用
            SSSE32.ICC_Reader_Close(Rhandle);

            int strLen = (int)rec[2];

            var ss = rec.Skip(3).Take(strLen).ToArray();

            string str = BitConverter.ToString(ss).Replace("-", "");
            long x = Convert.ToInt64(str, 16);

            return x + "";
        }

        public string ReadUHFRFID(string port, int Baud)
        {
            try
            {
                int portNum = int.Parse(port.Substring(3));
                // int FrmPortIndex = 0;
                string strException = string.Empty;
                fBaud = Convert.ToByte(3);
                if (fBaud > 2)
                    fBaud = Convert.ToByte(fBaud + 2);
                fComAdr = 255;//广播地址打开设备
                fCmdRet = RWDev.OpenComPort(portNum, ref fComAdr, fBaud, ref FrmPortIndex);
                if (fCmdRet != 0)
                {
                    string strLog = "连接读写器失败，失败原因： " + GetReturnCodeDesc(fCmdRet);

                    return "";
                }
                else
                {
                    frmcomportindex = FrmPortIndex;

                }
                return inventory();
            }
            catch (Exception ex)
            {
                LogManager.BorrowRecord("超高频读条码错误：" + ex.Message);
                return "";

            }
        }

        public static byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public static string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        private string GetReturnCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "操作成功";
                case 0x01:
                    return "询查时间结束前返回";
                case 0x02:
                    return "指定的询查时间溢出";
                case 0x03:
                    return "本条消息之后，还有消息";
                case 0x04:
                    return "读写模块存储空间已满";
                case 0x05:
                    return "访问密码错误";
                case 0x09:
                    return "销毁密码错误";
                case 0x0a:
                    return "销毁密码不能为全0";
                case 0x0b:
                    return "电子标签不支持该命令";
                case 0x0c:
                    return "对该命令，访问密码不能为全0";
                case 0x0d:
                    return "电子标签已经被设置了读保护，不能再次设置";
                case 0x0e:
                    return "电子标签没有被设置读保护，不需要解锁";
                case 0x10:
                    return "有字节空间被锁定，写入失败";
                case 0x11:
                    return "不能锁定";
                case 0x12:
                    return "已经锁定，不能再次锁定";
                case 0x13:
                    return "参数保存失败,但设置的值在读写模块断电前有效";
                case 0x14:
                    return "无法调整";
                case 0x15:
                    return "询查时间结束前返回";
                case 0x16:
                    return "指定的询查时间溢出";
                case 0x17:
                    return "本条消息之后，还有消息";
                case 0x18:
                    return "读写模块存储空间已满";
                case 0x19:
                    return "电子不支持该命令或者访问密码不能为0";
                case 0x1A:
                    return "标签自定义功能执行错误";
                case 0xF8:
                    return "检测天线错误";
                case 0xF9:
                    return "命令执行出错";
                case 0xFA:
                    return "有电子标签，但通信不畅，无法操作";
                case 0xFB:
                    return "无电子标签可操作";
                case 0xFC:
                    return "电子标签返回错误代码";
                case 0xFD:
                    return "命令长度错误";
                case 0xFE:
                    return "不合法的命令";
                case 0xFF:
                    return "参数错误";
                case 0x30:
                    return "通讯错误";
                case 0x31:
                    return "CRC校验错误";
                case 0x32:
                    return "返回数据长度有错误";
                case 0x33:
                    return "通讯繁忙，设备正在执行其他指令";
                case 0x34:
                    return "繁忙，指令正在执行";
                case 0x35:
                    return "端口已打开";
                case 0x36:
                    return "端口已关闭";
                case 0x37:
                    return "无效句柄";
                case 0x38:
                    return "无效端口";
                case 0xEE:
                    return "读写器处于主动模式";
                default:
                    return "";
            }
        }
        private string GetErrorCodeDesc(int cmdRet)
        {
            switch (cmdRet)
            {
                case 0x00:
                    return "其它错误";
                case 0x03:
                    return "存储器超限或不被支持的PC值";
                case 0x04:
                    return "存储器锁定";
                case 0x0b:
                    return "电源不足";
                case 0x0f:
                    return "非特定错误";
                default:
                    return "";
            }
        }

        private string inventory()
        {
            try
            {
                byte Ant = 0;
                int CardNum = 0;
                int Totallen = 0;
                int EPClen, m;
                byte[] EPC = new byte[50000];
                int CardIndex;
                string temps, temp;
                temp = "";
                string sEPC;
                byte MaskMem = 0;
                byte[] MaskAdr = new byte[2];
                byte MaskLen = 0;
                byte[] MaskData = new byte[100];
                byte MaskFlag = 0;
                byte AdrTID = 0;
                byte LenTID = 0;
                AdrTID = 0;
                LenTID = 6;
                MaskFlag = 0;
                int cbtime = System.Environment.TickCount;
                CardNum = 0;
                fCmdRet = RWDev.Inventory_G2(ref fComAdr, Qvalue, Session, MaskMem, MaskAdr, MaskLen, MaskData, MaskFlag, AdrTID, LenTID, TIDFlag, Target, InAnt, Scantime, FastFlag, EPC, ref Ant, ref Totallen, ref CardNum, frmcomportindex);

                string strLog = "询查命令： " + GetReturnCodeDesc(fCmdRet);

                ///////////设置网络断线重连
                if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4))//代表已查找结束，
                {
                    byte[] daw = new byte[Totallen];
                    Array.Copy(EPC, daw, Totallen);
                    temps = ByteArrayToHexString(daw);
                    m = 0;
                    if (CardNum == 0)
                    {
                        if (Session > 1)
                            AA_times = AA_times + 1;
                        CloseCOM();
                        return "";
                    }
                    //antstr = Convert.ToString(Ant, 2).PadLeft(4, '0');
                    for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                    {
                        EPClen = daw[m] + 1;
                        temp = temps.Substring(m * 2 + 2, EPClen * 2);
                        sEPC = temp.Substring(0, temp.Length - 2);
                        string RSSI = Convert.ToInt32(temp.Substring(temp.Length - 2, 2), 16).ToString();
                        m = m + EPClen + 1;
                        if (sEPC.Length != (EPClen - 1) * 2)
                        {
                            CloseCOM();
                            return "";
                        }
                        //  text_WriteData.Text = sEPC;
                        int len = int.Parse(sEPC.Substring(0, 2));
                        string newEpc = sEPC.Substring(2, len);
                        byte[] bEpc = HexStringToByteArray(newEpc);
                        //text_WriteData.Text =  Encoding.ASCII.GetString(bEpc);
                        string strlog = "读标签成功，值：" + Encoding.ASCII.GetString(bEpc);
                        // WriteLog(lrtxtLog, strlog, 0);
                        CloseCOM();
                        return Encoding.ASCII.GetString(bEpc);
                    }
                }
                CloseCOM();
                return "";
            }
            catch (Exception ex)
            {
                CloseCOM();
                return "";
            }

        }

        private void CloseCOM()
        {
            if (frmcomportindex > 0)
                fCmdRet = RWDev.CloseSpecComPort(frmcomportindex);
            if (fCmdRet == 0) frmcomportindex = -1;
        }
    }
}
