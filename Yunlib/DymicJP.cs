﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yunlib
{
    public partial class DymicJP : Form
    {
        //初始设置为小写
        private bool IsCapital = false;
        private List<string> list1;
        private List<string> list2;


        private const int WS_EX_NOACTIVATE = 0x08000000;
        private const int WM_MOUSEACTIVATE = 0x0021;
        private const int MA_NOACTIVATE = 0x0003;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= WS_EX_NOACTIVATE;
                return cp;
            }

        }

        protected override void WndProc(ref Message m)
        {
            //If we're being activated because the mouse clicked on us...
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                //Then refuse to be activated, but allow the click event to pass through (don't use MA_NOACTIVATEEAT)
                m.Result = (IntPtr)MA_NOACTIVATE;
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        public DymicJP()
        {
            InitializeComponent();
        }

        private void DymicJP_Load(object sender, EventArgs e)
        {
            string[] temArr = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
            list1 = new List<string>(temArr);

            string[] temArr2 = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            list2 = new List<string>(temArr2);

        }

        private void button31_Click(object sender, EventArgs e)
        {
            if (IsCapital)
            {
                foreach (Control c in tableLayoutPanel1.Controls)
                {
                    if (c is Button)
                    {
                        Button p = c as Button;
                        //这里可以加入其它逻辑
                        for(int i = 0;i<list2.Count; i++)
                        {
                            if(p.Text == list2[i])
                            {
                                p.Text = list2[i].ToLower();
                            }
                        }
                    }
                }
                IsCapital = false;
            }
            else
            {
                foreach (Control c in tableLayoutPanel1.Controls)
                {
                    if (c is Button)
                    {
                        Button p = c as Button;
                        //这里可以加入其它逻辑
                        for (int i = 0; i < list1.Count; i++)
                        {
                            if (p.Text == list1[i])
                            {
                                p.Text = list1[i].ToUpper();
                            }
                        }
                    }
                }
                IsCapital = true;
            }
        }

        public delegate void GetTextHandler(string text);//声明委托
                                                         // public event GetTextHandler getTextHandler = null;//定义委托事件
        public GetTextHandler getTextHandler;//委托对象
        private void button1_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (getTextHandler != null)
            {
                getTextHandler(b.Text);
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            string str = "Backspace";
            if (getTextHandler != null)
            {
                getTextHandler(str);
            }
        }

        private void button40_Click(object sender, EventArgs e)
        {
            string str = "Clear";
            if (getTextHandler != null)
            {
                getTextHandler(str);
            }
        }
    }
}
