﻿using System;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class ReturnSuccess : Form
    {
        public string IsNeedClose { get; set; }
        public string msg { get; set; }
        public ReturnSuccess()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        private void ReturnSuccess_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;

            //主界面窗体加载默认全屏
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            label3.Text = msg;
            CodeOptimize.SolveLightScreen(this);

         PlayVoice.play(this.Name);
            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.WriteProgramLog("ReturnSuccess_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //返回1Main加载时不执行开灯操作
           //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;
            timer1.Stop();

            //Main frm = Singleton<Main>.CreateInstance();
            Main frm = new Main();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblSecond.Text = int.Parse(lblSecond.Text) - 1 + "";
            if (int.Parse(lblSecond.Text) < 0)
            {
                //返回1Main加载时不执行开灯操作
               //  Yunlib.ParentForm.IsInitOpen = (int)EnumIsInitOpen.UinitOpen;

                timer1.Enabled = false;

                // Main frm = Singleton<Main>.CreateInstance();
                Main frm = new Main();
                frm.MdiParent = ParentForm;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }

        private void ReturnSuccess_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(IsNeedClose))
            {
                if(IsNeedClose == "1")
                {
                    DAMControl d = new DAMControl();
                    d.CloseDO(1);
                }
            }
        }
    }
}
