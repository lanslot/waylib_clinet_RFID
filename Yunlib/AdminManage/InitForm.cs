﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Extensions;
using Yunlib.Models;
using YunLib.BLL;
using YunLib.Common;

namespace Yunlib.AdminManage
{
    public partial class InitForm : Form
    {
        BorrowForAcessBLL BLL = new BorrowForAcessBLL();
        public InitForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000010; // 
                return paras;
            }
        }

        private void InitForm_Load(object sender, EventArgs e)
        {
            try
            {
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                cmbControl.Text = ConfigManager.Control;
                cmbScan.Text = ConfigManager.Scan;
                cmbPos.Text = ConfigManager.Pos;
                cmbUHF.Text = ConfigManager.UHF;
                txtLibaryID.Text = ConfigManager.LibraryID;
                txtMachineID.Text = ConfigManager.MachineID;
             //   cmbReadType.SelectedIndex = ConfigManager.ReadCardType;
                cmbBType.SelectedIndex = ConfigManager.BorrowType;
                cmbReadCardType.SelectedIndex = int.Parse(ConfigManager.ReadCardType);
                cmbReadBookType.SelectedIndex = ConfigManager.ReadBookType;
                cmbScanType.SelectedIndex = ConfigManager.ScanType;
                cmbCanRegister.SelectedIndex = ConfigManager.CanRegister;

                if (ConfigManager.IsNeedInitData == "1"){
                    button3.Enabled = false;
                    button3.BackColor = Color.Gray;
                }
            }catch(Exception ex)
            {
                LogManager.BorrowRecord("{0}---InitForm_Load出错:{1}".FormatWith(DateTime.Now, ex.Message));
            }

        }

        public static string servicePath = ConfigManager.ServicePath;
        private void btnSure_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Control", cmbControl.Text)) {
                    MessageBox.Show("Control同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Scan", cmbScan.Text))
                {
                    MessageBox.Show("Scan同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "Pos", cmbPos.Text))
                {
                    MessageBox.Show("Pos同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "UHF", cmbUHF.Text))
                {
                    MessageBox.Show("UHF同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ReadCardType", cmbReadCardType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ReadCardType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ReadBookType", cmbReadBookType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ReadBookType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ScanType", cmbScanType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("ScanType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "BorrowType", cmbBType.SelectedIndex.ToString()))
                {
                    MessageBox.Show("BorrowType同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "CanRegister", cmbCanRegister.SelectedIndex.ToString()))
                {
                    MessageBox.Show("CanRegister同步失败");
                    return;
                }

                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "LibraryID", txtLibaryID.Text))
                {
                    MessageBox.Show("LibraryID同步失败");
                    return;
                }
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "MachineID", txtMachineID.Text))
                {
                    MessageBox.Show("MachineID同步失败");
                    return;
                }
                if (radioButton1.Checked)
                {
                    if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ServicePath", "http://ceshi.yunlib.cn/"))
                    {
                        MessageBox.Show("ServicePath同步失败");
                        return;
                    }
                    else
                    {
                        servicePath = "http://ceshi.yunlib.cn/";
                    }
                }
                else
                {
                    if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "ServicePath", "http://service.yunlib.cn/"))
                    {
                        MessageBox.Show("ServicePath同步失败");
                        return;
                    }
                    else
                    {
                        servicePath = "http://service.yunlib.cn/";
                    }
                }

              //  Application.ExitThread();
                //Application.Exit();
                Application.Restart();
                //Process.GetCurrentProcess().Kill();

            }
            catch(Exception ex)
            {
                MessageBox.Show("保存配置失败");
                LogManager.BorrowRecord("{0}---初始化数据出错:{1}".FormatWith(DateTime.Now, ex.Message));
            }

        }

        private bool InitData()
        {
            try
            {
                var servicePath = ConfigManager.ServicePath;
                var libraryId = txtLibaryID.Text.Trim();
                var commonKey = ConfigManager.CommonKey;

                var timeStamp = DateTime.Now.Ticks;

                var signStr = "{0}{1}{2}".FormatWith(libraryId, timeStamp, commonKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signStr).ToLower());

                DataTable dt = BLL.GetMachineInitData();

                string jsonStr = JsonHelper.ToJson(dt).Replace("DoorNUM", "machLatNum").Replace("BarCode", "bookId ");
                //string jsonStr = GetData();
                JObject jo = new JObject();
                jo.Add("machineId", ConfigManager.MachineID);
                jo.Add("machineLattice", jsonStr);

                JObject joNew = new JObject();
                joNew.Add("json", jo.ToJson());

                JToken jtoken = JToken.Parse(joNew.ToJson());

                string jsonStr1 = "json=" + jtoken.ToString().Replace("\\\\\\\"", "\"").Replace("\\\"", "\"").Replace("\\", "").Replace("r", "").Replace("\"{", "{").Replace("\"[", "[").Replace("\"]", "]").Replace("]\"", "]").Replace("}\"", "}").Replace(" ", "");


                var url = "{0}public/machine/putMachine?timeStamp={1}&libraryId={2}&sign={3}".FormatWith(servicePath, timeStamp, libraryId, sign);
                var back = WebHelper.HttpWebRequest(url, jsonStr1, Encoding.GetEncoding("utf-8"), true).ToJObject();

                if (back["code"].ToString() == "100")
                {
                    MessageBox.Show("数据同步成功");
                    return true;
                }
                else
                {
                    MessageBox.Show("数据同步化失败");
                    LogManager.ErrorRecord("{0}-数据同步失败：{1}".FormatWith(DateTime.Now, back.ToString()));
                    return false;
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("数据同步化失败");
                LogManager.BorrowRecord("{0}--数据同步失败：{1}".FormatWith(DateTime.Now, ex.Message));
                return false;
            }
        }


        
      

        private void button1_Click_1(object sender, EventArgs e)
        {
            BackMain frm = new BackMain { MdiParent = ParentForm, Dock = DockStyle.Fill };

            this.Close();

            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool flag = InitData();
            if (flag)
            {
                if (!ConfigHelper.UpdateOrCreateAppSetting(ConfigurationFile.AppConfig, "IsNeedInitData", "1"))
                {
                    MessageBox.Show("修改配置文件失败！");

                }
                else
                {
                    MessageBox.Show("数据同步成功！");
                }

                button3.Enabled = false;
                button3.BackColor = Color.Gray;

            }
            else
            {
                MessageBox.Show("数据同步失败！");
            }
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }

   


}
