﻿namespace YunLib_Vertical
{
    partial class JPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.picN = new System.Windows.Forms.PictureBox();
            this.picB = new System.Windows.Forms.PictureBox();
            this.picBackspace = new System.Windows.Forms.PictureBox();
            this.picM = new System.Windows.Forms.PictureBox();
            this.picV = new System.Windows.Forms.PictureBox();
            this.picZ = new System.Windows.Forms.PictureBox();
            this.picCapslock = new System.Windows.Forms.PictureBox();
            this.picC = new System.Windows.Forms.PictureBox();
            this.picX = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.picK = new System.Windows.Forms.PictureBox();
            this.picJ = new System.Windows.Forms.PictureBox();
            this.picH = new System.Windows.Forms.PictureBox();
            this.picG = new System.Windows.Forms.PictureBox();
            this.picF = new System.Windows.Forms.PictureBox();
            this.picD = new System.Windows.Forms.PictureBox();
            this.picS = new System.Windows.Forms.PictureBox();
            this.picA = new System.Windows.Forms.PictureBox();
            this.picL = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.picU = new System.Windows.Forms.PictureBox();
            this.picY = new System.Windows.Forms.PictureBox();
            this.picI = new System.Windows.Forms.PictureBox();
            this.picP = new System.Windows.Forms.PictureBox();
            this.picO = new System.Windows.Forms.PictureBox();
            this.picW = new System.Windows.Forms.PictureBox();
            this.picQ = new System.Windows.Forms.PictureBox();
            this.picE = new System.Windows.Forms.PictureBox();
            this.picT = new System.Windows.Forms.PictureBox();
            this.picR = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pic0 = new System.Windows.Forms.PictureBox();
            this.pic9 = new System.Windows.Forms.PictureBox();
            this.pic8 = new System.Windows.Forms.PictureBox();
            this.pic7 = new System.Windows.Forms.PictureBox();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackspace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapslock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picX)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picL)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picR)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.75F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(763, 355);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 9;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.picN, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picB, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picBackspace, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picM, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picV, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picZ, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picCapslock, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picC, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.picX, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 280);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(757, 72);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // picN
            // 
            this.picN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picN.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_94;
            this.picN.Location = new System.Drawing.Point(491, 3);
            this.picN.Name = "picN";
            this.picN.Size = new System.Drawing.Size(69, 66);
            this.picN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picN.TabIndex = 10;
            this.picN.TabStop = false;
            this.picN.Click += new System.EventHandler(this.pictureBox_Click);
            this.picN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picN_MouseDown);
            this.picN.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picN_MouseUp);
            // 
            // picB
            // 
            this.picB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picB.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_93;
            this.picB.Location = new System.Drawing.Point(416, 3);
            this.picB.Name = "picB";
            this.picB.Size = new System.Drawing.Size(69, 66);
            this.picB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picB.TabIndex = 9;
            this.picB.TabStop = false;
            this.picB.Click += new System.EventHandler(this.pictureBox_Click);
            this.picB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictB_MouseDown);
            this.picB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictB_MouseUp);
            // 
            // picBackspace
            // 
            this.picBackspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBackspace.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_96;
            this.picBackspace.Location = new System.Drawing.Point(641, 3);
            this.picBackspace.Name = "picBackspace";
            this.picBackspace.Size = new System.Drawing.Size(113, 66);
            this.picBackspace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBackspace.TabIndex = 8;
            this.picBackspace.TabStop = false;
            this.picBackspace.Click += new System.EventHandler(this.pictureBox_Click);
            this.picBackspace.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picDel_MouseDown);
            this.picBackspace.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picDel_MouseUp);
            // 
            // picM
            // 
            this.picM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picM.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_95;
            this.picM.Location = new System.Drawing.Point(566, 3);
            this.picM.Name = "picM";
            this.picM.Size = new System.Drawing.Size(69, 66);
            this.picM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picM.TabIndex = 6;
            this.picM.TabStop = false;
            this.picM.Click += new System.EventHandler(this.pictureBox_Click);
            this.picM.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picM_MouseDown);
            this.picM.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picM_MouseUp);
            // 
            // picV
            // 
            this.picV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picV.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_92;
            this.picV.Location = new System.Drawing.Point(341, 3);
            this.picV.Name = "picV";
            this.picV.Size = new System.Drawing.Size(69, 66);
            this.picV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picV.TabIndex = 5;
            this.picV.TabStop = false;
            this.picV.Click += new System.EventHandler(this.pictureBox_Click);
            this.picV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picV_MouseDown);
            this.picV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picV_MouseUp);
            // 
            // picZ
            // 
            this.picZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picZ.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_89;
            this.picZ.Location = new System.Drawing.Point(116, 3);
            this.picZ.Name = "picZ";
            this.picZ.Size = new System.Drawing.Size(69, 66);
            this.picZ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picZ.TabIndex = 4;
            this.picZ.TabStop = false;
            this.picZ.Click += new System.EventHandler(this.pictureBox_Click);
            this.picZ.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picZ_MouseDown);
            this.picZ.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picZ_MouseUp);
            // 
            // picCapslock
            // 
            this.picCapslock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCapslock.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_88;
            this.picCapslock.Location = new System.Drawing.Point(3, 3);
            this.picCapslock.Name = "picCapslock";
            this.picCapslock.Size = new System.Drawing.Size(107, 66);
            this.picCapslock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picCapslock.TabIndex = 3;
            this.picCapslock.TabStop = false;
            this.picCapslock.Click += new System.EventHandler(this.picCapslock_Click);
            this.picCapslock.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picCapslock_MouseDown);
            this.picCapslock.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picCapslock_MouseUp);
            // 
            // picC
            // 
            this.picC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picC.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_91;
            this.picC.Location = new System.Drawing.Point(266, 3);
            this.picC.Name = "picC";
            this.picC.Size = new System.Drawing.Size(69, 66);
            this.picC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picC.TabIndex = 2;
            this.picC.TabStop = false;
            this.picC.Click += new System.EventHandler(this.pictureBox_Click);
            this.picC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picC_MouseDown);
            this.picC.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picC_MouseUp);
            // 
            // picX
            // 
            this.picX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picX.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_90;
            this.picX.Location = new System.Drawing.Point(191, 3);
            this.picX.Name = "picX";
            this.picX.Size = new System.Drawing.Size(69, 66);
            this.picX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picX.TabIndex = 1;
            this.picX.TabStop = false;
            this.picX.Click += new System.EventHandler(this.pictureBox_Click);
            this.picX.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picX_MouseDown);
            this.picX.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picX_MouseUp);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 11;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel4.Controls.Add(this.picK, 8, 0);
            this.tableLayoutPanel4.Controls.Add(this.picJ, 7, 0);
            this.tableLayoutPanel4.Controls.Add(this.picH, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.picG, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.picF, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.picD, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.picS, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.picA, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.picL, 9, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 203);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(757, 71);
            this.tableLayoutPanel4.TabIndex = 3;
            this.tableLayoutPanel4.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel4_Paint);
            // 
            // picK
            // 
            this.picK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picK.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_72;
            this.picK.Location = new System.Drawing.Point(565, 3);
            this.picK.Name = "picK";
            this.picK.Size = new System.Drawing.Size(69, 65);
            this.picK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picK.TabIndex = 13;
            this.picK.TabStop = false;
            this.picK.Click += new System.EventHandler(this.pictureBox_Click);
            this.picK.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picK_MouseDown);
            this.picK.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picK_MouseUp);
            // 
            // picJ
            // 
            this.picJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picJ.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_70;
            this.picJ.Location = new System.Drawing.Point(490, 3);
            this.picJ.Name = "picJ";
            this.picJ.Size = new System.Drawing.Size(69, 65);
            this.picJ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picJ.TabIndex = 12;
            this.picJ.TabStop = false;
            this.picJ.Click += new System.EventHandler(this.pictureBox_Click);
            this.picJ.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picJ_MouseDown);
            this.picJ.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picJ_MouseUp);
            // 
            // picH
            // 
            this.picH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picH.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_68;
            this.picH.Location = new System.Drawing.Point(415, 3);
            this.picH.Name = "picH";
            this.picH.Size = new System.Drawing.Size(69, 65);
            this.picH.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picH.TabIndex = 11;
            this.picH.TabStop = false;
            this.picH.Click += new System.EventHandler(this.pictureBox_Click);
            this.picH.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picH_MouseDown);
            this.picH.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picH_MouseUp);
            // 
            // picG
            // 
            this.picG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picG.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_66;
            this.picG.Location = new System.Drawing.Point(340, 3);
            this.picG.Name = "picG";
            this.picG.Size = new System.Drawing.Size(69, 65);
            this.picG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picG.TabIndex = 10;
            this.picG.TabStop = false;
            this.picG.Click += new System.EventHandler(this.pictureBox_Click);
            this.picG.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picG_MouseDown);
            this.picG.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picG_MouseUp);
            // 
            // picF
            // 
            this.picF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picF.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_64;
            this.picF.Location = new System.Drawing.Point(265, 3);
            this.picF.Name = "picF";
            this.picF.Size = new System.Drawing.Size(69, 65);
            this.picF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picF.TabIndex = 9;
            this.picF.TabStop = false;
            this.picF.Click += new System.EventHandler(this.pictureBox_Click);
            this.picF.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picF_MouseDown);
            this.picF.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picF_MouseUp);
            // 
            // picD
            // 
            this.picD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picD.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_62;
            this.picD.Location = new System.Drawing.Point(190, 3);
            this.picD.Name = "picD";
            this.picD.Size = new System.Drawing.Size(69, 65);
            this.picD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picD.TabIndex = 8;
            this.picD.TabStop = false;
            this.picD.Click += new System.EventHandler(this.pictureBox_Click);
            this.picD.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picD_MouseDown);
            this.picD.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picD_MouseUp);
            // 
            // picS
            // 
            this.picS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picS.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_60;
            this.picS.Location = new System.Drawing.Point(115, 3);
            this.picS.Name = "picS";
            this.picS.Size = new System.Drawing.Size(69, 65);
            this.picS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picS.TabIndex = 7;
            this.picS.TabStop = false;
            this.picS.Click += new System.EventHandler(this.pictureBox_Click);
            this.picS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picS_MouseDown);
            this.picS.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picS_MouseUp);
            // 
            // picA
            // 
            this.picA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picA.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_58;
            this.picA.Location = new System.Drawing.Point(40, 3);
            this.picA.Name = "picA";
            this.picA.Size = new System.Drawing.Size(69, 65);
            this.picA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picA.TabIndex = 6;
            this.picA.TabStop = false;
            this.picA.Click += new System.EventHandler(this.pictureBox_Click);
            this.picA.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picA_MouseDown);
            this.picA.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picA_MouseUp);
            // 
            // picL
            // 
            this.picL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picL.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_74;
            this.picL.Location = new System.Drawing.Point(640, 3);
            this.picL.Name = "picL";
            this.picL.Size = new System.Drawing.Size(69, 65);
            this.picL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picL.TabIndex = 5;
            this.picL.TabStop = false;
            this.picL.Click += new System.EventHandler(this.pictureBox_Click);
            this.picL.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picL_MouseDown);
            this.picL.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picL_MouseUp);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 10;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.Controls.Add(this.picU, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picY, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picI, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picP, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picO, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picW, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picQ, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picE, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picT, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.picR, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 126);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(757, 71);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // picU
            // 
            this.picU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picU.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_43;
            this.picU.Location = new System.Drawing.Point(453, 3);
            this.picU.Name = "picU";
            this.picU.Size = new System.Drawing.Size(69, 65);
            this.picU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picU.TabIndex = 10;
            this.picU.TabStop = false;
            this.picU.Click += new System.EventHandler(this.pictureBox_Click);
            this.picU.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picU_MouseDown);
            this.picU.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picU_MouseUp);
            // 
            // picY
            // 
            this.picY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picY.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_42;
            this.picY.Location = new System.Drawing.Point(378, 3);
            this.picY.Name = "picY";
            this.picY.Size = new System.Drawing.Size(69, 65);
            this.picY.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picY.TabIndex = 9;
            this.picY.TabStop = false;
            this.picY.Click += new System.EventHandler(this.pictureBox_Click);
            this.picY.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picY_MouseDown);
            this.picY.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picY_MouseUp);
            // 
            // picI
            // 
            this.picI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picI.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_44;
            this.picI.Location = new System.Drawing.Point(528, 3);
            this.picI.Name = "picI";
            this.picI.Size = new System.Drawing.Size(69, 65);
            this.picI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picI.TabIndex = 8;
            this.picI.TabStop = false;
            this.picI.Click += new System.EventHandler(this.pictureBox_Click);
            this.picI.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picI_MouseDown);
            this.picI.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picI_MouseUp);
            // 
            // picP
            // 
            this.picP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picP.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_46;
            this.picP.Location = new System.Drawing.Point(678, 3);
            this.picP.Name = "picP";
            this.picP.Size = new System.Drawing.Size(76, 65);
            this.picP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picP.TabIndex = 7;
            this.picP.TabStop = false;
            this.picP.Click += new System.EventHandler(this.pictureBox_Click);
            this.picP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picP_MouseDown);
            this.picP.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picP_MouseUp);
            // 
            // picO
            // 
            this.picO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picO.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_45;
            this.picO.Location = new System.Drawing.Point(603, 3);
            this.picO.Name = "picO";
            this.picO.Size = new System.Drawing.Size(69, 65);
            this.picO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picO.TabIndex = 6;
            this.picO.TabStop = false;
            this.picO.Click += new System.EventHandler(this.pictureBox_Click);
            this.picO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picO_MouseDown);
            this.picO.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picO_MouseUp);
            // 
            // picW
            // 
            this.picW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picW.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_38;
            this.picW.Location = new System.Drawing.Point(78, 3);
            this.picW.Name = "picW";
            this.picW.Size = new System.Drawing.Size(69, 65);
            this.picW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picW.TabIndex = 5;
            this.picW.TabStop = false;
            this.picW.Click += new System.EventHandler(this.pictureBox_Click);
            this.picW.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picW_MouseDown);
            this.picW.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picW_MouseUp);
            // 
            // picQ
            // 
            this.picQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picQ.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_37;
            this.picQ.Location = new System.Drawing.Point(3, 3);
            this.picQ.Name = "picQ";
            this.picQ.Size = new System.Drawing.Size(69, 65);
            this.picQ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picQ.TabIndex = 4;
            this.picQ.TabStop = false;
            this.picQ.Click += new System.EventHandler(this.pictureBox_Click);
            this.picQ.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picQ_MouseDown);
            this.picQ.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picQ_MouseUp);
            // 
            // picE
            // 
            this.picE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picE.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_39;
            this.picE.Location = new System.Drawing.Point(153, 3);
            this.picE.Name = "picE";
            this.picE.Size = new System.Drawing.Size(69, 65);
            this.picE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picE.TabIndex = 3;
            this.picE.TabStop = false;
            this.picE.Click += new System.EventHandler(this.pictureBox_Click);
            this.picE.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picE_MouseDown);
            this.picE.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picE_MouseUp);
            // 
            // picT
            // 
            this.picT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picT.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_41;
            this.picT.Location = new System.Drawing.Point(303, 3);
            this.picT.Name = "picT";
            this.picT.Size = new System.Drawing.Size(69, 65);
            this.picT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picT.TabIndex = 2;
            this.picT.TabStop = false;
            this.picT.Click += new System.EventHandler(this.pictureBox_Click);
            this.picT.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picT_MouseDown);
            this.picT.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picT_MouseUp);
            // 
            // picR
            // 
            this.picR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picR.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_40;
            this.picR.Location = new System.Drawing.Point(228, 3);
            this.picR.Name = "picR";
            this.picR.Size = new System.Drawing.Size(69, 65);
            this.picR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picR.TabIndex = 1;
            this.picR.TabStop = false;
            this.picR.Click += new System.EventHandler(this.pictureBox_Click);
            this.picR.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picR_MouseDown);
            this.picR.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picR_MouseUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.picClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(757, 40);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gPanelTitleBack_MouseDown);
            // 
            // picClose
            // 
            this.picClose.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_00;
            this.picClose.Location = new System.Drawing.Point(725, 3);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(29, 29);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picClose.TabIndex = 0;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Controls.Add(this.pic0, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic9, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic8, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic7, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic6, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pic1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 49);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(757, 71);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // pic0
            // 
            this.pic0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic0.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_10;
            this.pic0.Location = new System.Drawing.Point(678, 3);
            this.pic0.Name = "pic0";
            this.pic0.Size = new System.Drawing.Size(76, 65);
            this.pic0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic0.TabIndex = 9;
            this.pic0.TabStop = false;
            this.pic0.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic0_MouseDown);
            this.pic0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic0_MouseUp);
            // 
            // pic9
            // 
            this.pic9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic9.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_09;
            this.pic9.Location = new System.Drawing.Point(603, 3);
            this.pic9.Name = "pic9";
            this.pic9.Size = new System.Drawing.Size(69, 65);
            this.pic9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic9.TabIndex = 8;
            this.pic9.TabStop = false;
            this.pic9.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic9_MouseDown);
            this.pic9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic9_MouseUp);
            // 
            // pic8
            // 
            this.pic8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic8.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_08;
            this.pic8.Location = new System.Drawing.Point(528, 3);
            this.pic8.Name = "pic8";
            this.pic8.Size = new System.Drawing.Size(69, 65);
            this.pic8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic8.TabIndex = 7;
            this.pic8.TabStop = false;
            this.pic8.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic8_MouseDown);
            this.pic8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic8_MouseUp);
            // 
            // pic7
            // 
            this.pic7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic7.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_07;
            this.pic7.Location = new System.Drawing.Point(453, 3);
            this.pic7.Name = "pic7";
            this.pic7.Size = new System.Drawing.Size(69, 65);
            this.pic7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic7.TabIndex = 6;
            this.pic7.TabStop = false;
            this.pic7.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic7_MouseDown);
            this.pic7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic7_MouseUp);
            // 
            // pic6
            // 
            this.pic6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic6.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_06;
            this.pic6.Location = new System.Drawing.Point(378, 3);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(69, 65);
            this.pic6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic6.TabIndex = 5;
            this.pic6.TabStop = false;
            this.pic6.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic6_MouseDown);
            this.pic6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic6_MouseUp);
            // 
            // pic5
            // 
            this.pic5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic5.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_05;
            this.pic5.Location = new System.Drawing.Point(303, 3);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(69, 65);
            this.pic5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic5.TabIndex = 4;
            this.pic5.TabStop = false;
            this.pic5.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic5_MouseDown);
            this.pic5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic5_MouseUp);
            // 
            // pic4
            // 
            this.pic4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic4.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_04;
            this.pic4.Location = new System.Drawing.Point(228, 3);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(69, 65);
            this.pic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic4.TabIndex = 3;
            this.pic4.TabStop = false;
            this.pic4.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic4_MouseDown);
            this.pic4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic4_MouseUp);
            // 
            // pic3
            // 
            this.pic3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic3.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_03;
            this.pic3.Location = new System.Drawing.Point(153, 3);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(69, 65);
            this.pic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic3.TabIndex = 2;
            this.pic3.TabStop = false;
            this.pic3.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic3_MouseDown);
            this.pic3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic3_MouseUp);
            // 
            // pic2
            // 
            this.pic2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic2.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_02;
            this.pic2.Location = new System.Drawing.Point(78, 3);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(69, 65);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic2.TabIndex = 1;
            this.pic2.TabStop = false;
            this.pic2.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic2_MouseDown);
            this.pic2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic2_MouseUp);
            // 
            // pic1
            // 
            this.pic1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic1.Image = global::YunLib_Vertical.Properties.Resources.keyboard_d_01;
            this.pic1.Location = new System.Drawing.Point(3, 3);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(69, 65);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pic1.TabIndex = 0;
            this.pic1.TabStop = false;
            this.pic1.Click += new System.EventHandler(this.pictureBox_Click);
            this.pic1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic1_MouseDown);
            this.pic1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic1_MouseUp);
            // 
            // JPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::YunLib_Vertical.Properties.Resources.keyboard_bj_01;
            this.ClientSize = new System.Drawing.Size(763, 355);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "JPForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "JPForm";
            this.TopMost = true;
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.JPForm_MouseMove);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBackspace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCapslock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picX)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picL)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picR)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox picU;
        private System.Windows.Forms.PictureBox picY;
        private System.Windows.Forms.PictureBox picI;
        private System.Windows.Forms.PictureBox picP;
        private System.Windows.Forms.PictureBox picO;
        private System.Windows.Forms.PictureBox picW;
        private System.Windows.Forms.PictureBox picQ;
        private System.Windows.Forms.PictureBox picE;
        private System.Windows.Forms.PictureBox picT;
        private System.Windows.Forms.PictureBox picR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pic0;
        private System.Windows.Forms.PictureBox pic9;
        private System.Windows.Forms.PictureBox pic8;
        private System.Windows.Forms.PictureBox pic7;
        private System.Windows.Forms.PictureBox pic6;
        private System.Windows.Forms.PictureBox pic5;
        private System.Windows.Forms.PictureBox pic4;
        private System.Windows.Forms.PictureBox pic3;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox picN;
        private System.Windows.Forms.PictureBox picB;
        private System.Windows.Forms.PictureBox picBackspace;
        private System.Windows.Forms.PictureBox picM;
        private System.Windows.Forms.PictureBox picV;
        private System.Windows.Forms.PictureBox picZ;
        private System.Windows.Forms.PictureBox picCapslock;
        private System.Windows.Forms.PictureBox picC;
        private System.Windows.Forms.PictureBox picX;
        private System.Windows.Forms.PictureBox picK;
        private System.Windows.Forms.PictureBox picJ;
        private System.Windows.Forms.PictureBox picH;
        private System.Windows.Forms.PictureBox picG;
        private System.Windows.Forms.PictureBox picF;
        private System.Windows.Forms.PictureBox picD;
        private System.Windows.Forms.PictureBox picS;
        private System.Windows.Forms.PictureBox picA;
        private System.Windows.Forms.PictureBox picL;
    }
}