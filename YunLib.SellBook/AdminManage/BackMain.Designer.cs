﻿namespace Yunlib.SellBook.AdminManage
{
    partial class BackMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddBooks = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDoorManage = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDoorManage240 = new System.Windows.Forms.Button();
            this.btnReturnMain = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnUnderCarriage = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddBooks
            // 
            this.btnAddBooks.BackColor = System.Drawing.Color.Gray;
            this.btnAddBooks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddBooks.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnAddBooks.FlatAppearance.BorderSize = 0;
            this.btnAddBooks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnAddBooks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBooks.Font = new System.Drawing.Font("微软雅黑", 23F, System.Drawing.FontStyle.Bold);
            this.btnAddBooks.ForeColor = System.Drawing.Color.White;
            this.btnAddBooks.Location = new System.Drawing.Point(229, 148);
            this.btnAddBooks.Name = "btnAddBooks";
            this.btnAddBooks.Size = new System.Drawing.Size(187, 74);
            this.btnAddBooks.TabIndex = 0;
            this.btnAddBooks.Text = "上架书籍";
            this.btnAddBooks.UseVisualStyleBackColor = false;
            this.btnAddBooks.Click += new System.EventHandler(this.btnAddBooks_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 23F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(334, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "WayLib Manage System";
            // 
            // btnDoorManage
            // 
            this.btnDoorManage.BackColor = System.Drawing.Color.Gray;
            this.btnDoorManage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDoorManage.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnDoorManage.FlatAppearance.BorderSize = 0;
            this.btnDoorManage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnDoorManage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDoorManage.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Bold);
            this.btnDoorManage.ForeColor = System.Drawing.Color.White;
            this.btnDoorManage.Location = new System.Drawing.Point(8, 3);
            this.btnDoorManage.Name = "btnDoorManage";
            this.btnDoorManage.Size = new System.Drawing.Size(118, 68);
            this.btnDoorManage.TabIndex = 0;
            this.btnDoorManage.Text = "管理卡设置";
            this.btnDoorManage.UseVisualStyleBackColor = false;
            this.btnDoorManage.Click += new System.EventHandler(this.btnDoorManage_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDoorManage240);
            this.panel1.Controls.Add(this.btnDoorManage);
            this.panel1.Location = new System.Drawing.Point(606, 148);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 74);
            this.panel1.TabIndex = 2;
            // 
            // btnDoorManage240
            // 
            this.btnDoorManage240.BackColor = System.Drawing.Color.Gray;
            this.btnDoorManage240.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDoorManage240.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnDoorManage240.FlatAppearance.BorderSize = 0;
            this.btnDoorManage240.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnDoorManage240.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDoorManage240.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Bold);
            this.btnDoorManage240.ForeColor = System.Drawing.Color.White;
            this.btnDoorManage240.Location = new System.Drawing.Point(134, 3);
            this.btnDoorManage240.Name = "btnDoorManage240";
            this.btnDoorManage240.Size = new System.Drawing.Size(121, 68);
            this.btnDoorManage240.TabIndex = 1;
            this.btnDoorManage240.Text = "系统初始化";
            this.btnDoorManage240.UseVisualStyleBackColor = false;
            this.btnDoorManage240.Click += new System.EventHandler(this.btnDoorManage240_Click);
            // 
            // btnReturnMain
            // 
            this.btnReturnMain.BackColor = System.Drawing.Color.Gray;
            this.btnReturnMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReturnMain.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnReturnMain.FlatAppearance.BorderSize = 0;
            this.btnReturnMain.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnReturnMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReturnMain.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Bold);
            this.btnReturnMain.ForeColor = System.Drawing.Color.White;
            this.btnReturnMain.Location = new System.Drawing.Point(854, 650);
            this.btnReturnMain.Name = "btnReturnMain";
            this.btnReturnMain.Size = new System.Drawing.Size(129, 68);
            this.btnReturnMain.TabIndex = 1;
            this.btnReturnMain.Text = "返回程序首页";
            this.btnReturnMain.UseVisualStyleBackColor = false;
            this.btnReturnMain.Click += new System.EventHandler(this.btnReturnMain_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gray;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("微软雅黑", 23F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(229, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 74);
            this.button1.TabIndex = 0;
            this.button1.Text = "数据管理";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnUnderCarriage
            // 
            this.btnUnderCarriage.BackColor = System.Drawing.Color.Gray;
            this.btnUnderCarriage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnderCarriage.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnUnderCarriage.FlatAppearance.BorderSize = 0;
            this.btnUnderCarriage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnUnderCarriage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnderCarriage.Font = new System.Drawing.Font("微软雅黑", 23F, System.Drawing.FontStyle.Bold);
            this.btnUnderCarriage.ForeColor = System.Drawing.Color.White;
            this.btnUnderCarriage.Location = new System.Drawing.Point(229, 384);
            this.btnUnderCarriage.Name = "btnUnderCarriage";
            this.btnUnderCarriage.Size = new System.Drawing.Size(187, 74);
            this.btnUnderCarriage.TabIndex = 0;
            this.btnUnderCarriage.Text = "下架管理";
            this.btnUnderCarriage.UseVisualStyleBackColor = false;
            this.btnUnderCarriage.Click += new System.EventHandler(this.btnUnderCarriage_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gray;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("微软雅黑", 13F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(614, 266);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(118, 68);
            this.button3.TabIndex = 4;
            this.button3.Text = "开门";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // BackMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnReturnMain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUnderCarriage);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAddBooks);
            this.Name = "BackMain";
            this.Text = "WayLib管理";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddBooks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDoorManage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDoorManage240;
        private System.Windows.Forms.Button btnReturnMain;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnUnderCarriage;
        private System.Windows.Forms.Button button3;
    }
}