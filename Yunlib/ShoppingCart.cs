﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Yunlib.Common;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.Common;

namespace Yunlib
{
    public partial class ShoppingCart : Form
    {
        public ShoppingCart()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 数据总页数
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// 当前页数
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int PageSize
        {
            get;
            set;
        }

        public string DoorNumArr { get; set; }

        private List<string> lendList = new List<string>();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }
        string[] doorNums = null;
        List<string> arrList = null;
        private void ShoppingCart_Load(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;

            //总数据条数
            int TotalCount = DoorNumArr.Replace(" ", "").Split('&').Length - 1;

            PageIndex = 1;
            PageSize = 2;

            //获取总页数
            TotalPage = Convert.ToInt32(Math.Ceiling((double)TotalCount / PageSize));

            lblThisPage.Text = PageIndex + "";
            lblTotalPage.Text = TotalPage + "";
            label2.Text = "/";

            arrList = new List<string>();

            DoorNumArr.Replace(" ", "").Split('&').Each(x =>
            {
                if (!string.IsNullOrWhiteSpace(x))
                {
                    arrList.Add(x);
                }
            });

            List<string> viewList = new List<string>();
            //加载的时候,取前十条
            if (arrList.Count > PageSize)
            {
                viewList = arrList.Skip(0).Take(PageSize).ToList();
                //MessageBox.Show(arrList.ToJson());
            }
            else
            {
                viewList = arrList;
            }

            try
            {
                //主界面窗体加载默认全屏
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;

                CodeOptimize.SolveLightScreen(this);

                // Set the view to show details.
                listView1.View = View.Details;
                // Allow the user to edit item text.
                listView1.LabelEdit = false;
                // Allow the user to rearrange columns.
                listView1.AllowColumnReorder = false;
                // Display check boxes.
                listView1.CheckBoxes = true;

                // Display grid lines.
                listView1.GridLines = true;

                var w = (listView1.Width - 200) / 2 - 2;
                int h = this.listView1.Height / 10 - 4;

                ImageList imgList = new ImageList { ImageSize = new Size(1, h) };

                listView1.LargeImageList = imgList;

                // Create columns for the items and subitems.
                listView1.Columns.Add("选择", 100, HorizontalAlignment.Center);
                listView1.Columns.Add("柜号", 100, HorizontalAlignment.Center);
                listView1.Columns.Add("书名", w, HorizontalAlignment.Center);
                listView1.Columns.Add("作者", w, HorizontalAlignment.Center);

                listView1.Font = new Font("微软雅黑", 14, FontStyle.Regular);

                doorNums = DoorNumArr.Replace(" ", "").Split('&');

                //向ListView中添加数据
                AddDataInListView(PageIndex, viewList);

                DateTime end = DateTime.Now;
                TimeSpan ts = end - start;
                LogManager.WriteProgramLog("ShoppingCart_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddDataInListView(int pageIndex, List<string> arrList)
        {

            if (pageIndex <= 1)
            {
                picBtnLastPage.Enabled = false;
            }
            else
            {
                picBtnLastPage.Enabled = true;
            }

            if (pageIndex >= TotalPage)
            {
                picBtnNextPage.Enabled = false;
            }
            else
            {
                picBtnNextPage.Enabled = true;
            }

            for (int i = 0; i < arrList.Count; i++)   //添加10行数据
            {
                LocalBooks model = Json.ToObject<LocalBooks>(arrList[i]);
                ListViewItem lvi = new ListViewItem();

                //  lvi.ImageIndex = i;     //通过与imageList绑定，显示imageList中第i项图标
                //  lvi.Checked = true;
                lvi.Name = model.BarCode;

                foreach (string str in lendList) {
                    if (str.Contains(model.BarCode)) {
                        lvi.Checked = true;
                    }
                }

                lvi.SubItems.Add(model.DoorNUM.ToString());

                lvi.SubItems.Add("《{0}》".FormatWith(model.BName));
                lvi.SubItems.Add(model.Writer);

                if ((i + 1) % 2 == 0)
                {
                    lvi.BackColor = Color.FromArgb(255, 249, 249, 241);
                }
                this.listView1.Items.Add(lvi);
            }
        }

        private void SolveLightScreen()
        {
            listView1.Visible = false;
            label2.Visible = false;
            lblThisPage.Visible = false;
            lblTotalPage.Visible = false;
            picBtnClear.Visible = false;
            picBtnLastPage.Visible = false;
            picBtnNextPage.Visible = false;
            picBtnReturn.Visible = false;
            picBtnYJBorror.Visible = false;

            Application.DoEvents();

            listView1.Visible = true;
            label2.Visible = true;
            lblThisPage.Visible = true;
            lblTotalPage.Visible = true;
            picBtnClear.Visible = true;
            picBtnLastPage.Visible = true;
            picBtnNextPage.Visible = true;
            picBtnReturn.Visible = true;
            picBtnYJBorror.Visible = true;
        }

        private void picBtnClear_Click(object sender, EventArgs e)
        {
            int count = this.listView1.Items.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                this.listView1.Items.RemoveAt(i);

            }
            Cart.BooksNumArr = string.Empty;
            
        }

        private void picBtnReturn_Click(object sender, EventArgs e)
        {
            //BorrowBook frm =Singleton<BorrowBook>.CreateInstance();
            BorrowBook frm = new BorrowBook();
            frm.MdiParent = ParentForm;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void picBtnYJBorror_Click(object sender, EventArgs e)
        {

            DateTime start = DateTime.Now;

            string str = string.Empty;

            for (int i = 0; i < lendList.Count; i++)
            {
                str += lendList[i] + ",";
            }
            ReaderLogin rdl = new ReaderLogin { MdiParent = ParentForm, Dock = DockStyle.Fill, MoreBarCode = str };
            
            this.Close();

            rdl.Show();

            DateTime end = DateTime.Now;
            TimeSpan ts = end - start;
            LogManager.WriteProgramLog("ShoppingCart_Load 运行时间{0}".FormatWith(ts.TotalSeconds));
        }

        //下一页
        private void picBtnNextPage_Click(object sender, EventArgs e)
        {
            var viewList = new List<string>();
            if (PageIndex < TotalPage)
            {
                PageIndex += 1;

                //MessageBox.Show(PageIndex + "");

                //加载的时候,取前十条
                if (arrList.Count > PageIndex * PageSize)
                {
                    viewList = arrList.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                }
                else
                {
                    viewList = arrList.Skip((PageIndex - 1) * PageSize).ToList();
                }


                lblThisPage.Text = PageIndex + "";

                int count = this.listView1.Items.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    this.listView1.Items.RemoveAt(i);

                }

                AddDataInListView(PageIndex, viewList);
            }
        }

        //上一页
        private void picBtnLastPage_Click(object sender, EventArgs e)
        {
            var viewList = new List<string>();
            if (PageIndex > 1)
            {
                PageIndex -= 1;

                //MessageBox.Show(PageIndex + "");

                //加载的时候,取前十条
                if (arrList.Count > PageIndex * PageSize)
                {
                    viewList = arrList.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                }
                else
                {
                    viewList = arrList.Skip((PageIndex - 1) * PageSize).ToList();
                }


                lblThisPage.Text = PageIndex + "";

                int count = this.listView1.Items.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    this.listView1.Items.RemoveAt(i);
                }

                AddDataInListView(PageIndex, viewList);
            }
        }

        /// <summary>
        /// listview选中与被选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var item = listView1.Items[e.Index];
            var str = "{0}&{1}".FormatWith(item.Name,item.SubItems[1].Text);
            if (e.NewValue == CheckState.Checked)
            {
                lendList.Add(str);
            }
            else if (e.NewValue == CheckState.Unchecked)
            {
                lendList.Remove(str);
            }
        }
    }
}
