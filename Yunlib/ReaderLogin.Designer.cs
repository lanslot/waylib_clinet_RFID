﻿namespace Yunlib
{
    partial class ReaderLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReaderLogin));
            this.labelFw = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.lblAccountErrMsg = new System.Windows.Forms.Label();
            this.lblPwdErrMsg = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.picEncode = new System.Windows.Forms.PictureBox();
            this.pBtnLogin = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblDaoJiShi = new System.Windows.Forms.Label();
            this.lblMiao = new System.Windows.Forms.Label();
            this.lblGun = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDataTimeNow = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEncode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelFw
            // 
            this.labelFw.AutoSize = true;
            this.labelFw.BackColor = System.Drawing.Color.White;
            this.labelFw.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelFw.Location = new System.Drawing.Point(495, 545);
            this.labelFw.Name = "labelFw";
            this.labelFw.Size = new System.Drawing.Size(0, 17);
            this.labelFw.TabIndex = 10;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Yunlib.Properties.Resources.btnX;
            this.pictureBox2.Location = new System.Drawing.Point(485, 615);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(62, 63);
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // txtAccount
            // 
            this.txtAccount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAccount.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAccount.Location = new System.Drawing.Point(466, 116);
            this.txtAccount.Multiline = true;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(202, 39);
            this.txtAccount.TabIndex = 29;
            this.txtAccount.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtAccount_MouseDown);
            this.txtAccount.MouseLeave += new System.EventHandler(this.txtAccount_MouseLeave);
            // 
            // txtPwd
            // 
            this.txtPwd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPwd.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPwd.Location = new System.Drawing.Point(466, 204);
            this.txtPwd.Multiline = true;
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(202, 37);
            this.txtPwd.TabIndex = 28;
            this.txtPwd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtPwd_MouseDown);
            this.txtPwd.MouseLeave += new System.EventHandler(this.txtPwd_MouseLeave);
            // 
            // lblAccountErrMsg
            // 
            this.lblAccountErrMsg.AutoSize = true;
            this.lblAccountErrMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountErrMsg.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblAccountErrMsg.ForeColor = System.Drawing.Color.Green;
            this.lblAccountErrMsg.Location = new System.Drawing.Point(468, 165);
            this.lblAccountErrMsg.Name = "lblAccountErrMsg";
            this.lblAccountErrMsg.Size = new System.Drawing.Size(0, 17);
            this.lblAccountErrMsg.TabIndex = 30;
            // 
            // lblPwdErrMsg
            // 
            this.lblPwdErrMsg.AutoSize = true;
            this.lblPwdErrMsg.BackColor = System.Drawing.Color.Transparent;
            this.lblPwdErrMsg.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lblPwdErrMsg.ForeColor = System.Drawing.Color.Green;
            this.lblPwdErrMsg.Location = new System.Drawing.Point(470, 251);
            this.lblPwdErrMsg.Name = "lblPwdErrMsg";
            this.lblPwdErrMsg.Size = new System.Drawing.Size(0, 17);
            this.lblPwdErrMsg.TabIndex = 30;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.picEncode);
            this.panel2.Controls.Add(this.pBtnLogin);
            this.panel2.Controls.Add(this.lblPwdErrMsg);
            this.panel2.Controls.Add(this.lblAccountErrMsg);
            this.panel2.Controls.Add(this.txtPwd);
            this.panel2.Controls.Add(this.txtAccount);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(175, 216);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(700, 380);
            this.panel2.TabIndex = 11;
            // 
            // picEncode
            // 
            this.picEncode.Location = new System.Drawing.Point(241, 135);
            this.picEncode.Name = "picEncode";
            this.picEncode.Size = new System.Drawing.Size(113, 111);
            this.picEncode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEncode.TabIndex = 32;
            this.picEncode.TabStop = false;
            // 
            // pBtnLogin
            // 
            this.pBtnLogin.BackColor = System.Drawing.Color.Transparent;
            this.pBtnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pBtnLogin.Image = global::Yunlib.Properties.Resources.btnSure666;
            this.pBtnLogin.Location = new System.Drawing.Point(499, 282);
            this.pBtnLogin.Name = "pBtnLogin";
            this.pBtnLogin.Size = new System.Drawing.Size(146, 49);
            this.pBtnLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pBtnLogin.TabIndex = 31;
            this.pBtnLogin.TabStop = false;
            this.pBtnLogin.Click += new System.EventHandler(this.pBtnLogin_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::Yunlib.Properties.Resources.Login_Nav_bg666;
            this.pictureBox1.Location = new System.Drawing.Point(-6, -4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(756, 389);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = global::Yunlib.Properties.Resources.时间图标;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Location = new System.Drawing.Point(602, 41);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(26, 31);
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(39, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 14;
            this.label1.Visible = false;
            // 
            // lblDaoJiShi
            // 
            this.lblDaoJiShi.BackColor = System.Drawing.Color.Transparent;
            this.lblDaoJiShi.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDaoJiShi.ForeColor = System.Drawing.Color.Orange;
            this.lblDaoJiShi.Location = new System.Drawing.Point(227, 12);
            this.lblDaoJiShi.Name = "lblDaoJiShi";
            this.lblDaoJiShi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDaoJiShi.Size = new System.Drawing.Size(70, 31);
            this.lblDaoJiShi.TabIndex = 3;
            this.lblDaoJiShi.Text = "60";
            // 
            // lblMiao
            // 
            this.lblMiao.BackColor = System.Drawing.Color.Transparent;
            this.lblMiao.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblMiao.ForeColor = System.Drawing.Color.Orange;
            this.lblMiao.Location = new System.Drawing.Point(297, 11);
            this.lblMiao.Name = "lblMiao";
            this.lblMiao.Size = new System.Drawing.Size(36, 31);
            this.lblMiao.TabIndex = 3;
            this.lblMiao.Text = "秒";
            // 
            // lblGun
            // 
            this.lblGun.BackColor = System.Drawing.Color.Transparent;
            this.lblGun.Font = new System.Drawing.Font("微软雅黑", 17F);
            this.lblGun.ForeColor = System.Drawing.Color.White;
            this.lblGun.Location = new System.Drawing.Point(221, 13);
            this.lblGun.Name = "lblGun";
            this.lblGun.Size = new System.Drawing.Size(27, 31);
            this.lblGun.TabIndex = 4;
            this.lblGun.Text = "丨";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblGun);
            this.panel1.Controls.Add(this.lblMiao);
            this.panel1.Controls.Add(this.lblDaoJiShi);
            this.panel1.Controls.Add(this.lblDataTimeNow);
            this.panel1.Location = new System.Drawing.Point(634, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 57);
            this.panel1.TabIndex = 7;
            // 
            // lblDataTimeNow
            // 
            this.lblDataTimeNow.AutoSize = true;
            this.lblDataTimeNow.BackColor = System.Drawing.Color.Transparent;
            this.lblDataTimeNow.Font = new System.Drawing.Font("微软雅黑", 18F);
            this.lblDataTimeNow.ForeColor = System.Drawing.Color.White;
            this.lblDataTimeNow.Location = new System.Drawing.Point(3, 13);
            this.lblDataTimeNow.Name = "lblDataTimeNow";
            this.lblDataTimeNow.Size = new System.Drawing.Size(0, 31);
            this.lblDataTimeNow.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ReaderLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.labelFw);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReaderLogin";
            this.Text = "Login";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ReaderLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEncode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBtnLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelFw;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.Label lblAccountErrMsg;
        private System.Windows.Forms.Label lblPwdErrMsg;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDaoJiShi;
        private System.Windows.Forms.Label lblMiao;
        private System.Windows.Forms.Label lblGun;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDataTimeNow;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox picEncode;
        private System.Windows.Forms.PictureBox pBtnLogin;
    }
}