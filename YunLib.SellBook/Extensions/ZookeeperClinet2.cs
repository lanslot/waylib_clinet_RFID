﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.Common;
using Yunlib.PosExtensions;
using ZooKeeperNet;

namespace Yunlib
{
    public class ZooKeeperClient
    {

        public static ZooKeeper zk;
        public static Watcher wc = new Watcher();

        /// <summary>
        /// 创建节点
        /// </summary>
        public static void createNode()
        {

            //获取服务器IP
            string zIP = ConfigurationManager.AppSettings["ZooKeeperIP"];

            zk = new ZooKeeper(zIP, new TimeSpan(0, 0, 0, 30), null);

            //获取节点路径
            string zPath = ConfigurationManager.AppSettings["ZooKeeperPath"];
            //获取机器编号
            string machineID = ConfigurationManager.AppSettings["MachineId"];

            string path = zPath + "/" + machineID;
         //  var list = zk.GetChildren(zPath,true).ToList();
            
            //创建节点
            zk.Create(path, "childone".GetBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.Ephemeral);
       
            //绑定监听
            var stat = zk.Exists(path, wc);
            DateTime times = System.DateTime.Now;
            LogManager.BorrowRecord("**** 创建节点：｛0｝".FormatWith(times));
        }

        /// <summary>
        /// 建立连接
        /// </summary>
        public static void content()
        {
            //获取服务器IP
            string zIP = ConfigurationManager.AppSettings["ZooKeeperIP"];

            zk = new ZooKeeper(zIP, new TimeSpan(0, 0, 0, 1000), null);
        }

        public static void DelNode() {
            //获取节点路径
            string zPath = ConfigurationManager.AppSettings["ZooKeeperPath"];
            //获取机器编号
            string machineID = ConfigurationManager.AppSettings["MachineId"];

            string path = zPath + "/" + machineID;
            zk.Delete(path,-1);
        }
    }
}
