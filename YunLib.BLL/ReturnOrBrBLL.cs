﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;
using Yunlib.Entity;

namespace YunLib.BLL
{
    public class ReturnOrBrBLL
    {
        ReturnOrBrDAL DAL = new ReturnOrBrDAL();

        public BooksInfo GetBooksInfoByBarcode(string barCode)
        {
            return DAL.GetBooksInfoByBarcode(barCode);
        }

        public bool InsertLTKOneInfo(ChangeLogInfo model)
        {
            return DAL.InsertLTKOneInfo(model);
        }

        public ChangeKuInfo GetBorrowBooksInfoByBarcode(string barCode)
        {
            return DAL.GetBorrowBooksInfoByBarcode(barCode);
        }

        public bool DeleteChangeLogInfoByBarCode(ChangeLogInfo model)
        {
            return DAL.DeleteChangeLogInfoByBarCode(model);
        }
        public int GetOnceBorrowCount(string rdid)
        {
            return DAL.GetOnceBorrowCount(rdid);
        }

        public LocalBooks GetBookByBarCode(string barCode)
        {
            return DAL.GetBookByBarCode(barCode);
        }
        public Boolean AddCirculation(ChangeKuInfo model)
        {
            return DAL.AddCirculation(model);
        }
    }
}
