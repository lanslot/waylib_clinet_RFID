﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YunLib_Vertical
{
    public partial class FailForm : Form
    {

        public string Title { get; set; }
        public string Message { get; set; }
        public FailForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams paras = base.CreateParams;
                paras.ExStyle |= 0x02000000;
                return paras;
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MainForm frm = new MainForm();
            frm.MdiParent = ParentForm;
            frm.Parent = this.Parent;
            frm.Dock = DockStyle.Fill;

            this.Close();

            frm.Show();
        }

        private void FailForm_Load(object sender, EventArgs e)
        {
            lblMessage.Text = Message;
            lblTitle.Text = Title;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int time = int.Parse(lblCount.Text);
            time--;
            lblCount.Text = time.ToString();
            if (time <= 0)
            {
                MainForm frm = new MainForm();
                frm.MdiParent = ParentForm;
                frm.Parent = this.Parent;
                frm.Dock = DockStyle.Fill;

                this.Close();

                frm.Show();
            }
        }
    }
}
